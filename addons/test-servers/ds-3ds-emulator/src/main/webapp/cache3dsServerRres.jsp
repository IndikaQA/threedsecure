<%@ page import="java.io.BufferedReader, java.util.Map, java.util.HashMap" %><%

    // Save RRes string in application servlet context. Map value to acsTransId key, which has been a part of queryString.

    StringBuffer sb = new StringBuffer();
    BufferedReader br = request.getReader();
    String line;
    while ((line = br.readLine()) != null) {
        sb.append(line);
        sb.append("\n");
    }

    String rresString = sb.toString();
    String acsTransId = request.getParameter("acsTransId");

    if (application.getAttribute("rresCache") == null) {
        application.setAttribute("rresCache", new HashMap<String, String>());
    }
    Map<String, String> rresCache = (Map<String, String>) application.getAttribute("rresCache");

    rresCache.put(acsTransId, rresString);

    out.print("RRes cached successfully: " + acsTransId);
%>