package com.modirum.ds.services.elo.authserver;

public class UserSession {
    private String name;
    private String clientMutationId;
    private String id;
    private OAuthToken oauthToken;

    public String getName() {
        return name;
    }

    public UserSession setName(String name) {
        this.name = name;
        return this;
    }

    public String getClientMutationId() {
        return clientMutationId;
    }

    public UserSession setClientMutationId(String clientMutationId) {
        this.clientMutationId = clientMutationId;
        return this;
    }

    public String getId() {
        return id;
    }

    public UserSession setId(String id) {
        this.id = id;
        return this;
    }

    public OAuthToken getOauthToken() {
        return oauthToken;
    }

    public UserSession setOauthToken(OAuthToken oauthToken) {
        this.oauthToken = oauthToken;
        return this;
    }
}
