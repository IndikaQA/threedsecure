<%@ page import="com.modirum.ds.services.elo.authserver.EloAuthenticationServerStub" %>
<%@ page import="java.util.Optional" %><%
    boolean expired = Boolean.parseBoolean(Optional.ofNullable(request.getParameter("expired"))
                                                 .orElse("false"));
    System.out.println("Set accessToken expired flag to " + expired);
    EloAuthenticationServerStub
          .getInstance()
          .setAccessTokenExpiredFlag(expired);

    response.setStatus(200);
    return;
%>