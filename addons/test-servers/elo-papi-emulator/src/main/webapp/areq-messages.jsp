<%@ page import="java.io.BufferedReader, org.json.JSONObject, org.json.JSONException,
java.io.StringWriter, java.io.PrintWriter, java.util.Map, java.util.HashMap,
com.modirum.ds.services.elo.authserver.Response, com.modirum.ds.services.elo.authserver.EloAuthenticationServerStub" %><%
    Map<String, String> headers = new HashMap<>();
    headers.put("client_id", request.getHeader("client_id"));
    headers.put("access_token", request.getHeader("access_token"));
    Response authorizationResponse = EloAuthenticationServerStub
            .getInstance()
            .validateApiAccess(headers);
    if (!authorizationResponse.isSuccessfulResponse()) {
        out.print(authorizationResponse.getBody());
        return;
    }

    // parse request body into a json object
    StringBuffer sb = new StringBuffer();
    BufferedReader br = request.getReader();
    String line;
    while ((line = br.readLine()) != null) {
        sb.append(line);
        sb.append("\n");
    }
    String areqJsonString = sb.toString();

    JSONObject incomingAreqObject;
    try {
        incomingAreqObject = new JSONObject(areqJsonString);
    } catch (JSONException jsonEx) {
        StringWriter sw = new StringWriter();
        jsonEx.printStackTrace(new PrintWriter(sw));
        out.print(sw.toString());
        return;
    }

//    incomingAreqObject.put("acctNumber", "4000000000001234");
//    incomingAreqObject.put("cardExpiryDate", "2909");

    // just return original for now.
    out.print(incomingAreqObject.toString());
    return;
%>