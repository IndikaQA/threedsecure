<%@ page import="com.modirum.ds.services.elo.authserver.EloAuthenticationServerStub,org.apache.commons.io.IOUtils, java.util.Map, java.util.HashMap, com.modirum.ds.services.elo.authserver.Response" %><%
    Map<String, String> headers = new HashMap<>();
    headers.put("client_id", request.getHeader("client_id"));
    headers.put("Authorization", request.getHeader("Authorization"));
    headers.put("access_token", request.getHeader("access_token"));

    String queryRequest = IOUtils.toString(request.getReader());
    System.out.println("Request: " + queryRequest);
    Response papiResponse = EloAuthenticationServerStub
            .getInstance()
            .processGraphQL(queryRequest, headers);

    System.out.println("Json Response Returned = " + papiResponse.getBody());
    response.setStatus(papiResponse.getStatusCode());
    out.print(papiResponse.getBody());
    return;
%>