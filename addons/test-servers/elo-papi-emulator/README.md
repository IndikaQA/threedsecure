## About

elo-papi-emulator webapp is a test/emulator app to stub out the Elo PAPI (Detokenization) service enpoints.
Emulator can be enriched with a set of *.jsp pages with pre-defined responses for areq/GraphQL queries.

Main ticket with specs: https://github.com/Modirum/DS/issues/1268