package com.modirum.generic.scripts.threedsv2;

import com.eclipsesource.json.JsonValue;
import com.modirum.generic.scripts.common.threedsv2TestSuite;
import com.modirum.generic.utility.CSVTestParams;
import com.modirum.generic.utility.JsonData;
import com.modirum.generic.webdriver2.dstesttool.DSTestPage;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/*
 * DSAReqTestSuite - Parent class for DS AReq Test Suites
 */
public abstract class DSAReqTestSuite extends threedsv2TestSuite {
    /*
     * CSV Records data
     */
    protected Object[][] m_areqInclusionInfo;   // Stores inclusion requirements for AReq fields [test input]
    protected Object[][] m_aresInclusionInfo;   // Stores inclusion requirements and default values for ARes fields [expected results]

    /*
     * AReq Json data
     */
    protected JsonData m_required = new JsonData();
    protected JsonData m_optional = new JsonData();
    protected JsonData m_optional_priorauth = new JsonData();
    protected JsonData m_conditional_opt = new JsonData();
    protected JsonData m_conditional_install = new JsonData();
    protected JsonData m_conditional_brazil = new JsonData();
    protected JsonData m_conditional_addr = new JsonData();
    protected JsonData m_conditional_token = new JsonData();

    /*
     * Main
     */
    @Parameters({"browser", "ip_add", "port", "screenshots", "startURL", "alljson", "numFieldsPerTest", "report_type"})
    @BeforeClass(alwaysRun = true)
    public void setUp(String browser, String ip_add, String port, String screenshots, String startURL, String alljson, int numFieldsPerTest, int report_type) {
        super.setUp(browser, ip_add, port, screenshots, startURL, alljson, numFieldsPerTest, report_type);

        m_errorComponent = "D";
        // Initialize input info
        initAReqInclusionData();
        initDefaultAReqDataForTestClass();
    }

    /*****************************************
     * Common utility functions
     *****************************************/
    /*
     * This function gets the default Json for the test group that the given field belongs to
     */
    protected JsonData getTestGroupJson(String field) {
        String group = m_testGroupMap.get(field);

        if (group.equals("R")) {
            return m_required;
        }
        if (group.equals("O")) {
            return m_optional;
        }
        if (group.equals("O-priorauth")) {
            return m_optional_priorauth;
        }
        if (group.equals("C-optional")) {
            return m_conditional_opt;
        }
        if (group.equals("C-addr")) {
            return m_conditional_addr;
        }
        if (group.equals("C-brazil")) {
            return m_conditional_brazil;
        }
        if (group.equals("C-install")) {
            return m_conditional_install;
        }
        if (group.equals("C-token")) {
            return m_conditional_token;
        }
        return null;
    }

    /*****************************************
     * Initialization functions
     *****************************************/
    /*
     * This function initializes the expected data for the test class
     */
    protected void initExpectedData() {
        expectedData = new HashMap<String, String[]>();

        // Initialize common error codes from the errorCodesv2.csv file
        String path = m_testFilesPath + "expecteddata" + File.separator + "errorCodesv2.csv";
        CSVTestParams.getRecordsAsMapByColHeaders(expectedData, path, "ERROR CATEGORY",
                                                  new String[]{"errorCode", "errorDescription"});

        path = m_testFilesPath + "expecteddata" + File.separator + "DS_Erro.csv";
        m_erroInclusionInfo = CSVTestParams.getRecordsAsArrayByColHeaders(path,
                                                                          new String[]{"FIELD", "INCLUSION", "CONDITION", "EXPECTED VALUE"});

        initExpectedAResData();
    }

    /*
     * This function initializes the inclusion information of the AReq fields
     */
    protected void initAReqInclusionData() {
        Assert.assertTrue(m_areqInclusionInfo == null);

        String csvPath = m_testFilesPath + "inputs" + File.separator + "DS_AReq.csv";

        m_areqInclusionInfo = CSVTestParams.getRecordsAsArrayByColHeaders(csvPath,
                                                                          new String[]{"FIELD", m_testClassColumn, "CONDITIONAL GROUP"});
    }

    protected void initExpectedAResData() {
        String path = m_testFilesPath + "expecteddata" + File.separator + "DS_ARes.csv";

        m_aresInclusionInfo = CSVTestParams.getRecordsAsArrayByColHeaders(path,
                                                                          new String[]{"FIELD", m_testClassColumn, "CONDITION", "EXPECTED VALUE"});
    }

    /*
     * This function initializes the default AReq data for the given test class
     */
    protected void initDefaultAReqDataForTestClass() {
        String jsonPath = m_testFilesPath + "defaultMessages" + File.separator + m_json;

        JsonData allJson = new JsonData(jsonPath);
        //-- Set Test Class specific fields
        // Set messageType to AReq
        allJson.setValue("messageType", m_messageType);
        // Set deviceChannel to APP (01)
        allJson.setValue("deviceChannel", m_deviceChannel);
        // Set messageCategory to Payment (01)
        allJson.setValue("messageCategory", m_messageCategory);

        initDefaultAReqData(allJson);
    }

    /*
     * This function initializes the default AReq data common to all test classes in this test suite
     */
    protected void initDefaultAReqData(JsonData allJson) {
        m_testGroupMap = new HashMap<String, String>();
        //-- Set default Json fields based on inclusion
        for (Object[] field : m_areqInclusionInfo) {
            String category = (String) field[1];
            String condition = (String) field[2];

            if (category.equals("-")) {
                continue;
            } else {
                String testGroup = category;
                String key = (String) field[0];
                JsonValue val = allJson.getJsonValue((String) field[0]);
                if (category.equals("R")) {
                    // Required fields are needed for all default AReqs
                    m_required.addElement(key, val);
                    m_optional.addElement(key, val);
                    m_optional_priorauth.addElement(key, val);
                    m_conditional_opt.addElement(key, val);
                    m_conditional_install.addElement(key, val);
                    m_conditional_brazil.addElement(key, val);
                    m_conditional_addr.addElement(key, val);
                    m_conditional_token.addElement(key, val);
                } else if (category.equals("O")) {
                    if (condition.equals("priorauth")) {
                        testGroup = testGroup.concat("-priorauth");
                        m_optional_priorauth.addElement(key, val);
                    } else {
                        m_optional.addElement(key, val);
                    }
                } else if (category.equals("C")) {
                    testGroup = testGroup.concat("-" + condition);
                    if (condition.equals("optional")) {
                        m_conditional_opt.addElement(key, val);
                    } else if (condition.equals("install")) {
                        m_conditional_install.addElement(key, val);
                    } else if (condition.equals("brazil")) {
                        m_conditional_brazil.addElement(key, val);
                    } else if (condition.equals("addr")) {
                        m_conditional_addr.addElement(key, val);
                    } else if (condition.equals("token")) {
                        m_conditional_token.addElement(key, val);
                    } else {
                        System.out.println("[WARNING] Unknown conditional group [" + condition + "]");
                    }
                } else {
                    System.out.println("[WARNING] Unknown category [" + category + "]");
                }
                m_testGroupMap.put(key, testGroup);
            }
        }

        //-- Update default required field for "C-brazil" test cases
        m_conditional_brazil.setValue("merchantCountryCode", "076"); // Brazil country code
    }

    /*
     * This function sets the column of the input CSV file that will be used for the current test suite
     */
    protected void setTestClassColumn() {
        StringBuilder testclassColumn = new StringBuilder();
        if (m_deviceChannel.equals("01")) {
            testclassColumn.append("APP_");
        } else if (m_deviceChannel.equals("02")) {
            testclassColumn.append("BRW_");
        } else {
            testclassColumn.append("3RI_");
        }
        if (m_messageCategory.equals("01")) {
            testclassColumn.append("PA");
        } else {
            testclassColumn.append("NPA");
        }
        m_testClassColumn = testclassColumn.toString();
    }

    /*******************************
     * COMMON TEST METHODS
     ********************************/
    protected void _testDefaultJson(String testName, JsonData defaultJson) {
        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName, test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName, "");
        }
        m_reporter.logInfo("Starting test with no params");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] No default AReq Json info!");

        sendAndVerifyAReqNoError(softAssert, defaultJson);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertFalse(false);
    }

    protected void _testRequired_Absent(String testName, JsonData defaultJson, List<String> testFields) {
        StringBuilder paramLog = new StringBuilder();
        for (String field : testFields) {
            paramLog.append(field + ",");
        }
        paramLog.deleteCharAt(paramLog.length() - 1);

        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", "");
        }

        m_reporter.logInfo("Starting test with params [" + paramLog + "]");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        for (String testField : testFields) {
            inJson.removeElement(testField);
        }

        sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testEmpty(String testName, JsonData defaultJson, List<String> testFields) {
        logStartTestList(testName, testFields);

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        for (String testField : testFields) {
            inJson.setValue(testField, "");
        }

        if (testName.contains("Required")) {
            sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);
        } else if (testName.contains("Optional")) {
            sendAndVerifyAReqNoError(softAssert, inJson);
        } else {
            System.out.println("\n[WARNING] Unknown testEmpty method [" + testName + "]");
        }

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testDuplicate(String testName, JsonData defaultJson, List<String> testFields) {
        StringBuilder paramLog = new StringBuilder();
        for (String field : testFields) {
            paramLog.append(field + ",");
        }
        paramLog.deleteCharAt(paramLog.length() - 1);

        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", "");
        }
        m_reporter.logInfo("Starting test with params [" + paramLog + "]");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        for (String testField : testFields) {
            JsonValue val = inJson.getJsonValue(testField);
            inJson.addElement(testField, val);
        }

        sendAndVerifyAReqDuplicateElems(softAssert, inJson, testFields);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testConditional_Addr_Absent(String testName, JsonData defaultJson, String testField) {
        List<String> testFields = new LinkedList<String>();
        testFields.add(testField);

        logStartTestList(testName, testFields);

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        inJson.removeElement(testField);

        if (testField.equals("addrMatch")) {
            // Required case
            m_reporter.logInfo("REQUIRED CONDITION FOR addrMatch - AReq has shipping address fields");
            sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);

            // Optional case
            m_reporter.logInfo("OPTIONAL CONDITION FOR addrMatch - AReq does not have shipping address fields");
            inJson.removeElement("shipAddrCity");
            inJson.removeElement("shipAddrCountry");
            inJson.removeElement("shipAddrLine1");
            inJson.removeElement("shipAddrLine2");
            inJson.removeElement("shipAddrLine3");
            inJson.removeElement("shipAddrPostCode");
            inJson.removeElement("shipAddrState");

            sendAndVerifyAReqNoError(softAssert, inJson);
        } else {
            // address fields are either all present at the same time or absent at the same time
            //sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);

            // Spec for this is under clarification. for now, treat Address fields as optional
            sendAndVerifyAReqNoError(softAssert, inJson);
        }

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertFalse(false);
    }

    protected void _testConditional_Addr_Empty(String testName, JsonData defaultJson, String testField) {
        List<String> testFields = new LinkedList<String>();
        testFields.add(testField);

        logStartTestList(testName, testFields);

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        inJson.setValue(testField, "");

        if (testField.equals("addrMatch")) {
            // Required case
            m_reporter.logInfo("REQUIRED CONDITION FOR addrMatch - AReq has shipping address fields");
            sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);

            // Optional case
            m_reporter.logInfo("OPTIONAL CONDITION FOR addrMatch - AReq does not have shipping address fields");
            inJson.removeElement("shipAddrCity");
            inJson.removeElement("shipAddrCountry");
            inJson.removeElement("shipAddrLine1");
            inJson.removeElement("shipAddrLine2");
            inJson.removeElement("shipAddrLine3");
            inJson.removeElement("shipAddrPostCode");
            inJson.removeElement("shipAddrState");

            sendAndVerifyAReqNoError(softAssert, inJson);
        } else {
            // address fields are either all present at the same time or absent at the same time
            //sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);

            // Spec for this is under clarification. for now, treat Address fields as optional
            sendAndVerifyAReqNoError(softAssert, inJson);
        }

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testConditional_Install_Absent(String testName, JsonData defaultJson, String testField) {
        List<String> testFields = new LinkedList<String>();
        testFields.add(testField);

        logStartTestList(testName, testFields);

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        inJson.removeElement(testField);

        if (!testField.equals("purchaseInstalData")) {
            // Required case
            m_reporter.logInfo("REQUIRED CONDITION FOR recur* fields - AReq has purchaseInstalData");
            sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);

            // Optional case
            m_reporter.logInfo("OPTIONAL CONDITION FOR recur* fields - AReq does not have purchaseInstalData");
            inJson.removeElement("purchaseInstalData");
        }
        sendAndVerifyAReqNoError(softAssert, inJson);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testConditional_Install_Empty(String testName, JsonData defaultJson, String testField) {
        List<String> testFields = new LinkedList<String>();
        testFields.add(testField);

        logStartTestList(testName, testFields);

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        inJson.setValue(testField, "");

        if (!testField.equals("purchaseInstalData")) {
            // Required case
            m_reporter.logInfo("REQUIRED CONDITION FOR recur* fields - AReq has purchaseInstalData");
            sendAndVerifyAReqMissingElems(softAssert, inJson, testFields);

            // Optional case
            m_reporter.logInfo("OPTIONAL CONDITION FOR recur* fields - AReq does not have purchaseInstalData");
            inJson.removeElement("purchaseInstalData");
        }
        sendAndVerifyAReqNoError(softAssert, inJson);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testElementFormat(String testName, String testField, String testValue, String expCat) {
        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + testValue + "," + expCat + "]",
                                 test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + testValue + "," + expCat + "]", "");
        }
        m_reporter.logInfo("Starting test with params [" + testValue + "]");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        JsonData defaultJson = getTestGroupJson(testField);
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default AReq Json");
        JsonData inJson = new JsonData(defaultJson);

        inJson.setValue(testField, testValue);

        if (expCat.equals("validRes")) {
            sendAndVerifyAReqNoError(softAssert, inJson);
        } else { // error
            sendAndVerifyAReqErrorElem(softAssert, inJson, testField, expCat);
        }

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertFalse(false);
    }

    /*******************************
     * COMMON VERIFICATION METHODS
     ********************************/
    protected void sendAndVerifyAReqNoError(SoftAssert softAssert, JsonData inJson) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String areqString = inJson.convertToString();
        dsTestPage.setInputJsonData(areqString);

        m_reporter.logInfo("[AREQ]" + areqString);

        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        dsTestPage.getJsonResult();
        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();

        m_reporter.logInfo("[RESPONSE]" + response);

        if (!response.contains("{")) {
            softAssert.assertTrue(response.contains("{"),
                                  "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            //replyJson.showJsonString();

            String messageType = replyJson.getValueAsString("messageType");
            if (m_basicResCheckOnly) {
                softAssert.assertEquals(messageType, "ARes",
                                        "\n[FAILED TEST] Response is " + messageType + " not ARes!");
            } else {
                if (messageType.equals("ARes")) {
                    // check response = ARes case
                    String transStatus = "";
                    if (replyJson.containsField("transStatus")) {
                        transStatus = replyJson.getValueAsString("transStatus");
                    }

                    for (Object[] aresField : m_aresInclusionInfo) {
                        String field = (String) aresField[0];
                        String inclusion = (String) aresField[1];
                        String condition = (String) aresField[2];
                        String[] parse = condition.split(":");
                        String val = (String) aresField[3];

                        boolean isExist = replyJson.containsField(field);
                        boolean isRequired = false;

                        if (inclusion.equals("R")) {  // required field
                            isRequired = true;
                        } else if (inclusion.equals("O") ||
                                   (inclusion.equals("C") && parse[0].equals("optional"))) { // optional field
                            isRequired = false;
                        } else if (inclusion.equals("C")) { // conditional field
                            if (parse[0].equals("transStatus")) {
                                String[] transStatusParse = parse[1].split(",");
                                for (String c : transStatusParse) {
                                    if (transStatus.equals(c)) {
                                        isRequired = true;
                                    }
                                }
                            }
                        }
                        if (isRequired) {
                            softAssert.assertTrue(isExist, "\n[FAILED TEST] Missing required field [" + field + "]");
                        }
                        if (isExist) {
                            verifyResponseField(softAssert, val, field, inJson, replyJson);
                        }
                    }
                } else {
                    softAssert.assertEquals(messageType, "ARes",
                                            "\n[FAILED TEST] Response is " + messageType + " not ARes!");
                }
            }
        }
        dsTestPage.resetPage();
    }

    protected void sendAndVerifyAReqMissingElems(SoftAssert softAssert, JsonData inJson, List<String> testFields) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String areqString = inJson.convertToString();
        dsTestPage.setInputJsonData(areqString);

        m_reporter.logInfo("[AREQ]" + areqString);

        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();
        m_reporter.logInfo("[RESPONSE]" + response);
        if (!response.contains("{")) {
            softAssert.assertFalse(response.contains("{"),
                                   "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            String messageType = replyJson.getValueAsString("messageType");
            if (messageType.equals("Erro")) {
                // check error cases
                if (testFields.contains("messageType")) {
                    verifyErrorResponse(softAssert, inJson, replyJson, "error101", testFields);
                } else {
                    verifyErrorResponse(softAssert, inJson, replyJson, "error201", testFields);
                }
            } else {
                softAssert.assertEquals(messageType, "Erro",
                                        "\n[FAILED TEST] Response is " + messageType + " not Erro!");
            }
        }
        dsTestPage.resetPage();
    }

    protected void sendAndVerifyAReqDuplicateElems(SoftAssert softAssert, JsonData inJson, List<String> testFields) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String areqString = inJson.convertToString();
        dsTestPage.setInputJsonData(areqString);

        m_reporter.logInfo("[AREQ]" + areqString);
        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();
        m_reporter.logInfo("[RESPONSE]" + response);
        if (!response.contains("{")) {
            softAssert.assertFalse(response.contains("{"),
                                   "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            String messageType = replyJson.getValueAsString("messageType");
            if (messageType.equals("Erro")) {
                // check error cases
                verifyErrorResponse(softAssert, inJson, replyJson, "error204", testFields);
            } else {
                softAssert.assertEquals(messageType, "Erro",
                                        "\n[FAILED TEST] Response is " + messageType + " not Erro!");
            }
        }
        dsTestPage.resetPage();
    }

    protected void sendAndVerifyAReqErrorElem(SoftAssert softAssert, JsonData inJson, String testField, String expResult) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String areqString = inJson.convertToString();
        dsTestPage.setInputJsonData(areqString);

        m_reporter.logInfo("[AREQ]" + areqString);

        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();
        m_reporter.logInfo("[RESPONSE]" + response.replaceAll("\n", "<br>"));
        if (!response.contains("{")) {
            softAssert.assertFalse(response.contains("{"),
                                   "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            String messageType = replyJson.getValueAsString("messageType");
            if (messageType.equals("Erro")) {
                // check error cases
                List<String> testFields = new LinkedList<String>();
                testFields.add(testField);
                verifyErrorResponse(softAssert, inJson, replyJson, expResult, testFields);
            } else {
                softAssert.assertEquals(messageType, "Erro",
                                        "\n[FAILED TEST] Response is " + messageType + " not Erro!");
            }
        }
        dsTestPage.resetPage();
    }

    /*******************************
     * COMMON DATA PROVIDER METHODS
     ********************************/
    protected Object[][] getAReqDataProvider(String methodName) {
        if (methodName.contains("testRequired")) {
            return CSVTestParams.createListDP(m_areqInclusionInfo, m_required.size(), m_numFieldsPerTest, "R", "");
        }
        if (methodName.contains("testOptional")) {
            return CSVTestParams.createListDP(m_areqInclusionInfo, m_optional.size(), m_numFieldsPerTest, "O",
                                              ""); // gets both "O-default" and "O-priorauth"
        }
        // The following are testConditional sub-test groups
        if (methodName.contains("Brazil")) {
            return CSVTestParams.createListDP(m_areqInclusionInfo, m_conditional_brazil.size(), m_numFieldsPerTest, "C",
                                              "brazil");
        }
        if (methodName.contains("Addr")) {
            if (methodName.contains("Duplicate")) {
                return CSVTestParams.createListDP(m_areqInclusionInfo, m_conditional_addr.size(), m_numFieldsPerTest,
                                                  "C", "addr");
            } else {
                return CSVTestParams.createStringDP(m_areqInclusionInfo, m_conditional_addr.size(), "C", "addr");
            }
        }
        if (methodName.contains("Install")) {
            if (methodName.contains("Duplicate")) {
                return CSVTestParams.createListDP(m_areqInclusionInfo, m_conditional_install.size(), m_numFieldsPerTest,
                                                  "C", "install");
            } else {
                return CSVTestParams.createStringDP(m_areqInclusionInfo, m_conditional_install.size(), "C", "install");
            }
        }
        if (methodName.contains("Opt")) {
            return CSVTestParams.createListDP(m_areqInclusionInfo, m_conditional_opt.size(), m_numFieldsPerTest, "C",
                                              "optional");
        }
        if (methodName.contains("Token")) {
            return CSVTestParams.createStringDP(m_areqInclusionInfo, m_conditional_token.size(), "C", "token");
        }
        return null;
    }
}
