package com.modirum.generic.utility.reports;

public class ConsoleTestReporter implements ITestReporter {
    public ConsoleTestReporter() {
        System.out.println("Starting Test Report\n");
    }

    public void startTest(String testName, String testDescription) {
        System.out.println("\n[START TEST] " + testName + "\n\tDESCRIPTION: " + testDescription);
    }

    public void logInfo(String info) {
        System.out.println("[INFO]" + info);
    }

    public void logSkip(String info) {
        System.out.println("[SKIP]" + info);
    }

    public void logPass(String info) {
        System.out.println("[PASS]" + info);
    }

    public void logFail(String info) {
        System.out.println("[FAIL]" + info);
    }

    public void logWarning(String info) {
        System.out.println("[WARNING]" + info);
    }

    public void endTest() {
        System.out.println("[END TEST]");
    }
}
