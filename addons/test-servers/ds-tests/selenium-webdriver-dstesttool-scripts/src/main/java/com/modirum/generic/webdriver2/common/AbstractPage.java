package com.modirum.generic.webdriver2.common;

import com.modirum.generic.utility.SelDriver;

public abstract class AbstractPage {
    protected final SelDriver driver;

    public AbstractPage(SelDriver d) {
        driver = d;
    }

    public String getUrl() {
        return driver.getDriver().getCurrentUrl();
    }

    public String getTitle() {
        return driver.getDriver().getTitle();
    }
}
