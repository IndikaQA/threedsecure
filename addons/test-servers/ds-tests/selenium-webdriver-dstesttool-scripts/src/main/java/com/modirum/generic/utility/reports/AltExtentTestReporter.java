package com.modirum.generic.utility.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.apache.commons.lang3.StringUtils;

public class AltExtentTestReporter implements ITestReporter {
    private ExtentReports m_reporter = null;
    private ExtentHtmlReporter m_htmlReporter = null;


    private ExtentTest m_testReporter = null;

    public AltExtentTestReporter(String fullPath) {
        m_reporter = new ExtentReports();
        m_htmlReporter = new ExtentHtmlReporter(fullPath);
        m_reporter.attachReporter(m_htmlReporter);
    }

    public void startTest(String testName, String testDescription) {
        m_testReporter = m_reporter.createTest(testName, testDescription);
    }

    public void logInfo(String info) {
        m_testReporter.log(Status.INFO, StringUtils.replace(info, "\n", "<br>"));
    }

    public void logSkip(String info) {
        m_testReporter.log(Status.SKIP, StringUtils.replace(info, "\n", "<br>"));
    }

    public void logPass(String info) {
        m_testReporter.log(Status.PASS, StringUtils.replace(info, "\n", "<br>"));
    }

    public void logFail(String info) {
        m_testReporter.log(Status.FAIL, StringUtils.replace(info, "\n", "<br>"));
    }

    public void logWarning(String info) {
        m_testReporter.log(Status.WARNING, StringUtils.replace(info, "\n", "<br>"));
    }

    public void endTest() {
        m_reporter.flush();
    }
}
