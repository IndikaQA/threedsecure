/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 09.08.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.model.TDSModel;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService210;
import com.modirum.ds.tds20msgs.TDSMessage;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;

import javax.net.ssl.KeyManagerFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import static com.modirum.ds.util.DSUtils.createUUID;

public class MessageBurster extends Thread {
    static int countAll = 0;
    static int failAll = 0;
    static int okAll = 0;

    static Object countAllObj = new Object();
    static Object failAllObj = new Object();
    static Object okAllObj = new Object();

    static String merchantId = "771133";
    static String requestorId = "100001";
    static String requestorName = "Requestor Name";
    static String merchantName = "Merchant Name";
    static String acqBIN = "412345";
    static String expdate = "1912";
    static String pan = "4016000000002";
    static String keysStore = null;
    static String keysStorePass = null;
    static String keysPass = null;
    static String keyAlias = null;
    static String mv = null;
    static boolean panDynamic = false;
    static PrivateKey clientKey = null;
    static X509Certificate[] clientCert = null;
    static String tlsv = "TLS";
    static String ip = Utils.getLocalNonLoIp();
    MessageService xs;


    static void countAll() {
        synchronized (countAllObj) {
            countAll++;
        }
    }

    static void countAllFailed() {
        synchronized (failAllObj) {
            failAll++;
        }
    }

    static void countAllOk() {
        synchronized (okAllObj) {
            okAll++;
        }
    }

    /**
     * @param args serviceUrl number of bursters number of messages each burster should send1
     */
    public static void main(String[] args) {

        String serviceUrl = null;
        int bursters = 10;
        int messages = 100;

        boolean help = false;
        for (int i = 0; args != null && i < args.length; i++) {
            if ("-help".equals(args[i])) {
                help = true;
                break;
            } else if ("-pan".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                pan = args[i + 1];
            } else if ("-pandynamic".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                panDynamic = "true".equals(args[i + 1]);
            } else if ("-mid".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                merchantId = (args[i + 1]);
            } else if ("-expdate".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                expdate = args[i + 1];
            } else if ("-msgcount".equals(args[i]) && args.length > i + 1 && Misc.isInt(args[i + 1])) {
                messages = Misc.parseInt(args[i + 1]);
            } else if ("-url".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                serviceUrl = args[i + 1];
            } else if ("-bursters".equals(args[i]) && args.length > i + 1 && Misc.isInt(args[i + 1])) {
                bursters = Misc.parseInt(args[i + 1]);
            } else if ("-tls".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                tlsv = args[i + 1];
            } else if ("-tls".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                tlsv = args[i + 1];
            } else if ("-reqid".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                requestorId = args[i + 1];
            } else if ("-acqbin".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                acqBIN = args[i + 1];
            } else if ("-cks".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                keysStore = args[i + 1];
            } else if ("-cksp".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                keysStorePass = args[i + 1];
            } else if ("-ckskp".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                keysPass = args[i + 1];
            } else if ("-cka".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                keyAlias = args[i + 1];
            } else if ("-mv".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                mv = args[i + 1];
            }
        }

        if (help || serviceUrl == null) {
            System.out.println("usage: sender options");
            System.out.println("options:");
            System.out.println("	-pan pan");
            System.out.println("	-pandynamic true|false");
            System.out.println("	-mid merchant id");
            System.out.println("	-reqid 3ds requestor id");
            System.out.println("	-acqbin acuirer bin");
            System.out.println("	-cks client keystore");
            System.out.println("	-cksp client keystore pass");
            System.out.println("	-ckskp client keystore key pass");
            System.out.println("	-cka client keystore key alias");
            System.out.println("	-msgcount no of messages for burster");
            System.out.println("	-url service url");
            System.out.println("	-bursters  number of burster threads");
            System.out.println("	-mv  message version 2.1.0/2.0.1");
            System.out.println("	-tls TLS version: TLS, TLSv1.1 TLSv1.2");
            return;
        }

        if (keysStore != null && keysStorePass != null && keyAlias != null) {
            try {
                KeyStore keyStore = KeyStore.getInstance("JKS");
                java.io.FileInputStream fis = new java.io.FileInputStream(keysStore);
                keyStore.load(fis, keysStorePass.toCharArray());
                clientKey = (PrivateKey) (keyStore.getKey(keyAlias, keysPass !=
                                                                    null ? keysPass.toCharArray() : keysStorePass.toCharArray()));

                Certificate[] chain = keyStore.getCertificateChain(keyAlias);
                clientCert = new X509Certificate[chain.length];
                for (int i = 0; i < chain.length; i++) {
                    clientCert[i] = (X509Certificate) chain[i];
                }

                fis.close();

            } catch (Exception e) {
                e.printStackTrace(System.out);
                return;
            }

        }

        System.out.println(
                new java.util.Date() + " Starting " + bursters + " bursters, each to send " + messages + " to " +
                serviceUrl);
        MessageBurster[] bursterObjs = new MessageBurster[bursters];
        for (int i = 0; i < bursterObjs.length; i++) {
            MessageBurster xb = new MessageBurster(serviceUrl, messages, mv);
            bursterObjs[i] = xb;
            xb.start();

        }
        System.out.println(new java.util.Date() + " All bursters started");
        long start = System.currentTimeMillis();

        while (true) {
            try {
                Thread.sleep(100);
            } catch (Exception dc) {
                break;
            }

            int alivecount = 0;
            for (int i = 0; i < bursterObjs.length; i++) {
                MessageBurster xb = bursterObjs[i];
                if (xb.isAlive()) {
                    alivecount++;
                }
            }

            if (alivecount == 0) {
                break;
            }

        }

        long end = System.currentTimeMillis();
        System.out.println(new java.util.Date() + " " + bursterObjs.length + " Bursters completed");
        for (int i = 0; i < bursterObjs.length; i++) {
            MessageBurster xb = bursterObjs[i];
            System.out.println(
                    "Burster " + i + " Request/response timing summaries: " + "txcount: " + xb.counter + "  min: " +
                    xb.minResp + "ms  max: " + xb.maxResp + "ms avgResp: " +
                    (xb.counter > 0 ? (xb.respSum / xb.counter) : -1) + "ms failed " + xb.failed);
        }
        System.out.println(
                new java.util.Date() + " Total transactions processed " + countAll + " OK " + okAll + " Failed " +
                failAll + "\n" + "average per second " + ((double) countAll / ((double) ((end - start) / 1000L))));
    }

    static long messageId = System.currentTimeMillis();

    static {
        messageId = System.currentTimeMillis();
    }

    synchronized static long getMessageId() {
        messageId++;

        return messageId;
    }

    static void fillMsgStatic(TDSMessage m) {
        m.setMessageType(TDSModel.XmessageType.A_REQ.value());
        m.setTdsServerRefNumber("PosterForm");
        m.setTdsServerURL("https://" + ip + ":8843");
        m.setTdsRequestorURL("https://" + ip);
        m.setMessageVersion("2.0.1");
        m.setAcquirerBIN(acqBIN);
        m.setMcc("5544");
        m.setMerchantName(merchantName);
        m.setTdsRequestorID(requestorId);
        m.setTdsRequestorName(requestorName);
        m.setAcquirerMerchantID(merchantId);
        m.setMerchantCountryCode("233");

        m.setCardExpiryDate(expdate);

        m.setBrowserAcceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        m.setBrowserLanguage("en-US");
        m.setBrowserColorDepth("32");
        m.setBrowserScreenHeight("720");
        m.setBrowserScreenWidth("1280");
        m.setBrowserIP(ip);
        m.setBrowserTZ("120");
        m.setBrowserJavaEnabled(true);
        m.setBrowserUserAgent(
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36");

        m.setDeviceChannel("02");
        m.setMessageCategory("01");

        m.setPurchaseExponent("2");
        m.setPurchaseCurrency("978");
    }


    static void fillMsgDynamic(TDSMessage m, String pan, int counter) {
        m.setTdsServerTransID(createUUID(Long.MAX_VALUE - 9999999999L - Thread.currentThread().getId(), counter));
        m.setAcctNumber(pan);
        m.setPurchaseDate(DateUtil.formatDate(new java.util.Date(), "yyyyMMdd HH:mm:ss"));
        m.setPurchaseAmount("" + (counter % 20));
    }


    String serviceUrl;
    int messagesToSend;
    boolean run = true;
    int counter, minResp, maxResp, respSum, failed = 0;


    public MessageBurster(String serviceUrl, int messagesToSend, String v) {
        this.serviceUrl = serviceUrl;
        this.messagesToSend = messagesToSend;
        if ("2.1.0".equals(v)) {
            xs = MessageService210.getInstance();
        }
    }

    public final static int verifyPan(String inPAN) {

        if (inPAN == null) {
            return 10;
        }
        if (inPAN.length() < 12 || inPAN.length() > 19) {
            return 31;
        }

        // check if all are numbers
        for (int i = 0; i < inPAN.length(); i++) {
            // if(inPAN.charAt(i) >= "0" && inNumber.charAt(i) <= "9")
            // { number = number + inNumber.charAt(i); }
            if (inPAN.charAt(i) < '0' || inPAN.charAt(i) > '9') {
                return 11;
            }

        }

        int total = 0;
        int tmp = 0;
        for (int loc = inPAN.length() - 2; loc >= 0; loc -= 2) {
            total += 1 * Character.getNumericValue(inPAN.charAt(loc + 1));
            tmp = Character.getNumericValue(inPAN.charAt(loc)) * 2;
            if (tmp > 9) {
                total += 1;
            }
            total += tmp % 10;
        }
        if (inPAN.length() % 2 > 0) {
            total += 1 * Character.getNumericValue(inPAN.charAt(0));
        }

        return (total % 10);
        // return 0;
    }


    @Override
    public void run() {
        try {
            HttpsJSSEClient jssecl = new HttpsJSSEClient();
            jssecl.init(null, KeyManagerFactory.getDefaultAlgorithm(), tlsv, "true", clientKey, clientCert, null, 5000,
                        15000, false);
            TDSMessage xMsg = new TDSMessage();
            fillMsgStatic(xMsg);
            while (run && counter < messagesToSend) {
                long start, end = 0;
                String panx = pan;
                if (panDynamic) {
                    long pv = Long.valueOf(pan) + (10L * countAll);
                    while (verifyPan("" + pv) != 0) {
                        pv = pv + 1;
                    }

                    panx = "" + pv;
                }

                fillMsgDynamic(xMsg, panx, counter);
                byte[] xmlMessage = xs.toJSON(xMsg);
                byte[] resp = null;
                System.out.println(Thread.currentThread().getName() + "Sending message " + xMsg.getTdsServerTransID() +
                                   " counter " + counter);
                start = System.currentTimeMillis();
                try {
                    resp = jssecl.post(serviceUrl, "application/json; charset=\"utf-8\"", xmlMessage);
                    end = System.currentTimeMillis();
                    countAllOk();
                } catch (Exception e) {
                    end = System.currentTimeMillis();
                    failed++;
                    countAllFailed();
                    e.printStackTrace();
                } finally {
                    counter++;
                    countAll();
                }
                if (resp != null) {
                    System.out.println(Thread.currentThread().getName() + " Message counter " + counter + " with id " +
                                       xMsg.getTdsServerTransID() + " posted:" + new String(resp, "UTF-8"));
                }

                System.out.println(
                        Thread.currentThread().getName() + " Request/response roundtrip time " + (end - start) + " ms");
                int cresp = (int) (end - start);
                if (cresp > maxResp) {
                    maxResp = cresp;
                }
                if (cresp < minResp || minResp == 0) {
                    minResp = cresp;
                }
                respSum += cresp;
            }


        } catch (Exception gf) {
            gf.printStackTrace();
        }
    }

    public void stopIt() {
        run = false;
        this.interrupt();
    }

}
