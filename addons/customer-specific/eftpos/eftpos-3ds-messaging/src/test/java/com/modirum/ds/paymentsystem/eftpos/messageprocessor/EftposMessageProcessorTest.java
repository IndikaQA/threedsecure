package com.modirum.ds.paymentsystem.eftpos.messageprocessor;

import com.modirum.ds.enums.FieldName;
import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.model.threeds.MessageExtension;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.JsonMessageService;
import com.modirum.ds.services.ServiceLocator;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

public class EftposMessageProcessorTest {

    private final JsonMessageService jsonMessageService = new JsonMessageService();
    private EftposMessageProcessor eftposMessageProcessor;
    private MockUp<ServiceLocator> serviceLocatorMockUp;

    @Mocked
    private ServiceLocator serviceLocator;

    @BeforeEach
    public void beforeEach() {
        if (serviceLocatorMockUp == null) {
            serviceLocatorMockUp = new MockUp<ServiceLocator>() {
                @Mock
                public ServiceLocator getInstance() {
                    return serviceLocator;
                }

                @Mock
                public JsonMessageService getJsonMessageService() {
                    return jsonMessageService;
                }
            };
        }

        if (eftposMessageProcessor == null) {
            eftposMessageProcessor = new EftposMessageProcessor();
        }
    }

    @Test
    public void processAReqOutgoing_noMessageExtension() throws Exception {
        String json = "{}";
        JsonMessage aReqJsonMessage = jsonMessageService.parse(json);
        eftposMessageProcessor.processAReqOutgoing(aReqJsonMessage);

        String expected = "{\"messageExtension\":[{\"name\":\"Directory Server ID\",\"id\":\"A000000384*DSID\",\"criticalityIndicator\":false,\"data\":{\"directoryServerID\":\"A000000384\",\"version\":\""+ EftposMessageProcessor.DIRECTORY_SERVER_ID_MESSAGE_EXT_VERSION +"\"}}]}";
        Assertions.assertEquals(expected, aReqJsonMessage.toString());
    }

    @Test
    public void processAReqOutgoing_existingMessageExtension() throws Exception {
        String json = "{\"messageExtension\":[{\"id\":\"someMsgExtension\",\"criticalityIndicator\":false}]}";
        JsonMessage aReqJsonMessage = jsonMessageService.parse(json);
        eftposMessageProcessor.processAReqOutgoing(aReqJsonMessage);

        String expected = "{\"messageExtension\":[{\"id\":\"someMsgExtension\",\"criticalityIndicator\":false},{\"name\":\"Directory Server ID\",\"id\":\"A000000384*DSID\",\"criticalityIndicator\":false,\"data\":{\"directoryServerID\":\"A000000384\",\"version\":\""+ EftposMessageProcessor.DIRECTORY_SERVER_ID_MESSAGE_EXT_VERSION +"\"}}]}";
        Assertions.assertEquals(expected, aReqJsonMessage.toString());
    }

    @Test
    public void processAReqOutgoing_duplicateMessageExtension() throws Exception {
        String json = "{\"messageExtension\":[{\"name\":\"Directory Server ID\",\"id\":\"A000000384*DSID\",\"criticalityIndicator\":true,\"data\":{\"directoryServerID\":\"Duplicate\"}}]}";
        JsonMessage aReqJsonMessage = jsonMessageService.parse(json);
        eftposMessageProcessor.processAReqOutgoing(aReqJsonMessage);

        String expected = "{\"messageExtension\":[{\"name\":\"Directory Server ID\",\"id\":\"A000000384*DSID\",\"criticalityIndicator\":false,\"data\":{\"directoryServerID\":\"A000000384\",\"version\":\""+ EftposMessageProcessor.DIRECTORY_SERVER_ID_MESSAGE_EXT_VERSION +"\"}}]}";
        Assertions.assertEquals(expected, aReqJsonMessage.toString());
    }

    @Test
    public void processAReqOutgoing_belowLimitMessageExtensions() throws Exception {
        String originalJson = "{\"messageExtension\":[" +
                              "{\"name\":\"ext1\",\"id\":\"1\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext2\",\"id\":\"2\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext3\",\"id\":\"3\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext4\",\"id\":\"4\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext5\",\"id\":\"5\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext6\",\"id\":\"6\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext7\",\"id\":\"7\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext8\",\"id\":\"8\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext9\",\"id\":\"9\",\"criticalityIndicator\":false}" +
                              "]}";

        JsonMessage aReqJsonMessage = jsonMessageService.parse(originalJson);
        eftposMessageProcessor.processAReqOutgoing(aReqJsonMessage);

        Assertions.assertEquals(10, aReqJsonMessage.getMessageExtension().size());
        Assertions.assertEquals(EftposMessageProcessor.DS_AREQ_MSG_EXTENSION_ID, aReqJsonMessage.getMessageExtension().get(9).getId());
    }

    @Test
    public void processAReqOutgoing_exactLimitButWithDuplicateMessageExtensions() throws Exception {
        String originalJson = "{\"messageExtension\":[" +
                              "{\"name\":\"ext1\",\"id\":\"1\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext2\",\"id\":\"2\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext3\",\"id\":\"3\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext4\",\"id\":\"4\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"Directory Server ID\",\"id\":\"A000000384*DSID\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext6\",\"id\":\"6\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext7\",\"id\":\"7\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext8\",\"id\":\"8\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext9\",\"id\":\"9\",\"criticalityIndicator\":false}," +
                              "{\"name\":\"ext10\",\"id\":\"9\",\"criticalityIndicator\":false}" +
                              "]}";

        JsonMessage aReqJsonMessage = jsonMessageService.parse(originalJson);
        eftposMessageProcessor.processAReqOutgoing(aReqJsonMessage);

        Assertions.assertEquals(10, aReqJsonMessage.getMessageExtension().size());
        Assertions.assertEquals(EftposMessageProcessor.DS_AREQ_MSG_EXTENSION_ID, aReqJsonMessage.getMessageExtension().get(9).getId());
    }

    @Test
    public void processAResOutgoing_npa_withAuthenticationValue() throws Exception {
        String json = "{\"messageCategory\":\"02\",\"authenticationValue\":\"QZ1234\"}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"02\"}");
        eftposMessageProcessor.processAResOutgoing(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals("{\"messageCategory\":\"02\"}", aResJsonMessage.toString());
    }

    @Test
    public void processRReqOutgoing_npa_withAuthenticationValue() throws Exception {
        String json = "{\"messageCategory\":\"02\",\"authenticationValue\":\"QZ1234\"}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        rReqJsonMessage = eftposMessageProcessor.processRReqOutgoing(rReqJsonMessage);
        Assertions.assertEquals("{\"messageCategory\":\"02\"}", rReqJsonMessage.toString());
    }

    @DisplayName("validateAResIncoming [transStatus, eci, transStatusReason] : required transStatusReason and value existing")
    @ParameterizedTest
    @CsvSource({"N,07,03", "U,07,03", "R,07,03"})
    public void validateAResIncoming_transStatusReasonRequired_existingValue(String transStatus, String eci, String transStatusReason) throws Exception {
        String json = "{" +
                      "\"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"," +
                      "\"transStatusReason\":\""+transStatusReason+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"02\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateAResIncoming [transStatus, eci, transStatusReason] : required transStatusReason and value missing")
    @ParameterizedTest
    @CsvSource({"N,07,null", "U,07,null", "R,07,null",
                "N,07,\"\"", "U,07,\"\"", "R,07,\"\""})
    public void validateAResIncoming_transStatusReasonRequired_missingValue(String transStatus, String eci, String transStatusReason) throws Exception {
        String json = "{" +
                      "\"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"," +
                      "\"transStatusReason\":"+transStatusReason +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"02\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.transStatusReason, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cRequiredFieldMissing201, errors.get(0).getCode());
    }

    @DisplayName("validateAResIncoming [transStatus, eci, transStatusReason] : optional transStatusReason and null/empty value")
    @ParameterizedTest
    @CsvSource({"Y,05,null", "A,06,null", "C,07,null",
                "Y,05,\"\"", "A,06,\"\"", "C,07,\"\""})
    public void validateAResIncoming_transStatusReasonOptional_emptyOrNull(String transStatus, String eci, String transStatusReason) throws Exception {
        String json = "{" +
                      "\"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"," +
                      "\"transStatusReason\":"+transStatusReason +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"02\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.transStatusReason, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @DisplayName("validateAResIncoming [acsOperatorID] : optional acsOperatorID and null/empty value")
    @ParameterizedTest
    @CsvSource({"\"\"", "null"})
    public void validateAResIncoming_AcsOperatorID_emptyOrNull(String acsOperatorID) throws Exception {
        String json = "{" +
            "\"acsOperatorID\":"+acsOperatorID +
            "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"02\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.acsOperatorID, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cRequiredFieldMissing201, errors.get(0).getCode());
    }

    @DisplayName("validateAReqIncoming [acsOperatorID] : correct acsOperatorID")
    @ParameterizedTest
    @CsvSource({"\"VALIDTOR\""})
    public void validateAResIncoming_AcsOperatorID_correct(String acsOperatorID) throws Exception {
        String json = "{" +
            "\"acsOperatorID\":"+acsOperatorID +
            "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"02\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);
        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateAReqIncoming [purchaseCurrency] : correct purchase currency")
    @ParameterizedTest
    @ValueSource(strings = {"036", ""})
    public void validateAReqIncoming_validatePurchaseCurrencyCorrect(String purchaseCurrency) throws Exception {
        JsonMessage aReqJsonMessage = JsonMessageBuilder.newBuilder()
                .add(FieldName.purchaseCurrency, purchaseCurrency)
                .build();

        List<Error> errors = eftposMessageProcessor.validateAReqIncoming(aReqJsonMessage);
        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateAReqIncoming [purchaseCurrency] : incorrect purchase currency")
    @ParameterizedTest
    @ValueSource(strings = {"031", "049", "425", "039"})
    public void validateAReqIncoming_validatePurchaseCurrencyIncorrect(String purchaseCurrency) throws Exception {
        JsonMessage aReqJsonMessage = JsonMessageBuilder.newBuilder()
                .add(FieldName.purchaseCurrency, purchaseCurrency)
                .build();

        List<Error> errors = eftposMessageProcessor.validateAReqIncoming(aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.purchaseCurrency, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @Test
    public void validateAReqIncoming_validateExceededMessageExtension() throws Exception {
        List<MessageExtension> messageExtensions = new ArrayList<>(10);
        for(int i = 0; i < 10; i++) {
            MessageExtension messageExtension = new MessageExtension();
            messageExtension.setCriticalityIndicator(false);
            messageExtension.setName("data" + i);
            messageExtension.setId("id" + i);
            messageExtensions.add(messageExtension);
        }
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{}");
        aReqJsonMessage.setMessageExtension(messageExtensions);

        List<Error> errors = eftposMessageProcessor.validateAReqIncoming(aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.messageExtension, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @Test
    public void validateAReqIncoming_validateExactLimitButWithDuplicateMessageExtension() throws Exception {
        List<MessageExtension> messageExtensions = new ArrayList<>(10);
        for(int i = 0; i < 9; i++) {
            MessageExtension messageExtension = new MessageExtension();
            messageExtension.setCriticalityIndicator(false);
            messageExtension.setName("data" + i);
            messageExtension.setId("id" + i);
            messageExtensions.add(messageExtension);
        }
        MessageExtension duplicateExtension = new MessageExtension();
        duplicateExtension.setId(EftposMessageProcessor.DS_AREQ_MSG_EXTENSION_ID);
        messageExtensions.add(duplicateExtension);

        JsonMessage aReqJsonMessage = jsonMessageService.parse("{}");
        aReqJsonMessage.setMessageExtension(messageExtensions);

        List<Error> errors = eftposMessageProcessor.validateAReqIncoming(aReqJsonMessage);

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateRReqIncoming [transStatus, eci, transStatusReason] : required transStatusReason and value existing")
    @ParameterizedTest
    @CsvSource({"N,07,03", "U,07,03", "R,07,03"})
    public void validateRReqIncoming_transStatusReasonRequired_existingValue(String transStatus, String eci, String transStatusReason) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\"02\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"," +
                      "\"transStatusReason\":\""+transStatusReason+"\"" +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateRReqIncoming [transStatus, eci, transStatusReason] : optional transStatusReason and null/empty value")
    @ParameterizedTest
    @CsvSource({"Y,05,null", "A,06,null",
                "Y,05,\"\"", "A,06,\"\""})
    public void validateRReqIncoming_transStatusReasonOptional_emptyOrNull(String transStatus, String eci, String transStatusReason) throws Exception {
        String json = "{" +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"," +
                      "\"transStatusReason\":"+transStatusReason +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.transStatusReason, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @DisplayName("validateAResIncoming [messageCategory, transStatus, eci] : optional ECI and valid value")
    @ParameterizedTest
    @CsvSource({"01,C,05", "01,C,06", "01,C,07",
                "02,C,05", "02,C,06", "02,C,07"})
    public void validateAResIncoming_eciOptional_validValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateAResIncoming [messageCategory, transStatus, eci] : optional ECI but invalid value")
    @ParameterizedTest
    @CsvSource({"01,C,01", "01,C,02", "01,C,03",
                "02,C,01", "02,C,02", "02,C,03"})
    public void validateAResIncoming_eciOptional_invalidValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.eci, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @DisplayName("validateAReqIncoming [threeDSServerOperatorID] : incorrect threeDSServerOperatorID")
    @ParameterizedTest
    @CsvSource({"''"})
    public void validateAReqIncoming_validateThreeDSServerOperatorIdIncorrect(String threeDSServerOperatorID) throws Exception {
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{}");
        aReqJsonMessage.setDeviceChannel(TDSModel.DeviceChannel.cChannelBrow02.value());
        aReqJsonMessage.setThreeDSServerOperatorID(threeDSServerOperatorID);
        aReqJsonMessage.setMessageCategory(TDSModel.XMessageCategory.C02.value());
        aReqJsonMessage.setCardExpiryDate("9801");
        List<Error> errors = eftposMessageProcessor.validateAReqIncoming(aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.threeDSServerOperatorID, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cRequiredFieldMissing201, errors.get(0).getCode());
    }

    @DisplayName("validateAResIncoming [messageCategory, transStatus, eci] : optional ECI but null/empty value")
    @ParameterizedTest
    @CsvSource({"01,C,''", "01,C,''", "01,C,''",
                "02,C,''", "02,C,''", "02,C,''",
                "01,C,", "01,C,", "01,C,",
                "02,C,", "02,C,", "02,C,"})
    public void validateAResIncoming_eciOptional_nullOrEmptyValue(String messageCategory, String transStatus, String eci) throws Exception {
        JsonMessage aResJsonMessage = jsonMessageService.parse("{}");
        aResJsonMessage.setTransStatus(transStatus);
        aResJsonMessage.setEci(eci);
        aResJsonMessage.setAcsOperatorID("ACS-EMULATOR-OPER-ID");

        JsonMessage aReqJsonMessage = jsonMessageService.parse("{}");
        aReqJsonMessage.setMessageCategory(messageCategory);
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateAResIncoming [deviceChannel, transStatus] : required transStatus but null/empty value")
    @ParameterizedTest
    @CsvSource({"01,\"\"", "02,\"\"", "03,\"\"",
                "01,null", "02,null", "03,null"})
    public void validateAResIncoming_requiredTransStatus_nullOrEmptyValue(String deviceChannel, String transStatus) throws Exception {
        JsonMessage aResJsonMessage = jsonMessageService.parse("{\"transStatus\":"+transStatus+"}");
        aResJsonMessage.setEci("07");
        aResJsonMessage.setAcsOperatorID("ACS-EMULATOR-OPER-ID");

        JsonMessage aReqJsonMessage = jsonMessageService.parse("{}");
        aReqJsonMessage.setMessageCategory("02");
        aReqJsonMessage.setDeviceChannel(deviceChannel);
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.transStatus, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cRequiredFieldMissing201, errors.get(0).getCode());
    }

    @DisplayName("validateAResIncoming [messageCategory, transStatus, eci] : required ECI and valid value")
    @ParameterizedTest
    @CsvSource({"01,Y,05", "01,N,07", "01,U,07", "01,R,07", "01,A,06",
                "02,Y,05", "02,N,07", "02,U,07", "02,R,07", "02,A,06"})
    public void validateAResIncoming_eciRequired_validValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                    "  \"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"transStatusReason\":\"03\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateAResIncoming [messageCategory, transStatus, eci] : required ECI but invalid value")
    @ParameterizedTest
    @CsvSource({"01,Y,01", "01,N,02", "01,U,03", "01,R,05", "01,A,07",
                "02,Y,01", "02,N,02", "02,U,03", "02,R,05", "02,A,07"})
    public void validateAResIncoming_eciRequired_invalidValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"transStatusReason\":\"03\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.eci, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @DisplayName("validateAResIncoming [messageCategory, transStatus, eci] : required ECI but null/empty value")
    @ParameterizedTest
    @CsvSource({"01,Y,\"\"", "01,N,\"\"", "01,U,\"\"", "01,R,\"\"", "01,A,\"\"",
                "02,Y,null", "02,N,null", "02,U,null", "02,R,null", "02,A,null"})
    public void validateAResIncoming_eciRequired_nullOrEmptyValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\"," +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"transStatusReason\":\"03\"," +
                      "\"eci\":"+eci +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        List<Error> errors = eftposMessageProcessor.validateAResIncoming(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.eci, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cRequiredFieldMissing201, errors.get(0).getCode());
    }


    @DisplayName("validateRReqIncoming [deviceChannel, transStatus] : required transStatus but null/empty value")
    @ParameterizedTest
    @CsvSource({"01,\"\"", "02,\"\"", "03,\"\"",
                "01,null", "02,null", "03,null"})
    public void validateRReqIncoming_requiredTransStatus_nullOrEmptyValue(String deviceChannel, String transStatus) throws Exception {
        JsonMessage rReqJsonMessage = jsonMessageService.parse("{\"transStatus\":"+transStatus+"}");
        rReqJsonMessage.setEci("07");
        TDSRecord record = new TDSRecord();
        record.setMessageCategory("02");
        record.setDeviceChannel(deviceChannel);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, record);

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.transStatus, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cRequiredFieldMissing201, errors.get(0).getCode());
    }

    @DisplayName("validateRReqIncoming [messageCategory, transStatus, eci] : optional ECI and valid value")
    @ParameterizedTest
    @CsvSource({"01,C,05", "01,C,06", "01,C,07",
                "02,C,05", "02,C,06", "02,C,07"})
    public void validateRReqIncoming_eciOptional_validValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateRReqIncoming [messageCategory, transStatus, eci] : optional ECI but invalid value")
    @ParameterizedTest
    @CsvSource({"01,C,01", "01,C,02", "01,C,03",
                "02,C,01", "02,C,02", "02,C,03"})
    public void validateRReqIncoming_eciOptional_invalidValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.eci, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @DisplayName("validateRReqIncoming [messageCategory, transStatus, eci] : optional ECI but null/empty value")
    @ParameterizedTest
    @CsvSource({"01,C,''", "01,C,''", "01,C,''",
                "02,C,''", "02,C,''", "02,C,''",
                "01,C,", "01,C,", "01,C,",
                "02,C,", "02,C,", "02,C,"})
    public void validateRReqIncoming_eciOptional_nullOrEmptyValue(String messageCategory, String transStatus, String eci) throws Exception {
        JsonMessage rReqJsonMessage = jsonMessageService.parse("{}");
        rReqJsonMessage.setMessageCategory(messageCategory);
        rReqJsonMessage.setTransStatus(transStatus);
        rReqJsonMessage.setEci(eci);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateRReqIncoming [messageCategory, transStatus, eci] : required ECI and valid value")
    @ParameterizedTest
    @CsvSource({"01,Y,05", "01,N,07", "01,U,07", "01,R,07", "01,A,06",
                "02,Y,05", "02,N,07", "02,U,07", "02,R,07", "02,A,06"})
    public void validateRReqIncoming_eciRequired_validValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"transStatusReason\":\"03\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(0, errors.size());
    }

    @DisplayName("validateRReqIncoming [messageCategory, transStatus, eci] : required ECI but invalid value")
    @ParameterizedTest
    @CsvSource({"01,Y,01", "01,N,02", "01,U,03", "01,R,05", "01,A,07",
                "02,Y,01", "02,N,02", "02,U,03", "02,R,05", "02,A,07"})
    public void validateRReqIncoming_eciRequired_invalidValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"transStatusReason\":\"03\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.eci, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cInvalidFieldFormat203, errors.get(0).getCode());
    }

    @DisplayName("validateRReqIncoming [messageCategory, transStatus, eci] : required ECI but null/empty value")
    @ParameterizedTest
    @CsvSource({"01,Y,\"\"", "01,N,\"\"", "01,U,\"\"", "01,R,\"\"", "01,A,\"\"",
                "02,Y,null", "02,N,null", "02,U,null", "02,R,null", "02,A,null"})
    public void validateRReqIncoming_eciRequired_nullOrEmptyValue(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"transStatusReason\":\"03\"," +
                      "\"eci\":"+eci +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        List<Error> errors = eftposMessageProcessor.validateRReqIncoming(rReqJsonMessage, new TDSRecord());

        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(FieldName.eci, errors.get(0).getFieldName());
        Assertions.assertEquals(TDSModel.ErrorCode.cRequiredFieldMissing201, errors.get(0).getCode());
    }

    @DisplayName("processAResOutgoing [messageCategory, transStatus, eci] : required ECI")
    @ParameterizedTest
    @CsvSource({"01,Y,05", "01,N,07", "01,U,07", "01,R,07", "01,A,06",
                "02,Y,05", "02,N,07", "02,U,07", "02,R,07", "02,A,06"})
    public void processAResOutgoing_eciRequired(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        aResJsonMessage = eftposMessageProcessor.processAResOutgoing(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(eci, aResJsonMessage.getEci());
    }

    @DisplayName("processAResOutgoing [messageCategory, transStatus, eci] : required ECI")
    @ParameterizedTest
    @CsvSource({"01,Y,05", "01,N,07", "01,U,07", "01,R,07", "01,A,06",
                "02,Y,05", "02,N,07", "02,U,07", "02,R,07", "02,A,06"})
    public void processAResOutgoing_eciRequired_emptyEci(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        aResJsonMessage = eftposMessageProcessor.processAResOutgoing(aResJsonMessage, aReqJsonMessage);

        Assertions.assertEquals(eci, aResJsonMessage.getEci());
    }

    @DisplayName("processAResOutgoing [messageCategory, transStatus, eci] : optional ECI")
    @ParameterizedTest()
    @CsvSource({"01,C,05", "01,C,06", "01,C,07",
                "02,C,05", "02,C,06", "02,C,07",})
    public void processAResOutgoing_eciOptional(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage aResJsonMessage = jsonMessageService.parse(json);
        JsonMessage aReqJsonMessage = jsonMessageService.parse("{\"messageCategory\":\"" + messageCategory + "\"}");
        aResJsonMessage = eftposMessageProcessor.processAResOutgoing(aResJsonMessage, aReqJsonMessage);

        Assertions.assertFalse(jsonMessageService.isFieldPresent(aResJsonMessage, FieldName.eci));
    }

    @DisplayName("processRReqOutgoing [messageCategory, transStatus, eci] : required ECI")
    @ParameterizedTest
    @CsvSource({"01,Y,05", "01,N,07", "01,U,07", "01,R,07", "01,A,06",
                "02,Y,05", "02,N,07", "02,U,07", "02,R,07", "02,A,06"})
    public void processRReqOutgoing_eciRequired(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        rReqJsonMessage = eftposMessageProcessor.processRReqOutgoing(rReqJsonMessage);

        Assertions.assertEquals(eci, rReqJsonMessage.getEci());
    }

    @DisplayName("processRReqOutgoing [messageCategory, transStatus, eci] : optional ECI")
    @ParameterizedTest()
    @CsvSource({"01,C,05", "01,C,06", "01,C,07",
                "02,C,05", "02,C,06", "02,C,07",})
    public void processRReqOutgoing_eciOptional(String messageCategory, String transStatus, String eci) throws Exception {
        String json = "{" +
                      "\"messageCategory\":\""+messageCategory+"\"," +
                      "\"transStatus\":\""+transStatus+"\"," +
                      "\"eci\":\""+eci+"\"" +
                      "}";
        JsonMessage rReqJsonMessage = jsonMessageService.parse(json);
        rReqJsonMessage = eftposMessageProcessor.processRReqOutgoing(rReqJsonMessage);

        Assertions.assertFalse(jsonMessageService.isFieldPresent(rReqJsonMessage, FieldName.eci));
    }
}
