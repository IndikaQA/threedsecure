package com.modirum.ds.services.elo.papi.test.support;

import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.connection.RiderDataSource;
import com.github.database.rider.junit5.DBUnitExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Wrapper annotation that enables DBUnit DataSet features with default config.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ExtendWith(DBUnitExtension.class)
@DBUnit(cacheConnection = false, expectedDbType = RiderDataSource.DBType.H2,
        user = "sa",
        url = "jdbc:h2:mem:test;SCHEMA=PUBLIC;MODE=MySQL",
        password = "password",
        schema = "PUBLIC"
)
public @interface RunWithDB {
}
