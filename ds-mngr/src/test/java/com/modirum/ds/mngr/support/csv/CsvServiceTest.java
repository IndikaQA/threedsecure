package com.modirum.ds.mngr.support.csv;

import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.utils.Misc;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CsvServiceTest {
    private final CsvService csvService = new CsvService();

    @Test
    public void writeEmptyRecord() throws Exception {
        StringWriter stringWriter = new StringWriter();
        List<TDSRecord> tdsRecords = new ArrayList<>();

        csvService.writeCsv(new CsvParams<TDSRecord>()
                                 .setWriter(stringWriter)
                                 .setHeaders(new String[]{"MESSAGE_CATEGORY", "DS_TRANSACTION_ID"})
                                 .setRecords(tdsRecords)
                                 .setCsvMapper((item) -> new Object[]{
                                         item.getMessageCategory(),
                                         item.getDSTransID()
                                 }).setDelimiter(','));

        String csvContent = stringWriter.toString();
        assertTrue(Misc.isNotNullOrEmpty(csvContent));
        assertEquals("MESSAGE_CATEGORY,DS_TRANSACTION_ID\r\n", csvContent);
    }

    @Test
    public void writeSingleRecord() throws Exception {
        List<TDSRecord> tdsRecordList = new ArrayList<>();
        TDSRecord tdsRecord = new TDSRecord();
        tdsRecord.setAcctNumber("400000xxxxx0000");
        tdsRecord.setDSTransID("DsTransID");
        tdsRecordList.add(tdsRecord);

        StringWriter stringWriter = new StringWriter();
        csvService.writeCsv(new CsvParams<TDSRecord>()
                         .setWriter(stringWriter)
                         .setHeaders(new String[]{"ACCOUNT_NUMBER", "DS_TRANSACTION_ID"})
                         .setRecords(tdsRecordList)
                         .setCsvMapper((item) -> new Object[]{
                                 item.getAcctNumber(),
                                 item.getDSTransID()
                         }).setDelimiter(','));

        String csvContent = stringWriter.toString();
        assertTrue(Misc.isNotNullOrEmpty(csvContent));
        assertEquals("ACCOUNT_NUMBER,DS_TRANSACTION_ID\r\n"
                            + "400000xxxxx0000,DsTransID\r\n", csvContent);
    }

    @Test
    public void writeTwoRecordsWithNullColumn() throws Exception {
        List<TDSRecord> tdsRecordList = new ArrayList<>();
        TDSRecord tdsRecord = new TDSRecord();
        tdsRecord.setDSTransID("DsTransID");
        tdsRecordList.add(tdsRecord);
        TDSRecord anotherTdsRecord = new TDSRecord();
        anotherTdsRecord.setAcctNumber("400000xxxx0000");
        anotherTdsRecord.setDSTransID("DsTransID2");
        tdsRecordList.add(anotherTdsRecord);

        StringWriter stringWriter = new StringWriter();
        csvService.writeCsv(new CsvParams<TDSRecord>()
                                    .setWriter(stringWriter)
                                    .setHeaders(new String[]{"ACCOUNT_NUMBER", "DS_TRANSACTION_ID"})
                                    .setRecords(tdsRecordList)
                                    .setCsvMapper((item) -> new Object[]{
                                            item.getAcctNumber(),
                                            item.getDSTransID()
                                    }).setDelimiter(','));

        String csvContent = stringWriter.toString();
        assertTrue(Misc.isNotNullOrEmpty(csvContent));
        assertEquals("ACCOUNT_NUMBER,DS_TRANSACTION_ID\r\n"
                            + ",DsTransID\r\n"
                            + "400000xxxx0000,DsTransID2\r\n", csvContent);
    }
}
