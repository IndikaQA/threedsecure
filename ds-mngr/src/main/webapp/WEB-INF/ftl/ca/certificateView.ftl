<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="certificateList.html?x=${(.now?time)?html}"><@global.text key='text.certificates'/></a> &gt;
        <#if certificate?exists><span class="selected"><@global.text key='text.details'/>
            : ${(certificate.getName()!"")?html}</span></#if></h1>
    <h2><@global.text key='text.certificate'/></h2>
    <#if certificate?exists>
        <#if msg?exists>
            <span class="msg">${msg?html}</span><br/>
        </#if>
        <#assign col1=110/>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <@global.security.authorize access="hasAnyRole('caEdit')">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <form action="certificateRevoke.html" method="post">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="id" value="${(certificate.getId()!"")?html}"/>
                                        <input type="submit" name="button" value="<@global.text key='button.revoke'/>"
                                               <#if certificate.status=='R'>disabled</#if>/>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </@global.security.authorize>
                </td>
            </tr>
            <tr>
                <td width="${col1}" class="tdHeaderVertical">Id:</td>
                <td>${(certificate.id!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.name'/>:</td>
                <td>${(certificate.name!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.subjectDN'/>:</td>
                <td>${(certificate.subjectDN!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuerDN'/>:</td>
                <td>${(certificate.issuerDN!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.validity'/>:</td>
                <td><#if certificate.issued??>${certificate.issued?string(dateFormat+" HH:mm:ss")}</#if> -
                    <#if certificate.expires??>
                        <span <<#if certificate.expires<.now> style="color: RED;" </#if>>${certificate.expires?string(dateFormat+" HH:mm:ss")}</span></#if>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.x509data'/>:</td>
                <td>
                    <pre>${(certificate.x509data!'')?html}</pre>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.status'/>:</td>
                <td><@global.objstatus certificateStatuses certificate.status!""/></td>
            </tr>

            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.cert.acscount'/>:</td>
                <td class="bottom">${(ACSCount!"")?html}</a> &nbsp;</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.cert.tdsservercount'/>:</td>
                <td class="bottom"><a
                            href="../acquirers/tdsServerList.html?inClientCert=${(certificate.getId()!"")?html}&search=y">${(MICount!"")?html}</a>
                    &nbsp;
                </td>
            </tr>
        </table>
        <form id="dummy" action="" method="post">
            <input type="button" value="<@global.text key='button.downloadCert'/>"
                   onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}'; return false;"/>
            <input type="button" value="<@global.text key='button.downloadchain'/>"
                   onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}&chain=true'; return false;"/>
            <input type="button" value="<@global.text key='button.downloadchainp7'/>"
                   onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}&chain=true&p7=true'; return false;"/>
        </form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
