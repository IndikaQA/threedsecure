<#import "../common/global.ftl" as global/>
<@global.page>

    <script type="text/javascript" src="../js/jquery-3.4.1.min.js">
    </script>

    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <span class="selected"><@global.text key='text.reports'/></span>
    </h1>

    <h2><@global.text key='text.reports'/></h2>

    <br/>

    <div>
    <#list categorizedReportsTypes as reportCategory, reportsTypes>
        <#assign escapedReportCategory = reportCategory?replace("/" ,"") >
        <#assign escapedReportCategory = escapedReportCategory?replace(" " ,"") >

        <span id="${escapedReportCategory}-on" style="position:absolute; right:60px;">
            <a href="#" onclick="toggleReports('${escapedReportCategory}')"><@global.text key="text.report.list.show.reports"/></a>
        </span>
        <span id="${escapedReportCategory}-off" style="position:absolute; right:60px;" hidden="hidden">
            <a href="#" onclick="toggleReports('${escapedReportCategory}')"><@global.text key="text.report.list.hide.reports"/></a>
        </span>
        <h3>${reportCategory} </h3>

        <div id="${escapedReportCategory}" hidden="hidden">
            <#list reportsTypes as reportType>
                <span style="padding-left:12px;">
                    <a href="reportsEdit.html?type=${reportType.label}" name="a-edit-report-${reportType.label}" id="a-edit-report-${reportType.label}">
                        <@global.text key="${reportType.description}"/>
                    </a>
                </span>
                <br/>
            </#list>
        </div>
    </#list>
    </div>

    <script type="text/javascript">
        var reportTableStart = 0;

        function updateReportResults() {
            return $.get("reportsTable.html", { start: reportTableStart, limit: ${searcher.limit}},
                        function(data, textStatus, jqXHR) {
                            $("#pageData").html(data);
                        },
                        "html");
        }

    <#if autoRefresh??>
        var reportsRefreshTimeout = ${autoRefresh} * 1000;
        var reportsRefreshCycle = 0;

        function pollReportsResults() {
            var xhr = updateReportResults();
            xhr.always(function() {
                reportsRefreshCycle = (reportsRefreshCycle + 1) % 5;
                setTimeout(pollReportsResults, (reportsRefreshTimeout + (reportsRefreshTimeout * .5 * reportsRefreshCycle)));
            });
        }

        setTimeout(pollReportsResults, reportsRefreshTimeout);
    </#if>
    </script>

    <form id="pageForm" action="reportsList.html" method="post">
        <br/>
        <br/>
        <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
        <@global.spring.formSingleSelect path="searcher.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
        <input type="submit" name="submitbtn" value="Search"/>
        <div id="pageData" class="pageVisible">
        <#include "reportsTable.ftl"/>
        </div>
     </form>

    <script type="text/javascript">
        function toggleReports(category) {
            var categoryDiv = document.getElementById(category);
            var categoryOnDiv = document.getElementById(category + '-on');
            var categoryOffDiv = document.getElementById(category + '-off');
            if (categoryDiv.hidden) {
                categoryDiv.hidden = false;
                categoryOnDiv.hidden = true;
                categoryOffDiv.hidden = false;
            } else {
                categoryDiv.hidden = true;
                categoryOnDiv.hidden = false;
                categoryOffDiv.hidden = true;
            }
        }
    </script>

</@global.page>