<#import "../common/global.ftl" as global/>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<#if searcher.total &gt; 0 >
        <script type="text/javascript">
            reportTableStart = ${searcher.start};
        </script>
        <h2><@global.text key='text.found.total'/> ${searcher.total!""?html} <@global.text key='text.reports'/>
            , <@global.text key='text.showing'/> ${(searcher.start+1)!""?html} <@global.text key='text.to'/>
            <#if searcher.total &gt; (searcher.start+searcher.limit)>
                ${(searcher.start+searcher.limit)!""?html}
            <#else>
                ${(searcher.total)!""?html}
            </#if>
        </h2>
        <#assign colspan = "3"/>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="80" class="tdHeader"><@global.text key='text.status'/></td>
                <td width="80" class="tdHeader"><@global.text key='text.delete'/></td>
                <td width="150" class="tdHeader"><@global.text key='text.report.name'/></td>
                <td width="150" class="tdHeader"><@global.text key='general.paymentsystem'/></td>
                <td width="80" class="tdHeader"><@global.text key='text.report.from'/></td>
                <td width="80" class="tdHeader"><@global.text key='text.report.to'/></td>
                <td width="80" class="tdHeader"><@global.text key='text.report.created'/></td>
                <td width="80" class="tdHeader"><@global.text key='text.user'/></td>
            </tr>
            <#list found as report>
            <#assign reportStatus = (loca.getText((reportStatuses[report.status].description)!"", locale)!report.status)?html>
            <#assign reportName = (loca.getText((reportTypes[report.name].description)!"", locale)!report.name)?html>
            <#assign paymentSystemName = ((paymentSystemSearchMap[report.paymentSystemId?string])!report.paymentSystemId?string)?html>
            <tr <#if report_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                <td>
                    <#if report.filename?? && report.status?lower_case == 'c'>
                        <a href="reportsDownload.html?id=${report.id}" name="a-download-report-${report.id}" id="a-download-report-${report.id}">
                            ${reportStatus}
                        </a>
                    <#else>
                        ${reportStatus}
                    </#if>
                </td>
                <td><input type="submit" name="submit-delete-report-${report.id}" id="submit-delete-report-${report.id}" value="<@global.text "text.delete"/>"
                onclick="if (confirm('<@global.text key="text.report.confirm.delete"/>')) {
                        $.get('reportsDelete.html?id=${report.id}', function(data) {
                            updateReportResults();
                        });
                    }
                    return false;"/></td>
                <td>${reportName}</td>
                <td>${paymentSystemName}</td>
                <td>${((report.dateFrom?datetime("yyyy-MM-dd HH:mm:ss")?string(dateFormat+" HH:mm:ss"))!"")?html}</td>
                <td>${((report.dateTo?datetime("yyyy-MM-dd HH:mm:ss")?string(dateFormat+" HH:mm:ss"))!"")?html}</td>
                <td>${(report.createdDate?string(dateFormat+" HH:mm:ss")!"")?html}</td>
                <td>${report.createdUser!""?html}</td>
            </tr>
            </#list>
            <@global.pagingButtons searcher=searcher colspan=6/>
        </table>
    <#else>
            <script type="text/javascript">
                reportTableStart = 0;
            </script>
        <br/>
        <@global.text key='text.no.matches.found'/>
    </#if>