<#--General page macro - single page structure for screens -->
<#if locale??><#setting locale="${locale?string}"></#if>
<#if timeZone??> <#setting time_zone="${timeZone.getID()}"></#if>
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#import "spring.ftl" as spring/>
<#assign xhtmlCompliant = true in spring>
<#assign ctxPath = rc.getContextPath() >
<#macro page hideLogout=false>
    <!doctype html>
    <html lang="<#if locale??>${locale.language}<#else>en</#if>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <#--
            <!-- Change Typekit settings here!
            <script src="//use.typekit.net/fur5tqn.js"></script>
            <script>try{Typekit.load();}catch(e){}</script>
            -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>
            <#if processor?exists && processor.name?exists >${(processor.name!"")?html}</#if>
            <#if appNameInTitle?exists> : ${appNameInTitle?html}<#else> : ${(appName!"Modirum DS Manager")?html}</#if>
        </title>
        <link rel="stylesheet" href="${ctxPath?html}/css/normalize.css">
        <link rel="stylesheet" href="${ctxPath?html}/css/tooltip.css">
        <#if processor?? && processor.getStyleSheet()?? && processor.getStyleSheet()?length &gt; 0>
            <link rel="stylesheet" type="text/css" href="${processor.getStyleSheet()?html}"/>
        <#else>
            <link rel="stylesheet" type="text/css" href="${ctxPath?html}/css/main.css">
        </#if>
        <script type="text/javascript">
            var MDM = new Object();
        </script>
        <#if userName??><!-- dont expose if not logged in -->
        <script type="text/javascript" src="${ctxPath?html}/js/scripts.js">
            // need for browsers...
        </script>
        </#if>
    </head>
    <body <#if loginPage?exists && loginPage=="true">id="page-login"</#if> >
    <#if loginPage?exists && loginPage=="true">
        <#nested />
    <#else>
        <div id="main">
            <div id="menu">
                <div id="clientlogo">
                    <#if processor?exists && processor.getLogoImage()?exists >
                        <img src="${processor.getLogoImage()?html}" alt="Scheme logo"
                             id="clientlogoImg"/>
                    <#else>
                        <img src="${ctxPath?html}/img/scheme-logo.gif" alt="Processor logo" width="144" height="35"
                             id="clientlogoImg"/>
                    </#if>
                </div>
                <div id="merchant-name">
                    <div style="line-height: 1.2em;">
                        <#if userName??><@global.text key="text.loggedinas"/>: ${userName?html}</#if>
                        <br/>
                        <@selTimeZone />
                    </div>
                </div>
                <div id="menu-content">
                    <#if hideLogout?? && hideLogout==true>
                    <#else>
                        <ul id="menu-first-items">
                            <li><a href="${ctxPath?html}/logout"><@global.text key="text.logout"/></a></li>
                            <li <@selmenu  "${ctxPath?html}/passwordChange/view.html"/>><a
                                        href="${ctxPath?html}/passwordChange/view.html?x=${(.now?time)?html}"><@global.text key="text.change.password"/></a>
                            </li>
                        </ul>
                        <ul id="menu-home-items">
                            <li <@selmenu  "${ctxPath?html}/main/main.html"/>><a
                                        href="${ctxPath?html}/main/main.html?x=${(.now?time)?html}"><@global.text key="text.home"/></a>
                            </li>
                        </ul>
                    </#if>
                    <ul id="menu-main-items">
                        <@global.security.authorize access="hasRole('recordsView')">
                            <li <@selmenu  "${ctxPath?html}/tdsrecords/tdsrecords.html"/>><a
                                        href="${ctxPath?html}/tdsrecords/tdsrecords.html?x=${(.now?time)?html}"><img
                                            src="${ctxPath?html}/img/authrecords.svg"/><@global.text key="text.authrecords"/>
                                </a></li>
                        </@global.security.authorize>
                    </ul>
                    <ul id="menu-secondary-items">
                        <@global.security.authorize access="hasAnyRole('paymentsystemView', 'paymentsystemEdit', 'adminSetup', 'acquirerView', 'issuerView', 'userView', 'auditLogView', 'caView', 'caEdit')">
                            <li><@global.text key="text.administration"/></li>
                            <ul class="menu-submenu">

                                <@global.security.authorize access="hasAnyRole('paymentsystemView', 'paymentsystemEdit', 'adminSetup')">
                                    <li <@selmenu  "${ctxPath?html}/paymentsystems/paymentsystemsList"/>>
                                        <a href="${ctxPath?html}/paymentsystems/paymentsystemsList.html?x=${(.now?time)?html}"><@global.text key="text.paymentsystems"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('issuerView')">
                                    <li <@selmenu  "${ctxPath?html}/issuers/issuerList"/>>
                                        <a href="${ctxPath?html}/issuers/issuerList.html?x=${(.now?time)?html}"><@global.text key="text.issuers"/></a>
                                    </li>
                                    <#if attemptsACSEnabled?? && attemptsACSEnabled>
                                        <li <@selmenu "${ctxPath?html}/issuers/attemptsACSList"/>>
                                            <a href="${ctxPath?html}/issuers/attemptsACSList.html"><@global.text key="text.attempts.acs"/></a>
                                        </li>
                                    </#if>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('acquirerView')">
                                    <li <@selmenu  "${ctxPath?html}/acquirers/acquirerList"/>><a
                                                href="${ctxPath?html}/acquirers/acquirerList.html?x=${(.now?time)?html}"><@global.text key="text.acquirers"/></a>
                                    </li>
                                    <li <@selmenu  "${ctxPath?html}/acquirers/tdsServerList"/>><a style="margin-left: 8px;"
                                                                                           href="${ctxPath?html}/acquirers/tdsServerList.html?x=${(.now?time)?html}"><@global.text key="text.tdsserverprofiles"/></a>
                                    </li>
                                    <li <@selmenu  "${ctxPath?html}/acquirers/merchantList"/>><a
                                                style="margin-left: 8px;"
                                                href="${ctxPath?html}/acquirers/merchantList.html?x=${(.now?time)?html}"><@global.text key="text.merchants"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('userView')">
                                    <li <@selmenu  "${ctxPath?html}/users/user"/>><a
                                                href="${ctxPath?html}/users/usersList.html?x=${(.now?time)?html}"><@global.text key="text.users"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('caView')">
                                    <li <@selmenu  "${ctxPath?html}/ca/certificateList"/>><a
                                                href="${ctxPath?html}/ca/certificateList.html?x=${(.now?time)?html}"><@global.text key="text.certificates"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('caEdit')">
                                    <li <@selmenu  "${ctxPath?html}/ca/signCSR"/>><a
                                                href="${ctxPath?html}/ca/signCSR.html?x=${(.now?time)?html}"><@global.text key="text.menu.sign.csr"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('caEdit')">
                                    <li <@selmenu  "${ctxPath?html}/ca/uploadCertificate"/>><a
                                                href="${ctxPath?html}/ca/uploadCertificate.html?x=${(.now?time)?html}"><@global.text key="text.menu.upload.cert"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('auditLogView')">
                                    <li <@selmenu  "${ctxPath?html}/auditlogs/auditlogs.html"/>><a
                                                href="${ctxPath?html}/auditlogs/auditlogs.html"><@global.text key="text.auditlogs"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('reports')">
                                    <li <@selmenu  "${ctxPath?html}/reports/reportsList.html"/>><a
                                                href="${ctxPath?html}/reports/reportsList.html?x=${(.now?time)?html}"><@global.text key="text.reports"/></a>
                                    </li>
                                </@global.security.authorize>

                                <#if isSuperuser?? && isSuperuser>
                                    <@global.security.authorize access="hasAnyRole('recordsView')">
                                        <li <@selmenu  "${ctxPath?html}/tdsrecords/metrics.html"/>><a
                                                    href="${ctxPath?html}/tdsrecords/metrics.html?x=${(.now?time)?html}"><@global.text key="text.metrics"/></a>
                                        </li>
                                    </@global.security.authorize>
                                </#if>

                            </ul>
                        </@global.security.authorize>
                        <@global.security.authorize access="hasAnyRole('adminSetup','textView', 'userView')">
                            <li><@global.text key="text.ds.setup"/></li>
                            <ul class="menu-submenu">
                                <@global.security.authorize access="hasAnyRole('textView')">
                                    <li <@selmenu "${ctxPath?html}/localization/text"/>>
                                        <a href="${ctxPath?html}/localization/textsList.html?x=${(.now?time)?html}"><@global.text key="text.localization"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('adminSetup')">
                                    <li <@selmenu "${ctxPath?html}/settings/settingsList"/>>
                                        <a href="${ctxPath?html}/settings/settingsList.html?x=${(.now?time)?html}"><@global.text key="text.settings"/></a>
                                    </li>
                                    <#if settingsEnumEnabled?? && settingsEnumEnabled>
                                    <li <@selmenu "${ctxPath?html}/settings/settingsEnumList"/>>
                                        <a href="${ctxPath?html}/settings/settingsEnumList.html?x=${(.now?time)?html}"><@global.text key="text.settingsEnum"/></a>
                                    </li>
                                    </#if>
                                    <li <@selmenu "${ctxPath?html}/settings/keyInfo"/>>
                                        <a href="${ctxPath?html}/settings/keyInfo.html?x=${(.now?time)?html}"><@global.text key="text.keyinfo"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('userView')">
                                    <li <@selmenu "${ctxPath?html}/main/licenseinfo.html"/>>
                                        <a href="${ctxPath?html}/main/licenseinfo.html?x=${(.now?time)?html}"><@global.text key="text.licenseinfo"/></a>
                                    </li>
                                </@global.security.authorize>
                                <@global.security.authorize access="hasAnyRole('keyManage')">
                                    <li <@selmenu "${ctxPath?html}/keys/keySetupList.html"/>>
                                        <a href="${ctxPath?html}/keys/keySetupList.html?x=${(.now?time)?html}"><@global.text key="text.keyAdmin"/></a>
                                    </li>
                                </@global.security.authorize>
                            </ul
                        </@global.security.authorize>
                    </ul>
                </div>
            </div>
            <div id="content">
                <div id="content-header">
                    <div id="content-header-center">
                        <noscript>
						<span style="color: red;">
						Warning: Your browser does not support JavasScript or has Javascript disabled!<br/>
						This application requires JavaScript support for all functionality to run correctly!
						</span>
                        </noscript>
                        <div style="height: 80px; overflow-y: auto;">
                            <#if licenseWarning??><span
                                    style="white-space: pre-wrap; color: red;">${(licenseWarning!"")?html}</span>
                                <br/></#if>
                            <#if cryptoperiodWarning??><span
                                    style="white-space: pre-wrap; color: red;">${(cryptoperiodWarning!"")?html}</span></#if>
                            <#if certificateExpWarning??><span
                                    style="white-space: pre-wrap; color: red;">${(certificateExpWarning!"")?html}</span></#if>
                        </div>
                        &nbsp;
                    </div>
                    <div id="content-header-right">
                        <img src="${ctxPath?html}/img/modirum-logo.svg"/><br/>
                        <span style="font-size: 9pt;">
						<b>${(appName!"Modirum DS Manager")?html}</b>
						<#if loginPage?? && loginPage=="true">
                        <#else>
                            <br/>
							<div> <@global.text key="text.version"/> </div>
							<div style="margin-left: -100px;"> ${(appVersion!"1.0.0")?html} </div>
							<#if instanceId?? && instanceId &gt; 0>
                            <br/><@global.text key="text.instance.id"/> ${instanceId}
                        </#if>
                        </#if>
						</span>
                    </div>
                </div>
                <div id="content-main">
                    <div class="full-paragraph">
                        <#nested />
                    </div>
                </div>
                <div id="content-footer"><img src="${ctxPath?html}/img/powered-by-modirum.svg"/></div>
            </div>
        </div>
    </#if>
    <div id="calpopup" class="calpopup"></div>
    <script type="text/javascript">
        var inputElements = document.getElementsByTagName("input");
        for (i = 0; inputElements[i] != null && i < inputElements.length; i++) {
            inputElements[i].setAttribute("autocomplete", "off");
        }
    </script>
    </body>
    </html>
</#macro>
<#macro selmenu link>
    <#assign menuselected = ""/>
    <#if rc.getRequestUri()?contains("/main/main") && link?contains("/main/main")>
        <#assign menuselected = "class='menuselected'"/>
    </#if>
    <#if singleMerchantUser?? && singleMerchantUser=="true" &&
        rc.getRequestUri()?contains("/merchants/merchantView") && link?contains("/main/main")>
        <#assign menuselected = "class='menuselected'"/>
    </#if>
    <#assign linkend = link?substring(3) />
    <#if linkend?? && rc.getRequestUri()?contains(linkend)>
        <#assign menuselected = "class='menuselected'"/>
    </#if>
    ${menuselected?html}
</#macro>
<#macro text key>${(loca.getText(key!"", locale)!"")?html}</#macro>

<#macro created traceable>
    <tr>
        <td class="tdHeaderVertical"><@global.text key="text.created"/>:</td>
        <td>
            <#if traceable?? && traceable.createdDate??>
                ${traceable.createdDate?string(dateFormat+" HH:mm:ss")}
            </#if>
            <#if traceable?? && traceable.createdUser??>
                &nbsp; ${(traceable.createdUser!"")?html}
            </#if>
        </td>
    </tr>
</#macro>
<#macro lastModified traceable>
    <tr>
        <td class="tdHeaderVertical"><@global.text key="text.lastmodified"/>:</td>
        <td>
            <#if traceable?? && traceable.lastModifiedDate??>
                ${traceable.lastModifiedDate?string(dateFormat+" HH:mm:ss")}
            </#if>
            <#if traceable?? && traceable.lastModifiedUser??>
                &nbsp; ${(traceable.lastModifiedUser!"")?html}
            </#if>
        </td>
    </tr>
</#macro>
<#macro formInputSpecial path attributes="" fieldType="text">
    <#if htmlEscape?exists>
        <#assign status = springMacroRequestContext.getBindStatus(path, htmlEscape)>
    <#else>
        <#assign status = springMacroRequestContext.getBindStatus(path)>
    </#if>
<#-- assign a temporary value, forcing a string representation for any
kind of variable. This temp value is only used in this macro lib -->
    <#if status.value?exists && status.value?is_boolean>
        <#assign stringStatusValue=status.value?string>
    <#else>
        <#assign stringStatusValue=status.value?default("")>
    </#if>
    <input onkeydown="firstChange(this, event, '${(stringStatusValue!"")?js_string}');"
           onkeypress="firstChange(this, event, '${(stringStatusValue!"")?js_string}');" type="${fieldType}"
           id="${status.expression}" name="${status.expression}"
           value="${(stringStatusValue!"")?html}" ${attributes}<@spring.closeTag/>
    <@spring.bind path/>
</#macro>
<#macro pagingButtons searcher colspan=2>
    <tr>
        <td colspan="${colspan}">
            <input id="start" type="hidden" name="start" value="0"/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr style="border: none;">
                    <td align="left" width="150" style="border: none;">
                        <#if searcher.start &gt; 0>
                            <input type="submit" name="submitBtnF" value="<<< <@global.text key='text.firstpage'/>"
                                   onClick="document.getElementById('start').value='0'; return true;"/>
                            <input type="submit" name="submitBtnP" value="<< <@global.text key='text.prevpage'/>"
                                   onClick="document.getElementById('start').value='${(searcher.start-searcher.limit)}'; return true;"/>
                        </#if>
                    </td>
                    <td style="border: none;"
                        align="center"><@global.text key='text.page'/> ${(searcher.start/searcher.limit)?int+1}
                        / ${((searcher.total-1)/searcher.limit)?int+1} </td>
                    <td style="border: none;" align="right" width="150">
                        <#if searcher.total &gt; (searcher.start+searcher.limit)>
                            <input type="submit" name="submitBtnN" value="<@global.text key='text.nextpage'/> >>"
                                   onClick="document.getElementById('start').value='${(searcher.start+searcher.limit)}'; return true;"/>
                            <#if searcher.total%searcher.limit &gt; 0>
                                <#assign lastPageStart=searcher.total-(searcher.total%searcher.limit)>
                            <#else>
                                <#assign lastPageStart=searcher.total-searcher.limit>
                            </#if>
                            <input type="submit" name="submitBtnL" value="<@global.text key='text.lastpage'/> >>>"
                                   onClick="document.getElementById('start').value='${lastPageStart}'; return true;"/>
                        </#if>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</#macro>

<#macro objstatus objstatuses status>
    <#assign statusClasses = {"":"","A":"active","T":"template", "F":"locked", "L":"locked", "B":"blocked", "D":"blocked", "E":"ended","V":"active","R":"blocked","U":"ended","P":"active","N":"blocked"}>
    <#assign statusClass = statusClasses[(status!"")]!""/>
    <#if objstatuses?? && status?? && objstatuses[status?string]??><span
        class="${statusClass!""}">${objstatuses[status?string]?html}</span></#if>
</#macro>
<#-- universal -->
<#macro displayStatus status>
    <#assign objstatuses= {"":"","A":"${(loca.getText('text.active', locale)!'')?html}","T":"${(loca.getText('text.template', locale)!'')?html}",
    "F":"${(loca.getText('text.locked', locale)!'')?html}",
    "B":"${(loca.getText('text.blocked', locale)!'')?html}", "D":"${(loca.getText('text.disabled', locale)!'')?html}",
    "E":"${(loca.getText('text.ended', locale)!'')?html}", "V":"${(loca.getText('text.valid', locale)!'')?html}",
    "R":"${(loca.getText('text.revoked', locale)!'')?html}", "U":"${(loca.getText('text.unknown', locale)!'')?html}",
    "P":"${(loca.getText('text.participating', locale)!'')?html}",
    "N":"${(loca.getText('text.notparticipating', locale)!'')?html}",
    "L":"${(loca.getText('text.locked', locale)!'')?html}"} />
    <@global.objstatus objstatuses status/>
</#macro>

<#macro simpleCheckbox value>
    <input disabled="disabled" type="checkbox" <#if value?? && value >checked="checked"</#if>/>
</#macro>
<#macro selTimeZone>
    <form name="timeZone" id="timeZone" method="post" action="${ctxPath?html}/main/main.html">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <input type="hidden" name="cmd" value=""/>
        <#if serverTime??>
            ${serverTime?string((dateFormat!"yyyy-MM-dd")+" HH:mm:ss")} <select name="tz" style="width: 60px;"
                                                                                onchange="submitFormWithCmd(null,'timeZone', 'none');">
            <#list timeZones?keys as value>
                <option value="${value?html}"
                        <#if timeZone?? && timeZone.getID()==value>selected</#if>>${timeZones[value]?html}</option></#list>
        </select>
        </#if>
    </form>
</#macro>