<#import "../common/global.ftl" as global/>
<#import "../utility/tooltip.ftl" as tooltip/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt; <@global.text key='text.licenseinfo'/></h1>

    <h2><@global.text key='text.licenseinfo.detail'/></h2>
    <#if errormsg?exists>
        <span class="error">${(errormsg!"")?html}</span>
    </#if>
    <#if msg?exists>
        <span class="msg">${(msg!"")?html}</span>
    </#if>

    <#if li?exists>

        <#if cmd?exists && cmd=='edit'>
            <form id="lForm" action="licenseinfo.html" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="hidden" name="cmd" value="save"/>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="140" class="tdHeaderVertical">Product name:*</td>
                        <td width="440" style="vertical-align: top;">
                            <@global.spring.formInput path="li.product" attributes='size="40" maxlength="64"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="Exact value from provided license data" /></td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical">Product version:*</td>
                        <td style="vertical-align: top;">
                            <@global.spring.formInput path="li.version" attributes='size="16" maxlength="16"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="Exact value from provided license data" /></td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical">Licensee Name:*</td>
                        <td style="vertical-align: top;">
                            <@global.spring.formInput path="li.licensee" attributes='size="40" maxlength="64"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="Exact value from provided license data" /></td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical">Max Merchant count:*</td>
                        <td style="vertical-align: top;">
                            <@global.spring.formInput path="li.maxMerchants" attributes='size="16" maxlength="16"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="Exact value from provided license data, Integer value or 'Unlimited' or 'unlimited' as from license info" /></td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical">Extensions:</td>
                        <td style="vertical-align: top;">
                            <@global.spring.formInput path="li.extensions" attributes='size="40" maxlength="64"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="Exact value from provided license data, leave empty if no extensions licensed in license info" /></td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical"><@global.text key='text.li.issued'/>:*</td>
                        <td style="vertical-align: top;">
                            <@global.spring.formInput path="li.issued" attributes='size="12" maxlength="15"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="${issuedHelpTooltip}" /></td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical">Expiration:*</td>
                        <td style="vertical-align: top;">
                            <@global.spring.formInput path="li.expiration" attributes='size="12" maxlength="15"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                       <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="Exact value from provided license data, Date yyyy-MM-dd or 'Never' or 'never' as from license info" /></td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical">License key:*</td>
                        <td style="vertical-align: top;" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><@global.spring.formTextarea path="li.licenseKey" attributes='cols="78" rows="7" style="whitespace: pre-wrap; font-family: monospace;"'/>
                                        <br/>
                                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                                    </td>
                                    <td><@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="Paste here the exact value of license key of Your license data" /></td>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderVertical">&nbsp;</td>
                        <td colspan="2">
                            <input type="submit" value="<@global.text key='button.save'/>"/>
                            <input type="button" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'licenseinfo.html';return false;"/>
                            After save restart is needed!
                        </td>
                    </tr>
                </table>
            </form>
        <#else>
            <table witdh="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="140" class="tdHeaderVertical">Product name:*</td>
                    <td width="440">${(li.product!"")?html}
                    </td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">Product version:*</td>
                    <td style="vertical-align: top;">
                        ${(li.version!"")?html}
                    </td>

                </tr>
                <tr>
                    <td class="tdHeaderVertical">Licensee Name:*</td>
                    <td style="vertical-align: top;">${(li.licensee!"")?html} </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">Max Merchant count:*</td>
                    <td style="vertical-align: top;">${(li.maxMerchants!"")?html}</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">Extensions:</td>
                    <td style="vertical-align: top;">${(li.extensions!"")?html}</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">Issued:*</td>
                    <td style="vertical-align: top;">${(li.issued!"")?html}</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">Expiration:*</td>
                    <td style="vertical-align: top;">${(li.expiration!"")?html}</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">License key:*</td>
                    <td style="vertical-align: top;"><textarea name="lkey" readonly="true" cols="78" rows="7"
                                                               style=" whitespace: pre-wrap; background-color: #ededed; font-family: monospace;">${(li.licenseKey!"")?html}</textarea>
                    </td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">&nbsp;</td>
                    <td>

                        <@global.security.authorize access="hasAnyRole('userEdit')">
                            <input type="button" value="<@global.text key='button.edit'/>"
                                   onclick="document.location.href = 'licenseinfo.html?cmd=edit';return false;"/>
                        </@global.security.authorize>
                        <input type="button" value="<@global.text key='button.cancel'/>"
                               onclick="document.location.href = 'main.html';return false;"/>
                    </td>
                </tr>
            </table>
        </#if>

    <#else>
        <span style=" color: red;">License data was not found</span>
    </#if>
</@global.page>
