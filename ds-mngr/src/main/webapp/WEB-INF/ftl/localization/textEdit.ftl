<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt; <a
                href="../localization/textsList.html?x=${(.now?time)?html}"><@global.text key='text.localization'/></a>
        &gt; <@global.text key='text.textedit'/>:</h1>
    <h2><@global.text key='text.textedit'/></h2>
    <#if error?exists>
        <span class="error">${error?html}</span>
    </#if>
    <form action="textSave.html" id="tForm" accept-charset="UTF-8" method="POST">
        <input id="cmd" type="hidden" name="cmd" value="save"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="tdHeaderVertical">TID:</td>
                <td>
                    <@global.spring.formInput path="text.id" attributes='size="10" readonly="readonly" style="background: #cccccc;"'/>
                </td>
                <td>
                    <div class="helptext"><@global.text key='text.text.id'/>(system assigned)</div>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.language'/>:*</td>
                <td class="tdHeaderVertical">
                    <@global.spring.formSingleSelect path="text.locale" options=supportedLocales />
                    <br/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
                <td>
                    <div class="helptext"><@global.text key='text.select.language'/></div>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.text.key'/>:*</td>
                <td>
                    <@global.spring.formInput path="text.key" attributes='size="40" maxlength="96"'/>
                    <br/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
                <td>
                    <div class="helptext"><@global.text key='text.text.key.desc'/>(unique within locale)</div>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.text.value'/>:*</td>
                <td class="tdHeaderVertical">
                    <@global.spring.formTextarea path="text.message" attributes='rows="10" cols="60"'/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
                <td>
                    <div class="helptext"><@global.text key='text.enter.text.value'/></div>
                </td>
            </tr>
        </table>


        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="tdHeaderVertical">&nbsp;</td>
                <td colspan="2">
                    <input type="submit" name="submitbutton" value="<@global.text key='button.save'/>"/>
                    <#if text?exists && text.id?exists>
                        <input type="submit" name="savenew" value="<@global.text key='button.saveAsNew'/>"
                               onClick="delId();"/>
                        <input type="submit" name="delbutton" value="<@global.text key='button.delete'/>"
                               onClick="delText();"/>
                    </#if>
                    <input type="button" name="cancelbutton" value="<@global.text key='button.cancel'/>"
                           onclick="document.location.href = 'textsList.html'"/>
                </td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">
        function delText() {
            var cmd = document.getElementById("cmd");
            if (cmd != null)
                cmd.value = "delete";
        }

        function delId() {
            var id = document.getElementById("id");
            if (id != null)
                id.value = "";
        }

    </script>
</@global.page>
