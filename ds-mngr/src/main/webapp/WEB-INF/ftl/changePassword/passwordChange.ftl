<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt; <a
                href="../users/usersList.html?x=${(.now?time)?html}"><@global.text key='text.users'/></a> &gt;
        <#if user?exists><a
            href="../users/userView.html?id=${(user.getId()!"")?html}"><@global.text key='text.change.password'/>
            : ${(user.loginname!"")?html}</a></#if>
    </h1>
    <h2><@global.text key='text.change.password'/></h2>
    <#if user?exists && user.forcedPasswordChange>
        <@global.text key='text.msg.focedpassword.change'/>
    </#if>
    <form id="uForm" action="../passwordChange/change.html" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <@global.spring.formHiddenInput path='user.id'/>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="30%" class="tdHeaderVertical"><@global.text key='text.currentPassword'/>:</td>
                <td>
                    <@global.spring.formPasswordInput path="user.password" attributes="size='20' maxlength='32'"/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
            </tr>
            <tr>
                <td width="30%" class="tdHeaderVertical"><@global.text key='text.newPassword'/>:</td>
                <td>
                    <@global.spring.formPasswordInput path="user.newPassword" attributes="size='20' maxlength='32' oninput='getPasswordStrengthFeedback(this);'"/>
                    <span id="passwordStrengthFeedback" style="display:none;"></span>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
            </tr>
            <tr>
                <td width="30%" class="tdHeaderVertical"><@global.text key='text.newPasswordRepeat'/>:</td>
                <td>
                    <@global.spring.formPasswordInput path="user.newPasswordRepeat" attributes="size='20' maxlength='32'"/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
            </tr>
            <tr>
                <td width="30%" class="tdHeaderVertical">&nbsp;</td>
                <td>
                    <input type="submit" value="<@global.text key='button.save'/>"/>
                    <#if user?exists && user.forcedPasswordChange>
                        <input type="button" value="<@global.text key='button.cancel'/>" onclick="document.location.href = '../logout';" />
                    <#else>
                        <input type="button" value="<@global.text key='button.cancel'/>" onclick="document.location.href = '../main/main.html';" />
                    </#if>
                </td>
            </tr>
        </table>
    </form>
</@global.page>
