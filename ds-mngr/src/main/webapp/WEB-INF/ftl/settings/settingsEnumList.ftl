<#import "../common/global.ftl" as global/>
<@global.page>
    <link rel="stylesheet" href="../css/settings.css">
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt;
        <span class="selected"><@global.text key='text.settingsEnum'/></span></h1>

    <h2><@global.text key='text.settingsEnum'/></h2>
    <#if error?exists>
        <span class="error">${error?html}</span>
    </#if>

    <div id="formSubmitMessage" style="padding-top: 30px;">
    <#if successMessage??>
        <span class="active">${successMessage?html}</span>
    <#elseif errorMessage?exists>
        <span class="error">${errorMessage?html}</span>
    </#if>
    </div>

    <div style="padding-top: 30px; padding-bottom: 20px;">
        <a id="showAllSettingSections" class="rightside" style="font-weight: bold; cursor: pointer;" onclick="switchAllSection()">Show All + </a>&nbsp;
    </div>
    <form id="settingForm" action="settingsEnumList.html#formSubmitMessage" accept-charset="UTF-8" method="post">

    <#assign encryptionHint>
        Enter the setting value, if adding a setting that should be secret like password use prefix "enc:" before value, then such setting will automatically encrypted, and will be displayed later as sec:encryptedvalue
    </#assign>
    <#list settingSections as settingSection>
        <#assign isActiveSection = (activeSections?seq_contains(settingSection.name))>
        <h3>${settingSection.description}</h3>

        <a class="rightside" id="${settingSection.name}-list-toggle" onclick="switchSection('${settingSection.name}-list');" title="Toggle Section" style="cursor: pointer;">${isActiveSection?then('Hide Section -','Show Section +')}</a>
        <br/>
        <br/>

        <div id="${settingSection.name}-list" class="setting-section-accordion" style="${isActiveSection?then('','display:none;')} padding-top: 30px; padding-bottom: 40px;">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" id="${settingSection.name}-list-active-flag" name="shownSections" value="${settingSection.name}" ${isActiveSection?then('','disabled')}/>
                <br/>
            <table>
                <tbody>
            <#list settingSection.settingItems as settingItem>
                <tr>
                    <td style="text-align:left;padding-left:20px">${settingItem.key}</td>
                    <td>
                        <#if settingItem.isEditable>
                            <#if isActiveSection>
                                <@global.spring.bind path="settingsForm.settings['${settingItem.key}']"/>
                                <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                                <br/>
                            </#if>
                            <textarea class="setting-value" id="${settingItem.key}-input" name="settings.${settingItem.key}" rows="1" cols="50" style="min-height:34px; min-width:350px" placeholder="${settingItem.placeholder}...">${(settingItem.value)!}</textarea>
                        <#else>
                            <span class="setting-value">${(settingItem.value)!}</span>
                        </#if>

                    </td>
                    <td>
                        <#if settingItem.lastModified?exists>${settingItem.lastModified?string(dateFormat+" HH:mm:ss")}</#if>
                        <#if settingItem.lastModifiedBy?exists> &nbsp; ${(settingItem.lastModifiedBy!"")?html}</#if>
                    </td>
                </tr>
            </#list>
                </tbody>
            </table>
            <input type="submit" id="${settingSection.name}-submitbutton"  name="${settingSection.name}-submitbutton" value="Save All" style="margin-left:20px"/>

        </div>
    </#list>
    </form>
<script type="text/javascript" src="../js/settingsEnumList.js"></script>

</@global.page>
