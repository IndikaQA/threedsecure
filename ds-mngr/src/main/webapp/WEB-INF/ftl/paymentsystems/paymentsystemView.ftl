<#-- @ftlvariable name="paymentSystem" type="com.modirum.ds.db.model.PaymentSystem" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a>
        &gt;
        <a href="paymentsystemsList.html"><@global.text key='text.paymentsystems'/></a>
        &gt;
        <span class="selected"><@global.text key='text.details'/>: ${(paymentSystem.name!"")?html}</span>
    </h1>
    <#if hasAccessToAllPaymentSystems>
        <@global.security.authorize access="hasRole('adminSetup')">
            <a href="../paymentsystems/paymentsystemEdit.html?x=${(.now?time)?html}"><@global.text key='text.addNewPaymentSystem'/></a>
        </@global.security.authorize>
    </#if>
    <h2><@global.text key='text.paymentsystem.info'/></h2>

    <#if paymentSystem??>
        <table border="1" frame="void" rules="rows">
                <tr>
                    <@global.security.authorize access="hasRole('adminSetup')">
                        <td colspan="2">
                            <form action="paymentsystemEdit.html" method="post">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <input type="hidden" name="id" value="${(paymentSystem.id!"")?html}"/>
                                <input type="submit" name="button" value="<@global.text key='button.edit'/>"/>
                            </form>
                        </td>
                    </@global.security.authorize>
                    <@global.security.authorize access="hasAnyRole('paymentsystemView','adminSetup')">
                        <td colspan="2">
                            <form action="../paymentSystemSettings/paymentSystemSettingsList.html" method="post">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <input type="hidden" name="paymentSystemId" value="${(paymentSystem.id!"")?html}"/>
                                <input type="submit" name="button" value="<@global.text key='text.settings'/>"/>
                            </form>
                        </td>
                    </@global.security.authorize>
                    <td><br/></td>
                </tr>
           
            <tr>
                <td class="tdHeaderVertical">Id:</td>
                <td>${(paymentSystem.id!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.paymentsystem.name'/>:</td>
                <td>${(paymentSystem.name!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.paymentsystem.type'/>:</td>
                <td>${(paymentSystem.type!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.paymentsystem.port'/>:</td>
                <td>${(paymentSystem.port!"")?html}</td>
            </tr>
        </table>

        <@global.security.authorize access="hasAnyRole('issuerView', 'acquirerView')">
            <h2>${(paymentSystem.name!"")?html}&nbsp;<@global.text key='text.paymentsystem.components'/></h2>
        </@global.security.authorize>

        <@global.security.authorize access="hasAnyRole('issuerView')">
                <a href="#" onclick="window.search('../issuers/issuerList.html');"><@global.text key='text.manage.issuers'/></a> <br/>
        </@global.security.authorize>
        <@global.security.authorize access="hasAnyRole('acquirerView')">
            <a href="#" onclick="window.search('../acquirers/acquirerList.html');"><@global.text key='text.manage.acquirers'/></a> <br/>
            <a href="#" onclick="window.search('../acquirers/tdsServerList.html');"><@global.text key="text.tdsserverprofiles"/></a> <br/>
            <a href="#" onclick="window.search('../acquirers/merchantList.html');"><@global.text key="text.merchants"/></a>
        </@global.security.authorize>

        <form id="searchForm" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" name="paymentSystemId" value="${(paymentSystem.id!)}"/>
            <input type="hidden" name="search" value="Search"/>
        </form>

        <script type="text/javascript">
            function search(target) {
                if ((typeof target === 'string' || target instanceof String) && target !== null && target !== '') {
                    var searchForm = document.getElementById('searchForm');
                    searchForm.action = target;
                    searchForm.submit();
                }
            }
        </script>

    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
