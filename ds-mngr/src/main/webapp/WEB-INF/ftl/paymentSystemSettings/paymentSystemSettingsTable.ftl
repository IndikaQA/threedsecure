<#import "../common/global.ftl" as global/>
<#import "paymentSystemSettingsInput.ftl" as settingsInput/>
<#macro table title paymentSystemSettings viewRole editRole>
    <@global.security.authorize access="hasRole('${viewRole}')">
        <h3 style="margin-top:40px;">${title}</h3>
        <div id="content-2" style="color: black;">
            <table class="fixed" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="tdHeader" width="200px"><@global.text key='objectClass.Setting'/></td>
                    <td class="tdHeader" width="400px"><@global.text key='text.attr.value'/></td>
                    <td class="tdHeader" width="400px"><@global.text key='text.paymentsystemssettings.comment'/></td>
                    <td class="tdHeader" width="300px"><@global.text key='text.paymentsystemssettings.modified'/></td>

                    <td class="tdHeader" width="200px"></td>
                </tr>
                <#list paymentSystemSettings as psc>
                    <form action="../paymentSystemSettings/paymentSystemSettingsEdit.html">
                        <tr>
                            <td>
                                <input type="hidden" value="${psc.dsId}" name="dsId"/>
                                <input type="hidden" value="${psc.paymentSystemId}" name="paymentSystemId"/>
                                <input type="hidden" value="${psc.key}" name="key"/>
                                <input type="hidden" value="${psc.inputType}" name="inputType"/>


                                <span style="cursor:pointer;"
                                      title="<@global.text key='text.paymentsystemssettings.help.${psc.key}'/>"><@global.text key='text.paymentsystemssettings.presentablename.${psc.key}'/></span>
                            </td>
                            <td>
                                <#assign settingsValue>${(psc.value!"")?html}</#assign>
                                <@settingsInput.input val="${settingsValue}" type="${psc.inputType}" />
                            </td>
                            <td>
                                <span class="settings-comment">${(psc.comment!"")?html}</span>
                                <textarea name="comment" maxlength="256" class="settings-comment" style="display:none;"
                                          cols="50"></textarea>

                            </td>
                            <td>
                                <span style="white-space: pre-wrap">${(psc.lastModified!"")?html}</span>
                            </td>
                            <td>
                                <#if psc.inputType != 'display_only' >
                                    <@global.security.authorize access="hasRole('${editRole}')">
                                        <input class="editButton" type="button" name="button" value="Edit"
                                               autocomplete="off" onclick="edit(this);">
                                        <input class="saveButton" style="display:none;" type="submit" name="button"
                                               value="Save" autocomplete="off">
                                        <input class="cancelButton" style="display:none;" type="button" name="button"
                                               value="Cancel" autocomplete="off" onclick="cancel(this);">
                                    </@global.security.authorize>
                                </#if>
                            </td>
                        </tr>
                    </form>
                </#list>
            </table>
        </div>
    </@global.security.authorize>
</#macro>