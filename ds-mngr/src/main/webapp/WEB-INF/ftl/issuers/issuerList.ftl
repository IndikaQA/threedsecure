<#-- @ftlvariable name="searcher" type="com.modirum.ds.model.IssuerSearcher" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <span
                class="selected"><@global.text key='text.issuers'/></span></h1>

    <@global.security.authorize access="hasAnyRole('issuerEdit')">
        <a href="../issuers/issuerEdit.html?x=${(.now?time)?html}"><@global.text key='text.addNewIssuer'/></a> <a
            href="issuersBatch.html">Batch Issuers</a>   <a href="issuerCardRangesBatch.html">Batch Card Ranges</a>
    </@global.security.authorize>
    <h2><@global.text key='text.search.issuers'/></h2>
    <form id="uSearchForm" action="issuerList.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <span style="width: 200px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
    <@global.spring.formSingleSelect path="searcher.paymentSystemId" options=paymentSystemSearchMap/>
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.issuer.name'/>:</span>
    <@global.spring.formInput path="searcher.name" attributes='size="25" maxlength="50"'/>
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.issuer.BIN'/>:</span>
    <@global.spring.formInput path="searcher.BIN" attributes='size="${binLength}" maxlength="${binLength}"'/>
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.issuer.binMatch'/>:</span>
    <@global.spring.formInput path="searcher.binMatch" attributes='size="16" maxlength="19"'/>
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.issuer.accountnumber'/>:</span>
    <@global.spring.formInput path="searcher.pan" attributes='size="16" maxlength="19"'/>
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.issuer.status'/>:</span>
    <@global.spring.formSingleSelect path="searcher.status" options=issuerStatuses />
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.issuer.country'/>:</span>
    <@global.spring.formSingleSelect path="searcher.country" options=countryMapA2 />
    <br/>

    <span style="width: 200px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="searcher.order" options={"":"","id":"Id", "status":"Status", "name":"Name"} />
    <@global.spring.formSingleSelect path="searcher.orderDirection" options={"asc":"Ascending","desc":"Descending"} />
    <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
    <@global.spring.formSingleSelect path="searcher.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
    <br/>
    <input type="hidden" name="search" value="Search"/>
    <input type="submit" name="submitbtn" value="Search"/>
    <br/>

    <#if foundIssuers?exists>
        <#if searcher?exists && searcher.total &gt; 0 >
            <h2><@global.text key='text.found.total'/> ${searcher.total!""?html} <@global.text key='text.issuers'/>
                , <@global.text key='text.showing'/> ${(searcher.start+1)!""?html} <@global.text key='text.to'/>
                <#if searcher.total &gt; (searcher.start+searcher.limit)>
                    ${(searcher.start+searcher.limit)!""?html}
                <#else>
                    ${(searcher.total)!""?html}
                </#if>
            </h2>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <@tableTitle/>
                <#list foundIssuers as issuerx>
                    <tr <#if issuerx_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                        <@issuerDetails issuer=issuerx />
                    </tr>
                </#list>
                <@global.pagingButtons searcher=searcher colspan=6/>
            </table>
            </form>
        <#else>
            <@global.text key='text.no.matches.found'/>
        </#if>
    </#if>


</@global.page>

<#macro tableTitle>
    <tr>
        <td width="5%" class="tdHeader">Id</td>
        <td width="15%" class="tdHeader"><@global.text key='text.name'/></td>
        <td width="15%" class="tdHeader"><@global.text key='text.bin'/></td>
        <td width="15%" class="tdHeader"><@global.text key='text.email'/></td>
        <td width="10%" class="tdHeader"><@global.text key='text.country'/></td>
        <td width="10%" class="tdHeader"><@global.text key='general.paymentSystem'/></td>
        <td align="center" class="tdHeader"><@global.text key='text.status'/></td>
    </tr>
</#macro>

<#macro issuerDetails issuer>
    <td width="5%"><a href="../issuers/issuerView.html?id=${(issuer.id!"")?html}">${(issuer.id!"")?html}</a></td>
    <td width="15%"><a href="../issuers/issuerView.html?id=${(issuer.id!"")?html}">${(issuer.name!"")?html}</a></td>
    <td width="15%">${(issuer.BIN!"")?html}</td>
    <td width="15%">${(issuer.email!'')?html}</td>
    <td><#if issuer.country??>${((countryMapA2[issuer.country?string])!issuer.country)?html}</#if></td>
    <td>
        <#if issuer.paymentSystemId??>
            ${((paymentSystemComponentMap[issuer.paymentSystemId?string])!issuer.paymentSystemId)?html}
        </#if>
    </td>
    <td width="15%" align="center"><@global.objstatus issuerStatuses issuer.status!""/></td>
</#macro>
<#macro simpleCheckbox value>
    <td align="center">
        <input disabled="disabled" type="checkbox" <#if value?? && value >checked="checked"</#if>/>
    </td>
</#macro>
