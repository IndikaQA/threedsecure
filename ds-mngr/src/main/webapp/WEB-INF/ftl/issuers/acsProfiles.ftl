<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="issuerList.html?x=${(.now?time)?html}"><@global.text key='text.attempts.acs.profiles'/></a>
    </h1>
    <h2><@global.text key='text.attempts.acs.profiles'/></h2>
    <#if msg?exists>
        <span class="msg">${msg?html}</span><br/>
    </#if>
    <#if lists??>
        <form id="uForm" action="acsProfiles.html" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <table>
                <tr>
                    <td>
                        <table class="stripe">
                            <tr>
                                <th width="185"><@global.text key='text.acsprofile.name'/>
                                    //<@global.text key='text.acsprofile.refno'/>*
                                </th>
                                <th width="310"><@global.text key='text.acsprofile.url'/>*</th>
                                <th width="160"><@global.text key='text.acsprofile.in.clientcertid'/>
                                    <#if sslCertsManagedExternally?? && sslCertsManagedExternally><#else>*</#if>
                                </th>
                                <th width="160"><@global.text key='text.acsprofile.out.clientcertid'/></th>
                                <th><@global.text key='text.acsprofile.status'/></th>
                            </tr>
                            <#list lists.list2 as acp>
                                <tr>
                                    <#if acp.status?? && acp.status="E">
                                        <td>${(acp.name!"")?html}<br/>${(acp.refNo!"")?html}
                                            <@global.spring.formHiddenInput path='lists.list2[${acp_index}].id'/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].name"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].refNo"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].URL"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].threeDSMethodURL"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].inClientCert"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].outClientCert"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].startProtocolVersion"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].endProtocolVersion"/>
                                            <@global.spring.formHiddenInput path="lists.list2[${acp_index}].status"/>
                                        </td>
                                        <td>${(acp.URL!"")?html}<br/>${(acp.threeDSMethodURL!"")?html}</td>
                                        <td>
                                            <#if inCertificateList?? && acp.inClientCert??>
                                                ${(inCertificateList[acp.inClientCert?string]!"")?html}
                                            </#if>
                                            <#if protoVersionList?? && acp.startProtocolVersion??>
                                                <@global.text key='text.protoStart'/> ${(protoVersionList[acp.startProtocolVersion?string]!"")?html}
                                            </#if>
                                        </td>
                                        <td>
                                            <#if outCertificateList?? && acp.outClientCert??>
                                                ${(inCertificateList[acp.outClientCert?string]!"")?html}
                                            </#if>
                                            <#if protoVersionList?? && acp.endProtocolVersion??>
                                                <@global.text key='text.protoEnd'/> ${(protoVersionList[acp.endProtocolVersion?string]!"")?html}
                                            </#if>
                                        </td>
                                        <td><@global.objstatus acpStatuses acp.status!""/></td>
                                    <#else>
                                        <td>
                                            <@global.spring.formHiddenInput path='lists.list2[${acp_index}].id'/>
                                            <@global.spring.formInput path="lists.list2[${acp_index}].name" attributes='placeholder="ACS Name" size="25" maxlength="100"'/>
                                            <@listerror "lists","lists.list2[${acp_index}]","name"/>
                                            <br/>
                                            <@global.spring.formTextarea path="lists.list2[${acp_index}].refNo" attributes='style="width: 191px; height: 45px;resize:vertical" placeholder="ACS Reference Numbers"'/>
                                            <@listerror "lists","lists.list2[${acp_index}]","refNo"/>
                                        </td>
                                        <td align="right">
                                            <@global.spring.formInput path="lists.list2[${acp_index}].URL" attributes='placeholder="ACS Service URL required" size="40" maxlength="255"'/>
                                            <@listerror "lists","lists.list2[${acp_index}]","URL"/><br/>
                                            HS
                                            URL <@global.spring.formInput path="lists.list2[${acp_index}].threeDSMethodURL" attributes='placeholder="Browser Handshake URL optional" size="32" maxlength="255"'/>
                                            <@listerror "lists","lists.list2[${acp_index}]","threeDSMethodURL"/>

                                        </td>
                                        <td>
                                            <@global.spring.formSingleSelect path="lists.list2[${acp_index}].inClientCert" options=inCertificateList attributes=' style="width: 160px;"' />
                                            <@listerror "lists","lists.list2[${acp_index}]","inClientCert"/>
                                            <@global.spring.formSingleSelect path="lists.list2[${acp_index}].startProtocolVersion" options=protoVersionList attributes=' style="width: 160px;"' />
                                            <@listerror "lists","lists.list2[${acp_index}]","startProtocolVersion"/>
                                        </td>
                                        <td>
                                            <@global.spring.formSingleSelect path="lists.list2[${acp_index}].outClientCert" options=outCertificateList attributes=' style="width: 160px;"' />
                                            <@listerror "lists","lists.list2[${acp_index}]","outClientCert"/>
                                            <@global.spring.formSingleSelect path="lists.list2[${acp_index}].endProtocolVersion" options=protoVersionList attributes=' style="width: 160px;"' />
                                            <@listerror "lists","lists.list2[${acp_index}]","endProtocolVersion"/>
                                        </td>
                                        <td><@global.spring.formSingleSelect path="lists.list2[${acp_index}].status" options=acpStatuses attributes=' style="width: 65px;"' />
                                            <@listerror "lists","lists.list2[${acp_index}]","status"/>
                                        </td>
                                    </#if>
                                </tr>
                            </#list>
                        </table>
                        <input type="submit" name="addNewACSP" value="<@global.text key='button.addNewACSP'/>"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" name="save" value="<@global.text key='button.save'/>"/>
                        <input type="submit" value="<@global.text key='button.cancel'/>"
                               onclick="document.location.href = '../main/main.html'; return false;"/>
                    </td>
                </tr>
            </table>
        </form>
    </#if>
    <#macro listerror lists, path, field>
        <#if springMacroRequestContext.getErrors(lists)??>
            <#assign perrs = springMacroRequestContext.getErrors(lists).getFieldErrors(field) />
            <#list perrs as ferror>
                <#if ferror.objectName==path && ferror.field==field>
                    <span class="error">${(ferror.getDefaultMessage()!"")?html}</span>
                </#if>
            </#list>
        </#if>
    </#macro>
</@global.page>
