<#-- @ftlvariable name="acquirer" type="com.modirum.ds.db.model.Acquirer" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="acquirerList.html?x=${(.now?time)?html}"><@global.text key='text.acquirers'/></a> &gt;
        <#if acquirer?exists><span class="selected">Details: ${(acquirer.getName()!"")?html}</span></#if></h1>
    <h2><@global.text key='text.acquirer'/></h2>
    <#if acquirer?exists>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <form action="acquirerEdit.html" method="post">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="id" value="${(acquirer.getId()!"")?html}"/>
                                        <input type="submit" name="button" value="<@global.text key='button.edit'/>"/>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </@global.security.authorize>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical">Id:</td>
                <td>${(acquirer.id!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.name'/>:</td>
                <td>${(acquirer.name!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.acquirer.bin'/>:</td>
                <td>${(acquirer.BIN!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                <td>${(paymentSystemName!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.email'/>:</td>
                <td>${(acquirer.email!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.address'/>:</td>
                <td>${(acquirer.address!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.city'/>:</td>
                <td>${(acquirer.city!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeader"><@global.text key='text.country'/></td>
                <td><#if acquirer.country??>${(countryMapA2[acquirer.country?string]!acquirer.country)?html}</#if></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.phone'/>:</td>
                <td>${(acquirer.phone!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.status'/>:</td>
                <td><@global.objstatus acquirerStatuses acquirer.status!""/></td>
            </tr>
            <#if acquirer?exists && acquirer.modifiedDate?exists>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.acquirer.modified'/>:</td>
                    <td>
                        <#if acquirer?exists && acquirer.modifiedDate?exists>
                            ${acquirer.modifiedDate?string(dateFormat+" HH:mm:ss")}
                        </#if>
                        <#if acquirer?exists &&acquirer.modifiedacquirer?exists>
                            &nbsp; ${(acquirer.modifiedacquirer!"")?html}
                        </#if>
                    </td>
                </tr>
            </#if>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.acquirer.merchantcount'/>:</td>
                <td class="bottom"><a
                            href="merchantList.html?acquirerId=${(acquirer.getId()!"")?html}&search=y">${(acquirerMerCount!"")?html}</a>
                    &nbsp;
                    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
                        <form action="merchantEdit.html" method="post" style="display: inline-block;">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <input type="hidden" name="acqid" value="${(acquirer.getId()!"")?html}"/>
                            <input type="submit" name="button" value="<@global.text key='button.addNew'/>"
                                   class="bottom" style="margin: 0px;"/>
                        </form>
                    </@global.security.authorize>
                </td>
            </tr>
        </table>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
