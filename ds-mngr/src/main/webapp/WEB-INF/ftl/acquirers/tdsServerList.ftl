<#-- @ftlvariable name="filter" type="com.modirum.ds.mngr.model.ui.filter.TDSServerSearchFilter" -->
<#-- @ftlvariable name="tdsServerList" type="java.util.List<com.modirum.ds.db.model.TDSServerProfile>" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <span class="selected"><@global.text key='text.tdsserverservers'/></span>
    </h1>

    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
        <a href="tdsServerEdit.html?x=${(.now?time)?html}"><@global.text key='text.addNewTDSServer'/></a>
    </@global.security.authorize>

    <h2><@global.text key='text.search.tdsserverservers'/></h2>
    <style> input[type="text"], input[type="password"], select {
            margin-bottom: 2pt;
        } </style>
    <form id="uSearchForm" action="tdsServerList.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="hidden" name="cmd" value=""/>
    <span style="width: 110px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
    <@global.spring.formSingleSelect path="filter.paymentSystemId" options=paymentSystemSearchMap
        attributes='onchange="submitFormWithCmd(null, \'uSearchForm\' , \'refreshPaymentSystem\');"'/>
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.name'/>:</span>
    <@global.spring.formInput path="filter.name" attributes='size="25" maxlength="50"'/>
    <br/>
    <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
    <#else>
        <span style="width: 110px; display: inline-block;"><@global.text key='text.3ds.requestorId'/>:</span>
        <@global.spring.formInput path="filter.requestorID" attributes='size="25" maxlength="35"'/>
        <br/>
    </#if>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.3ds.operatorId'/>:</span>
    <@global.spring.formInput path="filter.operatorID" attributes='size="25" maxlength="35"'/>
    <br/>

    <span style="width: 110px; display: inline-block;"><@global.text key='text.in.certificate'/>:</span>
    <@global.spring.formSingleSelect path="filter.inClientCert" options=inCertificateList attributes='style="width: 200px;"'/>
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.status'/>:</span>
    <@global.spring.formSingleSelect path="filter.status" options=tdsServerStatuses />
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="filter.order" options={"":"","id":"Id", "status":"Status", "name":"Name"} />
    <@global.spring.formSingleSelect path="filter.orderDirection" options={"asc":"Ascending","desc":"Descending"} />
    <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
    <@global.spring.formSingleSelect path="filter.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
    <br/>
    <input type="hidden" name="search" value="Search"/>
    <input type="submit" name="submitbtn" value="<@global.text key='button.search'/>"/>
    <br/>
    <#if tdsServerList?exists>
        <#if filter?exists && filter.total &gt; 0 >
            <h2><@global.text key='text.found.total'/> ${filter.total!""?html} <@global.text key='text.tdsserverservers'/>
                , <@global.text key='text.showing'/> ${(filter.start+1)!""?html} <@global.text key='text.to'/>
                <#if filter.total &gt; (filter.start+filter.limit)>
                    ${(filter.start+filter.limit)!""?html}
                <#else>
                    ${(filter.total)!""?html}
                </#if>
            </h2>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" class="tdHeader">Id</td>
                    <td width="20%" class="tdHeader"><@global.text key='text.name'/></td>
                    <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
                    <#else>
                        <td width="10%" class="tdHeader"><@global.text key='text.3ds.requestorId'/></td>
                    </#if>
                    <td width="30%" class="tdHeader"><@global.text key='text.URL'/></td>
                    <td width="15%" class="tdHeader"><@global.text key='text.3ds.operatorId'/></td>
                    <td width="10%" class="tdHeader"><@global.text key='general.paymentSystem'/></td>
                    <td align="center" class="tdHeader"><@global.text key='text.status'/></td>
                </tr>
                <#list tdsServerList as tdss>
                    <tr <#if tdss_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                        <@MIDetails mi=tdss />
                    </tr>
                </#list>
                <@global.pagingButtons searcher=filter colspan=6/>
            </table>
            </form>
        <#else>
            <@global.text key='text.no.matches.found'/>
        </#if>
    </#if>
</@global.page>


<#macro MIDetails mi>
    <td><a href="tdsServerView.html?id=${(mi.id!"")?html}">${(mi.id!"")?html}</a></td>
    <td><a href="tdsServerView.html?id=${(mi.id!"")?html}">${(mi.name!"")?html}</a></td>
    <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
    <#else>
        <td><a href="tdsServerView.html?id=${(mi.id!"")?html}">${(mi.requestorID!"")?html}</a></td>
    </#if>
    <td>${(mi.URL!"")?html}</td>
    <td><div style="word-wrap: break-word; width:350px">${(mi.operatorID!"")?html}</div></td>
    <td>
        <#if mi.paymentSystemId??>
            ${((paymentSystemComponentMap[mi.paymentSystemId?string])!mi.paymentSystemId)?html}
        </#if>
    </td>
    <td align="center"><@global.objstatus tdsServerStatuses mi.status!""/></td>
</#macro>
