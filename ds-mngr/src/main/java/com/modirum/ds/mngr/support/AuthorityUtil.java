package com.modirum.ds.mngr.support;

import com.modirum.ds.db.model.User;
import com.modirum.ds.db.model.ui.PaymentSystemResource;
import com.modirum.ds.utils.Misc;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashSet;
import java.util.Set;

public class AuthorityUtil {

    private AuthorityUtil() {
        // util
    }

    public static User getUser() {
        if (SecurityContextHolder.getContext() != null &&
            SecurityContextHolder.getContext().getAuthentication() != null) {
            Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (user instanceof User) {
                return (User) user;
            }
        }
        return null;
    }

    public static boolean hasRole(String role) {
        return hasAnyRole(role);
    }

    /**
     * @return True, if the current user has any of the provided <code>roles</code>
     */
    public static boolean hasAnyRole(String... roles) {
        User user = getUser();
        if (user == null) {
            return false;
        }

        for (String role : roles) {
            if (user.hasRole(role)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isFactor2Passed() {
        User user = getUser();
        return user != null && user.isFactor2Passed();
    }

    public static Set<GrantedAuthority> getAuthorities(User user) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        if (user.getRoles().size() > 0) {
            for (String role : user.getRoles()) {
                authorities.add(new SimpleGrantedAuthority("ROLE_" + role)); // needed for sec4 "ROLE_"+
            }
        }

        return authorities;
    }

    public static String getLastModifiedUser() {
        User user = AuthorityUtil.getUser();
        if (user == null) {
            return "";
        }
        return Misc.cut(user.getLoginname() + " (" + user.getId() + ")", 60);
    }

    /**
     * Checks user access to the provided Payment System specific entity <code>psSpecificEntity</code>.
     * Entity must belong to the user's Payment System.
     */
    public static boolean isAccessAllowed(PaymentSystemResource psSpecificEntity) {
        if (psSpecificEntity == null) {
            return false;
        }

        User currentUser = getUser();
        if (currentUser.getPaymentSystemId() == null) { // superuser
            return true;
        }

        // PS-specific user views PS-specific entity
        if (currentUser.getPaymentSystemId().equals(psSpecificEntity.getPaymentSystemId())) {
            return true;
        }

        return false;
    }

    /**
     * Checks user access to the provided Payment System specific entity <code>psSpecificEntity</code>.
     * Entity must belong to user's Payment system and the user must have
     * at least one of the provided <code>roles</code> to gain access.
     */
    public static boolean isAccessAllowed(PaymentSystemResource psSpecificEntity, String... roles) {
        if (!hasAnyRole(roles)) {
            return false;
        }
        return isAccessAllowed(psSpecificEntity);
    }
}
