package com.modirum.ds.mngr.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CSR {

    String name;
    Date expires;
    String subjectDN;
    String issuerDN;
    String subjAltNames;
    String contstraints;
    String uploadData;
    String pemData;
    String uploadFileName;
    String sigalg;
    String extUse;
    Long caKeyId;
    MultipartFile uploadFile;
    String chain;
    boolean replace;

}
