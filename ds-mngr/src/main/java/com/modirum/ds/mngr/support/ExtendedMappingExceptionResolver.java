/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 10. mar 2016
 *
 */
package com.modirum.ds.mngr.support;

import com.modirum.ds.mngr.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ExtendedMappingExceptionResolver extends SimpleMappingExceptionResolver {
    private static final Logger log = LoggerFactory.getLogger(ExtendedMappingExceptionResolver.class);

    @Override
    public ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object arg2, Exception arg3) {
        log.error("Unexepcted errror", arg3);
        ModelAndView mv = super.doResolveException(request, response, arg2, arg3);
        if (BaseController.instance != null) {
            BaseController.instance.fillModelAttrs(mv);
        }
        return mv;
    }

    @Override
    public ModelAndView getModelAndView(String viewName, Exception ex) {
        log.error("Unexepcted errror", ex);
        ModelAndView mv = super.getModelAndView(viewName, ex);
        if (BaseController.instance != null) {
            BaseController.instance.fillModelAttrs(mv);
        }

        return mv;
    }


    @Override
    public ModelAndView getModelAndView(String viewName, Exception ex, HttpServletRequest request) {
        log.error("Unexepcted errror", ex);
        ModelAndView mv = super.getModelAndView(viewName, ex, request);
        if (BaseController.instance != null) {
            BaseController.instance.fillModelAttrs(mv);
        }

        return mv;
    }

}
