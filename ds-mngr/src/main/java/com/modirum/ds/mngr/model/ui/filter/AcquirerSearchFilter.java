package com.modirum.ds.mngr.model.ui.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DS-Admin acquirer search-list filter. Filled from search panel, used to lookup acquirers from DB.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AcquirerSearchFilter {
    private String name;
    private String BIN;
    private String status;
    private String country;
    private String order;
    private String orderDirection;
    private Integer start;
    private Integer limit;
    private Integer total;
    private Integer paymentSystemId;
}
