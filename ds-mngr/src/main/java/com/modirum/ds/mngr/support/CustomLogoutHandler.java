package com.modirum.ds.mngr.support;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.AuditLogService;
import com.modirum.ds.services.ServiceLocator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CustomLogoutHandler implements LogoutHandler {

    protected AuditLogService auditLogService = ServiceLocator.getInstance().getAuditLogService();

    @Override
    public void logout(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {
        //    	Method will set current time as logout time
        User user = AuthorityUtil.getUser();
        String sessionAge = (String) request.getAttribute("SessionMaxAge");
        SecurityContextHolder.getContext().setAuthentication(null);
        if (user != null) {
            auditLogService.logAction(user, user, DSModel.AuditLog.Action.LOGOFF,
                                      "User '" + user.getLoginname() + "', user has logged off" +
                                      ("true".equals(sessionAge) ? " (auto due session max age exceeded)" : ""));

            HttpSession sess = request.getSession(false);
            if (sess != null) {
                sess.setAttribute("Logout", "true");
                try {
                    sess.invalidate();
                } catch (Exception dc) {
                }
            }
        }
        String contextPath = request.getContextPath();
        response.setHeader("Set-Cookie", "JSESSIONID=; Path=\"" + contextPath + "\"; " +
                                         "Expires=Thu, 01 Jan 1970 00:00:00 GMT; HttpOnly");
        response.setHeader("Connection", "close");
    }
}
