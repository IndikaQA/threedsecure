/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 23.05.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.mngr.support;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.mngr.controller.BaseController;
import com.modirum.ds.services.ConfigService;
import com.modirum.ds.services.LocalizationService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.util.RegexUtil;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Locale;
import java.util.Random;
import java.util.regex.Pattern;

public class Helpers {
    private static final Logger log = LoggerFactory.getLogger(Helpers.class);

    // based from https://www.ietf.org/rfc/rfc3986.txt - standard for URL creation
    private static final String RFC_3986_CHARACTERS =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=\\r\\n";

    public static boolean isPhone(String phone) {
        if (phone == null || phone.length() < 4) {
            return false;
        }

        String p1 = Misc.replace(phone, " ", "");
        p1 = Misc.replace(p1, "+", "");
        if (p1.length() < 4 || p1.length() > 18) {
            return false;
        }

        return Misc.isNumber(p1);
    }

    public final static String genRndNumPassword(int len) {
        if (len > 60) {
            len = 60;
        }
        //AZ(41-5a/65-90) az (61-7A/97-122) 09 (30-39/48-57)
        StringBuffer pass = new StringBuffer(len);
        String random = "";

        Random rnd = new Random();
        for (int i = 0; i < len; i++) {
            redo:
            for (; true; ) {
                random = "" + (rnd.nextInt(9));
                // if good character
                // check if unique or not last

                if (pass.indexOf(random) < 0 || pass.indexOf(random) != pass.length() - 1) {
                    pass.append(random);
                    break;
                } else {
                    continue redo;
                }

            }

        } // for

        return pass.toString();
    }

    public static boolean isLoginLengthValid(String login, ConfigService cnfs, LocalizationService ls, Locale locale, BindingResult result) {
        int loginMinLength = 1;
        String loginMinLengthSetting = cnfs.getStringSetting(DsSetting.MNGR_USER_NAME_MINLENGTH.getKey());
        if (loginMinLengthSetting != null && Misc.isInRange(loginMinLengthSetting, loginMinLength, 16)) {
            loginMinLength = Misc.parseInt(loginMinLengthSetting);
        }
        int unl = Misc.defaultString(login).length();
        if (unl < loginMinLength || unl > 32) {
            result.addError(new FieldError("user", "login", login, false, null, null,
                                           //"Username length must be in range " + loginMinLength + "..32"));
                                           ls.getText("mn.err.username.length.range",
                                                      new String[]{"" + loginMinLength, "" + 32}, locale)));
            return false;
        }
        return true;
    }


    public static boolean isPasswordAsRequired(String password, String pFieldName, ConfigService cnfs, LocalizationService ls, Locale locale, BindingResult result) {
        boolean a = isPasswordComplexEnough(password, pFieldName, cnfs, ls, locale, result);
        boolean b = isPasswordMinLengthValid(password, pFieldName, cnfs, ls, locale, result);

        return a && b;
    }

    public static boolean isPasswordMinLengthValid(String password, String pFieldName, ConfigService cnf, LocalizationService ls, Locale locale, BindingResult result) {
        int passwordMinLength = 8;
        String passwordMinLengthSetting = cnf.getStringSetting(DsSetting.MNGR_USER_PASSWORD_MINLENGTH.getKey());
        if (passwordMinLengthSetting != null && Misc.isInRange(passwordMinLengthSetting, passwordMinLength, 32)) {
            passwordMinLength = Misc.parseInt(passwordMinLengthSetting);
        }
        if (Misc.defaultString(password).length() < passwordMinLength) {
            result.addError(new FieldError("user", pFieldName,
                                           //"Password must be at least " + passwordMinLength + " characters"));
                                           ls.getText("mn.err.password.length", new String[]{"" + passwordMinLength},
                                                      locale)));
            return false;
        }
        return true;
    }

    public static boolean isPasswordDiffOk(String oldPassword, String newPassword, String pFieldName, ConfigService cnf, LocalizationService ls, Locale locale, BindingResult result) {
        int passwordDiff = 20;
        String passwordMinDiffSetting = cnf.getStringSetting(DsSetting.MNGR_USER_PASSWORD_DIFF.getKey());

        if (passwordMinDiffSetting != null && Misc.isInRange(passwordMinDiffSetting, 10, 100)) {
            passwordDiff = Misc.parseInt(passwordMinDiffSetting);
        }

        int diff = Levenshtein.sameLengthDistance(oldPassword, newPassword, true);
        if (diff * 100 / newPassword.length() < passwordDiff) {
            result.addError(new FieldError("user", pFieldName,
                                           ls.getText("mn.err.password.mindiff", new String[]{"" + passwordDiff},
                                                      locale)));
            return false;
        }
        return true;
    }


    public static boolean isPasswordComplexEnough(String password, String pFieldName, ConfigService cnfs, LocalizationService ls, Locale locale, BindingResult result) {
        PasswordSymbolCategory lowercaseCategory = createPasswordSymbolCategory("lowercase", cnfs);
        PasswordSymbolCategory uppercaseCategory = createPasswordSymbolCategory("uppercase", cnfs);
        PasswordSymbolCategory numbersCategory = createPasswordSymbolCategory("numbers", cnfs);
        PasswordSymbolCategory symbolsCategory = createPasswordSymbolCategory("symbols", cnfs);
        symbolsCategory.setCustomSymbols(cnfs.getStringSetting(DsSetting.MNGR_USER_PASSWORD_SYMBOLS.getKey()));
        int complexitiesRequired = Misc.parseInt(cnfs.getStringSetting(DsSetting.MNGR_USER_PASSWORD_COMPLEXITIES_REQUIRED.getKey()));

        PasswordSymbolCategory[] categories = new PasswordSymbolCategory[]{lowercaseCategory, uppercaseCategory, numbersCategory, symbolsCategory};
        PasswordSymbolCategory latestCategoryMet = null;

        int settings = 0;
        for (PasswordSymbolCategory c : categories) {
            settings += c.getMinimumRequired();
            settings += c.getMaximumSequence();
        }
        // default behaviour
        if (settings == 0) {
            Pattern digit = Pattern.compile("[\\d]+");
            Pattern alpha = Pattern.compile("[a-zA-Z]+");
            if (digit.matcher(password).matches() || alpha.matcher(password).matches()) {
                result.addError(new FieldError("user", pFieldName, ls.getText("mn.err.password.content", locale)));
                return false;
            }

            return true;
        }

        boolean isOk = true;
        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);
            String msg = null;
            if (Character.isLowerCase(c)) {
                msg = handleCategorySequence(latestCategoryMet = lowercaseCategory);
            } else if (Character.isUpperCase(c)) {
                msg = handleCategorySequence(latestCategoryMet = uppercaseCategory);

            } else if (Character.isDigit(c)) {
                msg = handleCategorySequence(latestCategoryMet = numbersCategory);
            } else if (symbolsCategory.isSymbol(c)) {
                msg = handleCategorySequence(latestCategoryMet = symbolsCategory);

            } else {
                latestCategoryMet = null;
            }
            restartOtherCategoriesSequences(categories, latestCategoryMet);

            if (msg != null && latestCategoryMet != null) {
                //result.addError(new FieldError("user", pFieldName, msg));
                isOk = false;
            }
        }
        // must be after sequences checks
        int complexitiesFound = 0;
        for (PasswordSymbolCategory category : categories) {
            if (category.minimumRequirementsFulfilled()) {
                complexitiesFound++;
            }
        }


        int mexSeqDefined = 0;
        if (complexitiesRequired > 0 && (complexitiesFound < complexitiesRequired || !isOk)) {
            StringBuilder catsString = new StringBuilder();
            for (PasswordSymbolCategory category : categories) {
                if (category.getMaximumSequence() > 0) {
                    mexSeqDefined++;
                }

                if (category.getMinimumRequired() > 0) {
                    if (catsString.length() > 0) {
                        catsString.append(", ");
                    }
                    catsString.append(
                            (category.getMinimumRequired() > 1 ? "" + category.getMinimumRequired() : "") + " " +
                            category.getCategoryName());
                }
            }
            result.addError(new FieldError("user", pFieldName,
                                           //"Password must contain at least "+complexitiesRequired+" of the following categories: "+catsString));
                                           ls.getText("mn.err.password.complexities",
                                                      new String[]{"" + complexitiesRequired, catsString.toString()},
                                                      locale)));

            isOk = false;
        }

        // combined sequences message CR144
        if (!isOk && mexSeqDefined > 0) {
            StringBuilder cmsg = new StringBuilder(128);
            //cmsg.append("Maximum consecutive sequence");
            cmsg.append(ls.getText("mn.err.password.consecutive.sequence1", locale));

            int conds = 0;
            if (lowercaseCategory.getMaximumSequence() == uppercaseCategory.getMaximumSequence() &&
                uppercaseCategory.getMaximumSequence() == numbersCategory.getMaximumSequence()) {
                //cmsg.append(" for lowercase, uppercase and numbers is "+lowercaseCategory.getMaximumSequence()+" characters");
                cmsg.append(ls.getText("mn.err.password.consecutive.lowercase.uppercase.and.numbers.is",
                                       "" + lowercaseCategory.getMaximumSequence(), locale));
                conds++;
            } else if (lowercaseCategory.getMaximumSequence() == uppercaseCategory.getMaximumSequence()) {
                //cmsg.append(" for lowercase, uppercase  is "+lowercaseCategory.getMaximumSequence()+" characters");
                cmsg.append(ls.getText("mn.err.password.consecutive.lowercase.uppercase.is",
                                       "" + lowercaseCategory.getMaximumSequence(), locale));
                conds++;
                if (numbersCategory.getMaximumSequence() > 0) {
                    //cmsg.append(" and for numbers is "+numbersCategory.getMaximumSequence()+" characters");
                    cmsg.append(ls.getText("mn.err.password.consecutive.and.for.numbers.is",
                                           "" + numbersCategory.getMaximumSequence(), locale));
                    conds++;
                }
            } else if (lowercaseCategory.getMaximumSequence() == numbersCategory.getMaximumSequence()) {
                //cmsg.append(" for lowercase, numbers is "+lowercaseCategory.getMaximumSequence()+" characters");
                cmsg.append(ls.getText("mn.err.password.consecutive.for.lowercase.numbers.is",
                                       "" + lowercaseCategory.getMaximumSequence(), locale));
                conds++;
                if (uppercaseCategory.getMaximumSequence() > 0) {
                    //cmsg.append(" and for uppercase  is "+uppercaseCategory.getMaximumSequence()+" characters");
                    cmsg.append(ls.getText("mn.err.password.consecutive.and.for.uppercase.is",
                                           "" + uppercaseCategory.getMaximumSequence(), locale));
                    conds++;
                }
            } else if (uppercaseCategory.getMaximumSequence() == numbersCategory.getMaximumSequence()) {
                //cmsg.append(" for uppercase, numbers  is "+uppercaseCategory.getMaximumSequence()+" characters");
                cmsg.append(ls.getText("mn.err.password.consecutive.for.uppercase.numbers.is",
                                       "" + uppercaseCategory.getMaximumSequence(), locale));
                conds++;
                if (lowercaseCategory.getMaximumSequence() > 0) {
                    //cmsg.append(" and for lowercase  is "+lowercaseCategory.getMaximumSequence()+" characters");
                    cmsg.append(ls.getText("mn.err.password.consecutive.and.for.lowercase.is",
                                           "" + lowercaseCategory.getMaximumSequence(), locale));
                    conds++;
                }
            } else {
                int lcc = 0;
                if (lowercaseCategory.getMaximumSequence() > 0) {
                    //cmsg.append(" for lowercase is "+lowercaseCategory.getMaximumSequence());
                    cmsg.append(ls.getText("mn.err.password.consecutive.for.lowercase.is",
                                           "" + lowercaseCategory.getMaximumSequence(), locale));
                    lcc++;
                }

                if (uppercaseCategory.getMaximumSequence() > 0) {
                    if (lcc > 0) {
                        cmsg.append(", ");
                    }
                    //cmsg.append(" for uppercase  is "+uppercaseCategory.getMaximumSequence());
                    cmsg.append(ls.getText("mn.err.password.consecutive.for.uppercase.is",
                                           "" + uppercaseCategory.getMaximumSequence(), locale));
                    lcc++;
                }

                if (numbersCategory.getMaximumSequence() > 0) {
                    if (lcc > 0) {
                        cmsg.append(", ");
                    }

                    //cmsg.append(" for numbers is "+numbersCategory.getMaximumSequence());
                    cmsg.append(ls.getText("mn.err.password.consecutive.for.numbers.is",
                                           "" + numbersCategory.getMaximumSequence(), locale));
                    lcc++;
                }

                if (lcc > 0) {
                    //cmsg.append(" characters");
                    cmsg.append(ls.getText("mn.err.password.characters", locale));
                }

                conds++;
            }
            if (symbolsCategory.getMaximumSequence() > 0) {
                //cmsg.append((conds >0 ? " and " : "")+" for symbols is "+symbolsCategory.getMaximumSequence() +" characters");
                cmsg.append((conds > 0 ? ls.getText("mn.err.password.and", locale) : "") +
                            ls.getText("mn.err.password.consecutive.for.symbols.is",
                                       "" + symbolsCategory.getMaximumSequence(), locale));
            }

            result.addError(new FieldError("user", pFieldName, cmsg.toString()));
        }

        return isOk;
    }

    static PasswordSymbolCategory createPasswordSymbolCategory(String categoryName, ConfigService settingService) {
        int minimumRequired = Misc.parseInt(
                settingService.getStringSetting("mngr.user.password." + categoryName + ".min.required"));
        int maximumSequence = Misc.parseInt(
                settingService.getStringSetting("mngr.user.password." + categoryName + ".max.sequence"));
        return new PasswordSymbolCategory(categoryName, minimumRequired, maximumSequence);
    }

    static void restartOtherCategoriesSequences(PasswordSymbolCategory[] categories, PasswordSymbolCategory ignoredCategory) {
        for (PasswordSymbolCategory rollingCategory : categories) {
            if (ignoredCategory == null || !ignoredCategory.equals(rollingCategory)) {
                rollingCategory.restartSequence();
            }
        }
    }

    static String handleCategorySequence(PasswordSymbolCategory category) {
        if (category == null) {
            return null;
        }
        category.increment();
        return category.checkMaximumSequence();
    }


    public static Locale getDefaultLocale() {

        return getDefaultLocale(ServiceLocator.getInstance().getConfigService().getStringSetting(DsSetting.MNGR_LOCALE.getKey()));
    }

    public static java.util.Locale getDefaultLocale(String defLoc) {
        java.util.Locale defaultLocale = (java.util.Locale) BaseController.getSessionAttribute("dafaultlocale");

        if (defaultLocale != null) {
            return defaultLocale;
        }

        java.util.Locale[] llst = java.util.Locale.getAvailableLocales();

        if (defLoc != null && defLoc.length() == 5) {
            String lng = defLoc.substring(0, 2);
            String cntry = defLoc.substring(3);

            for (int i = 0; llst != null && i < llst.length; i++) {
                if (llst[i] != null && lng.equals(llst[i].getLanguage())) {
                    if (cntry.equals(llst[i].getCountry())) {
                        defaultLocale = llst[i];
                        break;
                    } else if (defaultLocale == null) {
                        defaultLocale = llst[i];
                    }
                }
            } // for
        } // if

        if (defaultLocale == null) {
            defaultLocale = java.util.Locale.US;
        }

        BaseController.setSessionAttribute("dafaultlocale", defaultLocale);

        return defaultLocale;
    }

    public static boolean isValidUrl(String url) {
        if(!url.startsWith("embed:")) {
            try {
                new java.net.URL(url);
            } catch (Exception e) {
                return false;
            } 
        }
        return RegexUtil.validAllowedCharacters(url, RFC_3986_CHARACTERS);
    }

}
