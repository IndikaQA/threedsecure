package com.modirum.ds.mngr.service;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.TDSMessageData;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.enums.TransactionAttribute;
import com.modirum.ds.jdbc.core.model.TdsRecordAttribute;
import com.modirum.ds.json.JsonUtil;
import com.modirum.ds.mngr.model.CardholderAddressExportFields;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.mngr.model.ErrorMessageExportFields;
import com.modirum.ds.mngr.model.TdsMessageInOutDatetimeFields;
import com.modirum.ds.model.threeds.ErrorMessage;
import com.modirum.ds.model.threeds.MerchantRiskIndicator;
import com.modirum.ds.model.threeds.MessageExtension;
import com.modirum.ds.model.threeds.MobilePhone;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.JsonMessageService;
import com.modirum.ds.services.LocalizationService;
import com.modirum.ds.services.TdsRecordAttributeService;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.util.DsStringUtils;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.ISO3166;
import com.modirum.ds.utils.ISO4217;
import com.modirum.ds.utils.Misc;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

@Service
public class TdsRecordExportService {
    public static final String CSV_DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ssZ";
    public static final String CSV_DATE_FORMAT = "MM/dd/yyyy";
    public static final String CSV_TIME_FORMAT = "HH:mm:ss";
    private final String DIRECTORY_SERVER_ID_KEY = "Directory Server ID";

    private TimeZone timeZone;
    private Locale locale;
    private final Logger log = LoggerFactory.getLogger(TdsRecordExportService.class);
    private final Map<Integer, String> paymentSystemsMap = new LinkedHashMap<>();

    private PersistenceService persistenceService = ServiceLocator.getInstance().getPersistenceService();
    private LocalizationService localizationService = ServiceLocator.getInstance().getLocalizationService();
    private JsonMessageService jsonMessageService = ServiceLocator.getInstance().getJsonMessageService();
    @Autowired
    private CryptoService cryptoService;
    @Autowired
    private TdsRecordAttributeService tdsRecordAttributeService;

    public final static String[] HEADERS = new String[]{
            "PAYMENT_SYSTEM_NAME",
            "TRANSACTION_DATE",
            "TRANSACTION_TIME",
            "RECORD_ID",
            "PROTOCOL",
            "DEVICE_CHANNEL",
            "MESSAGE_CATEGORY",
            "TRANSACTION_TYPE",
            "PAN_BIN",
            "PAN_LAST4",
            "CARD_TOKEN_EXP_DATE",
            "CARDHOLDER_IP_ADDRESS",
            "CARDHOLDER_NAME",
            "CARDHOLDER_BILL_ADDR_CITY",
            "CARDHOLDER_BILL_ADDR_COUNTRY",
            "CARDHOLDER_BILL_ADDR_LINE1",
            "CARDHOLDER_BILL_ADDR_LINE2",
            "CARDHOLDER_BILL_ADDR_LINE3",
            "CARDHOLDER_BILL_ADDR_POSTAL_CODE",
            "CARDHOLDER_BILL_ADDR_STATE",
            "CARDHOLDER_SHIP_ADDR_CITY",
            "CARDHOLDER_SHIP_ADDR_COUNTRY",
            "CARDHOLDER_SHIP_ADDR_LINE1",
            "CARDHOLDER_SHIP_ADDR_LINE2",
            "CARDHOLDER_SHIP_ADDR_LINE3",
            "CARDHOLDER_SHIP_ADDR_POSTAL_CODE",
            "CARDHOLDER_SHIP_ADDR_STATE",
            "CARDHOLDER_MOBILE_PHONE_NUMBER",
            "ACCT_TYPE",
            "AMOUNT",
            "CURRENCY",
            "EXPONENT",
            "TOTAL",
            "PURCHASE_DATE_AND_TIME",
            "RECURRING_EXPIRY",
            "RECURRING_FREQUENCY",
            "ECI",
            "ACQUIRER",
            "ACQUIRER_BIN",
            "MERCHANT_NAME",
            "MERCHANT_ID",
            "MERCHANT_COUNTRY",
            "MERCHANT_COUNTRY_NAME",
            "MERCHANT_CATEGORY_CODE",
            "MERCHANT_RISK_IND",
            "SDK_TRANS_ID",
            "SDK_APP_ID",
            "3DSS_REF_NUMBER",
            "3DS_TRANS_ID",
            "3DS_URL",
            "3DS_REQ_AUTH_IND",
            "3DS_SERVER_OPER_ID",
            "3RI_IND",
            "MERCHANT_URL",
            "REQUESTOR_ID",
            "REQUESTOR_NAME",
            "ISSUER",
            "ISSUER_BIN",
            "DS_TRANS_ID",
            "DS_REF_NUMBER",
            "DS_URL",
            "DS_START_PROTOCOL_VER",
            "DS_END_PROTOCOL_VER",
            "DIRECTORY_SERVER_ID",
            "ACS_REF_NUMBER",
            "ACS_TRANS_ID",
            "ACS_URL",
            "ACS_OPERATOR_ID",
            "AUTH_METHOD",
            "AUTH_TYPE",
            "AUTH_VALUE",
            "RESULTS_STATUS",
            "INTERACTION_CTR",
            "CHALLENGE_CANCEL_IND",
            "EMV_PAYMENT_TOKEN_IND",
            "EMV_PAYMENT_TOKEN_IND_SOURCE",
            "SDK_REF_NUMBER",
            "PREQ_MESSAGE_DATE",
            "PRES_MESSAGE_DATE",
            "AREQ_IN_DATETIME",
            "AREQ_OUT_DATETIME",
            "ARES_IN_DATETIME",
            "ARES_OUT_DATETIME",
            "RREQ_IN_DATETIME",
            "RREQ_OUT_DATETIME",
            "RRES_IN_DATETIME",
            "RRES_OUT_DATETIME",
            "TRANSACTION_STATUS_CODE",
            "TRANSACTION_STATUS",
            "TRANSACTION_STATUS_REASON_CODE",
            "TRANSACTION_STATUS_REASON",
            "WHITELIST_STATUS",
            "WHITELIST_STATUS_SOURCE",
            "FINAL_STATUS",
            "COMPLETED",
            "MESSAGE_EXTENSION",
            "ERROR_COMPONENT",
            "ERROR_MESSAGE_TYPE",
            "ERROR_CODE",
            "ERROR_DESCRIPTION",
            "ERROR_DETAILS"
    };

    /**
     * Writes TDS Record export CSV
     * @param writer              CSV writer
     * @param tdsRecordIdList     list of TDS records to export
     * @param timeZone            time zone used for record timestamps
     * @param locale              locale used for record descriptions
     * @throws Exception
     */
    public void writeCsv(Writer writer, List<Long> tdsRecordIdList, TimeZone timeZone, Locale locale) throws Exception {
        this.timeZone = timeZone;
        this.locale = locale;
        initPaymentSystemMappings();

        try {
            log.debug("TdsRecordExportService writing csv starting ...");

            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                    .withRecordSeparator(System.lineSeparator())
                    .withHeader(HEADERS)
                    .withDelimiter(','));

            if (Misc.isNotNullOrEmpty(tdsRecordIdList)) {
                for (Long tdsRecordId : tdsRecordIdList) {
                    csvPrinter.printRecord(buildRecord(tdsRecordId));
                }
            }

            csvPrinter.flush();

            log.debug("TdsRecordExportService writing csv ended");
        } catch (Exception e) {
            log.error("TdsRecordExportService encountered an error while writing csv.", e);
            throw e;
        }
    }

    private Object[] buildRecord(Long tdsRecordId) throws Exception {
        TDSRecord item = persistenceService.getPersistableById(tdsRecordId, TDSRecord.class);

        List<TDSMessageData> tdsMessageDataList = persistenceService.getTDSMessagesByRecordId(tdsRecordId);
        tdsMessageDataList = Optional.ofNullable(tdsMessageDataList)
                .orElse(Collections.emptyList());

        TdsMessageInOutDatetimeFields tdsMessageInOutDatetimeFields = new TdsMessageInOutDatetimeFields();

        List<TDSMessageData> errorMessageDataList = new ArrayList<>(tdsMessageDataList.size());

        String directoryServerID = null;
        String threeDSRequestorAuthenticationInd = null;
        String threeDSServerOperatorId = null;
        String threeRIInd = null;
        String acctType = null;
        CardholderAddressExportFields billingAddress = new CardholderAddressExportFields();;
        CardholderAddressExportFields shippingAddress = new CardholderAddressExportFields();
        String cardholderName = null;
        MobilePhone mobilePhone = null;
        String challengeCancel = null;
        String dsReferenceNumber = null;
        String dsStartProtocolVersion = null;
        String dsEndProtocolVersion = null;
        String dsURL= null;
        String payTokenInd = null;
        String payTokenSource = null;
        String interactionCounter = null;
        List<MessageExtension> incomingAReqMessageExtension = null;
        List<MessageExtension> outgoingAReqMessageExtension = null;
        List<MessageExtension> incomingAResMessageExtension = null;
        List<MessageExtension> outgoingAResMessageExtension = null;
        List<MessageExtension> incomingRReqMessageExtension = null;
        List<MessageExtension> outgoingRReqMessageExtension = null;
        List<MessageExtension> incomingRResMessageExtension = null;
        List<MessageExtension> outgoingRResMessageExtension = null;
        List<MessageExtension> pReqMessageExtension = null;
        List<MessageExtension> pResMessageExtension = null;
        MerchantRiskIndicator merchantRiskIndicator = null;
        String whitelistStatus = null;
        String whitelistStatusSource = null;

        for (TDSMessageData tdsMessageData : tdsMessageDataList) {

            String formattedDatetime = DateUtil.formatDate(tdsMessageData.getMessageDate(), CSV_DATE_TIME_FORMAT, timeZone);
            JsonMessage jsonMessage = null;

            try {
                String messageContents = cryptoService.decryptData(tdsMessageData.getContents());
                jsonMessage = jsonMessageService.parse(messageContents);
            } catch (Exception e) {
                log.debug("Cannot extract message contents for for TDS Message ID {}", tdsMessageData.getId());
            }

            List<MessageExtension> messageExtension = null;
            if (jsonMessage != null) {
                messageExtension = getMessageExtension(tdsMessageData.getId(), jsonMessage);
                if (StringUtils.isEmpty(directoryServerID)) {
                    directoryServerID = getDirectoryServerID(messageExtension);
                }
            }

            TDSModel.XmessageType xmessageType = getXmessageType(tdsMessageData);

            if (xmessageType == null) {
                continue;
            }

            switch (xmessageType) {
                case P_REQ:
                    tdsMessageInOutDatetimeFields.setPreqMessageDatetime(formattedDatetime);
                    pReqMessageExtension = messageExtension;
                    break;
                case P_RES:
                    tdsMessageInOutDatetimeFields.setPresMessageDatetime(formattedDatetime);
                    pResMessageExtension = messageExtension;
                    if (jsonMessage != null) {
                        dsStartProtocolVersion = jsonMessage.getDSStartProtocolVersion();
                        dsEndProtocolVersion = jsonMessage.getDSEndProtocolVersion();
                    }
                    break;
                case A_REQ:
                    if (tdsMessageData.getSourceIP().startsWith("3DS")) {
                        tdsMessageInOutDatetimeFields.setAreqInDatetime(formattedDatetime);
                        incomingAReqMessageExtension = messageExtension;
                        if (jsonMessage != null) {
                            threeDSRequestorAuthenticationInd = jsonMessage.getThreeDSRequestorAuthenticationInd();
                            threeDSServerOperatorId = jsonMessage.getThreeDSServerOperatorID();
                            threeRIInd = jsonMessage.getThreeRIInd();
                            acctType = jsonMessage.getAcctType();
                            billingAddress = new CardholderAddressExportFields(jsonMessage.getBillAddrCity(),
                                    jsonMessage.getBillAddrCountry(), jsonMessage.getBillAddrLine1(),
                                    jsonMessage.getBillAddrLine2(), jsonMessage.getBillAddrLine3(),
                                    jsonMessage.getBillAddrPostCode(), jsonMessage.getBillAddrState());
                            shippingAddress = new CardholderAddressExportFields(jsonMessage.getShipAddrCity(),
                                    jsonMessage.getShipAddrCountry(), jsonMessage.getShipAddrLine1(),
                                    jsonMessage.getShipAddrLine2(), jsonMessage.getShipAddrLine3(),
                                    jsonMessage.getShipAddrPostCode(), jsonMessage.getShipAddrState());
                            cardholderName = jsonMessage.getCardholderName();
                            mobilePhone = jsonMessage.getMobilePhone();
                            merchantRiskIndicator = jsonMessage.getMerchantRiskIndicator();
                        }
                    } else if (tdsMessageData.getSourceIP().startsWith("DS")) {
                        tdsMessageInOutDatetimeFields.setAreqOutDatetime(formattedDatetime);
                        outgoingAReqMessageExtension = messageExtension;
                        if (jsonMessage != null) {
                            if (dsReferenceNumber == null) {
                                dsReferenceNumber = jsonMessage.getDSReferenceNumber();
                            }
                            dsURL = jsonMessage.getDSURL();
                        }
                    }
                    if (jsonMessage != null) {
                        if (payTokenInd == null) {
                            payTokenInd = jsonMessage.getPayTokenInd();
                        }
                        if (payTokenSource == null) {
                            payTokenSource = jsonMessage.getPayTokenSource();
                        }
                        if (whitelistStatus == null) {
                            whitelistStatus = jsonMessage.getWhitelistStatus();
                        }
                        if (whitelistStatusSource == null) {
                            whitelistStatusSource = jsonMessage.getWhiteListStatusSource();
                        }
                    }
                    break;
                case A_RES:
                    if (tdsMessageData.getSourceIP().startsWith("ACS")) {
                        tdsMessageInOutDatetimeFields.setAresInDatetime(formattedDatetime);
                        incomingAResMessageExtension = messageExtension;
                    } else if (tdsMessageData.getSourceIP().startsWith("DS")) {
                        tdsMessageInOutDatetimeFields.setAresOutDatetime(formattedDatetime);
                        outgoingAResMessageExtension = messageExtension;
                    }
                    if (jsonMessage != null) {
                        if (whitelistStatus == null) {
                            whitelistStatus = jsonMessage.getWhitelistStatus();
                        }
                        if (whitelistStatusSource == null) {
                            whitelistStatusSource = jsonMessage.getWhiteListStatusSource();
                        }
                    }
                    break;
                case R_REQ:
                    if (tdsMessageData.getSourceIP().startsWith("ACS")) {
                        tdsMessageInOutDatetimeFields.setRreqInDatetime(formattedDatetime);
                        incomingRReqMessageExtension = messageExtension;
                        if (jsonMessage != null && interactionCounter == null) {
                            interactionCounter = jsonMessage.getInteractionCounter();
                        }
                    } else if (tdsMessageData.getSourceIP().startsWith("DS")) {
                        tdsMessageInOutDatetimeFields.setRreqOutDatetime(formattedDatetime);
                        outgoingRReqMessageExtension = messageExtension;
                    }
                    if (jsonMessage != null) {
                        if (challengeCancel == null) {
                            challengeCancel = jsonMessage.getChallengeCancel();
                        }
                        if (whitelistStatus == null) {
                            whitelistStatus = jsonMessage.getWhitelistStatus();
                        }
                        if (whitelistStatusSource == null) {
                            whitelistStatusSource = jsonMessage.getWhiteListStatusSource();
                        }
                    }
                    break;
                case R_RES:
                    if (tdsMessageData.getSourceIP().startsWith("3DS")) {
                        tdsMessageInOutDatetimeFields.setRresInDatetime(formattedDatetime);
                        incomingRResMessageExtension = messageExtension;
                    } else if (tdsMessageData.getSourceIP().startsWith("DS")) {
                        tdsMessageInOutDatetimeFields.setRresOutDatetime(formattedDatetime);
                        outgoingAResMessageExtension = messageExtension;
                    }
                    break;
                case ERRO:
                    errorMessageDataList.add(tdsMessageData);
                    break;
                default:
            }

        }

        String transStatusReasonDescription = "";
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(item.getTransStatusReason())) {
            transStatusReasonDescription = TDSModel.XtransStatusReason.contains(item.getTransStatusReason()) ?
                    TDSModel.XtransStatusReason.getTextFromValue(item.getTransStatusReason()) :
                    "Unknown transStatusReason";
        }

        String finalStatus = "";
        if (item.getLocalStatus() != null) {
            String localStatusText = localizationService.getText( "ar.localstatus." + item.getLocalStatus(), locale);
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(localStatusText)) {
                finalStatus = localStatusText;
            }
        }

        Acquirer acquirer = persistenceService.getAcquirerByBINAndPaymentSystem(item.getAcquirerBIN(), item.getPaymentSystemId());
        String acquirerName = acquirer != null ? acquirer.getName() : null;

        TdsRecordAttribute tdsServerUrlAttribute = tdsRecordAttributeService
                .getAttributeByTDSRecordIDAndKey(tdsRecordId, TransactionAttribute.TDS_SERVER_URL.getKey());
        String tdsServerUrl = tdsServerUrlAttribute != null ? tdsServerUrlAttribute.getAsciiValue() : null;

        Issuer issuer = item.getLocalIssuerId() != null ? persistenceService.getPersistableByIdUnchecked(item.getLocalIssuerId(), Issuer.class) : null;
        String issuerName = issuer != null ? issuer.getName() : null;

        ErrorMessageExportFields errorMessageFields = new ErrorMessageExportFields(null, null,null, null, null);
        if (!errorMessageDataList.isEmpty()) {
            TDSMessageData tdsMessageData = errorMessageDataList.get(0);
            String contents = cryptoService.decryptData(tdsMessageData.getContents());
            ErrorMessage errorMessage = jsonMessageService.convertIgnoreUnknownProperties(contents, ErrorMessage.class);
            errorMessageFields = new ErrorMessageExportFields(errorMessage.getErrorComponent(), errorMessage.getErrorMessageType(),
                    errorMessage.getErrorCode(), errorMessage.getErrorDescription(), errorMessage.getErrorDetail());
        }
        return new Object[]{
                paymentSystemsMap.get(item.getPaymentSystemId()),
                DateUtil.formatDate(item.getLocalDateStart(), CSV_DATE_FORMAT, timeZone),
                DateUtil.formatDate(item.getLocalDateStart(), CSV_TIME_FORMAT, timeZone),
                item.getId(),
                item.getProtocol(),
                item.getDeviceChannel(),
                item.getMessageCategory(),
                item.getTransType(),
                DsStringUtils.extractFirst(6, item.getAcctNumber()),
                DsStringUtils.extractLast(4, item.getAcctNumber()),
                item.getCardExpiryDate(),
                item.getBrowserIP(),
                cardholderName,
                billingAddress.getAddrCity(),
                billingAddress.getAddrCountry(),
                billingAddress.getAddrLine1(),
                billingAddress.getAddrLine2(),
                billingAddress.getAddrLine3(),
                billingAddress.getAddrPostCode(),
                billingAddress.getAddrState(),
                shippingAddress.getAddrCity(),
                shippingAddress.getAddrCountry(),
                shippingAddress.getAddrLine1(),
                shippingAddress.getAddrLine2(),
                shippingAddress.getAddrLine3(),
                shippingAddress.getAddrPostCode(),
                shippingAddress.getAddrState(),
                getPhoneNumber(mobilePhone),
                acctType,
                item.getPurchaseAmount(),
                item.getPurchaseCurrency(),
                item.getPurchaseExponent(),
                formatTotalWithCurrency(item.getPurchaseAmount(), item.getPurchaseExponent(), item.getPurchaseCurrency()),
                DateUtil.formatDate(item.getPurchaseDate(), CSV_DATE_TIME_FORMAT, timeZone),
                item.getRecurringExpiry(),
                item.getRecurringFrequency(),
                item.getECI(),
                acquirerName,
                item.getAcquirerBIN(),
                item.getMerchantName(),
                item.getAcquirerMerchantID(),
                item.getMerchantCountry(),
                getCountryName(item.getMerchantCountry()),
                item.getMCC(),
                merchantRiskIndicator != null ? JsonUtil.toJsonString(merchantRiskIndicator) : null,
                item.getSDKTransID(),
                item.getSDKAppID(),
                item.getMIReferenceNumber(),
                item.getTdsTransID(),
                tdsServerUrl,
                threeDSRequestorAuthenticationInd,
                threeDSServerOperatorId,
                threeRIInd,
                item.getRequestorURL(),
                item.getRequestorID(),
                item.getRequestorName(),
                issuerName,
                item.getIssuerBIN(),
                item.getDSTransID(),
                dsReferenceNumber,
                dsURL,
                dsStartProtocolVersion,
                dsEndProtocolVersion,
                directoryServerID,
                item.getACSRefNo(),
                item.getACSTransID(),
                item.getACSURL(),
                item.getACSOperatorID(),
                item.getAuthenticationMethod(),
                item.getAuthenticationType(),
                item.getAuthenticationValue(),
                item.getResultsStatus(),
                interactionCounter,
                challengeCancel,
                payTokenInd,
                payTokenSource,
                item.getSDKReferenceNumber(),
                tdsMessageInOutDatetimeFields.getPreqMessageDatetime(),
                tdsMessageInOutDatetimeFields.getPresMessageDatetime(),
                tdsMessageInOutDatetimeFields.getAreqInDatetime(),
                tdsMessageInOutDatetimeFields.getAreqOutDatetime(),
                tdsMessageInOutDatetimeFields.getAresInDatetime(),
                tdsMessageInOutDatetimeFields.getAresOutDatetime(),
                tdsMessageInOutDatetimeFields.getRreqInDatetime(),
                tdsMessageInOutDatetimeFields.getRreqOutDatetime(),
                tdsMessageInOutDatetimeFields.getRresInDatetime(),
                tdsMessageInOutDatetimeFields.getRresOutDatetime(),
                item.getTransStatus(),
                TDSModel.XtransStatus.getTextFromValue(item.getTransStatus()),
                item.getTransStatusReason(),
                transStatusReasonDescription,
                whitelistStatus,
                whitelistStatusSource,
                finalStatus,
                DSModel.TSDRecord.Status.COMPLETED.equals(item.getLocalStatus()) ? "Yes" : "No",
                getMessageExtensionText(incomingAReqMessageExtension, outgoingAReqMessageExtension, incomingAResMessageExtension,
                        outgoingAResMessageExtension, incomingRReqMessageExtension, outgoingRReqMessageExtension,
                        incomingRResMessageExtension, outgoingRResMessageExtension, pReqMessageExtension, pResMessageExtension),
                errorMessageFields.getComponent(),
                errorMessageFields.getMessageType(),
                errorMessageFields.getCode(),
                errorMessageFields.getDescription(),
                errorMessageFields.getDetails()
        };
    }

    /**
     * Initializes the PaymentSystem mappings only once
     * as reference lookup for PAYMENT_SYSTEM_NAME column.
     * This will be used throughout the iteration of all csv records.
     */
    private void initPaymentSystemMappings() {
        List<PaymentSystem> paymentSystems = ServiceLocator.getInstance().getPaymentSystemService().getPaymentSystems();
        paymentSystems.forEach(paymentSystem -> paymentSystemsMap.put(paymentSystem.getId(), paymentSystem.getName()));
    }

    private TDSModel.XmessageType getXmessageType(TDSMessageData tdsMessageData) {
        try {
            return TDSModel.XmessageType.fromValue(tdsMessageData.getMessageType());
        } catch (IllegalArgumentException e) {
            log.debug("Retrieved TDSMessageData with messageType={}. This will be ignored in this csv extraction for now.", tdsMessageData.getMessageType());
            return null;
        }
    }

    private String getCountryName(Short code) {
        if (code != null) {
            try {
                ISO3166 country = ISO3166.findFromNumeric(code);
                return country != null ? country.getCountryName() : null;
            } catch (Exception e) {
                // country name cannot be extracted
                log.debug("Cannot extract country name for code {}", code);
            }
        }
        return null;
    }

    /**
     * Returns a string following this format: <final amount> <currency 3-letter code> (<currency numeric code>).
     * Computation of final amount and conversion of currency both follow ISO 4217.
     * For example, if amount = 100, exponent = 2, and currency = 840, the string returned will be "1.00 USD (840)".
     */
    private String formatTotalWithCurrency(Long amount, Short exponent, Short currency) {
        if (amount != null && exponent != null && currency != null) {
            try {
                double amountExponent = Math.pow(10, exponent);
                float finalAmount = (float) amount / (float) amountExponent;
                ISO4217 isoCurrency = ISO4217.findFromNumeric(currency);
                String currencyCode = isoCurrency.getA3code();
                return String.format("%." + exponent + "f %s (%d)", finalAmount, currencyCode, currency);
            } catch (Exception e) {
                // total cannot be formatted
                log.debug("Cannot format total for amount {} exponent {} currency {}", amount, exponent, currency);
            }
        }
        return null;
    }

    private List<MessageExtension> getMessageExtension(Long tdsMessageId, JsonMessage jsonMessage) {
        try {
            return Optional.ofNullable(jsonMessage.getMessageExtension())
                    .orElse(Collections.emptyList());
        } catch (Exception e) {
            log.debug("Cannot get message extension for TDS Message ID {}", tdsMessageId);
        }
        return Collections.emptyList();
    }

    private String getDirectoryServerID(List<MessageExtension> messageExtensions) {
        for (MessageExtension messageExtension : messageExtensions) {
            if (DIRECTORY_SERVER_ID_KEY.equals(messageExtension.getName())) {
                return messageExtension.getId();
            }
        }
        return null;
    }

    private String getPhoneNumber(MobilePhone mobilePhone) {
        String phoneNumber = null;
        if (mobilePhone != null && Misc.isNotNullOrEmpty(mobilePhone.getCc()) && Misc.isNotNullOrEmpty(mobilePhone.getSubscriber())) {
            return String.format("+%s%s", mobilePhone.getCc(), mobilePhone.getSubscriber());
        }
        return phoneNumber;
    }

    private void appendMessageExtension(StringBuilder builder, String label, List<MessageExtension> messageExtension) {
        if (CollectionUtils.isNotEmpty(messageExtension)) {
            builder.append(label).append(JsonUtil.toJsonString(messageExtension)).append("\n\n");
        }
    }

    private String getMessageExtensionText(List<MessageExtension> incomingAReqMessageExtension, List<MessageExtension> outgoingAReqMessageExtension,
                                           List<MessageExtension> incomingAResMessageExtension, List<MessageExtension> outgoingAResMessageExtension,
                                           List<MessageExtension> incomingRReqMessageExtension, List<MessageExtension> outgoingRReqMessageExtension,
                                           List<MessageExtension> incomingRResMessageExtension, List<MessageExtension> outgoingRResMessageExtension,
                                           List<MessageExtension> pReqMessageExtension, List<MessageExtension> pResMessageExtension) {
        StringBuilder builder = new StringBuilder();
        appendMessageExtension(builder, "Incoming AReq: ", incomingAReqMessageExtension);
        appendMessageExtension(builder, "Outgoing AReq: ", outgoingAReqMessageExtension);
        appendMessageExtension(builder, "Incoming ARes: ", incomingAResMessageExtension);
        appendMessageExtension(builder, "Outgoing ARes: ", outgoingAResMessageExtension);
        appendMessageExtension(builder, "Incoming RReq: ", incomingRReqMessageExtension);
        appendMessageExtension(builder, "Outgoing RReq: ", outgoingRReqMessageExtension);
        appendMessageExtension(builder, "Incoming RRes: ", incomingRResMessageExtension);
        appendMessageExtension(builder, "Outgoing RRes: ", outgoingRResMessageExtension);
        appendMessageExtension(builder, "PReq: ", pReqMessageExtension);
        appendMessageExtension(builder, "PRes: ", pResMessageExtension);
        return builder.toString();
    }
}
