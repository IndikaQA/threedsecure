package com.modirum.ds.mngr.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SessionAuthMaxAgeInterceptor extends org.springframework.web.servlet.handler.HandlerInterceptorAdapter {

    transient static Logger log = LoggerFactory.getLogger(SessionAuthMaxAgeInterceptor.class);

    private int maxAge = 36000;

    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws IOException, ServletException {

        HttpSession s = req.getSession(false);
        if (s != null && s.getCreationTime() < System.currentTimeMillis() - maxAge * 1000L) {
            req.setAttribute("SessionMaxAge", "true");
            new CustomLogoutHandler().logout(req, res, null);
            return false;
        }

        return true;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        if (maxAge < 1800 || maxAge > 3600 * 100) {
            return;
        }

        this.maxAge = maxAge;
    }

}
