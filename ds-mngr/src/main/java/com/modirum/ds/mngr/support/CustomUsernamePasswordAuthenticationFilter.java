package com.modirum.ds.mngr.support;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.DSModel.User.Status;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.AuditLogService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.web.context.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final transient Logger log = LoggerFactory.getLogger(CustomUsernamePasswordAuthenticationFilter.class);

    //private UserService usersService = ServiceLocator.getInstance().getUserService();
    private final PersistenceService persistenceService = ServiceLocator.getInstance().getPersistenceService();

    private int maxAttempts = 3;
    private int maxInactiveInterval = 900;
    private final RequestCache requestCache = new HttpSessionRequestCache();

    protected AuditLogService auditLogService = ServiceLocator.getInstance().getAuditLogService();

    @Override
    public void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, javax.servlet.FilterChain chain, Authentication authResult) throws IOException, ServletException {

        /*
         * This strange looking hack actually works, as oppossed to every documented approach...
         * When the parent auth handler is called, it will decide where to take us next depending where we came from
         * Always forcing the targetUrl is suboptimal because we don't pick up where we were
         * All attempts to replace the success handler function fail because the replacement is never called no matter what syntax is used in the config file
         *
         * if we have no previous addr req (meaning we came stright to login page), them we go to targetUrl after auth
         * if we have a reasonable req (session timed or or trying to go directly to a page), then we end up there after auth
         * if we hit the servlet root, it'll try to toss us back there, but we don't want that, so throw away that cached req and we will go to targetUrl like we want
         * for this purpose, assume any cached request ending with / is an attempt to send us to root because all valid page addr should end with .html
         */
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        if (savedRequest != null && savedRequest.getRedirectUrl().endsWith("/")) {
            requestCache.removeRequest(request, response);
        }
        User user = (User) authResult.getPrincipal();

        // update user's login counter
        if (Misc.isNullOrEmpty(user.getFactor2Authmethod()) || user.isFactor2Passed()) {
            try {
                boolean anyUpdate = false;
                if (Status.ACTIVE.equals(user.getStatus())) {
                    user.setStatus(Status.ACTIVE);
                    anyUpdate = true;
                }
                if (user.getLockedDate() != null) {
                    user.setLockedDate(null);
                    anyUpdate = true;
                }
                if (user.getLoginAttempts() > 0) {
                    user.setLoginAttempts(0);
                    anyUpdate = true;
                }
                if (anyUpdate) {
                    persistenceService.saveOrUpdate(user);
                }

                StringBuffer details = new StringBuffer();
                details.append(" IP: " + request.getRemoteAddr());
                details.append(" Auth-Method: " + Misc.merge((String) request.getAttribute("authmethod"),
                                                             (String) request.getAttribute("authmethod2")));
                details.append(" User-Agent: " + request.getHeader("User-Agent"));
                auditLogService.logAction(user, user, DSModel.AuditLog.Action.LOGIN,
                                          "Login success user '" + user.getLoginname() + "' details: " + details);

            } catch (Exception e) {
                log.error("Failed to update user", e);
            }
        }

        HttpSession s = createHttpSession(request, response);

        WebContext wctx = (WebContext) s.getAttribute("WebContext");
        if (wctx == null) {
            wctx = new WebContext();
            wctx.setSettingService(ServiceLocator.getInstance().getConfigService());
            wctx.setServletContext(s.getServletContext());
            wctx.setSession(s);
            s.setAttribute("WebContext", wctx);
        }
        wctx.setLang(user.getLocale());

        super.successfulAuthentication(request, response, chain, authResult);
    }

    @Override
    public void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);

        try {
            log.info("Login failed " + failed.getMessage(), failed);
            User user = (User) request.getAttribute("user");
            String username = null;
            if (user != null) {
                if (failed != null && failed instanceof AccountExpiredException) {
                    user.setLockedDate(new Date());
                    user.setStatus(Status.DISABLED);
                    log.warn("Disbling user " + user.getLoginname() + "(" + user.getId() +
                             ") because of no recent login");
                } else {
                    username = user.getLoginname();
                    Integer c = user.getLoginAttempts();
                    user.setLoginAttempts((c == null ? 1 : c + 1));
                    if (user.getLoginAttempts() >= maxAttempts) {
                        user.setLockedDate(new Date());
                        user.setStatus(Status.TEMP_LOCKED);
                        log.warn("Locked user " + user.getLoginname() + " because of too many " +
                                 user.getLoginAttempts() + " bad attempts");
                    }
                }
                persistenceService.saveOrUpdate(user);
            } else {
                username = (String) request.getAttribute("username");
            }

            StringBuilder details = new StringBuilder();
            details.append(" IP: " + request.getRemoteAddr());
            details.append(" Err :" + failed.getClass().getSimpleName() + " " + failed.getMessage());
            details.append(" User-Agent: " + request.getHeader("User-Agent"));

            if (user != null && failed != null && failed instanceof DisabledException) {
                auditLogService.logAction(user, null, DSModel.AuditLog.Action.LOGINFAIL,
                                          "Login fail user '" + username + "', user has blocked status, details: " +
                                          details);
            }
            if (user != null && failed != null && failed instanceof AccountExpiredException) {
                auditLogService.logAction(user, null, DSModel.AuditLog.Action.LOGINFAIL,
                                          "Login fail user '" + username + "', account has been expired, details: " +
                                          details);
            } else if (user != null && failed != null && failed instanceof LockedException) {
                auditLogService.logAction(user, user, DSModel.AuditLog.Action.LOGINFAIL,
                                          "Login fail user '" + username +
                                          "', too many failed login attempts, details: " + details);
            } else if (user != null) {
                auditLogService.logAction(user, user, DSModel.AuditLog.Action.LOGINFAIL,
                                          "Login fail user '" + username + "', , invalid user/password, details: " +
                                          details);
            } else {
                auditLogService.logAction(user != null ? user : new User(), null, DSModel.AuditLog.Action.LOGINFAIL,
                                          "Login fail user '" + username + "', invalid user, details: " + details);
            }
        } catch (Exception e) {
            log.error("Login Error ", e);
            try {
                if (request.getSession(false) != null) {
                    request.getSession(false).invalidate();
                }
            } catch (Exception dc) {
            }

            new ServletException(e);
        }
    }

    public void setMaxAttempts(int maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    public int getMaxAttempts() {
        return maxAttempts;
    }

    public HttpSession createHttpSession(HttpServletRequest request, HttpServletResponse response) {

        if (log.isDebugEnabled()) {
            log.debug("Make session and handle cookie stuff");
        }

        HttpSession nsess = request.getSession(true);
        nsess.setMaxInactiveInterval(maxInactiveInterval);
        // make https session cookie HttpOnly and Secure if https
        String serverInfo = this.getServletContext().getServerInfo();

        if (log.isDebugEnabled()) {
            log.debug("ServerInfo: " + serverInfo);
        }

        if (serverInfo != null && serverInfo.indexOf("WebSphere") > -1) {
            // nothing can help..must be set in container config
        } else {
            String sessionid = nsess.getId();
            String contextPath = request.getContextPath();
            String secure = "";
            if (request.isSecure() || "https".equals(request.getScheme())) {
                secure = "; Secure";
            }

            String scookie = "JSESSIONID=" + sessionid + "; Path=" + contextPath + ";" + " HttpOnly" + secure;
            if (log.isDebugEnabled()) {
                log.debug("Set-Cookie: " + scookie);
            }

            response.setHeader("Set-Cookie", scookie);
        }
        return nsess;
    }

    public int getMaxInactiveInterval() {
        return maxInactiveInterval;
    }

    public void setMaxInactiveInterval(int maxInactiveInterval) {
        this.maxInactiveInterval = maxInactiveInterval;
    }

}
