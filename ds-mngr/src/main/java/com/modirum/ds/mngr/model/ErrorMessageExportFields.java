package com.modirum.ds.mngr.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Encapsulates error columns needed in CSV Export of transactions.
 */
@Setter
@Getter
@AllArgsConstructor
public class ErrorMessageExportFields {
    private final String component;
    private final String messageType;
    private final String code;
    private final String description;
    private final String details;
}
