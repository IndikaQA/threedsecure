package com.modirum.ds.mngr.support;

import com.modirum.ds.db.model.User;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ForcePasswordChangeInterceptor extends org.springframework.web.servlet.handler.HandlerInterceptorAdapter {

    transient static Logger log = LoggerFactory.getLogger(ForcePasswordChangeInterceptor.class);

    private String passwordChangeUri = "/passwordChange/view.html";
    private String allowPath = "/passwordChange/";
    private int changePasswordDays = 90;

    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws IOException, ServletException {

        if (handler != null && (handler.getClass().getCanonicalName().indexOf("com.modirum.ds.") >= 0
                                // dont protect static stuff only controllers
                                // seems this has been changed in sprin lately that the object no longer controller by HandlerMethod
                                || handler instanceof org.springframework.web.method.HandlerMethod &&
                                   ((org.springframework.web.method.HandlerMethod) handler).getBean().getClass().getCanonicalName().indexOf(
                                           "com.modirum.ds.") >= 0) &&
            req.getRequestURI().indexOf(req.getContextPath() + allowPath) != 0) {
            User user = AuthorityUtil.getUser();

            if (user != null && (Misc.isNullOrEmpty(user.getFactor2Authmethod()) || user.isFactor2Passed())) {
                if (user.getLastPassChangeDate() != null) {
                    Calendar nextChange = new GregorianCalendar();
                    nextChange.setTime(user.getLastPassChangeDate());
                    nextChange.add(Calendar.DATE, changePasswordDays);

                    Calendar now = new GregorianCalendar();
                    now.setTime(new Date(System.currentTimeMillis()));

                    if (nextChange.before(now)) {
                        // it is time to change password!
                        user.setForcedPasswordChange(true);
                        res.sendRedirect(req.getContextPath() + passwordChangeUri);
                        return false;
                    }

                } else // if never changed force a change now
                {
                    user.setForcedPasswordChange(true);
                    res.sendRedirect(req.getContextPath() + passwordChangeUri);
                    return false;
                }
            }
        }
        return true;
    }

    public String getPasswordChangeUri() {
        return passwordChangeUri;
    }

    public void setPasswordChangeUri(String passwordChangeUri) {
        this.passwordChangeUri = passwordChangeUri;
    }

    public int getChangePasswordDays() {
        return changePasswordDays;
    }

    public void setChangePasswordDays(int changePasswordDays) {
        this.changePasswordDays = changePasswordDays;
    }

    public String getAllowPath() {
        return allowPath;
    }

    public void setAllowPath(String allowPath) {
        this.allowPath = allowPath;
    }

}
