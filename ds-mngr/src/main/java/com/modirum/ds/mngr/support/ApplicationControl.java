/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 29.01.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.mngr.support;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.AuditLogService;
import com.modirum.ds.services.ConfigService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import org.hibernate.annotations.Immutable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ServletConfigAware;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@Service
public class ApplicationControl implements org.springframework.context.ApplicationListener<org.springframework.context.event.ApplicationContextEvent>, ServletContextAware, ServletConfigAware {
    private final transient Logger log = LoggerFactory.getLogger(ApplicationControl.class);

    protected AuditLogService auditLogService = ServiceLocator.getInstance().getAuditLogService();
    protected ConfigService settingService = ServiceLocator.getInstance().getConfigService();

    static final Object syno = new Object();
    static long triggered = 0;
    static int triggeredcnt = 0;
    static long triggeredOff = 0;

    private ContextRefreshedEvent prevEvt;
    protected ServletContext servletContext;
    protected ServletConfig servletConfig;
    protected String certAuthUri;

    static ApplicationControl instance;

    public ApplicationControl() {
        instance = this;
    }


    // Nested class to hold valid ranges for customized property values.
    @Immutable
    static final class PropertyRange {
        public final double min;
        public final double max;

        public PropertyRange(final double min, final double max) {
            this.min = min;
            this.max = max;
        }

        public boolean isInRange(final String value) {
            return Misc.isInRange(value, min, max);
        }
    }

    // The bean properties below are allowed to be overwritten by context parameters setup by admins in deployment specific locations.
    public static final Map<String, PropertyRange> CUSTOM_PARAMETER_MAP = Collections.unmodifiableMap(
            new HashMap<String, PropertyRange>() {
                private static final long serialVersionUID = 1L;

                {
                    // from MPI manager tool web.xml
                    put("authenticationProvider.lockDelaySec", new PropertyRange(60 * 60, Integer.MAX_VALUE));
                    // from mpiAdmin-security.xml
                    put("customUsernamePasswordAuthenticationFilter.maxAttempts", new PropertyRange(2, 6));
                    // from mvc-config.xml:
                    put("forcePasswordChangeInterceptor.changePasswordDays", new PropertyRange(1, 90));
                }
            });


    public static final String BEANS_XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                                                  " <beans xmlns=\"http://www.springframework.org/schema/beans\"" +
                                                  " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
                                                  " xsi:schemaLocation=\"http://www.springframework.org/schema/beans	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd\">";
    public static final String BEANS_XML_FOOTER = "\n</beans>";

    @Override
    public synchronized void onApplicationEvent(ApplicationContextEvent evt) {
        if (evt != null && evt instanceof ContextRefreshedEvent) {
            onApplicationEvent((ContextRefreshedEvent) evt);
        }
        if (evt != null && evt instanceof ContextClosedEvent) {
            onApplicationEvent((ContextClosedEvent) evt);
        }
    }

    public synchronized void onApplicationEvent(ContextRefreshedEvent evt) {
        synchronized (syno) {
            // skip on repeating post of same event
            if (prevEvt != null && prevEvt.equals(evt)) {
                log.info("Repeating post of refresh event " + evt.hashCode() + "/" + evt.getTimestamp() +
                         ", ignoring..");
                return;
            }
            prevEvt = evt;

            // skip on frequent refresh event
            if (System.currentTimeMillis() - triggered < 5000) {
                log.info("Too frequent post of refresh event " + evt.hashCode() + "/" + evt.getTimestamp() +
                         ", ignoring..");
                return;
            }

            triggered = System.currentTimeMillis();
            triggeredcnt++;
        }

        if (servletContext != null) {
            try {
                if (Misc.isNotNullOrEmpty(servletContext.getInitParameter("log4j.configuration"))) {
                    Utils.configureLog4j(servletContext.getInitParameter("log4j.configuration"));
                }
            } catch (Throwable dc) {
                System.out.println(
                        "WARN: Failed to configure manager log4j, most likely other logging framework in use " + dc);
            }
        }

        startLogging();
        try {
            loadCustomizedParameters();
        } catch (Throwable dc) {
            log.error("Failed to load/apply custom statrup parameters", dc);
        }

    }

    private void loadCustomizedParameters() throws Exception {
        if (servletContext == null) {
            return;
        }

        for (String key : CUSTOM_PARAMETER_MAP.keySet()) {

            final String value = settingService.getStringSetting(key);

            // Skip any parameters, which don't seem to have a reasonable value.
            if (value == null || !checkCustomParameterValueSanity(key, value)) {
                if (value != null) {
                    log.warn("The custom parameter setting key \"" + key + "\" with value \"" + value + "\"" +
                             " has been ignored due to infringing PCI-DSS requirements. A default value will be used instead.");
                }
                continue;
            }

            final String[] beanIdAndProperty = key.split("\\.", 2);
            if (beanIdAndProperty.length != 2) {
                log.warn("Ignoring invalid custom context parameter key given: \"" + key +
                         "\" Format should be beanID.propertyName");
                continue;
            }

            setOneBeanProperty(beanIdAndProperty[0], beanIdAndProperty[1], Integer.parseInt(value));
        }


        if ("true".equals(settingService.getStringSetting(DsSetting.MNGR_SSL_AUTH_ENABLED.getKey()))) {

            //ServletRegistration.Dynamic srd = this.servletContext.getServlet(this.servletConfig.getServletName());
            //com.modirum.ds.mngr.support.CertLoginServlet.class.getName());


            //srd.addMapping("/certlogin");

            if (!"true".equals(settingService.getStringSetting(DsSetting.MNGR_SSL_AUTHPROXY_ENABLED.getKey()))) {
                certAuthUri = "/ssllogin.html";
                log.info("Setting up Tomcat TLS Auth for DS Manager at " + certAuthUri);
				/*
				javax.servlet.HttpConstraintElement sc = new javax.servlet.HttpConstraintElement(ServletSecurity.EmptyRoleSemantic.PERMIT,
						ServletSecurity.TransportGuarantee.CONFIDENTIAL, "sslauthenticated");
				javax.servlet.ServletSecurityElement sel = new javax.servlet.ServletSecurityElement(sc);
				//servletContext.getServletRegistration(this.servletConfig.getServletName()).

				//ConstraintSecurityHandler.createConstraintsWithMappingsForPath("foo", "/foo/*", element);
				//srd.setServletSecurity(sel);
				 */
            } else {
                String sslAuthUrlProxy = settingService.getStringSetting(DsSetting.MNGR_SSL_AUTHPROXY_URL.getKey());
                if (Misc.isNotNullOrEmpty(sslAuthUrlProxy)) {
                    certAuthUri = sslAuthUrlProxy;
                } else {
                    certAuthUri = "../ssllogin2.html";
                }
                log.info("Setting up Proxy TLS Auth for DS Manager at " + certAuthUri);

            }
            //srd.setAsyncSupported(true);


        }
    }


    private boolean checkCustomParameterValueSanity(final String key, final String value) {
        return CUSTOM_PARAMETER_MAP.containsKey(key) && CUSTOM_PARAMETER_MAP.get(key).isInRange(value);
    }


    private void setOneBeanProperty(final String beanId, final String name, final int value) {
        log.info("Overriding the configuration " + beanId + name + " from custom context paramaters with value " +
                 value);
        org.springframework.web.context.support.XmlWebApplicationContext springctx = (org.springframework.web.context.support.XmlWebApplicationContext) prevEvt.getApplicationContext();
        try {
            final Object customBean = springctx.getBean(beanId);
            switch (beanId) {
                case "authenticationProvider":
                    ((CustomAuthenticationProvider) customBean).setLockDelaySec(value);
                    break;
                case "customUsernamePasswordAuthenticationFilter":
                    switch (name) {
                        case "maxAttempts":
                            ((CustomUsernamePasswordAuthenticationFilter) customBean).setMaxAttempts(value);
                            break;
                    }
                    break;
                case "forcePasswordChangeInterceptor":
                    ((ForcePasswordChangeInterceptor) customBean).setChangePasswordDays(value);
                    break;
            }
        } catch (Exception e) {
            log.warn("There was a problem setting the custom value for bean ID \"" + beanId + "\" property \"" + name +
                     "\"");
        }
    }

    @Override
    public void setServletContext(ServletContext arg0) {
        servletContext = arg0;
    }

    public void onApplicationEvent(ContextClosedEvent arg0) {
        synchronized (syno) {
            if (triggeredOff > System.currentTimeMillis() - 1500) {
                return;
            }

            triggeredOff = System.currentTimeMillis();
        }

        this.stopLogging();
    }


    @Transactional
    public void startLogging() {
        auditLogService.logAction(User.SYSTEM_USER, null, DSModel.AuditLog.Action.APPSTART,
                                  "Logging started " + getAppName() + " on " + Utils.getLocalNonLoIp());
    }

    @Transactional
    public void stopLogging() {
        auditLogService.logAction(User.SYSTEM_USER, null, DSModel.AuditLog.Action.APPSTOP,
                                  "Logging stopped " + getAppName() + " on " + Utils.getLocalNonLoIp());
    }

    @Override
    public void setServletConfig(ServletConfig arg0) {
        this.servletConfig = arg0;

    }

    public String getCertAuthUri() {
        return certAuthUri;
    }


    public static String getAppName() {

        String appName = instance != null &&
                         instance.servletContext != null ? instance.servletContext.getServletContextName() : null;
        if (appName == null) {
            appName = "Modirum DS Manager";
        }
        return appName;
    }

}
