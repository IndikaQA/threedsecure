package com.modirum.ds.mngr.support;

import com.modirum.ds.db.model.AuditLog;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.AuditLogService;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.LocalizationService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.UserService;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.cert.X509Certificate;
import java.util.Locale;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    private static final Logger log = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

    private final UserService usersService = ServiceLocator.getInstance().getUserService();
    private final LocalizationService ls = ServiceLocator.getInstance().getLocalizationService();

    //private PasswordEncryptor encryptor =
    protected AuditLogService auditLogService = ServiceLocator.getInstance().getAuditLogService();

    private long lockDelaySec = new Long(3600); // default one hour

    boolean checkPassword(String pplain, String phash) {
        return CryptoService.sha256WithUniquePredictable(pplain).equals(phash);
    }

    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        String username = auth.getName(); //String.valueOf(auth.getPrincipal());
        String password = null;
        X509Certificate cert = null;
        String remoteAddress = "NA";
        Locale loca = Helpers.getDefaultLocale();
        if (auth.getCredentials() != null && auth.getCredentials() instanceof X509Certificate) {
            cert = (X509Certificate) auth.getCredentials();
        } else {
            password = String.valueOf(auth.getCredentials());
        }

        User user = null;
        try {
            user = usersService.getUserByLoginname(username);
        } catch (Exception e) {
            log.error("Authentication error, whille looking up user", e);
            throw new AuthenticationServiceException("System failure, contact administrator", e);
        }

        if (user == null) {
            log.warn("User by loginname '" + username + "' was not found");
        }

        // required for CustomUsernamePasswordAuthenticationFilter!!!
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest req = attributes.getRequest();
        req.setAttribute("username", username);
        req.setAttribute("user", user);

        Object details = auth.getDetails();
        if (details != null && details instanceof WebAuthenticationDetails) {
            remoteAddress = ((WebAuthenticationDetails) details).getRemoteAddress();
        }

        boolean authenticated = false;
        if (user != null && cert != null) {
            String cn = null;
            String s = cert.getSubjectX500Principal().toString();
            String[] parts = Misc.split(s, ",");
            for (String part : parts) {
                String[] subparts = Misc.split(part, "=");
                if ("SERIALNUMBER".equalsIgnoreCase(subparts[0])) {
                    cn = subparts[1];
                    break;
                }
                if ("CN".equalsIgnoreCase(subparts[0])) {
                    cn = subparts[1];
                }
            }

            log.info("Checking ID code/CN match " + user.getFactor2Data1() + "/" + cn);
            authenticated = user.getFactor2Data1() != null && user.getFactor2Data1().equals(cn);
            user.setFactor2Passed(authenticated);
            req.setAttribute("authmethod", "certificate");
        } else if (user != null) {
            authenticated = checkPassword(password, user.getPassword());
            req.setAttribute("authmethod", "password");
        }

        if (user != null && authenticated) {
            if (DSModel.User.Status.DISABLED.equals(user.getStatus())) {
                // block by administrator
                // return null like user was not found
                throw new DisabledException("User " + user.getLoginname() + " account disabled ");
            } else if (user.getLockedDate() != null && DSModel.User.Status.TEMP_LOCKED.equals(user.getStatus())) {
                long nowSec = System.currentTimeMillis() / 1000;
                long lockTime = user.getLockedDate().getTime() / 1000;
                // fixed: Not walid to use  == to compare objects
                if (lockTime != 0 && nowSec - lockTime <= lockDelaySec) {
                    // automatic block because of many login attempts
                    // return null like user was not found
                    throw new LockedException("Too many login attemtps for user '" + username + "'");
                }
            }
            // //pci dss 8.1.4 Remove/disable inactive user accounts within 90 days.
            else if (DSModel.User.Status.ACTIVE.equals(user.getStatus())) {
                AuditLog prevLogin = null;
                AuditLog prevAction = null;
                java.util.Date latestWhen = null;
                try {
                    prevLogin = auditLogService.getLastLoginRecord(user.getId());
                    prevAction = auditLogService.getLastModidified(user);
                } catch (Exception ie) {
                    log.error("Authentication error, whille looking up user", ie);
                    throw new AuthenticationServiceException("System failure, contact administrator", ie);
                }

                if (prevLogin != null) {
                    latestWhen = prevLogin.getWhen();
                }
                if (latestWhen == null && prevAction != null ||
                    prevAction != null && prevAction.getWhen() != null && prevAction.getWhen().after(latestWhen)) {
                    latestWhen = prevAction.getWhen();
                }

                java.util.Date d90DaysAgo = DateUtil.addDays(new java.util.Date(), -90);
                if (latestWhen != null && latestWhen.before(d90DaysAgo)) // if not modified after

                {    // first attempt no recorde exist yet
                    log.warn("Login failed user '" + username + "' (" + user.getId() + ") from " + remoteAddress +
                             " dormant account (pci dss v3.2 8.1.4) no login or administrative action activity for last 90 days found");
                    throw new AccountExpiredException(
                            ls.getText("mn.user.account.disabled.due.long.inactivity", username, loca));
                }

            }    // exclude all other states than active
            else if (!DSModel.User.Status.ACTIVE.equals(user.getStatus())) {
                log.warn("Login failed user '" + username + "' (" + user.getId() + ") from " + remoteAddress +
                         " status is " + user.getStatus());

                if (DSModel.User.Status.TEMPLATE.equals(user.getStatus())) {
                    //throw new DisabledException("User '"+username+ "' not a valid account (template)");
                    throw new DisabledException(ls.getText("mn.user.account.invalid.template", username, loca));
                } else if (DSModel.User.Status.ENDED.equals(user.getStatus())) {
                    //throw new DisabledException("User'"+username+ "' not valid, an ended/terminated account");
                    throw new DisabledException(ls.getText("mn.user.account.invalid.terminated", username, loca));
                } else {
                    //throw new LockedException("User '"+username+ "' account blocked/disabled");
                    throw new LockedException(ls.getText("mn.user.account.blocked", username, loca));
                }
            }

            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user, "",
                                                                                                AuthorityUtil.getAuthorities(
                                                                                                        user));
            return token;
        }

        // for bad logins add extra delay
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
        }

        if (cert != null) {
            throw new BadCredentialsException("Invalid Username/Certificate");
        }

        throw new BadCredentialsException("Invalid Username/Password");// does not match for " + username);
    }

    public boolean supports(Class<? extends Object> arg0) {
        boolean result = true;
        return result;
    }

    public long getLockDelaySec() {
        return lockDelaySec;
    }

    public void setLockDelaySec(long lockDelaySec) {
        if (lockDelaySec > 600) {
            this.lockDelaySec = lockDelaySec;
        }
    }
}
