package com.modirum.ds.messageprocessor;

import com.modirum.ds.enums.PaymentSystemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Registry that contains customized Payment System specific EMV 3DS protocol message processors.
 * Custom message processors are loosely bound to DS core and are only registered on-demand, if requested class
 * is present in classpath during runtime.
 */
public class PaymentSystemMsgProcessorRegistry {

    private final static Logger LOG = LoggerFactory.getLogger(PaymentSystemMsgProcessorRegistry.class);

    public final static DefaultPaymentSystemMessageProcessor DEFAULT = new DefaultPaymentSystemMessageProcessor();
    private final static Map<PaymentSystemType, PaymentSystemMessageProcessor> paymentSystemProcessors = new HashMap<>();

    public static PaymentSystemMessageProcessor get(String paymentSystemType) {
        Optional<PaymentSystemType> psTypeOptional = PaymentSystemType.getPaymentSystemType(paymentSystemType);
        if (!psTypeOptional.isPresent()) {
            LOG.warn("Unknown payment system type: {}. Using default message processing.", paymentSystemType);
            return DEFAULT;
        }

        PaymentSystemType psType = psTypeOptional.get();
        PaymentSystemMessageProcessor paymentSystemMessageProcessor = paymentSystemProcessors.get(psType);
        if (paymentSystemMessageProcessor != null) {
            return paymentSystemMessageProcessor;
        }

        // register requested message processor
        switch (psType) {
            case eftpos:
                paymentSystemMessageProcessor = getEftposMessageProcessor();
                break;

            case elo:
                paymentSystemMessageProcessor = DEFAULT; // no custom processing for elo.
                break;

            default:
                LOG.warn("Custom Message Processing service not found for Payment System type: {}", psType.name());
                paymentSystemMessageProcessor = DEFAULT;
        }

        paymentSystemProcessors.put(psType, paymentSystemMessageProcessor);

        LOG.info("Initialized {} processor for Payment System type: {}",
                paymentSystemMessageProcessor.getClass().getSimpleName(), psType.name());

        return paymentSystemMessageProcessor;
    }

    private static PaymentSystemMessageProcessor getEftposMessageProcessor() {
        try {
            EmvMessageProcessorFactory processorFactory = (EmvMessageProcessorFactory)
                    Class.forName("com.modirum.ds.paymentsystem.eftpos.EftposMessageProcessorFactory").newInstance();
            return processorFactory.create();
        } catch (Exception e) {
            LOG.error("Cannot instantiate EftposMessageProcessorFactory, using default Message Processor.", e);
            return DEFAULT;
        }
    }
}