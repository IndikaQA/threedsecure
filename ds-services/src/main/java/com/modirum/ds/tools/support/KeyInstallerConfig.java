package com.modirum.ds.tools.support;

import com.modirum.ds.tools.Importer;
import com.modirum.ds.tools.KeysInstaller;
import com.modirum.ds.utils.Misc;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.io.FileInputStream;

import static com.modirum.ds.tools.support.KeyInstallerHelper.getPrompt;

@Configuration
public class KeyInstallerConfig {

    @Primary
    @Bean(name = "dsDataSource")
    public DataSource createDataSource() {
        // load settings from properties file
        System.out.println("Enter location of ds.properties default " + Importer.DEFAULT_CONF_PROPS + ":");
        String propertyFile = getPrompt("Enter valid path or blank for default: ");
        KeysInstaller.settings = new java.util.Properties();
        String fileName = Misc.isNotNullOrEmpty(propertyFile) ? propertyFile : Importer.DEFAULT_CONF_PROPS;
        try {
            KeysInstaller.settings.load(new FileInputStream(fileName));
            System.out.println(KeysInstaller.settings.getProperty("db.driver"));
        } catch (Exception e) {
            System.out.println("Could not read property file: " + fileName);
            e.printStackTrace(System.out);
        }

        // create data source
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(KeysInstaller.settings.getProperty("db.driver"));
        dataSource.setUrl(KeysInstaller.settings.getProperty("db.url"));
        dataSource.setUsername(KeysInstaller.settings.getProperty("db.user"));
        dataSource.setPassword(KeysInstaller.settings.getProperty("db.password"));
        return dataSource;
    }
}
