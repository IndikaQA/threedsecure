package com.modirum.ds.imports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class DataBatchResultStatus {

    private Collection<String> errors;
    private Map<DataChangeStateOperation, Integer> status;

    public DataBatchResultStatus(final Collection<String> errors) {
        this.errors = errors;
    }

    public DataBatchResultStatus(final Map<DataChangeStateOperation, Integer> status) {
        this.status = status;
    }

    public DataBatchResultStatus(final String errorMessage) {
        this.errors = new ArrayList<>();
        this.errors.add(errorMessage);
    }

    public Collection<String> getErrors() {
        return errors;
    }

    public Map<DataChangeStateOperation, Integer> getStatus() {
        return status;
    }
}
