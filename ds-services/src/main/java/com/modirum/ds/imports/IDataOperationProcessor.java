package com.modirum.ds.imports;

import java.util.List;

public interface IDataOperationProcessor<D extends BaseBatchModel> {

    /**
     * Process list of ImportMerchantModel objects.
     * It is up to implementation to decide how this list will be processed.
     */
    void process(final List<D> dataList);

    /**
     * Returns total number of all processed elements.
     */
    int processedCount();
}
