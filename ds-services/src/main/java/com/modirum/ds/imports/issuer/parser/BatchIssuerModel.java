package com.modirum.ds.imports.issuer.parser;

import com.modirum.ds.imports.BaseBatchModel;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class BatchIssuerModel extends BaseBatchModel {

    private String bin;
    private String name;
    private String phone;
    private String email;
    private String address;
    private String city;
    private String country;
    private String status;
}
