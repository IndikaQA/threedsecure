package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.db.dao.PersistenceService;

import java.util.List;

public class DeleteMerchantOperationProcessor implements IDataOperationProcessor<BatchMerchantModel> {

    private final PersistenceService persistenceService;

    private int processedCount;

    public DeleteMerchantOperationProcessor(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public void process(final List<BatchMerchantModel> merchants) {
        merchants.stream()
                 .peek(e -> processedCount++)
                 .forEach(m -> persistenceService.deleteMerchantByNameAndPaymentSystemId(m.getName(), Integer.parseInt(m.getPaymentSystemId())));
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }
}
