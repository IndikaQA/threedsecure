package com.modirum.ds.imports;

import java.util.List;

public class DataBatchBundle<M extends BaseBatchModel> {

    private final CSVHeaderFormat headerFormat;
    private final List<M> importModels;

    public DataBatchBundle(final CSVHeaderFormat headerFormat, final List<M> importModels) {
        this.headerFormat = headerFormat;
        this.importModels = importModels;
    }

    public CSVHeaderFormat getHeaderFormat() {
        return headerFormat;
    }

    public List<M> getImportModels() {
        return importModels;
    }
}
