package com.modirum.ds.imports.issuer;

import com.modirum.ds.imports.AbstractBatchDataValidator;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.imports.ValidationsList;
import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.providers.CountriesProvider;
import com.modirum.ds.services.IssuerService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.validator.routines.EmailValidator;
import com.modirum.ds.services.PaymentSystemsService;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.modirum.ds.util.DSUtils.toSet;

public class IssuerBatchModelValidator extends AbstractBatchDataValidator<BatchIssuerModel> {

    private static final List<String> SUPPORTED_STATUSES = Arrays.asList("active", "disabled");
    private final IssuerService issuerService;

    protected IssuerBatchModelValidator(final DataBatchHandlerConfiguration configuration,
                                        final PaymentSystemsService paymentSystemsService,
                                        final IssuerService issuerService) {
        super(configuration, paymentSystemsService);
        this.issuerService = issuerService;
    }

    @Override
    protected Map<CSVHeaderFormat, ValidationsList<BatchIssuerModel>> initValidators() {
        final Map<CSVHeaderFormat, ValidationsList<BatchIssuerModel>> map = new HashMap<>();

        ValidationsList<BatchIssuerModel> addValidations = new ValidationsList<>();
        addValidations.add(this::isValidCountries);
        addValidations.add(this::isValidEmails);
        addValidations.add(this::isStatusValid);
        addValidations.add(this::isValidPaymentSystemId);
        addValidations.add(this::isValidIssuerBin);
        addValidations.add(this::isValidIssuerName);
        map.put(CSVHeaderFormat.ADD_ISSUER, addValidations);

        ValidationsList<BatchIssuerModel> changeValidations = new ValidationsList<>();
        changeValidations.add(this::isValidPaymentSystemId);
        changeValidations.add(this::isValidIssuerName);
        map.put(CSVHeaderFormat.CHANGE_ISSUER_STATE, changeValidations);

        return map;
    }

    private Boolean isStatusValid(final FilterParams<BatchIssuerModel> params) {
        if (!CSVHeaderFormat.ADD_ISSUER.equals(params.headerFormat)) {
            return true;
        }
        boolean result = true;
        Set<String> statuses = toSet(params.importModels.stream()
                .map(BatchIssuerModel::getStatus)
                .filter(Objects::nonNull));
        statuses.removeAll(SUPPORTED_STATUSES);
        if (!statuses.isEmpty()) {
            result = false;
            addError(invalidStatuses(statuses));
            if (!params.continueOnError) {
                return result;
            }
        }
        return result;
    }

    private boolean isValidCountries(final FilterParams<BatchIssuerModel> params) {
        if (!CSVHeaderFormat.ADD_ISSUER.equals(params.headerFormat)) {
            return true;
        }
        boolean result = true;
        Set<String> actualCountryA3Codes = toSet(params.importModels.stream()
                .map(BatchIssuerModel::getCountry)
                .filter(Objects::nonNull));
        List<String> existingA3CountriesCodes = CountriesProvider.getA2CountriesCodes();
        for (String a3CountryCode : actualCountryA3Codes) {
            if (!existingA3CountriesCodes.contains(a3CountryCode)) {
                addError(invalidA3CityCode(a3CountryCode));
                result = false;
                if (!params.continueOnError) {
                    return false;
                }
            }
        }
        return  result;
    }

    private boolean isValidEmails(final FilterParams<BatchIssuerModel> params) {
        if (!CSVHeaderFormat.ADD_ISSUER.equals(params.headerFormat)) {
            return true;
        }
        boolean result = true;
        EmailValidator validator = EmailValidator.getInstance();
        Set<String> emails = toSet(params.importModels.stream()
                .map(BatchIssuerModel::getEmail)
                .filter(Objects::nonNull));
        for (String email: emails) {
            if (!validator.isValid(email)) {
                result = false;
                addError(invalidEmailError(email));
                if (!params.continueOnError) {
                    return result;
                }
            }
        }

        return result;
    }

    private boolean isValidPaymentSystemId(final FilterParams<BatchIssuerModel> params) {
        List<String> paymentSystemIds = params.importModels
            .stream()
            .map(BatchIssuerModel::getPaymentSystemId)
            .collect(Collectors.toList());
        return isValidPaymentSystemsIDs(params.continueOnError, paymentSystemIds);
    }

    private String invalidStatuses(final Set<String> statuses) {
        return String.format("Found invalid statuses: %s.", statuses);
    }

    private String invalidA3CityCode(final String countryA3Code) {
        return String.format("A3 Country code=%s is invalid. Expected 3 digit code in ISO 3166 format.", countryA3Code);
    }

    private String invalidEmailError(final String email) {
        return String.format("Found invalid email: %s", email);
    }

    private boolean isValidIssuerBin(final FilterParams<BatchIssuerModel> params) {
        List<BatchIssuerModel> batchIssuerModels =  params.importModels;
        boolean result = true;
        for (BatchIssuerModel issuer : batchIssuerModels) {
            if (StringUtils.isNotEmpty(issuer.getBin()) &&
                StringUtils.isNotEmpty(issuer.getPaymentSystemId()) &&
                NumberUtils.isDigits(issuer.getPaymentSystemId()) &&
                issuerService.isExistingDuplicateIssuersBin(null,
                                                            issuer.getBin(),
                                                            Integer.parseInt(issuer.getPaymentSystemId()))) {
                addError(String.format("Duplicate Issuer Bin = '%s' under Payment System with id = '%s'.", issuer.getBin(), issuer.getPaymentSystemId()));
                result = false;
                if (!params.continueOnError) {
                    return false;
                }

            }
        }

        List<String> binAndPaymentSystemIdList =
                batchIssuerModels.stream()
                                 .filter(issuer -> StringUtils.isNotEmpty(issuer.getBin()) &&
                                                   StringUtils.isNotEmpty(issuer.getPaymentSystemId()))
                                 .map(issuer -> issuer.getBin() + "-" + issuer.getPaymentSystemId())
                                 .collect(Collectors.toList());

        for (BatchIssuerModel issuer : batchIssuerModels) {
            if (StringUtils.isNotEmpty(issuer.getBin()) &&
                StringUtils.isNotEmpty(issuer.getPaymentSystemId())) {

                String issuerBinAndPaymentSystemId = issuer.getBin() + "-" + issuer.getPaymentSystemId();
                int issuerBinFrequency = Collections.frequency(binAndPaymentSystemIdList, issuerBinAndPaymentSystemId);
                if (issuerBinFrequency > 1) {
                    addError(String.format("Issuer Bin = '%s' with PaymentSystem id = '%s' exists %s times in the batch upload file.", issuer.getBin(), issuer.getPaymentSystemId(), issuerBinFrequency));
                    result = false;
                    if (!params.continueOnError) {
                        return false;
                    }
                }

            }
        }
        return result;
    }

    private boolean isValidIssuerName(final FilterParams<BatchIssuerModel> params) {
        List<BatchIssuerModel> batchIssuerModels = params.importModels;
        boolean result = true;
        for (BatchIssuerModel issuer : batchIssuerModels) {

            if (StringUtils.isNotEmpty(issuer.getName()) &&
                NumberUtils.isDigits(issuer.getPaymentSystemId())) {

                if (issuerService.isIssuerExists(issuer.getName(), Integer.parseInt(issuer.getPaymentSystemId()), null)) {
                    if (CSVHeaderFormat.ADD_ISSUER.equals(params.headerFormat)) {
                        addError(String.format("Issuer with Name = '%s' and Payment System ID = '%s' already exists.", issuer.getName(), issuer.getPaymentSystemId()));
                        result = false;
                        if (!params.continueOnError) {
                            return result;
                        }
                    }
                } else if (CSVHeaderFormat.CHANGE_ISSUER_STATE.equals(params.headerFormat)) {
                    addError(String.format("Issuer with Name = '%s' and Payment System ID = '%s' doesn't exists.", issuer.getName(), issuer.getPaymentSystemId()));
                    result = false;
                    if (!params.continueOnError) {
                        return result;
                    }
                }
            }
        }

        List<String> nameAndPaymentSystemIdList =
                batchIssuerModels.stream()
                                 .filter(issuer -> StringUtils.isNotEmpty(issuer.getName()) &&
                                                   StringUtils.isNotEmpty(issuer.getPaymentSystemId()))
                                 .map(issuer -> issuer.getName() + "-" + issuer.getPaymentSystemId())
                                 .collect(Collectors.toList());

        for (BatchIssuerModel issuer : batchIssuerModels) {
            if (StringUtils.isNotEmpty(issuer.getName()) &&
                StringUtils.isNotEmpty(issuer.getPaymentSystemId())) {

                String nameAndPaymentSystemId = issuer.getName() + "-" + issuer.getPaymentSystemId();
                int nameFrequency = Collections.frequency(nameAndPaymentSystemIdList, nameAndPaymentSystemId);
                if (nameFrequency > 1) {
                    addError(String.format("Issuer Name = '%s' with PaymentSystem id = '%s' exists %s times in the batch upload file.",
                                           issuer.getName(), issuer.getPaymentSystemId(), nameFrequency));
                    result = false;
                    if (!params.continueOnError) {
                        return false;
                    }
                }

            }
        }
        return result;
    }
}
