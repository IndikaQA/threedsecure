package com.modirum.ds.imports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

/**
 * Class that imports CSV file.
 */
public class DataBatchHandler<M extends BaseBatchModel> {

    private static final Logger LOG = LoggerFactory.getLogger(DataBatchHandler.class);

    private final AbstractBatchDataValidator<M> importModelValidator;
    private final AbstractCsvParser<M> parser;
    private final AbstractOperationProcessorProvider processorsProvider;

    public DataBatchHandler(final AbstractCsvParser<M> parser,
                            final AbstractBatchDataValidator<M> importModelValidator,
                            final AbstractOperationProcessorProvider processorsProvider) {
        this.importModelValidator = importModelValidator;
        this.processorsProvider = processorsProvider;
        this.parser = parser;
    }

    /**
     * Validate content of a file without importing the data.
     */
    public DataBatchResultStatus validateData(final String fileContent) {
        try {
            DataBatchBundle<M> importModel = parser.parse(fileContent);
            importModelValidator.setPaymentSystemIds(importModel.getImportModels());
            if (!importModelValidator.isValid(importModel.getHeaderFormat(), importModel.getImportModels())) {
                return new DataBatchResultStatus(importModelValidator.getErrors());
            }
            return new DataBatchResultStatus(new HashMap<>());

        } catch (CSVParserException e) {
            LOG.debug("Error during validation of file: " + e.getMessage(), e);
            return new DataBatchResultStatus(e.getMessage());
        }
    }

    /**
     * Parse fileContent input, validates the format and perform necessary operations.
     */
    public DataBatchResultStatus validateAndImportData(final String fileContent) {
        try {
            DataBatchBundle<M> importModel = parser.parse(fileContent);
            importModelValidator.setPaymentSystemIds(importModel.getImportModels());
            if (!importModelValidator.isValid(importModel.getHeaderFormat(), importModel.getImportModels())) {
                return new DataBatchResultStatus(importModelValidator.getErrors());
            }

            final Map<DataChangeStateOperation, List<M>> map = importModel
                    .getImportModels()
                    .stream()
                    .collect(groupingBy(BaseBatchModel::getOperation));

            final Map<DataChangeStateOperation, Integer> results = new HashMap<>();

            for (DataChangeStateOperation operation : DataChangeStateOperation.values()) {
                List<M> models = map.get(operation);
                if (models != null && !models.isEmpty()) {
                    IDataOperationProcessor processor = processorsProvider.getProcessor(operation);
                    processor.process(models);
                    results.put(operation, processor.processedCount());
                }
            }

            return new DataBatchResultStatus(results);

        } catch (final CSVParserException e) {
            LOG.debug("Error during import file: " + e.getMessage(), e);
            return new DataBatchResultStatus(e.getMessage());
        }
    }
}
