package com.modirum.ds.imports;

import com.modirum.ds.db.dao.PersistenceService;

import java.util.Map;

public abstract class AbstractOperationProcessorProvider {

    protected final PersistenceService persistenceService;

    private Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> processorMap;

    /**
     * Initialize providers map.
     */
    public abstract Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> initProviders();

    public AbstractOperationProcessorProvider(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public IDataOperationProcessor<? extends BaseBatchModel> getProcessor(final DataChangeStateOperation operation) {
        if (processorMap == null) {
            processorMap = initProviders();
        }
        return processorMap.get(operation);
    }
}
