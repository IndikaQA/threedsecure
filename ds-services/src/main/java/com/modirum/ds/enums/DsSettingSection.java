package com.modirum.ds.enums;

public enum DsSettingSection {
    DS_CORE("DS Core"),
    DS_MNGR("DS Manager"),
    EMV_FIELDS_MASKING("EMV Fields Masking"),
    ISSUER_ACS("Issuer and ACS"),
    FSS("FSS"),
    TDSM_RELAY("3DS Method Relay"),
    TDS_SERVER("3DS Server"),
    CLEAN_UP_JOB("Cleanup Job"),
    MODIRUM_ID_AS_CLIENT_SERVICE("Modirum ID Client Service"),
    STAGE2AUTHENTICATION("Stage 2 Authentication"),
    PRODUCT_LICENSE("Product License"),
    EMV_MESSAGE_ERROR("EMV Message Error"),
    SSL_CERTIFICATE("SSL Certificate"),
    EMV_FIELDS_VALIDATION("EMV Fields Validation"),
    IMPORTER_SERVICE("Importer Service")

    ;
    private String description;
    DsSettingSection(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
