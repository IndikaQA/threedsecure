package com.modirum.ds.util;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Common utility functions for regex
 */
public class RegexUtil {

    /**
     * Checks if regex pattern is valid.
     *
     * @param pattern The regex pattern to be validated.
     * @return True if regex pattern is valid
     */
    public static boolean isValidPattern(String pattern) {

        try {
            Pattern.compile(pattern);
        } catch (PatternSyntaxException e) {
            return false;
        }

        return true;
    }

    /**
     * Checks if the input contains only allowed characters defined by @param allowedCharacters.
     *
     * @param input
     * @param allowedCharacters
     * @return
     */
    public static boolean validAllowedCharacters(String input, String allowedCharacters) {
        Pattern pattern = Pattern.compile("[" + Pattern.quote(allowedCharacters) + "]*");
        return input.matches(pattern.pattern());
    }
}
