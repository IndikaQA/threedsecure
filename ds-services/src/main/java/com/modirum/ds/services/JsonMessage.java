package com.modirum.ds.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.modirum.ds.enums.FieldName;
import com.modirum.ds.model.threeds.MerchantRiskIndicator;
import com.modirum.ds.model.threeds.MessageExtension;
import com.modirum.ds.model.threeds.MobilePhone;
import com.modirum.ds.util.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * An API object that represents the 3DS json message.
 * A wrapper that hides dependency from 3rd party library
 * so it won't leak into core business layers.
 */
public class JsonMessage {

    private final JsonNode rootNode;
    private final ObjectMapper objectMapper;

    JsonMessage(JsonNode jsonNode, ObjectMapper objectMapper) {
        this.rootNode = jsonNode;
        this.objectMapper = objectMapper;
    }

    public JsonNode getRootNode() {
        return rootNode;
    }

    /**
     * Use the fieldName to remove the field/object
     *
     * @param fieldName Uses dot notation for nested json field names
     */
    private void remove(String fieldName) {
        if (!fieldName.contains(".")) {
            ((ObjectNode) rootNode).remove(fieldName);
        } else {
            String[] fieldNamePath = fieldName.split("\\.");
            JsonNode parentNode = getLeafParent(fieldNamePath);
            if (parentNode instanceof ObjectNode) {
                ((ObjectNode) parentNode).remove(fieldNamePath[fieldNamePath.length - 1]);
            }
        }
    }

    public void setAuthenticationValue(String authenticationValue) {
        setString(FieldName.authenticationValue, authenticationValue);
    }

    public String getAuthenticationValue() {
        return getString(FieldName.authenticationValue);
    }

    public void setThreeDSServerOperatorID(String threeDSServerOperatorID) {
        setString(FieldName.threeDSServerOperatorID, threeDSServerOperatorID);
    }

    public String getThreeDSServerOperatorID() {
        return getString(FieldName.threeDSServerOperatorID);
    }

    public void setAcsOperatorID(String acsOperatorID) {
        setString(FieldName.acsOperatorID, acsOperatorID);
    }
    public String getAcsOperatorID() {
        return getString(FieldName.acsOperatorID);
    }

    public void setMessageCategory(String messageCategory) {
        setString(FieldName.messageCategory, messageCategory);
    }

    public String getMessageCategory() {
        return getString(FieldName.messageCategory);
    }

    public void setEci(String eci) {
        setString(FieldName.eci, eci);
    }

    public String getEci() {
        return getString(FieldName.eci);
    }

    public void setTransStatus(String transStatus) {
        setString(FieldName.transStatus, transStatus);
    }

    public String getTransStatus() {
        return getString(FieldName.transStatus);
    }

    public String getPurchaseCurrency() {
        return getString(FieldName.purchaseCurrency);
    }

    public void setTransStatusReason(String transStatusReason) {
        setString(FieldName.transStatusReason, transStatusReason);
    }

    public String getTransStatusReason() {
        return getString(FieldName.transStatusReason);
    }

    public String getDeviceChannel() {
        return getString(FieldName.deviceChannel);
    }

    public void setDeviceChannel(String deviceChannel) {
        setString(FieldName.deviceChannel, deviceChannel);
    }

    public void setMessageExtension(List<MessageExtension> messageExtension) {
        remove(FieldName.messageExtension);
        addArrayItems(FieldName.messageExtension, messageExtension.toArray());
    }

    public List<MessageExtension> getMessageExtension() {
        ArrayNode arrayNode = findArrayNode(FieldName.messageExtension);
        if (arrayNode == null) {
            return null;
        } else {
            List<MessageExtension> messageExtensionList = new ArrayList<>();
            arrayNode.iterator().forEachRemaining(jsonNode -> {
                MessageExtension item = objectMapper.convertValue(jsonNode, MessageExtension.class);
                messageExtensionList.add(item);
            });
            return messageExtensionList;
        }
    }

    public Boolean getBrowserJavaEnabled() {
        return getBoolean(FieldName.browserJavaEnabled);
    }

    public String getCardExpiryDate() {
        return getString(FieldName.cardExpiryDate);
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        setString(FieldName.cardExpiryDate, cardExpiryDate);
    }

    public String getThreeDSRequestorAuthenticationInd() {
        return getString(FieldName.threeDSRequestorAuthenticationInd);
    }

    public String getThreeRIInd() {
        return getString(FieldName.threeRIInd);
    }

    public String getAcctType() {
        return getString(FieldName.acctType);
    }

    public String getBillAddrCity() {
        return getString(FieldName.billAddrCity);
    }

    public String getBillAddrCountry() {
        return getString(FieldName.billAddrCountry);
    }

    public String getBillAddrLine1() {
        return getString(FieldName.billAddrLine1);
    }

    public String getBillAddrLine2() {
        return getString(FieldName.billAddrLine2);
    }

    public String getBillAddrLine3() {
        return getString(FieldName.billAddrLine3);
    }

    public String getBillAddrPostCode() {
        return getString(FieldName.billAddrPostCode);
    }

    public String getBillAddrState() {
        return getString(FieldName.billAddrState);
    }

    public String getShipAddrCity() {
        return getString(FieldName.shipAddrCity);
    }

    public String getShipAddrCountry() {
        return getString(FieldName.shipAddrCountry);
    }

    public String getShipAddrLine1() {
        return getString(FieldName.shipAddrLine1);
    }

    public String getShipAddrLine2() {
        return getString(FieldName.shipAddrLine2);
    }

    public String getShipAddrLine3() {
        return getString(FieldName.shipAddrLine3);
    }

    public String getShipAddrPostCode() {
        return getString(FieldName.shipAddrPostCode);
    }

    public String getShipAddrState() {
        return getString(FieldName.shipAddrState);
    }

    public String getCardholderName() {
        return getString(FieldName.cardholderName);
    }

    public MobilePhone getMobilePhone() {
        JsonNode fieldNode = rootNode.at(getParentPath(FieldName.mobilePhone));
        if (fieldNode.isObject()) {
            try {
                return objectMapper.convertValue(fieldNode, MobilePhone.class);
            } catch (Exception e) {
            }
        }
        return null;
    }

    public String getChallengeCancel() {
        return getString(FieldName.challengeCancel);
    }

    public String getDSReferenceNumber() {
        return getString(FieldName.dsReferenceNumber);
    }

    public String getDSStartProtocolVersion() {
        return getString(FieldName.dsStartProtocolVersion);
    }

    public String getDSEndProtocolVersion() {
        return getString(FieldName.dsEndProtocolVersion);
    }

    public String getDSURL() {
        return getString(FieldName.dsURL);
    }

    public String getPayTokenInd() {
        return getString(FieldName.payTokenInd);
    }

    public String getPayTokenSource() {
        return getString(FieldName.payTokenSource);
    }

    public String getInteractionCounter() {
        return getString(FieldName.interactionCounter);
    }

    public MerchantRiskIndicator getMerchantRiskIndicator() {
        JsonNode fieldNode = rootNode.at(getParentPath(FieldName.merchantRiskIndicator));
        if (fieldNode.isObject()) {
            try {
                return objectMapper.convertValue(fieldNode, MerchantRiskIndicator.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getWhitelistStatus() {
        return getString(FieldName.whiteListStatus);
    }

    public String getWhiteListStatusSource() {
        return getString(FieldName.whiteListStatusSource);
    }

    /**
     * This gets the Boolean value of the given json field path starting from the rootNode.
     *
     * @param fieldName Uses dot notation for nested json field names
     * @return Returns the Boolean value if found, else returns null.
     */
    private Boolean getBoolean(String fieldName) {
        String booleanString = getString(fieldName, rootNode);
        if (BooleanUtils.isBoolean(booleanString)) {
            return Boolean.parseBoolean(booleanString);
        }
        return null;
    }

    /**
     * Returns true if fieldName is present but with empty or null value.
     *
     * @param fieldName Uses dot notation for nested json field names
     * @return
     */
    public boolean isEmptyOrNullField(String fieldName) {
        JsonNode fieldNode = rootNode.at(getParentPath(fieldName));
        return !fieldNode.isMissingNode() &&
               (fieldNode.isNull() ||
                (fieldNode.isTextual() && StringUtils.isEmpty(fieldNode.asText())) ||
                (fieldNode.isArray() && fieldNode.isEmpty()) ||
                (fieldNode.isObject() && fieldNode.isEmpty()) ||
                (!fieldNode.isTextual() && StringUtils.isEmpty(fieldNode.toString())));
    }

    /**
     * This gets the String value of the given json field path starting from the rootNode.
     *
     * @param fieldName Uses dot notation for nested json field names
     * @return Returns the string value if found, else returns null.
     */
    public String getString(String fieldName) {
        return getString(fieldName, rootNode);
    }

    /**
     * This gets the String value of the given json field path starting from the given parentNode.
     */
    private String getString(String fieldName, JsonNode parentNode) {
        JsonNode fieldNode = parentNode.at(getParentPath(fieldName));
        return fieldNode.isMissingNode() ?
            null :
            (fieldNode.isNull() ? null : fieldNode.asText());
    }

    /**
     * This sets the String value of the given json field path.
     *
     * @param fieldName Uses dot notation for nested json field names
     */
    private void setString(String fieldName, String value) {
        if (StringUtils.isEmpty(value)) {
            remove(fieldName);
            return;
        }

        if (fieldName.contains(".")) {
            String[] fieldNamePath = fieldName.split("\\.");
            String parentPath = "/" + StringUtils.join(fieldNamePath, '/', 0, fieldNamePath.length - 1);
            JsonNode parentNode = rootNode.at(parentPath);
            if (parentNode.isMissingNode()) {
                parentNode = createParentObject(fieldNamePath);
            }
            ((ObjectNode) parentNode).put(fieldNamePath[fieldNamePath.length - 1], value);
        } else {
            ((ObjectNode) rootNode).put(fieldName, value);
        }
    }

    /**
     * Add array items on the given json field path.
     *
     * @param fieldName  Uses dot notation for nested json field names
     * @param arrayItems Array of objects to be added on the json
     */
    private void addArrayItems(String fieldName, Object... arrayItems) {
        ArrayNode arrayNode = findArrayNode(fieldName);
        if (arrayNode == null) {
            arrayNode = createArrayNode(fieldName);
        }
        for (Object arrayItem : arrayItems) {
            arrayNode.addPOJO(arrayItem);
        }
    }

    /**
     * Returns the ArrayNode pointed by the given json field path.
     *
     * @param fieldName Uses dot notation for nested json field names
     */
    private ArrayNode findArrayNode(String fieldName) {
        String[] fieldNamePath = fieldName.split("\\.");
        String fieldPath = "/" + StringUtils.join(fieldNamePath, '/', 0, fieldNamePath.length);
        JsonNode fieldNode = rootNode.at(fieldPath);
        if (!fieldNode.isMissingNode()) {
            return (ArrayNode) fieldNode;
        } else  {
            return null;
        }
    }

    private ArrayNode createArrayNode(String fieldName) {
        ArrayNode arrayNode;
        if (fieldName.contains(".")) {
            String[] fieldNamePath = fieldName.split("\\.");
            ObjectNode parentNode = createParentObject(fieldNamePath);
            arrayNode = parentNode.putArray(fieldNamePath[fieldNamePath.length - 1]);
        } else {
            arrayNode = ((ObjectNode) this.rootNode).putArray(fieldName);
        }

        return arrayNode;
    }

    /**
     * This creates necessary parent objects, if not existing yet, as preparation on the leaf nodes.
     */
    private ObjectNode createParentObject(String[] fieldNamePath) {
        ObjectNode currentNode = (ObjectNode) this.rootNode;
        for (int index = 0; index < fieldNamePath.length - 2; index++) {
            JsonNode fieldNode = currentNode.findValue(fieldNamePath[index]);
            if (fieldNode.isMissingNode()) {
                ObjectNode newObjectNode = currentNode.putObject(fieldNamePath[index]);
                currentNode = newObjectNode;
            }
        }
        return currentNode;
    }

    /**
     * Gets the leaf parent node from the json tree, if existing, based on the fieldNamePath
     */
    JsonNode getLeafParent(String[] fieldNamePath) {
        if (fieldNamePath.length > 1) {
            String parentPath = "/" + StringUtils.join(fieldNamePath, '/', 0, fieldNamePath.length - 1);
            return rootNode.at(parentPath);
        }
        return rootNode;
    }

    @Override
    public String toString() {
        return this.rootNode.toString();
    }

    public String toPrettyString() {
        return this.rootNode.toPrettyString();
    }

    String getParentPath(String fieldName) {
        String[] fieldNamePath = fieldName.split("\\.");
        return "/" + StringUtils.join(fieldNamePath, '/', 0, fieldNamePath.length);
    }

    public boolean isArray(String fieldName) {
        JsonNode fieldNode = rootNode.at(getParentPath(fieldName));
        return fieldNode != null && !fieldNode.isMissingNode() && fieldNode.isArray();
    }

    public boolean isObject(String fieldName) {
        JsonNode fieldNode = rootNode.at(getParentPath(fieldName));
        return fieldNode != null && !fieldNode.isMissingNode() && fieldNode.isObject();
    }

}
