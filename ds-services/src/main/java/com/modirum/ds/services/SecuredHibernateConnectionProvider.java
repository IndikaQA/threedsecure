/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 03.05.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.hsm.HSMDevice;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author andri This class is used to meet PCI DSS requirements that db passwords are secured in separate files
 * @deprectated
 */
// functionality moved to batch process..
public class SecuredHibernateConnectionProvider extends org.hibernate.c3p0.internal.C3P0ConnectionProvider
        // 4.3 org.hibernate.c3p0.internal.C3P0ConnectionProvider
        // 4.2 org.hibernate.service.jdbc.connections.internal.C3P0ConnectionProvider
{
    private static final long serialVersionUID = 1L;

    public void configure(java.util.Properties props) {
        super.configure(props);
    }

    // hibernate 4.2 properties replaced by map
    public void configure(Map props) {

        String password = (String) props.get("connection.password");
        if (password == null) {
            password = (String) props.get("hibernate.connection.password");
        }

        if (password != null && password.startsWith("vep://")) {
            try {
                HSMDevice se = (HSMDevice) Class.forName((String) props.get("hsmDevice")).newInstance();
                se.initializeHSM((String) props.get("hsmConfig"));
                password = password.substring(6);
                password = CryptoService.decryptConfigPassword(password, se);
                props.put("connection.password", password);
                props.put("hibernate.connection.password", password);

            } catch (Exception e) {
                throw new RuntimeException("Failed to decrypt password ", e);
            }
        }

        super.configure(props);
    }

    //com.mchange.v2.c3p0.impl.AbstractPoolBackedDataSource
    // dig out real connection failure and add to exception chain so you dont need to
    // pull off your hair to figure out what the issue is from
    //Caused by: com.mchange.v2.resourcepool.CannotAcquireResourceException: A ResourcePool could not acquire a resource from its primary factory or source.
    public Connection getConnection() throws SQLException {
        try {
            return super.getConnection();
        } catch (SQLException se) {
            Class cpp = null;
            Method m = null;
            try {
                cpp = this.getClass().getClassLoader().loadClass("com.mchange.v2.c3p0.PooledDataSource");
                m = cpp.getMethod("getLastAcquisitionFailureDefaultUser", null);

                if (cpp != null && super.isUnwrappableAs(cpp)) {
                    Object ppds = super.unwrap(cpp);
                    if (m != null) {
                        Throwable realle = (Throwable) m.invoke(ppds, null);
                        se.setNextException(new SQLException(realle));
                    }
                }
            } catch (Throwable t) {
            }

            throw se;
        }
    }

}
