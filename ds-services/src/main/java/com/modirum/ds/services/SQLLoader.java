/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 04.04.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.utils.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;

public class SQLLoader {

    protected transient static Logger log = LoggerFactory.getLogger(SQLLoader.class);

    final long DT24H = 24L * 3600L * 1000L;
    int lastStartLine = 0;
    int lineCount = 0;

    /**
     * @param sqlFileName - sql file name to load
     * @return how many statements executed
     * @throws Exception
     */
    public int process(String sqlFileName, String fileEncoding, java.util.Properties settings) throws Exception {
        PersistenceService tfs = ServiceLocator.getInstance().getPersistenceService();
        Context ctx = new Context();
        ctx.getCtxHolder().setApplicationAttribute("properties", settings);
        ctx.getCtxHolder().setApplicationAttribute(PersistenceService.class.getName(), tfs);

        File sqlFile = new File(sqlFileName);
        log.info("Starting to process file " + sqlFile.getAbsolutePath());
        FileInputStream fis = null;
        BufferedReader reader = null;
        Connection con = null;

        long timingStart = System.currentTimeMillis();

        int done = 0;
        int updated = 0;
        try {

            fis = new FileInputStream(sqlFile);
            reader = new BufferedReader(new java.io.InputStreamReader(fis, fileEncoding));
            log.info("File " + sqlFile.getAbsolutePath() + " opened");

            con = tfs.getStatelessSession().connection();
            log.info("Db connection " + con.getMetaData().getURL() + " opened");

            do {

                String statement = getNextStatment(reader);
                if (statement == null || statement.trim().length() < 1) {
                    log.info("No more staments found, quitting..");
                    break;
                }

                log.info("Exec: " + statement);
                if (statement.toLowerCase().startsWith("select")) {
                    ResultSet rs = con.createStatement().executeQuery(statement);
                    StringBuilder rsb = new StringBuilder();
                    while (rs.next()) {

                        for (int s = 1; true; s++) {
                            try {
                                String val = rs.getString(s);
                                rsb.append("\t" + val);

                            } catch (Exception dc) {
                                break;
                            }
                        }
                        rsb.append("\n");

                    }
                    rs.close();
                    log.info("Select returned:\n" + rsb);

                } else {
                    updated += con.createStatement().executeUpdate(statement);
                }

                done++;

            } while (true);

            if (!con.getAutoCommit()) {
                con.commit();
            }

        } catch (Exception e) {
            log.error("Error sql loading.. (near line " + lastStartLine + ")", e);
            throw e;

        } finally {
            if (fis != null || reader != null) {
                try {
                    reader.close();
                } catch (Exception dc) {
                }
                try {
                    fis.close();
                } catch (Exception dc) {
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception dc) {
                }
            }
        }

        log.info("Total " + done + " statments executed in " + (System.currentTimeMillis() - timingStart));
        log.info("Total " + updated + " rows affected");

        return done;
    }

    protected String getNextStatment(java.io.BufferedReader rder) throws Exception {
        StringBuffer stmnt = new StringBuffer();
        String line = null;
        lastStartLine = lineCount;

        int countquotes = 0;

        do {

            line = rder.readLine();
            this.lineCount++;
            if (line != null) {
                if (line.trim().indexOf("--") == 0) {
                    // skip comments
                } else if (line.indexOf(";") > 0 && line.indexOf("\\;") != line.indexOf(";") - 1) {
                    // terminate statment if has ;
                    String sql = line.substring(0, line.indexOf(";"));
                    int countquotesx = countquotes(sql);
                    if ((countquotesx + countquotes) % 2 == 0) {
                        stmnt.append(sql);
                        break;
                    } else {
                        countquotes += countquotes(line);
                        stmnt.append(line + "\r\n");
                    }
                } else {
                    countquotes += countquotes(line);
                    stmnt.append(line + "\r\n");
                }

            }
        } while (line != null);

        if (stmnt.length() > 0) {
            return stmnt.toString();
        }

        return null;
    }

    int countquotes(String s) {
        int cnt = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '\'') {
                cnt++;
            }
        }
        return cnt;
    }
}
