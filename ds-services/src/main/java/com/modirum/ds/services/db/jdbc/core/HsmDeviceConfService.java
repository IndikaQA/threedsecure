package com.modirum.ds.services.db.jdbc.core;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.jdbc.core.dao.HsmDeviceConfDAO;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("hsmDeviceConfService")
public class HsmDeviceConfService {

    @Resource
    private HsmDeviceConfDAO hsmDeviceConfDAO;

    /**
     * Retrieves an HSM Device Conf given its HSM Device ID and default DS ID = 0
     * @param hsmDeviceId
     * @return
     */
    public HsmDeviceConf get(Long hsmDeviceId) {
        return hsmDeviceConfDAO.get(hsmDeviceId, DSModel.HsmDeviceConf.dsId.DEFAULT_DS);
    }

    /**
     * Deletes an HSM Device Conf
     * @param hsmDeviceConf
     */
    public void delete(HsmDeviceConf hsmDeviceConf) {
        hsmDeviceConfDAO.delete(hsmDeviceConf);
    }

    /**
     * Inserts or updates an HSM Device Conf depending if the record already exists
     * @param hsmDeviceConf
     */
    public void saveOrUpdate(HsmDeviceConf hsmDeviceConf) {
        if (hsmDeviceConfDAO.exists(hsmDeviceConf.getHsmDeviceId(), hsmDeviceConf.getDsId())) {
            update(hsmDeviceConf);
        } else {
            insert(hsmDeviceConf);
        }
    }

    /**
     * Inserts an HSM Device Conf
     * @param hsmDeviceConf
     */
    public void insert(HsmDeviceConf hsmDeviceConf) {
        hsmDeviceConfDAO.insert(hsmDeviceConf);
    }

    /**
     * Updates an HSM Device Conf
     * @param hsmDeviceConf
     */
    public void update(HsmDeviceConf hsmDeviceConf) {
        hsmDeviceConfDAO.update(hsmDeviceConf);
    }
}
