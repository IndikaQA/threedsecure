package com.modirum.ds.services.db.jdbc.audit;


import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;
import com.modirum.ds.db.model.User;
import com.modirum.ds.jdbc.audit.dao.AuditLogDAO;
import com.modirum.ds.jdbc.audit.model.AuditLog;
import com.modirum.ds.jdbc.core.model.DSInit;
import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service("auditLogJdbcService")
public class AuditLogJdbcService {

    private static final Logger log = LoggerFactory.getLogger(AuditLogJdbcService.class);

    @Resource
    private AuditLogDAO auditLogDAO;

    /**
     * Logs <code>dsInit</code> updates to audit log.
     */
    public void logUpdate(DSInit dsInit, User user) {
        String auditDetail = "DS Init entry with key " + dsInit.getInitKey() + ", value " +  dsInit.getInitValue();

        logUpdate(auditDetail, user, dsInit.getClass());
    }

    public void logUpdate(PaymentSystemConfig paymentSystemConfig, User user) {
        String auditDetail = String.format(
                "Payment system config entry with dsId %d, paymentSystemId %d, key %s, value %s, comment %s",
                paymentSystemConfig.getDsId(), paymentSystemConfig.getPaymentSystemId(), paymentSystemConfig.getKey(),
                paymentSystemConfig.getValue(), paymentSystemConfig.getComment());

        logUpdate(auditDetail, user, paymentSystemConfig.getClass());
    }

    public void logInsert(HsmDevice hsmDevice, User user) {
        String auditDetail = String.format("HSM Device entry with ID %d", hsmDevice.getId());

        logInsert(auditDetail, user, hsmDevice.getId(), hsmDevice.getClass());
    }

    public void logUpdate(HsmDevice hsmDevice, User user) {
        String auditDetail = String.format("HSM Device entry with ID %d", hsmDevice.getId());

        logUpdate(auditDetail, user, hsmDevice.getId(), hsmDevice.getClass());
    }

    public void logInsert(HsmDeviceConf hsmDeviceConf, User user) {
        String auditDetail = String.format("HSM Device Conf entry with HSM Device ID %d, dsId %d and config %s",
                hsmDeviceConf.getHsmDeviceId(), hsmDeviceConf.getDsId(), hsmDeviceConf.getConfig());

        logInsert(auditDetail, user, hsmDeviceConf.getClass());
    }

    public void logUpdate(HsmDeviceConf hsmDeviceConf, User user) {
        String auditDetail = String.format("HSM Device Conf entry with HSM Device ID %d, dsId %d and config %s",
                hsmDeviceConf.getHsmDeviceId(), hsmDeviceConf.getDsId(), hsmDeviceConf.getConfig());

        logUpdate(auditDetail, user, hsmDeviceConf.getClass());
    }

    public void logDelete(HsmDeviceConf hsmDeviceConf, User user) {
        String auditDetail = String.format("HSM Device Conf entry with HSM Device ID %d, dsId %d and config %s",
                hsmDeviceConf.getHsmDeviceId(), hsmDeviceConf.getDsId(), hsmDeviceConf.getConfig());

        logDelete(auditDetail, user, hsmDeviceConf.getClass());
    }

    private void logInsert(String auditDetail, User user, Class<?> clazz) {
        logInsert(auditDetail, user, null, clazz);
    }

    private void logInsert(String auditDetail, User user, Long objectId, Class<?> clazz) {
        log(auditDetail, user, objectId, clazz, DSModel.AuditLog.Action.INSERT);
    }

    private void logUpdate(String auditDetail, User user, Class<?> clazz) {
        logUpdate(auditDetail, user, null, clazz);
    }

    private void logUpdate(String auditDetail, User user, Long objectId, Class<?> clazz) {
        log(auditDetail, user, objectId, clazz, DSModel.AuditLog.Action.UPDATE);
    }

    private void logDelete(String auditDetail, User user, Class<?> clazz) {
        log(auditDetail, user, null, clazz, DSModel.AuditLog.Action.DELETE);
    }

    private void log(String auditDetail, User user, Long objectId, Class<?> clazz, String action) {
        objectId = Misc.isNullOrEmpty(objectId) ? 0L : objectId;
        AuditLog auditLog = AuditLog.builder()
                .byuserId(user != null ? user.getId() : 0L)
                .whendate(new Date())
                .byuser(user != null ? user.getLoginname() : null)
                .action(action)
                .details(auditDetail)
                .objectClass(clazz.getSimpleName())
                .objectId(objectId).build();

        try {
            auditLogDAO.insert(auditLog);
        } catch (Exception e) {
            log.error("AuditLog error ", e);
            log.info("{} {} {} {} {} {}", auditLog.getWhendate(), auditLog.getAction(), auditLog.getByuser(),
                    auditLog.getObjectClass(), auditLog.getObjectId(), auditLog.getDetails());
        }
    }
}
