/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 13.03.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import java.security.SecurityPermission;

public interface Permissions {

    SecurityPermission DS_SecurityPermission = new SecurityPermission("DS_SecurityPermission");
}
