/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 28.04.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.exceptions.KeyDataIdNotFoundException;
import com.modirum.ds.util.CryptoUtil;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.GuardedObject;
import java.security.Key;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

@Service
public class KeyService extends com.modirum.ds.hsm.KeyServiceBase {

    private transient static Logger log = LoggerFactory.getLogger(KeyService.class);

    public interface KeyAlias {

        String hmacKey = "hmacKey"; // hmac for pans
        String hmacKeyExt = "hmacKeyExt"; // hmac for pans for external system
        String dataKey = "dataKey"; // to encrypt data for certain items
        String schemeRootCert = "schemeRootCert"; // scheme root cert
        String schemeIntermedCert = "schemeIntermedCert"; // scheme intermedite ca cert
        String schemeIntermedCSR = "schemeIntermedCSR"; // scheme inermediate ca CSR
        String trustedRootCert = "trustedRootCert"; // other trusted root certs
        String clientAuthCert = "clientAuthCert";
        String sharedSecret = "sharedSecret";

        String sdkECKey = "sdkECKey"; // AReq.sdkEncData decrypt EC SDK key
        String sdkRSAKey = "sdkRSAKey"; // AReq.sdkEncData decrypt RSA SDK key
        String sdkRSACSR = "sdkRSACSR";

        // LEGACY SUPPORT //
        String signerKeystore = "signerKeystore"; // keysigners for key management dual control
        String keysKey = "keysKey"; // main (HSM) wrapper key for other (software) keys for dual control key management
        String masterkeyInfo = "masterkeyInfo"; // HSM master key information (id,KCV, block size)
    }

    // LEGACY
    public interface KeySignerAlias {
        String KeySigner1 = "keysigner1";
        String KeySigner2 = "keysigner2";
    }

    public final static int RRSAKeyMin = 2048;
    public final static int RRSAKeyMax = 8192;

    public static final String sigAlgRSASHA1 = "SHA1withRSA";
    public static final String sigAlgRSASHA256 = "SHA256withRSA";
    public static final String sigAlgRSASHA384 = "SHA384withRSA";
    public static final String sigAlgRSASHA512 = "SHA512withRSA";
    public static final String sigAlgECDSASHA256 = "SHA256WithECDSA";
    public static final long day = 24L * 3600000L;
    final static byte[] zeroblock8 = {0, 0, 0, 0, 0, 0, 0, 0};
    final static byte[] zeroblock16 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private final transient Map<String, KeyObject> keyCache = new HashMap<>(); // keyAlias to (decrypted) key cache
    private static final long keyCachePeriod = 12 * 60 * 60000; // reload key at least once in 12 hours
    final static int minSymKeyLenBytes = 16;
    final static int minUniqueBytesInKey = 8;
    final static int minUniqueLettersInPassword = 5;
    final static int minUniqueNumbersInPassword = 4;

    public static final int cMaxCryptoperiod = 1440;

    public final static String BEGIN_CERTIFICATE = "-----BEGIN CERTIFICATE-----";
    public final static String END_CERTIFICATE = "-----END CERTIFICATE-----";

    final static int warnDays = 30;
    public static final String signDateFormat = "yyyy-MM-dd HH:mm";

    static class KeyAndId {
        Key key;
        Long id;
    }

    static class KeyObject {
        GuardedObject key;
        long loaded;
        Long kid;
    }

    private final String ksType = "JKS";
    private final String ksProvider = "SUN";
    Date lastKeyDate;
    int lastKeysCount;

    // initialize safe default
    private static SecureRandom sr = new SecureRandom(CryptoUtil.SHA512(
            ("QQuu6343636ds9r4h" + new Date() + "s5NLHWM" + Runtime.getRuntime().totalMemory() + "Zd9Bsl" +
             System.currentTimeMillis() + " " + Math.random()).getBytes(StandardCharsets.UTF_8)));

    public KeyService() {
        getBouncyCastleProvider();

        // now initialize SHA1PRNG
        try {
            sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(CryptoUtil.SHA512(
                    ("sAAxdasPPQUds9r4h" + new Date() + "s5NNSGHWM" + Runtime.getRuntime().totalMemory() + "Zsld9Bsl" +
                     System.currentTimeMillis() + " " + Math.random()).getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void resetCache() {
        keyCache.clear();
    }

    public static byte[] genKey(int length) {
        byte[] keyMat = new byte[length];
        int test = 0;
        while (true) {
            sr.nextBytes(keyMat);
            test++;
            if (test > 5 || validateKey(keyMat)) {
                break;
            }
        }

        return keyMat;
    }

    public static byte[] genRnd(int length) {
        byte[] keyMat = new byte[length];
        sr.nextBytes(keyMat);
        return keyMat;
    }

    public static boolean validateKey(byte[] km) {
        if (km == null || km.length < minSymKeyLenBytes) {
            throw new RuntimeException(
                    "Too weak key.. length " + (km != null ? km.length * 8 : 0) + " bits, min allowed " +
                    (minSymKeyLenBytes * 8) + " bits");
        }

        Map<Integer, Integer> numMap = new TreeMap<>();

        for (int i = 0; i < km.length; i++) {
            Integer cx = (int) km[i];
            numMap.put(cx, cx);
        }

        return (numMap.size() >= minUniqueBytesInKey || numMap.size() >= km.length / 4);
    }

    public boolean isKeysChanged() throws Exception {
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        Map<String, Object> query = new HashMap<>(1);
        List<KeyData> sl = pse.getPersitableList(KeyData.class, "keyDate", "desc", true, 0, 1, query);

        Long count = (Long) query.get("total");
        boolean countchanged = false;
        boolean lmfchanged = false;
        if (count != null && lastKeysCount != count.intValue()) {
            countchanged = true;
            lastKeysCount = count.intValue();
        }

        KeyData lms = sl != null && sl.size() > 0 ? sl.get(0) : null;
        Date nowLastKeysUpdated = lms != null ? lms.getKeyDate() : null;
        if (lastKeyDate == null || nowLastKeysUpdated != null && nowLastKeysUpdated.after(lastKeyDate)) {
            lastKeyDate = nowLastKeysUpdated;
            lmfchanged = true;
        }
        return countchanged || lmfchanged;
    }

    public KeyData loadRawKeyData(String keyAlias, String status) throws Exception {
        // todo add legacy flag to support keysigners.
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();

        KeyData rawDataKey = pse.getKeyDataByAlias(keyAlias, status);
        if (rawDataKey == null) {
            throw new RuntimeException(keyAlias + " " + status + " key not found, run key installer first");
        }
        return rawDataKey;
    }

    public KeyStore getSignerKeyStore() throws Exception {
        KeyObject ko = keyCache.get(KeyAlias.signerKeystore);
        if (ko == null || ko.loaded < System.currentTimeMillis() - 4 * keyCachePeriod) {
            ko = new KeyObject();
            ko.key = new java.security.GuardedObject(loadSignerKeyStore(), Permissions.DS_SecurityPermission);
            ko.loaded = System.currentTimeMillis();
            synchronized (keyCache) {
                keyCache.put(KeyAlias.signerKeystore, ko);
            }
        }

        return (KeyStore) ko.key.getObject();
    }

    /**
     * Looks up the latest (by date) SDK RSA private key from the database.
     *
     * @return SDK RSA private key, or null if not found.
     * @throws Exception in case of errors during retrieving the key.
     */
    public KeyData getSdkRsaPrivateKey() throws Exception {
        PersistenceService persistenceService = ServiceLocator.getInstance().getPersistenceService();
        List<KeyData> sdkRsaKeys = persistenceService.getKeyDatasByAlias(KeyAlias.sdkRSAKey, KeyData.KeyStatus.working,
                "keyDate", "desc", null, null, null);

        if (sdkRsaKeys == null || sdkRsaKeys.size() == 0) {
            return null;
        }

        return sdkRsaKeys.get(0);
    }

    /**
     * Looks up the first (by id) SDK EC private key from the database.
     *
     * @return SDK EC private key, or null if not found.
     * @throws Exception in case of errors during key retrieval.
     */
    public KeyData getSdkEcPrivateKey() throws Exception {
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        return pse.getKeyDataByAlias(KeyAlias.sdkECKey, KeyData.KeyStatus.working);
    }

    // todo doesn't need to be secure, just cert retrieval
    public String getSDKRSACertifcateString() throws Exception {
        KeyObject ko = keyCache.get(KeyAlias.sdkRSAKey + "CertString");
        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
            ko = new KeyObject();
            PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
            List<KeyData> sdkKey = pse.getKeyDatasByAlias(KeyAlias.sdkRSAKey, KeyData.KeyStatus.working, "keyDate",
                                                          "desc", null, null, null);
            //.getKeyDataByAlias(KeyAlias.sdkRSAKey", KeyData.KeyStatus.working);
            if (sdkKey != null && sdkKey.size() > 0) {
                StringBuilder certs = new StringBuilder(4096);
                for (KeyData kx : sdkKey) {
                    if (certs.length() > 0) {
                        certs.append("\r\n");
                    }
                    certs.append(parsePEMCert(kx.getKeyData(), true));
                }

                ko.key = new java.security.GuardedObject(certs.toString(), Permissions.DS_SecurityPermission);
            }
            ko.loaded = System.currentTimeMillis();
            synchronized (keyCache) {
                keyCache.put(KeyAlias.sdkRSAKey + "CertString", ko);
            }
        }

        return ko.key != null ? (String) ko.key.getObject() : null;
    }

    // todo doesn't need to be secure, just cert retrieval
    public String getSDKECCertifcateString() throws Exception {
        KeyObject ko = keyCache.get(KeyAlias.sdkECKey + "CertString");
        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
            ko = new KeyObject();
            PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
            List<KeyData> sdkKey = pse.getKeyDatasByAlias(KeyAlias.sdkECKey, KeyData.KeyStatus.working, "keyDate",
                                                          "desc", null, null, null);
            if (sdkKey != null && sdkKey.size() > 0) {
                StringBuilder certs = new StringBuilder(4096);
                for (KeyData kx : sdkKey) {
                    if (certs.length() > 0) {
                        certs.append("\r\n");
                    }
                    certs.append(parsePEMCert(kx.getKeyData(), true));
                }

                ko.key = new java.security.GuardedObject(certs.toString(), Permissions.DS_SecurityPermission);
            }
            ko.loaded = System.currentTimeMillis();
            synchronized (keyCache) {
                keyCache.put(KeyAlias.sdkECKey + "CertString", ko);
            }
        }

        return ko.key != null ? (String) ko.key.getObject() : null;
    }

    public KeyData getPrivateKeyAndCertChainById(Long certId) throws KeyDataIdNotFoundException {
        // todo add legacy flag to support caching and keysigners.
        return loadPrivateKeyAndCertChain(certId);

//        KeyObject ko = keyCache.get(KeyAlias.clientAuhtCert + "." + certId);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            ko.key = new java.security.GuardedObject(loadPrivateKeyAndCertChain(certId, "RSA"),
//                                                     Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            synchronized (keyCache) {
//                keyCache.put(KeyAlias.clientAuhtCert + "." + certId, ko);
//            }
//        }
//        return (Object[]) ko.key.getObject();
    }

    @SuppressWarnings("unchecked")
    public List<X509Certificate> getSchemeRoots() throws Exception {
        // todo add legacy flag to support caching and keysigners.
        List<X509Certificate> working = loadSchemeRoots(KeyData.KeyStatus.working);
        List<X509Certificate> retired = loadSchemeRoots(KeyData.KeyStatus.retired);
        List<X509Certificate> all = new LinkedList<>();
        all.addAll(working);
        all.addAll(retired);
        return all;

//        KeyObject ko = keyCache.get(KeyAlias.schemeRootCert);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            List<X509Certificate> working = loadSchemeRoots(KeyData.KeyStatus.working);
//            List<X509Certificate> retired = loadSchemeRoots(KeyData.KeyStatus.retired);
//            List<X509Certificate> all = new java.util.LinkedList<>();
//            all.addAll(working);
//            all.addAll(retired);
//            ko.key = new java.security.GuardedObject(all, Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            synchronized (keyCache) {
//                keyCache.put(KeyAlias.schemeRootCert, ko);
//            }
//        }
//
//        return (List<X509Certificate>) ko.key.getObject();
    }

    @SuppressWarnings("unchecked")
    public List<X509Certificate> getSchemeIntermediates() throws Exception {
        // todo add legacy flag to support caching and keysigners.
        return loadSchemeIntermediates();

//        KeyObject ko = keyCache.get(KeyAlias.schemeIntermedCert);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            ko.key = new java.security.GuardedObject(loadSchemeIntermediates(), Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            synchronized (keyCache) {
//                keyCache.put(KeyAlias.schemeIntermedCert, ko);
//            }
//        }
//
//        return (List<X509Certificate>) ko.key.getObject();
    }

    @SuppressWarnings("unchecked")
    public List<X509Certificate> getTrustedRoots() throws Exception {
        // todo add legacy flag to support caching and keysigners.
        return loadTrustedRoots();
//        KeyObject ko = keyCache.get(KeyAlias.trustedRootCert);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            ko.key = new java.security.GuardedObject(loadTrustedRoots(), Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            synchronized (keyCache) {
//                keyCache.put(KeyAlias.trustedRootCert, ko);
//            }
//        }
//
//        return (List<X509Certificate>) ko.key.getObject();
    }

    public KeyStore getAllTrustedRootsAsKeyStore() throws Exception {
        KeyObject ko = keyCache.get("AllTrustedRoots" + ".AsKeyStore");
        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
            List<X509Certificate> roots = getSchemeRoots();

            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(null, null);
            for (X509Certificate rx : roots) {
                ks.setCertificateEntry(rx.getSubjectDN().getName(), rx);
            }
            List<X509Certificate> roots2 = getTrustedRoots();
            for (X509Certificate rx : roots2) {
                ks.setCertificateEntry(rx.getSubjectDN().getName(), rx);
            }

            ko = new KeyObject();
            ko.key = new java.security.GuardedObject(ks, Permissions.DS_SecurityPermission);
            ko.loaded = System.currentTimeMillis();

            synchronized (keyCache) {
                keyCache.put("AllTrustedRoots" + ".AsKeyStore", ko);
            }
        }

        return (KeyStore) ko.key.getObject();
    }

    /**
     * Legacy roots loading - checks keydata using signers.
     */
    public List<X509Certificate> loadSchemeRoots(String status) throws Exception {
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        List<X509Certificate> roots = new ArrayList<>(10);
        List<KeyData> rootsData = pse.getKeyDatasByAlias(KeyAlias.schemeRootCert + "%", status, "keyAlias", null, null,
                                                         null, null);

        for (KeyData rx : Misc.nullSafe(rootsData)) {
            // TODO enable via legacy flag
//            verifyKeyData(rx, this.getSignerKeyStore());
            X509Certificate[] certs = parseX509Certificates(Base64.decode(parsePEMCert(rx.getKeyData())), null);
            roots.add(certs[0]);
        }

        return roots;
    }

    public List<X509Certificate> loadSchemeIntermediates() throws Exception {
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        List<X509Certificate> intermeds = new ArrayList<X509Certificate>(10);
        List<KeyData> intermedsData = pse.getKeyDatasByAlias(KeyAlias.schemeIntermedCert + "%",
                                                             KeyData.KeyStatus.working, "keyAlias", null, null, null,
                                                             null);

        for (KeyData rx : Misc.nullSafe(intermedsData)) {
            // TODO enable via legacy flag
//            verifyKeyData(rx, this.getSignerKeyStore());
            X509Certificate[] certs = parseX509Certificates(Base64.decode(parsePEMCert(rx.getKeyData())), null);
            intermeds.add(certs[0]);
        }

        return intermeds;
    }

    public List<X509Certificate> loadTrustedRoots() throws Exception {
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        List<X509Certificate> roots = new ArrayList<>(10);
        List<KeyData> rootsData = pse.getKeyDatasByAlias(KeyAlias.trustedRootCert + "%", KeyData.KeyStatus.working,
                                                         "keyAlias", null, null, null, null);

        for (KeyData rx : Misc.nullSafe(rootsData)) {
            // TODO enable via legacy flag
//            verifyKeyData(rx, this.getSignerKeyStore());
            X509Certificate[] certs = parseX509Certificates(Base64.decode(parsePEMCert(rx.getKeyData())), null);
            roots.add(certs[0]);
        }
        return roots;
    }

    public KeyStore loadSignerKeyStore() throws Exception {
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        KeyData ksData = pse.getKeyDataByAlias(KeyAlias.signerKeystore, KeyData.KeyStatus.working);
        if (ksData == null) {
            throw new RuntimeException("Signers not found, run key autoinstaller first");
        }

        KeyStore sks = this.loadKeyStore(Base64.decode(ksData.getKeyData()), new char[0]);

        // TODO enable via legacy flag
//        verifyKeyData(ksData, sks);

        return sks;
    }

    public final void verifyKeyData(KeyData keyData, KeyStore signerKs) throws Exception {
        if (keyData == null) {
            throw new RuntimeException("Key not found, run key autoinstaller first");
        }
        if (Misc.isNullOrEmpty(keyData.getKeyData())) {
            throw new RuntimeException("Key " + keyData.getKeyAlias() + " status " + keyData.getStatus() +
                                       " not found, run key installer first");
        }

        if (keyData.getKeyDate() == null) {
            throw new RuntimeException(
                    "Key " + keyData.getKeyAlias() + " keydate is null, could not verify crypto period ");
        }

        if (keyData.getCryptoPeriodDays() == null) {
            throw new RuntimeException(
                    "Key " + keyData.getKeyAlias() + " crypto period is null, could not verify crypto period ");
        }

        // PADSS 2.5.4 Procedures for enforcing key changes at the end of the defined cryptoperiod.
        if (KeyData.KeyStatus.working.equals(keyData.getStatus()) &&
            System.currentTimeMillis() > keyData.getKeyDate().getTime() + keyData.getCryptoPeriodDays() * day) {
            RuntimeException rte = new RuntimeException(
                    "Crypto period of Key " + keyData.getId() + " " + keyData.getKeyAlias() + " status " +
                    keyData.getStatus() + " installed " +
                    DateUtil.formatDate(keyData.getKeyDate(), KeyService.signDateFormat, true) + " GMT" +
                    " with crypto period of " + keyData.getCryptoPeriodDays() +
                    " days, has been expired, PADSS 2.5.4 application is requred to enforce crypto periods, so cannot continue," +
                    " to fix the issue install new keys and migrate data if needed");

            throw rte;
        }

        if (KeyData.KeyStatus.working.equals(keyData.getStatus()) && System.currentTimeMillis() >
                                                                     keyData.getKeyDate().getTime() +
                                                                     (keyData.getCryptoPeriodDays() - warnDays) * day) {
            int daysToExpire = (int) ((keyData.getKeyDate().getTime() + keyData.getCryptoPeriodDays() * day -
                                       System.currentTimeMillis()) / day);
            log.warn("Key " + keyData.getId() + " " + keyData.getKeyAlias() + " " + keyData.getStatus() +
                     " crypto period is about to expire in " + daysToExpire +
                     " days, please install new keys prior cypto period end, to prevent application to fail");
        }

        Certificate[] cert1 = signerKs.getCertificateChain(KeyService.KeySignerAlias.KeySigner1);
        Certificate[] cert2 = signerKs.getCertificateChain(KeyService.KeySignerAlias.KeySigner2);

        String signDate = DateUtil.formatDate(keyData.getKeyDate(), KeyService.signDateFormat, true);
        ByteArrayOutputStream sbos = new ByteArrayOutputStream();
        sbos.write(keyData.getKeyAlias().getBytes(StandardCharsets.UTF_8));
        sbos.write(keyData.getKeyData().getBytes(StandardCharsets.UTF_8));
        sbos.write(keyData.getKeyCheckValue().getBytes(StandardCharsets.UTF_8));
        sbos.write(String.valueOf(keyData.getCryptoPeriodDays()).getBytes(StandardCharsets.UTF_8));
        sbos.write(signDate.getBytes(StandardCharsets.UTF_8));
        sbos.write(keyData.getSignedBy1().getBytes(StandardCharsets.UTF_8));
        sbos.write(keyData.getSignedBy2().getBytes(StandardCharsets.UTF_8));

        String sigAlg1 = getSigalgFromSignature(keyData.getSignature1());
        Signature rsaSig1 = Signature.getInstance(sigAlg1);
        rsaSig1.initVerify(cert1[0]);
        rsaSig1.update(sbos.toByteArray());
        boolean s1Ok = rsaSig1.verify(Base64.decode(getSigFromSignature(keyData.getSignature1())));
        //        if (!s1Ok) {
        //            throw new RuntimeException("Sgnature1 is not correct for keydata " + keyData.getKeyAlias() + " id " + keyData.getId());
        //        }

        String sigAlg2 = getSigalgFromSignature(keyData.getSignature2());
        Signature rsaSig2 = Signature.getInstance(sigAlg2);
        rsaSig2.initVerify(cert2[0]);
        rsaSig2.update(sbos.toByteArray());
        boolean s2Ok = rsaSig2.verify(Base64.decode(getSigFromSignature(keyData.getSignature2())));
        //        if (!s2Ok) {
        //            throw new RuntimeException("Sgnature2 is not correct for keydata " + keyData.getKeyAlias() + " id " + keyData.getId());
        //        }

        if (KeyAlias.masterkeyInfo.equals(keyData.getKeyAlias()) &&
            KeyData.KeyStatus.working.equals(keyData.getStatus())) {
            String kcvMaster = ServiceLocator.getInstance().getHSMDevice(keyData.getHsmDeviceId()).getMasterKeyCheckValue();
            if (!kcvMaster.equals(keyData.getKeyCheckValue())) {
                RuntimeException rte = new RuntimeException("Device provided master key check value " + kcvMaster +
                                                            " is not equal to installed and signed value " +
                                                            keyData.getKeyCheckValue() +
                                                            " device failure, out of the process master key change or wrong device..");

                throw rte;
            }
        }
    }

    public static boolean isRootCert(X509Certificate cert) {
        return cert.getIssuerDN().getName().equals(cert.getSubjectDN().getName());
    }

    public java.security.KeyStore loadKeyStore(byte[] data, char[] ksPassword) throws Exception {
        java.security.KeyStore keyStore = null;
        keyStore = java.security.KeyStore.getInstance(ksType, ksProvider);
        keyStore.load(new ByteArrayInputStream(data), ksPassword);
        return keyStore;
    }

    public final static String getSigalgFromSignature(String sig) {
        if (sig != null && sig.startsWith(KeyService.sigAlgRSASHA1)) {
            return KeyService.sigAlgRSASHA1;
        }
        if (sig != null && sig.startsWith(KeyService.sigAlgRSASHA256)) {
            return KeyService.sigAlgRSASHA256;
        }

        return KeyService.sigAlgRSASHA1;
    }

    public final static String getSigFromSignature(String sig) {
        if (sig != null && sig.startsWith(KeyService.sigAlgRSASHA1)) {
            return sig.substring(KeyService.sigAlgRSASHA1.length());
        }
        if (sig != null && sig.startsWith(KeyService.sigAlgRSASHA256)) {
            return sig.substring(KeyService.sigAlgRSASHA256.length());
        }

        return sig;
    }

    public static String getKeyHashCheckValue(byte[] key) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(key);
        String hash = HexUtils.toString(digest);

        return hash.substring(hash.length() - 8);
    }

    public static String calculateAESKeyCheckValue(byte[] key) throws Exception {
        Cipher aes = Cipher.getInstance("AES/CBC/NoPadding"); // PKCS5Padding
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        aes.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
        byte[] kcvfull = aes.doFinal(zeroblock16);
        String kcvffullHex = HexUtils.toString(kcvfull);

        return kcvffullHex.substring(kcvffullHex.length() - 6);
    }

    public static String calculateRSAPrivateKeyCheckValue(PrivateKey priKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, priKey);
        byte[] kcvfull = cipher.doFinal(zeroblock8);
        String kcvffullHex = HexUtils.toString(kcvfull);

        return kcvffullHex.substring(kcvffullHex.length() - 6);
    }

    public KeyData getKey(Long id) throws Exception {
        // todo load from DB + unwrap. Optionally verifyData if legacy flag exists
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        KeyData keyData = pse.getKeyDataById(id);
        if (keyData == null || Misc.isNullOrEmpty(keyData.getKeyData())) {
            throw new RuntimeException("By Id " + id + " key not found");
        }
        return keyData;
    }

    public KeyData loadPrivateKeyAndCertChain(Long id) throws KeyDataIdNotFoundException {
        // todo add legacy flag to support keysigners.
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        KeyData keyData = pse.getKeyDataById(id);
        if (keyData == null || Misc.isNullOrEmpty(keyData.getKeyData())) {
            throw new KeyDataIdNotFoundException("By Id " + id + " key not found");
        }
        return keyData;
    }

    public X509Certificate loadCertificate(Long id, String alg) throws Exception {
        // todo load from DB + unwrap. Optionally verifyData if legacy flag exists
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        KeyData keyData = pse.getKeyDataById(id);
        if (keyData == null || Misc.isNullOrEmpty(keyData.getKeyData())) {
            throw new RuntimeException("By Id " + id + " key not found");
        }

        String certDataPEM = parsePEMCert(keyData.getKeyData());
        byte[] cerPlain = Base64.decode(certDataPEM);
        X509Certificate[] certs = parseX509Certificates(cerPlain, alg);
        return certs[0];

//        try {
//            PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
//            KeyData masterKeyInfo = pse.getKeyDataByAlias(KeyAlias.masterkeyInfo, KeyData.KeyStatus.working);
//            if (masterKeyInfo == null) {
//                throw new RuntimeException(KeyAlias.masterkeyInfo + " not found, run key autoinstaller first");
//            }
//            verifyKeyData(masterKeyInfo, this.getSignerKeyStore());
//
//            KeyData hsmKey = pse.getKeyDataByAlias(KeyAlias.keysKey, KeyData.KeyStatus.working);
//            verifyKeyData(hsmKey, this.getSignerKeyStore());
//
//            KeyData pkData = pse.getKeyDataById(id);
//            if (pkData == null || Misc.isne(pkData.getKeyData())) {
//                throw new RuntimeException("By Id " + id + " key not found");
//            }
//            verifyKeyData(pkData, this.getSignerKeyStore());
//
//            String certDataPEM = parsePEMCert(pkData.getKeyData());
//            byte[] cerPlain = Base64.decode(certDataPEM);
//            X509Certificate[] certs = parseX509Certificates(cerPlain, alg);
//            return certs[0];
//        } catch (Exception e) {
//            log.error("RSA cert " + id + " load error", e);
//            throw e;
//        }
    }

    public KeyData getHMACKey() throws Exception {
        return getSecretKey(KeyAlias.hmacKey, KeyData.KeyStatus.working, CryptoService.HMAC_ALG);
    }

    public KeyData getHMACKey(String alias) throws Exception {
        return getSecretKey(alias, KeyData.KeyStatus.working, CryptoService.HMAC_ALG);
    }

    public Long getHMACKeyId() throws Exception {
        KeyData keyData = loadSecretKey(KeyAlias.hmacKey, KeyData.KeyStatus.working, CryptoService.HMAC_ALG);
        return keyData != null ? keyData.getId() : null;
        // todo caching for legacy support
//        KeyObject ko = keyCache.get(KeyAlias.hmacKey);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            KeyData keyData = loadSecretKey(KeyAlias.hmacKey, KeyData.KeyStatus.working, CryptoService.HMAC_ALG);
//            ko.key = new GuardedObject(keyData, Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            ko.kid = keyData.getId();
//            synchronized (keyCache) {
//                keyCache.put(KeyAlias.hmacKey, ko);
//            }
//        }
//
//        return ko.kid;
    }

    public KeyData getSecretKey(String alias, String status, String alg) throws Exception {
        // todo caching for legacy support
        return loadSecretKey(alias, status, alg);


//        final String mapKey = alias + "." + status;
//        KeyObject ko = keyCache.get(mapKey);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            KeyData keyData = loadSecretKey(alias, status, alg);
//            ko.key = new java.security.GuardedObject(keyData, Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            ko.kid = keyData.getId();
//            synchronized (keyCache) {
//                keyCache.put(mapKey, ko);
//            }
//        }
//        return (java.security.Key) ko.key.getObject();
    }

    public KeyData getSecretKey(Long kid, String alg) throws Exception {
        // todo caching for legacy support
        return loadSecretKey(kid, alg);


//        KeyObject ko = keyCache.get(KeyAlias.dataKey + "." + kid);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            ko.key = new java.security.GuardedObject(loadSecretKey(kid, alg), Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            ko.kid = kid;
//            synchronized (keyCache) {
//                keyCache.put(KeyAlias.dataKey + "." + kid, ko);
//            }
//        }
//        return (java.security.Key) ko.key.getObject();
    }

    public Long getKeyId(String alias, String status, String alg) throws Exception {
        // todo cache legacy
        KeyData keyData = loadSecretKey(alias, status, alg);
        return keyData != null ? keyData.getId() : null;


//        KeyObject ko = keyCache.get(alias + "." + status);
//        if (ko == null || ko.loaded < System.currentTimeMillis() - keyCachePeriod) {
//            ko = new KeyObject();
//            KeyAndId kid = loadSecretKey(alias, status, alg);
//            ko.key = new java.security.GuardedObject(kid.key, Permissions.DS_SecurityPermission);
//            ko.loaded = System.currentTimeMillis();
//            ko.kid = kid.id;
//            synchronized (keyCache) {
//                keyCache.put(alias + "." + status, ko);
//            }
//        }
//
//        return ko.kid;
    }

    /**
     * Retrieves a single per DS instance data encryption key (dataKey).
     * @return DS data encryption key.
     */
    public Optional<KeyData> getDataEncryptionKey() {
        PersistenceService persistenceService = ServiceLocator.getInstance().getPersistenceService();
        try {
            KeyData dataKey = persistenceService.getKeyDataByAlias(KeyAlias.dataKey, KeyData.KeyStatus.working);
            return Optional.of(dataKey);
        } catch (Exception e) {
            log.error("Error retrieving dataKey.", e);
        }
        return Optional.empty();
    }

    public KeyData loadSecretKey(String alias, String status, String alg) throws Exception {
        // todo add legacy flag to support keysigners.
        log.info("Load SecretKey " + alias + " key " + status);
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();

        KeyData encryptedSecretKey = pse.getKeyDataByAlias(alias, status);
        if (encryptedSecretKey == null || Misc.isNullOrEmpty(encryptedSecretKey.getKeyData())) {
            throw new RuntimeException("Key " + alias + " not found, use keys autoinstaller first");
        }
        return encryptedSecretKey;
    }

    public KeyData loadSecretKey(Long kid, String alg) throws Exception {
        // todo flag legacy support
        log.info("Load secret key " + kid + " " + alg);
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();

        KeyData encryptedKey = pse.getKeyDataById(kid);
        if (encryptedKey == null || Misc.isNullOrEmpty(encryptedKey.getKeyData())) {
            throw new RuntimeException("Key id " + kid + " not found, use keys autoinstaller first");
        }
        return encryptedKey;
    }

    public static java.security.Provider getBouncyCastleProvider() {
        java.security.Provider bcpe = java.security.Security.getProvider(
                org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME);
        if (bcpe == null) {
            bcpe = new org.bouncycastle.jce.provider.BouncyCastleProvider();
            java.security.Security.addProvider(bcpe);
        }

        return bcpe;
    }

    public static byte[] createP7B(java.util.List<X509Certificate> certs) throws Exception {
        getBouncyCastleProvider();
        CMSTypedData msg = new CMSProcessableByteArray("".getBytes());
        CMSSignedDataGenerator edGen = new CMSSignedDataGenerator();
        edGen.addCertificates(new JcaCertStore(certs));
        CMSSignedData ed = edGen.generate(msg, false);

        return ed.getEncoded();
    }

    public static String createCryptoPeriodWarnings(PersistenceService persistenceService) throws Exception {
        StringBuilder warnings = new StringBuilder();
        java.util.List<KeyData> kdl = persistenceService.getKeyDataByStatus(KeyData.KeyStatus.working);
        for (KeyData kd : kdl) {
            if (KeyData.KeyStatus.working.equals(kd.getStatus()) &&
                System.currentTimeMillis() > kd.getKeyDate().getTime() + (kd.getCryptoPeriodDays() - 30) * day) {
                int daysToExpire = (int) (
                        (kd.getKeyDate().getTime() + kd.getCryptoPeriodDays() * day - System.currentTimeMillis()) /
                        day);
                warnings.append("Key ").append(kd.getId()).append(" ").append(kd.getKeyAlias()).append(" ").append(
                        kd.getStatus()).append(" crypto period").append(daysToExpire > -1 ? " is about to expire in " +
                                                                                            daysToExpire + " days" :
                                                                                " expired " + (-daysToExpire) +
                                                                                " days ago").append(
                        ", please install new keys prior to the end of the crypto period, to prevent the application from failing\n");
            }
        }
        return warnings.toString();
    }
}
