/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 7. jaan 2016
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.json.JsonObjectBuilderImpl;
import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XJWE;
import com.modirum.ds.tds21msgs.XJWEprotected;
import com.modirum.ds.tds21msgs.XJWSCompact;
import com.modirum.ds.tds21msgs.XacsSignedContentPayload;
import com.modirum.ds.tds21msgs.XcardRangeData;
import com.modirum.ds.utils.GenericPool;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.PublicArrayOutputStream;
import com.modirum.ds.utils.StringBuilderReader;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.eclipse.persistence.oxm.json.JsonObjectBuilderResult;
import org.eclipse.persistence.oxm.json.JsonStructureSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Service for transforming ThereeDSecure v2 messages.
 */
public abstract class MessageService<T extends TDSMessageBase> {

    protected static Logger log = LoggerFactory.getLogger(MessageService.class);

    protected static final int POOL_SIZE = 16;

    public static final String CT_JSON = "application/json";
    public static final String CT_JSON_UTF8 = "application/json;charset=UTF-8";

    protected JAXBContext tdsJsonCtx;
    protected JAXBContext tdsJsonCtx2;

    protected GenericPool<Marshaller> marshallerPool;
    protected GenericPool<Unmarshaller> unmarshallerPool;
    protected GenericPool<Unmarshaller> unmarshallerPoolValidating;
    protected GenericPool<Validator> validatorPool;

    protected static GenericPool<Map<String, AtomicInteger>> mapPool = new GenericPool<>(256);

    protected Schema schema;
    protected boolean treatEmptyValueAsMissing = false;
    protected Class<T> mTypeClass;
    protected JsonWriterFactory writerFactory;
    protected JsonWriterFactory writerFactoryNoPretty;

    /**
     * Get Supported TDS version.
     */
    public abstract String getSupportedVersion();

    /**
     * Create new message.
     */
    public abstract T createNewMessage();

    /**
     * Get card ranga data.
     */
    public abstract List<XcardRangeData> getCardRangeData(T m);

    /**
     * Constructor.
     */
    protected MessageService(String schemaPath) {
        try {
            mTypeClass = (Class<T>) (createNewMessage().getClass());
            Map<String, Object> properties = new HashMap<>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);
            writerFactory = Json.createWriterFactory(properties);
            Map<String, Object> properties2 = new HashMap<>(1);
            properties2.put(JsonGenerator.PRETTY_PRINTING, false);
            writerFactoryNoPretty = Json.createWriterFactory(properties2);

            Map<String, Object> pm = new HashMap<>();
            pm.put("eclipselink.media-type", "application/json");
            pm.put("eclipselink.json.include-root", false);

            Map<String, Source> metadata = new HashMap<>();
            metadata.put("com.modirum.ds.tds20msgs",
                         new StreamSource(this.getClass().getResource("/tds20special.xml").toString()));
            pm.put(JAXBContextProperties.OXM_METADATA_SOURCE, metadata);
            pm.put(JAXBContextProperties.JSON_ATTRIBUTE_PREFIX, "@");
            pm.put(JAXBContextProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);
            pm.put(JAXBContextProperties.JSON_TYPE_COMPATIBILITY, true);

            tdsJsonCtx = JAXBContextFactory.createContext(
                    new Class[]{TDSMessage.class, XJWE.class, XJWEprotected.class, XacsSignedContentPayload.class}, pm);

            // for jaxb source validation with root wo root jaxb xsd validation did not work
            tdsJsonCtx2 = JAXBContext.newInstance(TDSMessage.class);
            marshallerPool = new GenericPool<>(POOL_SIZE);
            unmarshallerPool = new GenericPool<>(POOL_SIZE);
            unmarshallerPoolValidating = new GenericPool<>(POOL_SIZE);
            validatorPool = new GenericPool<>(POOL_SIZE);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schema = sf.newSchema(this.getClass().getResource(schemaPath));

        } catch (Exception e) {
            throw new RuntimeException("MessageService failed to initialize", e);
        }
    }

    public static Reader getJSONRootedreader(String rootName, byte[] data, Charset cset) throws CharacterCodingException {
        ByteBuffer bb = ByteBuffer.wrap(data);
        CharBuffer cb = cset.newDecoder().decode(bb);
        StringBuilder sb = new StringBuilder(rootName.length() + 5 + cb.length());
        sb.append("{\"").append(rootName).append("\":");
        sb.append(cb);
        sb.append('}');
        return new StringBuilderReader(sb);
    }

    public static String getMessageVersion(String[] json) {
        return getFieldValue("messageVersion", json);
    }

    public static String getFieldValue(String fieldName, String[] json) {
        fieldName = "\"" + fieldName + "\"";
        int start = json[0].indexOf(fieldName);
        if (start >= 0) {
            int startValue = json[0].indexOf("\"", start + fieldName.length());
            if (startValue > start) {
                int endValue = json[0].indexOf("\"", startValue + 1);
                if (endValue > startValue) {
                    return json[0].substring(startValue + 1, endValue);
                }
            }
        }
        return null;
    }

    public class JsonAndMessage {
        public JsonObject json;
        public T message;
    }

    public static class JsonAndDucplicates {
        public JsonObject json;
        public Map<String, AtomicInteger> dups;
    }

    public static JsonAndDucplicates parseJsonAndDups(String[] json) {
        JsonAndDucplicates jsonAndDucplicates = new JsonAndDucplicates();
        Map<String, AtomicInteger> dups = mapPool.reuse();
        if (dups == null) {
            dups = new HashMap<>(4);
        }

        JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        jsonAndDucplicates.json = Json.createReader(new StringReader(json[0])).readObject();

        if (dups.size() > 0) {
            jsonAndDucplicates.dups = (dups);
        } else {
            mapPool.recycle(dups);
        }

        return jsonAndDucplicates;
    }

    public static JsonAndDucplicates parseJsonAndDups(byte[] json) {
        JsonAndDucplicates jm = new JsonAndDucplicates();
        Map<String, AtomicInteger> dups = mapPool.reuse();
        if (dups == null) {
            dups = new HashMap<>(4);
        }

        JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        jm.json = Json.createReader(new ByteArrayInputStream(json)).readObject();

        if (dups.size() > 0) {
            jm.dups = (dups);
        } else {
            mapPool.recycle(dups);
        }

        return jm;
    }


    public JsonAndMessage parseJsonAndMsg(String[] json) throws Exception {
        JsonAndMessage jm = new JsonAndMessage();
        Map<String, AtomicInteger> dups = mapPool.reuse();
        if (dups == null) {
            dups = new java.util.HashMap<>(4);
        }

        JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        jm.json = Json.createReader(new StringReader(json[0])).readObject();

        T o = fromJSON(jm.json);
        if (dups.size() > 0) {
            o.setDuplicateCounts(dups);
        }
        jm.message = o;

        return jm;
    }

    public static void maskJSONStrValue(StringBuilder json, String fname, int offs) {
        String key = "\"" + fname + "\"";
        int is = 0;
        while (true) {
            int start = json.indexOf(key, is);
            if (start >= 0) {
                is = start + 1;
                int startValue = json.indexOf("\"", start + key.length() + 1);
                if (startValue > start) {
                    is = startValue + 1;
                    int endValue = json.indexOf("\"", startValue + 1);
                    if (endValue > startValue) {
                        for (int i = startValue + offs + 1; i < endValue - offs; i++) {
                            json.setCharAt(i, '#');
                        }
                        is = endValue + 1;
                    }
                }
            } else {
                break;
            }
        }
    }

    public static StreamSource getJSONRootedSource(String rootName, byte[] data) {
        try {
            return new StreamSource(getJSONRootedreader(rootName, data, StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected Validator getValidator() {
        Validator va = validatorPool.reuse();
        return va == null ? schema.newValidator() : va;
    }

    protected void returnValidator(Validator va) {
        validatorPool.recycle(va);
    }

    protected Marshaller getJSONMarshaller() throws JAXBException {
        Marshaller marshaller = marshallerPool.reuse();
        if (marshaller == null) {
            marshaller = tdsJsonCtx.createMarshaller();
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON); //"application/json");

            marshaller.setProperty(MarshallerProperties.JSON_TYPE_COMPATIBILITY, true);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        }

        return marshaller;
    }

    protected void returnMarshaller(Marshaller marshaller) {
        marshallerPool.recycle(marshaller);
    }

    public Unmarshaller getJSONUnmarshaller() throws JAXBException {
        Unmarshaller unmarshaller = unmarshallerPool.reuse();
        if (unmarshaller == null) {
            unmarshaller = tdsJsonCtx.createUnmarshaller();
            unmarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        }
        return unmarshaller;
    }

    public Unmarshaller getJSONUnmarshallerValidating() throws JAXBException {
        Unmarshaller unmarshaller = unmarshallerPoolValidating.reuse();
        if (unmarshaller == null) {
            unmarshaller = tdsJsonCtx.createUnmarshaller();
            unmarshaller.setProperty("eclipselink.json.include-root", true);
            unmarshaller.setSchema(schema);
        }

        return unmarshaller;
    }

    public void returnUnmarshallerValidating(Unmarshaller unmarshaller) {
        unmarshallerPoolValidating.recycle(unmarshaller);
    }

    public void returnUnmarshaller(Unmarshaller unmarshaller) {
        unmarshallerPool.recycle(unmarshaller);
    }

    static class FieldErrorHandler implements ErrorHandler, ValidationEventHandler {

        List<Error> errors;
        SAXParseException prevException;
        boolean treatEmptyValueAsMissing;

        FieldErrorHandler(List<Error> errors) {
            this.errors = errors;
        }

        @Override
        public void warning(SAXParseException exception) {
        }

        @Override
        public void error(SAXParseException e) {
            createError(e);
        }

        @Override
        public void fatalError(SAXParseException exception) throws SAXException {
            throw exception;
        }

        public void createError(SAXParseException exception) {
            String errorMessage = exception.getMessage();
            int i1 = -1;
            int i2 = -1;
            Logger log2 = LoggerFactory.getLogger(FieldErrorHandler.class);
            log.info("eleos:{}", errorMessage);
            if (errorMessage != null && (i1 = errorMessage.indexOf("The value '")) > -1 &&
                (i2 = errorMessage.indexOf("' of element '")) > i1) {
                // 2.0.1 spec 2017 july trad empty as invalid format
                Error e = new Error();
                //errors.add(e);
                log.info("peos4 xsd validation:{}, {}, treat empty as missing: {}", errors.size(),
                         errors.size() > 0 ? errors.get(0) : "", treatEmptyValueAsMissing);


                e.setCode(
                        treatEmptyValueAsMissing ? ErrorCode.cRequiredFieldMissing201 : ErrorCode.cInvalidFieldFormat203);
                e.setFieldName(errorMessage.substring(i2 + 14, errorMessage.indexOf('\'', i2 + 14 + 1)));

                if (e.getFieldName() != null && e.getFieldName().startsWith("tdsServer")) {
                    e.setFieldName(Misc.replace(e.getFieldName(), "tdsServer", "threeDSServer")); //"3DSServer"
                } else if (e.getFieldName() != null && e.getFieldName().startsWith("tdsReq")) {
                    e.setFieldName(Misc.replace(e.getFieldName(), "tdsReq", "threeDSReq")); // "3DSReq"
                }

                String value1 = errorMessage.substring(i1 + 11, errorMessage.indexOf('\'', i1 + 11 + 1));
                String pattern = null;
                String length = null;
                String maxlength = null;
                String minlength = null;

                if (prevException != null) {
                    String exDetail2 = prevException.getMessage();
                    int iv1 = exDetail2.indexOf("Value '");
                    if (iv1 > -1) {
                        String value2 = exDetail2.substring(iv1 + 7, exDetail2.indexOf('\'', iv1 + 7 + 1));

                        if (value2.equals(value1)) {
                            int p1 = -1;
                            if ((p1 = exDetail2.indexOf("to pattern '")) > -1) {
                                pattern = exDetail2.substring(p1 + 12, exDetail2.indexOf('\'', p1 + 12 + 1));
                            }
                            //cvc-maxLength-valid: Value '771177AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' with length = '40' is not facet-valid with respect to maxLength '24' for type 'XacquirerMerchantID'.
                            else if ((p1 = exDetail2.indexOf("with length = '")) > -1) {
                                length = exDetail2.substring(p1 + 15, exDetail2.indexOf('\'', p1 + 15 + 1));
                                int p2 = -1;
                                if ((p2 = exDetail2.indexOf("to maxLength '")) > -1) {
                                    maxlength = exDetail2.substring(p2 + 14, exDetail2.indexOf('\'', p2 + 14 + 1));
                                } else if ((p2 = exDetail2.indexOf("to minLength '")) > -1) {
                                    minlength = exDetail2.substring(p2 + 14, exDetail2.indexOf('\'', p2 + 14 + 1));
                                }
                            }
                        }
                    }

                }


                int start = errorMessage.indexOf("'");
                int end = errorMessage.indexOf("'", start + 1);
                System.out.println("start:" + start + " ,end:" + end);
                String realValue = errorMessage.substring(start + 1, end);
                System.out.println("value:" + realValue);
                log2.info("kostas realValue{}:", realValue);
                log2.info("errorMessage:{}:", errorMessage);
                log2.info("start:{},end:{}", start, end);

                if (realValue != null && realValue.length() > 0) {
                    e.setCode(ErrorCode.cInvalidFieldFormat203);
                    errors.add(e);
                } else {
                    e.setCode(ErrorCode.cRequiredFieldMissing201);
                }

                log2.info("Final error code:{}", e.getCode());

                if (value1 != null && "acctNumber".equals(e.getFieldName())) {
                    value1 = Misc.maskNumsOnly(value1, 5, 3);
                } // dont create too long value echos in error
                else if (value1 != null && value1.length() > 20) {
                    value1 = value1.substring(0, 10) + ".." + (value1.length() - 20) + ".." +
                             value1.substring(value1.length() - 10);
                }

                if (value1 != null && pattern != null) {
                    e.setDescription("Invalid value '" + value1 + "' must match '" + pattern + "'");
                } else if (value1 != null && length != null) {
                    String mismatch = "";
                    if (maxlength != null) {
                        mismatch = "exceeds max allowed " + maxlength;
                    }
                    if (minlength != null) {
                        mismatch = "smaller of min allowed " + minlength;
                    }

                    e.setDescription("Invalid value '" + value1 + "' length " + length + " " + mismatch);
                    errors.add(e);
                } else {
                    //errors.add(e);
                    e.setDescription(exception.getMessage());
                }
            } else if (errorMessage != null && (i1 = errorMessage.indexOf("' can occur a maximum of '")) > -1 &&
                    (i2 = errorMessage.indexOf("' times in the current sequence. This limit was exceeded.")) > i1) {
                Error e = new Error();
                e.setCode(ErrorCode.cInvalidFieldFormat203);
                e.setFieldName(errorMessage.substring(errorMessage.indexOf("'") + 1, i1));
                e.setDescription(String.format("Invalid size. Cannot exceed %s elements",
                        errorMessage.substring(i1 + "' can occur a maximum of '".length(), i2)));
                errors.add(e);
            } else // pattern error
            {
                prevException = exception;
            }
        }

        @Override
        public boolean handleEvent(ValidationEvent event) {
            log.info("event s " + event.getSeverity() + " m " + event.getMessage() + " node " +
                     event.getLocator().getNode() + " url " + event.getLocator().getURL() + " o " +
                     event.getLocator().getObject() + " ln " + event.getLocator().getLineNumber() + " col " +
                     event.getLocator().getColumnNumber());
            return true; //event.getSeverity()<event.FATAL_ERROR;
        }
    }

    public Schema getSchema() {
        return schema;
    }

    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    public boolean isTreatEmptyValueAsMissing() {
        return treatEmptyValueAsMissing;
    }

    public void setTreatEmptyValueAsMissing(boolean treatEmptyValueAsMissing) {
        this.treatEmptyValueAsMissing = treatEmptyValueAsMissing;
    }

    public void toJSON(T tdsMsg, OutputStream os) throws JAXBException {
        Marshaller m = getJSONMarshaller();
        m.marshal(tdsMsg, os);
        returnMarshaller(m);
    }

    public T fromJSONWrapRoot(byte[] json) throws JAXBException {
        Unmarshaller u = getJSONUnmarshallerValidating();
        T o = u.unmarshal(getJSONRootedSource("TDSMessage", json), mTypeClass).getValue();
        returnUnmarshallerValidating(u);
        return o;
    }

    public T fromJSON(byte[] json) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        Map<String, AtomicInteger> dups = mapPool.reuse();
        if (dups == null) {
            dups = new HashMap<>(4);
        }

        JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        T o = u.unmarshal(new StreamSource(new ByteArrayInputStream(json)), mTypeClass).getValue();
        returnUnmarshaller(u);
        if (dups.size() > 0) {
            o.setDuplicateCounts(dups);
        } else {
            mapPool.recycle(dups);
        }

        return o;
    }

    public T fromJSON(char[] json) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        Map<String, AtomicInteger> dups = mapPool.reuse();
        if (dups == null) {
            dups = new HashMap<>(4);
        }
        JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        T o = u.unmarshal(new StreamSource(new CharArrayReader(json)), mTypeClass).getValue();
        returnUnmarshaller(u);
        if (dups.size() > 0) {
            o.setDuplicateCounts(dups);
        } else {
            mapPool.recycle(dups);
        }
        return o;
    }

    public T fromJSON(InputStream jsonis) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        Map<String, AtomicInteger> dups = mapPool.reuse();
        if (dups == null) {
            dups = new HashMap<>(4);
        }
        JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        T o = u.unmarshal(new StreamSource(jsonis), mTypeClass).getValue();
        returnUnmarshaller(u);
        if (dups.size() > 0) {
            o.setDuplicateCounts(dups);
        } else {
            mapPool.recycle(dups);
        }
        return o;
    }

    public T fromJSON(Reader jsonis) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        Map<String, AtomicInteger> dups = mapPool.reuse();
        if (dups == null) {
            dups = new HashMap<>(4);
        }
        JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        T o = u.unmarshal(new StreamSource(jsonis), mTypeClass).getValue();
        returnUnmarshaller(u);
        if (dups.size() > 0) {
            o.setDuplicateCounts(dups);
        } else {
            mapPool.recycle(dups);
        }
        return o;
    }

    public T fromJSON(JsonObject json) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        T o = u.unmarshal(new JsonStructureSource(json), mTypeClass).getValue();
        returnUnmarshaller(u);
        return o;
    }

    public boolean isErrorMessage(TDSMessageBase tdsMsg) {
        return TDSModel.XmessageType.ERRO.value().equals(tdsMsg.getMessageType());
    }

    public byte[] toJSON(JsonObject tdsMsg) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(512);
        JsonWriter jsw = writerFactory.createWriter(bos);
        jsw.writeObject(tdsMsg);

        return bos.toByteArray();
    }

    public byte[] toJSONNoPretty(JsonObject tdsMsg) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(512);
        JsonWriter jsw = writerFactoryNoPretty.createWriter(bos);
        jsw.writeObject(tdsMsg);

        return bos.toByteArray();
    }

    public PublicBOS toJSONBos(JsonObject tdsMsg) {
        PublicBOS bos = new PublicBOS(512);
        JsonWriter jsw = writerFactory.createWriter(bos);
        jsw.writeObject(tdsMsg);
        jsw.close();

        return bos;
    }

    public StringWriter toJSONStrWriter(JsonObject tdsMsg) {
        StringWriter sw = new StringWriter(512);
        JsonWriter jsw = writerFactory.createWriter(sw);
        jsw.writeObject(tdsMsg);
        jsw.close();

        return sw;
    }

    public StringWriter toJSONStrWriterNoPretty(JsonObject tdsMsg) {
        StringWriter sw = new StringWriter(512);
        JsonWriter jsw = writerFactoryNoPretty.createWriter(sw);
        jsw.writeObject(tdsMsg);
        jsw.close();

        return sw;
    }

    public JsonObjectBuilder toJSONBuilder(JsonObject jsonObj) {
        JsonObjectBuilder job = Json.createObjectBuilder();
        for (Entry<String, JsonValue> e : jsonObj.entrySet()) {
            job.add(e.getKey(), e.getValue());
        }
        return job;
    }

    public byte[] toJSON(T tdsMsg) throws JAXBException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(512);
        Marshaller m = getJSONMarshaller();
        m.marshal(tdsMsg, bos);
        returnMarshaller(m);

        return bos.toByteArray();
    }

    public byte[] toJSONNoPretty(T tdsMsg) throws JAXBException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(512);
        Marshaller m = getJSONMarshaller();
        Object backup = m.getProperty(Marshaller.JAXB_FORMATTED_OUTPUT);
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
        m.marshal(tdsMsg, bos);
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, backup);
        returnMarshaller(m);

        return bos.toByteArray();
    }

    public JsonObjectBuilder toJSONOB(T tdsMsg) throws JAXBException {
        JsonObjectBuilderResult bos = new JsonObjectBuilderResult();
        Marshaller m = getJSONMarshaller();
        m.marshal(tdsMsg, bos);
        returnMarshaller(m);
        return bos.getJsonObjectBuilder();
    }

    public PublicBOS toJSONBos(T tdsMsg) throws JAXBException {
        PublicBOS bos = new PublicBOS(512);
        Marshaller m = getJSONMarshaller();
        m.marshal(tdsMsg, bos);
        returnMarshaller(m);
        return bos;
    }

    public char[] toJSONCharArray(T tdsMsg) throws JAXBException {
        CharArrayWriter bos = new CharArrayWriter(512);
        Marshaller m = getJSONMarshaller();
        m.marshal(tdsMsg, bos);
        returnMarshaller(m);
        return bos.toCharArray();
    }

    public boolean isInvalidStructureNPE(Exception em) {
        boolean structMismatch = false;
        StackTraceElement[] stels = em.getStackTrace();
        for (int i = stels.length - 1; i > 0; i--) {
            if (stels[i].toString().contains(
                    "org.eclipse.persistence.internal.oxm.record.json.JsonStructureReader$JsonAttributes.attributes")) {
                structMismatch = true;
                break;
            }
        }

        return structMismatch;
    }

    public static JsonArrayBuilder toArrayBuilder(String parent, JsonArray jarray, List<Error> errors) {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int cnt = 0;
        for (JsonValue jv : jarray) {
            if (ValueType.ARRAY == jv.getValueType()) {
                if (errors != null && ((JsonArray) jv).isEmpty()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203,
                                         (parent != null ? parent : "") + "[" + cnt + "]", "json array is empty"));
                }
                jab.add(MessageService.toArrayBuilder((parent != null ? parent : "") + "[" + cnt + "]", (JsonArray) jv,
                                                      errors));
            } else if (ValueType.OBJECT == jv.getValueType()) {
                if (errors != null && ((JsonObject) jv).isEmpty()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203,
                                         (parent != null ? parent : "") + "[" + cnt + "]", "json object is empty"));
                }
                jab.add(MessageService.toObjectBuilder((parent != null ? parent : "") + "[" + cnt + "]",
                                                       (JsonObject) jv, errors));
            } else {
                jab.add(jv);
            }
            cnt++;
        }
        return jab;
    }

    public static JsonObjectBuilder toObjectBuilder(String parent, JsonObject jsonObj, List<Error> errors) {
        JsonObjectBuilder job = Json.createObjectBuilder();
        for (Entry<String, JsonValue> e : jsonObj.entrySet()) {
            if (e.getValue() != null && ValueType.ARRAY == e.getValue().getValueType()) {
                if (errors != null && ((JsonArray) e.getValue()).isEmpty()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203,
                                         (parent != null ? parent + "." : "") + e.getKey(), "json array is empty"));
                }
                job.add(e.getKey(), MessageService.toArrayBuilder(e.getKey(), (JsonArray) e.getValue(), errors));
            } else if (e.getValue() != null && ValueType.OBJECT == e.getValue().getValueType()) {
                if (errors != null && ((JsonObject) e.getValue()).isEmpty()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203,
                                         (parent != null ? parent + "." : "") + e.getKey(), "json object is empty"));
                }
                job.add(e.getKey(),
                        toObjectBuilder((parent != null ? parent + "." : "") + e.getKey(), (JsonObject) e.getValue(),
                                        errors));
            } else {
                job.add(e.getKey(), e.getValue());
            }
        }
        return job;
    }

    public static JsonObjectBuilder createFilteredJson(JsonObject jsonSrc, JsonArray addExtensions, String mt, String ch, String cat, String txStat, List<String> filters, List<Error> errors) {
        JsonObjectBuilder job = Json.createObjectBuilder();
        boolean extensionsDone = false;
        for (Entry<String, JsonValue> e : jsonSrc.entrySet()) {
            // key = MSGType + fieldname + Device + msg cat +
            String key = mt + "." + e.getKey() + "." + ch + "." + cat;
            String key2 = mt + "." + e.getKey() + "." + ch + "." + cat + "." + txStat;
            String key3 = mt + "." + e.getKey();

            // emtpy array and object detection
            if (e.getValue() != null && ValueType.ARRAY == e.getValue().getValueType()) {
                if (errors != null && ((JsonArray) e.getValue()).isEmpty()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, e.getKey(), "json array is empty"));
                }
            } else if (e.getValue() != null && ValueType.OBJECT == e.getValue().getValueType()) {
                if (errors != null && ((JsonObject) e.getValue()).isEmpty()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, e.getKey(), "json object is empty"));
                }
            }

            // needed field filtering
            if (filters == null || filters.contains(key) || filters.contains(key2) || filters.contains(key3)) {
                if (errors != null) {
                    if (e.getValue() != null && ValueType.ARRAY == e.getValue().getValueType()) {
                        JsonArrayBuilder ab = MessageService.toArrayBuilder(e.getKey(), (JsonArray) e.getValue(),
                                                                            errors);
                        if ("messageExtension".equals(e.getKey())) {
                            // add new extensions
                            if (addExtensions != null) {
                                for (JsonValue e1 : addExtensions.asJsonArray()) {
                                    ab.add(e1);
                                }
                                extensionsDone = true;
                            }

                        }
                        job.add(e.getKey(), ab);
                    } else if (e.getValue() != null && ValueType.OBJECT == e.getValue().getValueType()) {
                        job.add(e.getKey(),
                                MessageService.toObjectBuilder(e.getKey(), (JsonObject) e.getValue(), errors));
                    } else {
                        job.add(e.getKey(), e.getValue());
                    }
                } else {
                    if ("messageExtension".equals(e.getKey()) && ValueType.ARRAY == e.getValue().getValueType()) {
                        extensionsDone = true;
                        // add new extensions
                        if (addExtensions != null) {
                            for (JsonValue e1 : addExtensions.asJsonArray()) {
                                ((JsonArray) e.getValue()).add(e1);
                            }
                            extensionsDone = true;
                        }
                    }

                    job.add(e.getKey(), e.getValue());
                }
            }
        }
        // add new extensions
        if (addExtensions != null && !extensionsDone) {
            JsonArrayBuilder ab = MessageService.toArrayBuilder("messageExtension", addExtensions, errors);
            job.add("messageExtension", ab);
        }

        return job;
    }

    public static void addExtensions(JsonObjectBuilder jsonSrc, JsonArray addExtensions) {
        if (addExtensions != null) {
            JsonArrayBuilder ab = MessageService.toArrayBuilder("messageExtension", addExtensions, null);
            jsonSrc.add("messageExtension", ab);
        }
    }

    public static String getStrValue(JsonObject o, String key) {
        if (o.containsKey(key)) {
            return o.getString(key);
        }
        return null;
    }

    public static String getNumValue(JsonObject o, String key) {
        if (o.containsKey(key)) {
            JsonNumber jso = o.getJsonNumber(key);
            if (jso != null) {
                return jso.toString();
            }
        }
        return null;
    }

    public static class PublicBOS extends PublicArrayOutputStream {

        public PublicBOS() {
            this(32);
        }

        public PublicBOS(int size) {
            super(size);
        }

        public PublicBOS(byte[] buf) {
            super(buf);
        }
    }

    public static XJWSCompact fromCompactJWS(String compactJWS) {
        XJWSCompact jws = new XJWSCompact();
        int dot1 = compactJWS.indexOf('.');
        String protStr = compactJWS.substring(0, dot1);
        int dot2 = compactJWS.indexOf('.', dot1 + 1);
        jws.setProtected(protStr);
        jws.setPayload(compactJWS.substring(dot1 + 1, dot2));

        int dot3 = compactJWS.indexOf('.', dot2 + 1);
        if (dot3 < 0) {
            jws.getSignatures().add(compactJWS.substring(dot2 + 1));
        } else {
            while (dot3 > -1) {
                jws.getSignatures().add(compactJWS.substring(dot2 + 1, dot3));
                dot3 = compactJWS.indexOf('.', dot3 + 1);
            }
        }

        return jws;
    }

    public static String toCompactJWS(XJWSCompact jwsCompact) {
        StringJoiner joiner = new StringJoiner(".");
        joiner.add(jwsCompact.getProtected());
        joiner.add(jwsCompact.getPayload());

        if (jwsCompact.getSignatures() != null) {
            for (String signature : jwsCompact.getSignatures()) {
                joiner.add(signature);
            }
        }
        return joiner.toString();
    }
}
