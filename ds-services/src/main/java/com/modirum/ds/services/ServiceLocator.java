/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 29.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.db.model.HsmDevice;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.db.model.HsmDeviceConf;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.sms.SMSService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ServiceLocator {

    protected transient static Logger log = LoggerFactory.getLogger(ServiceLocator.class);
    static ServiceLocator instance;

    public static int maxSchedulerThreads = 50;
    ScheduledThreadPoolExecutor sexec;
    IssuerService issuerService;
    CardRangeService cardRangeService;
    AcquirerService acquirerService;
    MerchantService merchantService;
    ThreeDSProfileService threeDSProfileService;
    ConfigService configService;
    LocalizationService localizationService;
    PersistenceService persistenceService;
    KeyService keyService;
    private HashMap<Long, HSMDevice> hsmDevices; // ID to HSM device map
    UserService userService;
    AuditLogService auditLogService;
    SMSService smsService = null;
    JsonMessageService jsonMessageService;
    CryptoService cryptoService;
    PaymentSystemsService paymentSystemService;
    ProductInfoService productInfoService;
    static Object syno = new Object();

    public static ServiceLocator getInstance() {
        return getInstance(null);
    }

    public static ServiceLocator getInstance(PersistenceService pse) {
        if (instance == null) {
            try {
                synchronized (syno) {
                    if (instance == null) {
                        instance = pse != null ? new ServiceLocator(pse) : new ServiceLocator();
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException("ServiceLocator init failure", e);
            }
        }

        return instance;
    }

    protected ServiceLocator() {
        this(new PersistenceService());
    }

    protected ServiceLocator(PersistenceService pse) {
        persistenceService = pse;
        configService = new ConfigService(persistenceService);
        cardRangeService = new CardRangeService(persistenceService);
        acquirerService = new AcquirerService(persistenceService);
        merchantService = new MerchantService(persistenceService);
        threeDSProfileService = new ThreeDSProfileService(persistenceService);
        localizationService = new LocalizationService(persistenceService);
        keyService = new KeyService();
        userService = new UserService(persistenceService);
        auditLogService = new AuditLogService(persistenceService);
        issuerService = new IssuerService(persistenceService, auditLogService);
        jsonMessageService = new JsonMessageService();
        cryptoService = new CryptoService();
        paymentSystemService = new PaymentSystemsService(persistenceService);
        productInfoService = new ProductInfoService(persistenceService);
    }

    public CardRangeService getCardRangeService() {
        return this.cardRangeService;
    }

    public PaymentSystemsService getPaymentSystemService() {
        return this.paymentSystemService;
    }

    public ThreeDSProfileService getThreeDSProfileService() {
        return threeDSProfileService;
    }

    public MerchantService getMerchantService() {
        return merchantService;
    }

    public IssuerService getIssuerService() {
        return issuerService;
    }

    public void setIssuerService(IssuerService issuerService) {
        this.issuerService = issuerService;
    }

    public ConfigService getConfigService() {
        return configService;
    }

    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }

    public LocalizationService getLocalizationService() {
        return localizationService;
    }

    public void setLocalizationService(LocalizationService localizationService) {
        this.localizationService = localizationService;
    }

    public PersistenceService getPersistenceService() {
        return persistenceService;
    }

    public void setPersistenceService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public void setInstance(ServiceLocator i) {
        instance = i;
    }

    public KeyService getKeyService() {
        return keyService;
    }

    public void setKeyService(KeyService keyService) {
        this.keyService = keyService;
    }

    /**
     * Looks up, initializes and returns all HSM devices registered in DS. Service lookup from DB is performed once at
     * first call to this method. Subsequent calls return cached instances.
     *
     * @return Map of hsmID - HSM device instances.
     */
    public Map<Long, HSMDevice> getHSMDevices() {
        if (hsmDevices != null) {
            return hsmDevices;
        }

        hsmDevices = new HashMap<>();
        try {
            List<HsmDevice> dbHsmDevices = persistenceService.listActiveHsmDevices();
            if (dbHsmDevices == null || dbHsmDevices.isEmpty()) {
                log.error("No HSM Devices registered in database.");
                return null;
            }

            String instanceId = new Context().getServerIntanceId();
            for (HsmDevice dbHsm : dbHsmDevices) {
                HSMDevice hsmDevice = (HSMDevice) Class.forName(dbHsm.getClassName()).newInstance();

                String cnfStr = null;
                if (Misc.isNullOrEmpty(cnfStr)) {
                    HsmDeviceConf hsmConf = persistenceService.getHsmDeviceConfig(dbHsm.getId(), 0);
                    cnfStr = hsmConf != null ? hsmConf.getConfig() : null;
                }
                hsmDevice.initializeHSM(cnfStr);
                hsmDevices.put(dbHsm.getId(), hsmDevice);
            }
        } catch (Exception e) {
            log.error("Error initializing HSM devices", e);
            throw new RuntimeException("Error initializing HSM devices", e);
        }
        return hsmDevices;
    }

    public HSMDevice getHSMDevice(Long hsmDeviceId) {
        return getHSMDevices().get(hsmDeviceId);
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public AuditLogService getAuditLogService() {
        return auditLogService;
    }

    public void setAuditLogService(AuditLogService auditLogService) {
        this.auditLogService = auditLogService;
    }

    public SMSService getSMSService() {
        if (smsService == null) {
            String smsServiceClass = null;
            try {
                smsServiceClass = configService.getStringSetting(DsSetting.SMS_CLASS.getKey());
                if (Misc.isNotNullOrEmpty(smsServiceClass)) {
                    smsService = (SMSService) Class.forName(smsServiceClass).newInstance();
                }
                smsService = new com.modirum.ds.utils.sms.SMSServiceCardBoardFish();

            } catch (Throwable t) {
                throw new RuntimeException("Failure create SMSService " + smsServiceClass, t);
            }
        }

        return smsService;
    }

    public SMSService getSmsService() {
        return smsService;
    }

    public void setSmsService(SMSService smsService) {
        this.smsService = smsService;
    }

    public ScheduledThreadPoolExecutor getScheduledExecutorService() {
        if (sexec == null) {
            // JDK unexpected behaviour ScheduledThreadPoolExecutor thread count does not grow beyond core
            // so ensure system works as expected core shall be equal to max
            log.info("Initializing DS ThreadPool maxThreads=" + maxSchedulerThreads);
            sexec = new ScheduledThreadPoolExecutor(maxSchedulerThreads,
                    new com.modirum.ds.utils.DaemonThreadFactory("DSExec"));
            sexec.setMaximumPoolSize(maxSchedulerThreads);
            sexec.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
            sexec.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        }

        return sexec;
    }

    public void unsetScheduledExecutorService() {
        sexec = null;
    }

    public AcquirerService getAcquirerService() {
        return acquirerService;
    }

    public void setAcquirerService(AcquirerService acquirerService) {
        this.acquirerService = acquirerService;
    }

    public JsonMessageService getJsonMessageService() {
        return jsonMessageService;
    }

    public CryptoService getCryptoService() {
        return cryptoService;
    }

    public ProductInfoService getProductInfoService(){ return productInfoService; }
}
