/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 5. jaan 2017
 *
 */
package com.modirum.ds.model;

import java.io.Serializable;
import java.util.LinkedList;

public class Timings implements Serializable {

    private static final long serialVersionUID = 1L;
    protected final static LinkedList<Timings> reuseTimings = new LinkedList<>();

    public long reqStart;
    public int cnt;
    public int reqMid;
    public int reqEnd;

    public int resStart;
    public int resMid;
    public int resEnd;
    public int resultCode;
    public int extSum; // external plugin time like fss

    public static Timings getInstance() {
        if (reuseTimings.size() > 0) {
            synchronized (reuseTimings) {
                if (reuseTimings.size() > 0) {
                    Timings t = reuseTimings.removeFirst();
                    if (t != null) {
                        return t;
                    }
                }
            }
        }

        return new Timings();
    }

    @Override
    public void finalize() throws Throwable {
        if (reuseTimings.size() < 500) {
            this.reqMid = this.reqEnd = this.resStart = this.resMid = this.resEnd = this.cnt = this.resultCode = 0;
            this.reqStart = 0;
            synchronized (reuseTimings) {
                reuseTimings.add(this);
            }
        } else {
            super.finalize();
        }
    }
}