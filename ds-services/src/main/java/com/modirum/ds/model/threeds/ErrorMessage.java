package com.modirum.ds.model.threeds;

/**
 * EMVCo 3DS 2.0.1 draft 5
 * <p/>
 * All the fields defined in the class are mandatory in all error messages.
 */
public class ErrorMessage {
    private String threeDSServerTransID;
    private String acsTransID;
    private String dsTransID;
    private String errorCode;
    private String errorDescription;
    private String errorMessageType;
    private String errorDetail;
    private String errorComponent;
    private final String messageType = "Erro";
    private String messageVersion;
    private String sdkTransID;

    public String getThreeDSServerTransID() {
        return threeDSServerTransID;
    }

    public void setThreeDSServerTransID(String threeDSServerTransID) {
        this.threeDSServerTransID = threeDSServerTransID;
    }

    public String getAcsTransID() {
        return acsTransID;
    }

    public void setAcsTransID(String acsTransID) {
        this.acsTransID = acsTransID;
    }

    public String getDsTransID() {
        return dsTransID;
    }

    public void setDsTransID(String dsTransID) {
        this.dsTransID = dsTransID;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorMessageType() {
        return errorMessageType;
    }

    public void setErrorMessageType(String errorMessageType) {
        this.errorMessageType = errorMessageType;
    }

    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    public String getErrorComponent() {
        return errorComponent;
    }

    public void setErrorComponent(String errorComponent) {
        this.errorComponent = errorComponent;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public String getSdkTransID() {
        return sdkTransID;
    }

    public void setSdkTransID(String sdkTransID) {
        this.sdkTransID = sdkTransID;
    }
}