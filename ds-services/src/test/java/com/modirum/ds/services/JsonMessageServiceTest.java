package com.modirum.ds.services;

import com.modirum.ds.enums.FieldName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JsonMessageServiceTest {

    private JsonMessageService jsonMessageService = new JsonMessageService();

    @Test
    public void maskEmailAddressWithCustom() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\"}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        String emailMask = "(?<=^.{1}).+(?=.{1}@)";

        jsonMessageService.maskValue(jsonMessage, "emailAddress", emailMask, JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals("{\"emailAddress\":\"m***a@modirum.com\"}", jsonMessage.toString());

    }

    @Test
    public void maskEmailAddressEmptyValue() throws Exception {
        String json = "{\"emailAddress\":\"\"}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        String emailMask = "(?<=^.{1}).+(?=.{1}@)";

        jsonMessageService.maskValue(jsonMessage, "emailAddress", emailMask, JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals("{\"emailAddress\":\"\"}", jsonMessage.toString());

    }

    @Test
    public void maskNonExisting() throws Exception {
        String json = "{\"name\":\"name\"}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        String emailMask = "(?<=^.{1}).+(?=.{1}@)";

        jsonMessageService.maskValue(jsonMessage, "emailAddress", emailMask, JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals(json, jsonMessage.toString());

    }

    @Test
    public void maskWithDefault() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\"}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessageService.maskValue(jsonMessage, "emailAddress", ".*", JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals("{\"emailAddress\":\"***\"}", jsonMessage.toString());
    }

    @Test
    public void maskNestedField() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\",\"homePhone\":{\"cc\":\"+63\",\"subscriber\":\"321312\"}}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        jsonMessageService.maskValue(jsonMessage, "homePhone.subscriber", ".*", JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals("{\"emailAddress\":\"mike.rustia@modirum.com\",\"homePhone\":{\"cc\":\"+63\",\"subscriber\":\"***\"}}", jsonMessage.toString());
    }

    @Test
    public void maskNonExistingNestedField() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\",\"homePhone\":{\"cc\":\"+63\"}}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        jsonMessageService.maskValue(jsonMessage, "homePhone.subscriber", ".*", JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals(json, jsonMessage.toString());
    }

    @Test
    public void maskNonExistingParentField() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\"}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        jsonMessageService.maskValue(jsonMessage, "homePhone.subscriber", ".*", JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals(json, jsonMessage.toString());
    }

    @Test
    public void maskObject() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\",\"homePhone\":{\"cc\":\"+63\",\"subscriber\":\"321312\"}}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        jsonMessageService.maskValue(jsonMessage, "homePhone", ".*", JsonMessageService.DEFAULT_MASK_REPLACEMENT);
        Assertions.assertEquals("{\"emailAddress\":\"mike.rustia@modirum.com\",\"homePhone\":{\"cc\":\"***\",\"subscriber\":\"***\"}}", jsonMessage.toString());
    }

    @Test
    public void invalidMaskingPattern() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\"}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);

        String emailMask = "*{*{*{";

        jsonMessageService.maskValue(jsonMessage, "emailAddress", emailMask, JsonMessageService.DEFAULT_MASK_REPLACEMENT);

        Assertions.assertEquals(json, jsonMessage.toString());

    }

    @Test
    public void writeOutputStream() throws Exception  {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\"}";

        JsonMessage jsonMessage = jsonMessageService.parse(json);
        MessageService.PublicBOS publicBOS = jsonMessageService.writeOutputStream(new MessageService.PublicBOS(512), jsonMessage);

        Assertions.assertEquals(json, publicBOS.toString());
    }

    @Test
    public void fieldPresentButNull() throws Exception {
        String json = "{\"eci\":null}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessageService.isFieldPresent(jsonMessage, FieldName.eci));
    }

    @Test
    public void fieldPresentButEmpty() throws Exception {
        String json = "{\"eci\":\"\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessageService.isFieldPresent(jsonMessage, FieldName.eci));
    }

    @Test
    public void fieldPresentWithValue() throws Exception {
        String json = "{\"eci\":\"05\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessageService.isFieldPresent(jsonMessage, FieldName.eci));
    }

    @Test
    public void fieldNotPresent() throws Exception {
        String json = "{}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertFalse(jsonMessageService.isFieldPresent(jsonMessage, FieldName.eci));
    }
}
