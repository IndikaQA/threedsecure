package com.modirum.ds.enums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

public class PaymentSystemTypeTest {

    @Test
    public void getPaymentSystemType_existing_success() {
        Optional<PaymentSystemType> psType = PaymentSystemType.getPaymentSystemType(PaymentSystemType.eftpos.name());
        Assertions.assertNotNull(psType);
        Assertions.assertTrue(psType.isPresent());
    }

    @Test
    public void getPaymentSystemType_nonexistent_success() {
        Optional<PaymentSystemType> psType = PaymentSystemType.getPaymentSystemType("randomValue");
        Assertions.assertNotNull(psType);
        Assertions.assertFalse(psType.isPresent());
    }
}
