package com.modirum.ds.imports.issuer;

import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.issuer.operation.ImportIssuerModelProvider;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.PaymentSystemsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class IssuerBatchModelValidatorTest {

    @Mock
    private PaymentSystemsService paymentSystemsService;

    @Mock
    private IssuerService issuerService;

    private User user = new User();

    @Test
    public void testSupportedHeaders() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        IssuerBatchModelValidator validator = new IssuerBatchModelValidator(configuration, paymentSystemsService, issuerService);
        assertEquals(2, validator.getSupportedHeaders().size());
    }

    @Test
    public void testInvalidModel() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        configuration.setContinueOnError(true);
        IssuerBatchModelValidator validator = new IssuerBatchModelValidator(configuration, paymentSystemsService, issuerService);

        when(paymentSystemsService.isPaymentSystemExists(any())).thenReturn(true);

        ImportIssuerModelProvider issuerModelProvider = new ImportIssuerModelProvider();
        boolean valid = validator.isValid(CSVHeaderFormat.ADD_ISSUER, Arrays.asList(issuerModelProvider.getInalidModel()));
        Collection<String> errors = validator.getErrors();

        assertEquals(3, errors.size());
        assertFalse(valid);
    }

    @Test
    public void testValidModel() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        IssuerBatchModelValidator validator = new IssuerBatchModelValidator(configuration, paymentSystemsService, issuerService);

        when(paymentSystemsService.isPaymentSystemExists(any())).thenReturn(true);

        ImportIssuerModelProvider issuerModelProvider = new ImportIssuerModelProvider();
        boolean valid = validator.isValid(CSVHeaderFormat.ADD_ISSUER, Arrays.asList(issuerModelProvider.getValidModel()));
        Collection<String> errors = validator.getErrors();

        assertEquals(0, errors.size());
        assertTrue(valid);
    }
}