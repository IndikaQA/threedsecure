package com.modirum.ds.imports.merchant;

import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.merchant.operation.ImportMerchantModelProvider;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.AcquirerService;
import com.modirum.ds.services.PaymentSystemsService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ThreeDSProfileService;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MerchantBatchModelValidatorTest {

    @Mock
    private AcquirerService acquirerService;

    @Mock
    private ThreeDSProfileService threeDSProfileService;

    @Mock
    private PaymentSystemsService paymentSystemsService;

    @Mock
    private PersistenceService persistenceService;

    private User user = new User();

    @Test
    public void testSupportedHeaders() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        MerchantBatchModelValidator validator = new MerchantBatchModelValidator(configuration, acquirerService, threeDSProfileService, persistenceService, paymentSystemsService);
        assertEquals(2, validator.getSupportedHeaders().size());
    }

    @Test
    public void testInvalidModel() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        configuration.setContinueOnError(true);
        MerchantBatchModelValidator validator = new MerchantBatchModelValidator(configuration, acquirerService, threeDSProfileService, persistenceService, paymentSystemsService);

        when(paymentSystemsService.isPaymentSystemExists(any())).thenReturn(true);

        ImportMerchantModelProvider merchantModelProvider = new ImportMerchantModelProvider();
        boolean valid = validator.isValid(CSVHeaderFormat.ADD_MERCHANT, Arrays.asList(merchantModelProvider.getInvalidImportModel()));
        Collection<String> errors = validator.getErrors();

        assertEquals(4, errors.size());
        assertFalse(valid);
    }

    @Test
    public void testValidModel() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        MerchantBatchModelValidator validator = new MerchantBatchModelValidator(configuration, acquirerService, threeDSProfileService, persistenceService, paymentSystemsService);

        when(paymentSystemsService.isPaymentSystemExists(any())).thenReturn(true);
        when(acquirerService.getAcquirer(any(), any())).then(i -> {
            Acquirer acquirer = new Acquirer();
            acquirer.setId(NumberUtils.toInt(i.getArgument(0)));
            return acquirer;
        });
        when(threeDSProfileService.getActiveTdsServerProfile(any(), any())).then(i -> {
            TDSServerProfile tdsServerProfile = new TDSServerProfile();
            tdsServerProfile.setId(NumberUtils.toLong(i.getArgument(0)));
            return tdsServerProfile;
        });

        ImportMerchantModelProvider merchantModelProvider = new ImportMerchantModelProvider();
        boolean valid = validator.isValid(CSVHeaderFormat.ADD_MERCHANT, Arrays.asList(merchantModelProvider.getValidImportModel()));
        Collection<String> errors = validator.getErrors();

        assertEquals(0, errors.size());
        assertTrue(valid);
    }
}