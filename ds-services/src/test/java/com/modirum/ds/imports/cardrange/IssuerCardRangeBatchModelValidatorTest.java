package com.modirum.ds.imports.cardrange;

import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.cardrange.operation.ImportCardRangeModelProvider;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.PaymentSystemsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class IssuerCardRangeBatchModelValidatorTest {

    @Mock
    private PaymentSystemsService paymentSystemsService;

    @Mock
    private IssuerService issuerService;

    private User user = new User();

    @Test
    public void testSupportedHeaders() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        IssuerCardRangeBatchModelValidator validator = new IssuerCardRangeBatchModelValidator(configuration, issuerService, paymentSystemsService);
        assertEquals(2, validator.getSupportedHeaders().size());
    }

    @Test
    public void testInvalidModel() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        configuration.setContinueOnError(true);
        IssuerCardRangeBatchModelValidator validator = new IssuerCardRangeBatchModelValidator(configuration, issuerService, paymentSystemsService);

        when(paymentSystemsService.isPaymentSystemExists(any())).thenReturn(true);

        ImportCardRangeModelProvider cardRangeModelProvider = new ImportCardRangeModelProvider();
        boolean valid = validator.isValid(CSVHeaderFormat.ADD_CARD_RANGE, Arrays.asList(cardRangeModelProvider.getInvalidModel()));
        Collection<String> errors = validator.getErrors();

        assertEquals(3, errors.size());
        assertFalse(valid);
    }

    @Test
    public void testValidModel() {
        DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(user);
        IssuerCardRangeBatchModelValidator validator = new IssuerCardRangeBatchModelValidator(configuration, issuerService, paymentSystemsService);

        when(paymentSystemsService.isPaymentSystemExists(any())).thenReturn(true);
        when(issuerService.isIssuerExists(any(), any(), any())).thenReturn(true);

        ImportCardRangeModelProvider cardRangeModelProvider = new ImportCardRangeModelProvider();
        boolean valid = validator.isValid(CSVHeaderFormat.ADD_CARD_RANGE, Arrays.asList(cardRangeModelProvider.getValidModel()));
        Collection<String> errors = validator.getErrors();

        assertEquals(0, errors.size());
        assertTrue(valid);
    }
}