package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.db.dao.PersistenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AddIssuerOperationProcessorTest {

    private ImportIssuerModelProvider modelProvider = new ImportIssuerModelProvider();

    @Mock
    private PersistenceService persistenceService;

    @Test
    void testProcess() throws Exception {
        BatchIssuerModel model = modelProvider.getValidModel();
        AddIssuerOperationProcessor processor = new AddIssuerOperationProcessor(persistenceService);
        processor.process(Collections.singletonList(model));
        verify(persistenceService).save(any());
    }

}