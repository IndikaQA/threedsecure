package com.modirum.ds.web.helper;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PurchaseCurrencyHelperTest {

    @Test
    public void testExcludedAllSingleCurrency() {
        PurchaseCurrencyHelper helper = new PurchaseCurrencyHelper("-*,036", new HashMap<>());
        assertFalse(helper.isExcluded("036"));
        assertFalse(helper.isLimitedNotSupported("036"));
        assertTrue(helper.isLimitedNotSupported("037"));
        assertTrue(helper.isSupported("036"));
        assertFalse(helper.isSupported("037"));
        assertEquals(helper.getAllowedAsString(), "036");
    }

    @Test
    public void testMultipleCurrency() {
        PurchaseCurrencyHelper helper = new PurchaseCurrencyHelper("036,037", new HashMap<>());
        assertFalse(helper.isExcluded("036"));
        assertFalse(helper.isLimitedNotSupported("036"));
        assertFalse(helper.isLimitedNotSupported("037"));
        assertTrue(helper.isSupported("044"));
        assertTrue(helper.isSupported("036"));
        assertTrue(helper.isSupported("037"));
        assertFalse(helper.isSupported("038"));
    }

    @Test
    public void testSingleCurrency() {
        PurchaseCurrencyHelper helper = new PurchaseCurrencyHelper("036", new HashMap<>());
        assertFalse(helper.isExcluded("036"));
        assertFalse(helper.isLimitedNotSupported("036"));
        assertFalse(helper.isLimitedNotSupported("037"));
        assertTrue(helper.isSupported("036"));
    }

    @Test
    public void testMultipleCurrencyWithRemoved() {
        PurchaseCurrencyHelper helper = new PurchaseCurrencyHelper("037,-044", new HashMap<>());
        assertFalse(helper.isLimitedNotSupported("036"));
        assertFalse(helper.isLimitedNotSupported("037"));
        assertTrue(helper.isSupported("037"));
        assertFalse(helper.isSupported("044"));
        assertTrue(helper.isExcluded("044"));
    }

    @Test
    public void testMultipleCurrencyWithRemovedAndExcluded() {
        Map<String, String> exclusions = new HashMap<>();
        exclusions.put("036", "036");
        exclusions.put("044", "044");
        PurchaseCurrencyHelper helper = new PurchaseCurrencyHelper("038,040", exclusions);
        assertFalse(helper.isSupported("036"));
        assertTrue(helper.isSupported("048"));
        assertFalse(helper.isSupported("044"));
        assertTrue(helper.isExcluded("036"));
        assertTrue(helper.isExcluded("044"));
    }

    @Test
    public void testLimitedSingleCurrencyWithExcluded() {
        Map<String, String> exclusions = new HashMap<>();
        exclusions.put("044", "044");
        PurchaseCurrencyHelper helper = new PurchaseCurrencyHelper("-*,036", exclusions);
        assertTrue(helper.isSupported("036"));
        assertFalse(helper.isSupported("048"));
        assertFalse(helper.isSupported("044"));
        assertFalse(helper.isLimitedNotSupported("036"));
        assertTrue(helper.isLimitedNotSupported("037"));
    }

    @Test
    public void testNull() {
        PurchaseCurrencyHelper helper = new PurchaseCurrencyHelper(null, new HashMap<>());
        assertTrue(helper.isSupported("048"));
        assertTrue(helper.isSupported("044"));
    }
}