package com.modirum.ds.web;

import com.modirum.ds.web.context.WebContext;
import mockit.Expectations;
import mockit.Mocked;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;

public class WebContextTest {

    @Test
    public void testGetRequestServerPort_fromHeader_success(@Mocked HttpServletRequest request) {
        WebContext webContext = new WebContext();
        webContext.setRequest(request);
        final int expectedResult = 8080;

        new Expectations() {{
            webContext.getRequest();
            result = request;
            request.getHeader("X-Forwarded-Port");
            result = expectedResult;
        }};

        Assertions.assertEquals(expectedResult, webContext.getRequestServerPort());
    }

    @Test
    public void testGetRequestServerPort_fromContainer_success(@Mocked HttpServletRequest request) {
        WebContext webContext = new WebContext();
        webContext.setRequest(request);
        final int expectedResult = 8080;

        new Expectations() {{
            webContext.getRequest();
            result = request;
            request.getHeader("X-Forwarded-Port");
            result = null;
            request.getServerPort();
            result = expectedResult;
        }};

        Assertions.assertEquals(expectedResult, webContext.getRequestServerPort());
    }
}
