<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.modirum.ds.utils.DateUtil,java.util.Date"%>
<html>
<head>
    <title>Modirum DS</title>
</head>
<body>
<H2>Modirum DS - 3DS Directory Server</H2>
<div align="center">
Server time: <%=DateUtil.formatDate(new Date(),"yyyyMMdd HH:mm:ss", true) %> GMT
</div>
</body>
</html>