/*
 * Copyright (C) 2018 Modirum
 * All rights reserved.
 * Estonia Tallinn, https://www.modirum.com
 *
 * @author Andri Kruus,andri
 * Created on Nov 23, 2018 2:55:51 PM
 */
package com.modirum.ds.ext.fss;

import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.CachedObject;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Map;

public class DDCollectionService {
    static Logger log = LoggerFactory.getLogger(DDCollectionService.class);
    public final static String cProductName = "Modirum DDC";
    //public final static String VERSION = "1.1";
    public static byte[] OK = Misc.toBytes("OK");
    public static byte[] ERR = Misc.toBytes("ERR");
    public final static int HEADERS_MAX_LEN = 8192;
    public final static int SCRIPTDATA_MAX_LEN = 4096;
    public java.util.List<String> COOKIE_NAMES;
    protected String cookieName = "TDSMR";
    protected Map<String, CachedObject<DeviceData>> deviceDataCache = new java.util.TreeMap<>();
    protected String ddCollectScriptJSPPath = "/WEB-INF/3dsmrelay.jsp";
    protected String forwardURI = "/DDF";
    protected String collectURI = "./DDC";
    protected String fpScriptUrl = "../../ddc/fingerprintjs2/fingerprint2.js";
    protected String ecPath = "../../ddc/evercookie/";
    protected String cookieDomain = null;
    protected boolean captureIP = true;
    protected boolean captureHeaders = true;
    protected boolean captureCookie = true;
    protected boolean addCookieIfMissing = true;
    protected boolean useEverCookie = false;
    // defaults to skip
    protected String[] skipHeaders = {"cookie", "host", "x-ssl-client-cert", "content-length", "origin", "x-forwarded-for", "referer"};
    protected int cookieAge = 3600 * 24 * 365;
    protected boolean singleCollect = false;
    protected String ddVersion = getBuildVersion();

    public static class DeviceData {
        public String version = getBuildVersion();
        public String ident; // xid or tdsTransId
        public CharSequence headers;
        public CharSequence scriptData;
        public StringBuilder cookie;
        public String ipAddress;
        public String TDS2_Navigator_language;
        public String TDS2_Navigator_jsEnabled;
        public String TDS2_Navigator_javaEnabled;
        public String TDS2_Screen_colorDepth;
        public String TDS2_Screen_height;
        public String TDS2_Screen_width;
        public String TDS2_Screen_pixelDepth;
        public String TDS2_TimezoneOffset;
        public String TDS2_Navigator_Accept;
        public String TDS2_Navigator_UA;
    }

    public DDCollectionService() {
        COOKIE_NAMES = new java.util.ArrayList<>(5);
        COOKIE_NAMES.addAll(java.util.Arrays.asList("TDSMR", "assignedDeviceId"));
    }


    /**
     * Requred request attributes prior forward
     * TACS: req.setAttribute("xid" , xidvalue);
     * 3DSRelay: req.setAttribute("tdsId" , threeDServerTransId);
     *
     * @param req
     * @param resp
     * @return initial empty devicedata
     * @throws ServletException
     * @throws java.io.IOException
     */
    public DeviceData forward(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
        String txId = (String) req.getAttribute("tdsId");
        if (txId == null) {
            txId = (String) req.getAttribute("xid");
        }

        CachedObject<DeviceData> prepared = new CachedObject<>();
        prepared.setLoaded(System.currentTimeMillis());
        DeviceData dd = new DeviceData();
        dd.version = ddVersion;
        dd.ident = txId;
        if (this.captureIP) {
            dd.ipAddress = req.getRemoteAddr();
        }

        dd.TDS2_Navigator_Accept = req.getHeader("accept");
        dd.TDS2_Navigator_UA = req.getHeader("user-agent");
        if (this.captureHeaders) {
            StringBuilder hdrs = null;
            java.util.Enumeration<String> hit = req.getHeaderNames();
            for (; hit.hasMoreElements(); ) {
                String hn = hit.nextElement();
                if (!Misc.in(hn, skipHeaders)) {
                    StringBuilder sb = this.getHeaders(req, hn);
                    if (sb != null) {
                        if (hdrs == null) {
                            hdrs = new StringBuilder(1024);
                        }
                        if (hdrs.length() > 0) {
                            hdrs.append('\n');
                        }
                        hdrs.append(hn.length() > 128 ? hn.substring(0, 128) : hn);
                        hdrs.append(": ");
                        hdrs.append(sb);
                    }
                }
            }
            if (hdrs != null && hdrs.length() > HEADERS_MAX_LEN) {
                hdrs.setLength(HEADERS_MAX_LEN);
            }
            dd.headers = hdrs;
        }

        StringBuilder ck = this.getCookies(req);
        if (this.captureCookie && ck != null) {
            dd.cookie = ck;
        }
        Cookie cknew = null;
        if (this.addCookieIfMissing && (ck == null || ck.indexOf(cookieName + "=") < 0)) {
            String rnd = Misc.calculateSHA256AndEncodeBASE64(
                    "Nano" + System.nanoTime() + "." + Math.random()).substring(0, 16);
            cknew = new Cookie(cookieName, Misc.padCutStringLeft(txId + "_" + rnd, 'X', 51));
            cknew.setMaxAge(cookieAge);
            if (cookieDomain != null) {
                cknew.setDomain(cookieDomain);
            }
            resp.addCookie(cknew);
        }
        if (this.useEverCookie) {
            if (cknew == null) {
                String rnd = Misc.calculateSHA256AndEncodeBASE64(
                        "Nano" + System.nanoTime() + "." + Math.random()).substring(0, 16);
                cknew = new Cookie(cookieName, Misc.cutLeft(rnd + "_" + txId, 51));
                cknew.setMaxAge(cookieAge);
            }
            req.setAttribute("ecURI", this.ecPath);
            req.setAttribute("everCookieValue", cknew.getValue());
            req.setAttribute("everCookieName", cknew.getName());
        }

        prepared.setObject(dd);
        synchronized (deviceDataCache) {
            deviceDataCache.put(txId, prepared);
        }

        req.setAttribute("fpScriptUrl", fpScriptUrl);
        req.setAttribute("singleCollect", singleCollect);
        req.setAttribute("collectURI", collectURI);
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=utf-8");
        resp.setHeader("Cache-Control", "no-store");
        req.getRequestDispatcher(ddCollectScriptJSPPath).include(req, resp);

        // clean up here if not implemente periodic
        if (deviceDataCache.size() > 500) {
            //CompletableFuture.runAsync(()-> cleanupDDCache());
            ServiceLocator.getInstance().getScheduledExecutorService().submit(() -> cleanupDDCache());
        }
        return dd;
    }

    /**
     * Forward collection script post to this method to get filled device data
     *
     * @param req
     * @param resp
     * @return filled or half filled devicedata depends what was posted
     * @throws ServletException
     * @throws java.io.IOException
     */
    public DeviceData collect(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
        String d = req.getParameter("deviceData");
        String sv = req.getParameter("version");
        String ec = req.getParameter("EC");
        String tm = req.getParameter("TM");
        String tdsTransId = req.getParameter("tdsTransId");
        String xid = req.getParameter("xid");
        DeviceData ddx = null;

        resp.setHeader("Cache-Control", "no-store");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/plain; charset=utf-8");
        log.info("Got deviceData dsTransId:{}, xid:{}, tm {}, ver:{}, data:{}, ec:{}", tdsTransId, xid, tm, sv,
                 Misc.maskIntelligent(d, 25), Misc.maskIntelligent(ec, 10));

        if (Misc.isNotNullOrEmpty(tdsTransId) || Misc.isNotNullOrEmpty(xid)) {
            CachedObject<DeviceData> co = deviceDataCache.get(Misc.isNullOrEmpty(xid) ? tdsTransId : xid);
            if (co != null) {
                long ms = System.currentTimeMillis() - co.getLoaded();
                log.info("Got deviceData dsTransId:{}, xid:{} roundtrip in {}ms", tdsTransId, xid, ms);
                ddx = co.getObject();
                if (Misc.isNotNullOrEmptyTrimmed(d)) {
                    log.info("Adding captured device data ..");
                    if (d != null && d.length() > SCRIPTDATA_MAX_LEN) {
                        d = d.substring(0, SCRIPTDATA_MAX_LEN);
                    }
                    ddx.scriptData = d;
                }
                if (Misc.isNotNullOrEmptyTrimmed(sv)) {
                    //ddx.version=sv;
                }
                String uajs = req.getParameter("TDS2_Navigator_UA");
                if (Misc.isNotNullOrEmptyTrimmed(uajs)) {
                    ddx.TDS2_Navigator_UA = uajs;
                    ddx.TDS2_Navigator_javaEnabled = req.getParameter("TDS2_Navigator_javaEnabled");
                    ddx.TDS2_Navigator_jsEnabled = req.getParameter("TDS2_Navigator_jsEnabled");
                    ddx.TDS2_Navigator_language = req.getParameter("TDS2_Navigator_language");
                    ddx.TDS2_Screen_colorDepth = req.getParameter("TDS2_Screen_colorDepth");
                    ddx.TDS2_Screen_height = req.getParameter("TDS2_Screen_height");
                    ddx.TDS2_Screen_pixelDepth = req.getParameter("TDS2_Screen_pixelDepth");
                    ddx.TDS2_Screen_width = req.getParameter("TDS2_Screen_width");
                    ddx.TDS2_TimezoneOffset = req.getParameter("TDS2_TimezoneOffset");
                }
                if (ddx.cookie == null && this.captureCookie) {
                    ddx.cookie = this.getCookies(req);
                }
                if (Misc.isNotNullOrEmptyTrimmed(ec)) // && ddx.cookie==null)
                {
                    if (ddx.cookie == null) {
                        ddx.cookie = new StringBuilder();
                    }
                    String ecx = (ec.length() > 64 ? ec.substring(0, 64) : ec);
                    int ix = -1;
                    if (ecx.length() > 7 && ((ix = ddx.cookie.indexOf(ecx)) < 0 || ix > 230)) {
                        log.info("Adding captured evercookie " + ecx + " to cookie data");
                        ddx.cookie.insert(0, ecx + "; ");
                    } else if (ecx.length() > 7) {
                        log.info("Cookie equal evercookie " + ecx + " already in captured cookies, not duplicating");
                    }
                }
                co.setLoaded(System.currentTimeMillis());
                synchronized (co) {
                    co.notifyAll();
                }

                resp.getOutputStream().write(OK);
            } else {
                log.warn("Unable to connect devicedata prepared with " + tdsTransId + "/" + xid + " not exists");
                resp.getOutputStream().write(ERR);
            }
        } else {
            log.warn("Unable to connect dots with deviceData no tdsTransId/xid");
            resp.getOutputStream().write(ERR);
        }
        return ddx;
    }

    public void cleanupDDCache() {
        String[] keys = null;
        synchronized (deviceDataCache) {
            keys = deviceDataCache.keySet().toArray(new String[deviceDataCache.size()]);
        }
        int cnt = 0;
        long expiredForGood = System.currentTimeMillis() - 60000;
        for (String key : keys) {
            CachedObject<DeviceData> co = deviceDataCache.get(key);
            if (co != null && co.getLoaded() < expiredForGood) {
                synchronized (deviceDataCache) {
                    deviceDataCache.remove(key);
                }
                cnt++;
            }
        }
        log.info("cleanupDDCache romoved expired " + cnt + " of " + " all " + keys.length + " DDs");
    }

    public StringBuilder getHeaders(HttpServletRequest req, String hn) {
        StringBuilder hdrs = null;
        java.util.Enumeration<String> hev = req.getHeaders(hn);
        if (hev != null) {
            hdrs = new StringBuilder(1024);
            for (; hev.hasMoreElements(); ) {
                String hv = hev.nextElement();
                hdrs.append(hv);
                if (!hev.hasMoreElements()) {
                    break;
                }
                hdrs.append("; ");
            }
        }
        return hdrs;
    }

    public StringBuilder getCookies(HttpServletRequest req) {
        StringBuilder cookies = null;
        StringBuilder priocookies = null;
        java.util.Enumeration<String> hev = req.getHeaders("cookie");
        if (hev != null) {
            cookies = new StringBuilder(1024);
            priocookies = new StringBuilder(255);
            for (; hev.hasMoreElements(); ) {
                String hv = hev.nextElement();
                for (int cn = 0; cn < COOKIE_NAMES.size(); cn++) {
                    int cs = -1;
                    while (hv != null && (cs = hv.indexOf(COOKIE_NAMES.get(cn))) > -1) {
                        int ce = hv.indexOf(';', cs);
                        String cv = hv.substring(cs, ce > cs ? ce : hv.length());
                        if (priocookies.indexOf(cv) < 0) {
                            priocookies.append(cv).append(';');
                        }
                        hv = Misc.replace(hv, cv + (ce > -1 ? ";" : ""), "");
                    }
                }
                cookies.append(hv);
                if (!hev.hasMoreElements()) {
                    break;
                }
                cookies.append("; ");
            }

            if (priocookies.length() > 0) {
                cookies.insert(0, priocookies);
            }
        }
        return cookies;
    }

    public String getFpScriptUrl() {
        return fpScriptUrl;
    }

    /**
     * set fingerprint script url defaults to '../../ddc/fingerprintjs2/fingerprint2.js'
     *
     * @param fpScriptUrl
     */
    public void setFpScriptUrl(String fpScriptUrl) {
        this.fpScriptUrl = fpScriptUrl;
    }

    public String getEvercookiePath() {
        return ecPath;
    }

    /**
     * define evercookie implementation path/uri defaults to '../../ddc/evercookie/'
     *
     * @param ecPath
     */
    public void setEvercookiePath(String ecPath) {
        this.ecPath = ecPath;
    }

    public String getCookieDomain() {
        return cookieDomain;
    }

    /**
     * define cookie domoan for normal cookie
     *
     * @param cookieDomain
     */
    public void setCookieDomain(String cookieDomain) {
        this.cookieDomain = cookieDomain;
    }

    public boolean isCaptureIP() {
        return captureIP;
    }

    /**
     * if to capture ip from req.getRemoteAddr() defaults as true
     *
     * @param captureIP
     */
    public void setCaptureIP(boolean captureIP) {
        this.captureIP = captureIP;
    }

    public boolean isCaptureHeaders() {
        return captureHeaders;
    }

    /**
     * if to capture headers except undesired ones defaults as true
     *
     * @param captureHeaders
     */
    public void setCaptureHeaders(boolean captureHeaders) {
        this.captureHeaders = captureHeaders;
    }

    public boolean isCaptureCookie() {
        return captureCookie;
    }

    /**
     * if to capture normal and evercookies defaults to true
     *
     * @param captureCookie
     */
    public void setCaptureCookie(boolean captureCookie) {
        this.captureCookie = captureCookie;
    }

    public boolean isAddCookieIfMissing() {
        return addCookieIfMissing;
    }

    /**
     * set true if normal coookie to be added  defaults true
     *
     * @param addCookieIfMissing
     */
    public void setAddCookieIfMissing(boolean addCookieIfMissing) {
        this.addCookieIfMissing = addCookieIfMissing;
    }

    public boolean isUseEverCookie() {
        return useEverCookie;
    }

    /**
     * set true if evercookie is to be used (in addition to normal cookies)
     * defautls as false
     *
     * @param useEverCookie
     */
    public void setUseEverCookie(boolean useEverCookie) {
        this.useEverCookie = useEverCookie;
    }

    public int getCookieAge() {
        return cookieAge;
    }

    /**
     * set cookie life in seconds defaults to what is equivalent of 365 days
     *
     * @param cookieAge
     */
    public void setCookieAge(int cookieAge) {
        this.cookieAge = cookieAge;
    }

    public Map<String, CachedObject<DeviceData>> getDeviceDataCache() {
        return deviceDataCache;
    }

    public boolean isSingleCollect() {
        return singleCollect;
    }

    /**
     * if all data evecookie, devicedata to be collected in one  call or 2 calls
     * defaults to 2 calls /false
     *
     * @param singleCollect
     */
    public void setSingleCollect(boolean singleCollect) {
        this.singleCollect = singleCollect;
    }


    public String getForwardURI() {
        return forwardURI;
    }

    /**
     * informal of uri used/exposed  when forward is called needed by 3ds relay may be not needed in tacs case
     *
     * @param forwardURI
     */
    public void setForwardURI(String forwardURI) {
        this.forwardURI = forwardURI;
    }

    public String getCollectURI() {
        return collectURI;
    }

    /**
     * set relative uri to post collected data defautls to ./DDC
     *
     * @param forwardURI
     */
    public void setCollectURI(String collectURI) {
        this.collectURI = collectURI;
    }

    public String getCookieName() {
        return cookieName;
    }

    /**
     * defines cookie name
     * defaults to "TDSMR"  fro 3dsmetod relay, TACS need to set this to "assignedDeviceId"
     *
     * @param cookieName
     */
    public void setCookieName(String cookieName) {
        this.cookieName = cookieName;
        if (this.COOKIE_NAMES.contains(cookieName)) {
            this.COOKIE_NAMES.add(cookieName);
        }
    }


    static String buildVersion = null;
    static long buildDate = 0;
    static java.util.jar.Manifest mf = null;

    public static final String getBuildVersion() {
        if (buildVersion != null) {
            return buildVersion;
        }

        if (mf == null) {
            try {
                java.util.Enumeration<java.net.URL> resources = DDCollectionService.class.getClassLoader().getResources(
                        "META-INF/MANIFEST.MF");

                while (resources.hasMoreElements()) {
                    java.io.InputStream mfs = null;
                    try {
                        mfs = resources.nextElement().openStream();
                        java.util.jar.Manifest mfx = new java.util.jar.Manifest();
                        mfx.read(mfs);
                        mfs.close();
                        mfs = null;
                        String it = mfx.getMainAttributes().getValue("Implementation-Title");
                        if (it != null && (it.trim().equalsIgnoreCase(cProductName) ||
                                           it.trim().equalsIgnoreCase(cProductName + " Core"))) {
                            mf = mfx;
                            break;
                        }

                    } catch (Exception e) {
                        // dont print anything
                        e.printStackTrace();
                    } finally {
                        if (mfs != null) {
                            try {
                                mfs.close();
                            } catch (Exception dc) {
                            }
                            mfs = null;
                        }
                    }

                } // while

            } catch (Exception e) {
                // dont print anything
                e.printStackTrace();
            }
        } // if

        String bd = null;
        String checkSum = null;
        String pro = cProductName;
        String ven = null;
        String bv = buildVersion;
        if (mf != null) {
            pro = mf.getMainAttributes().getValue("Implementation-Title");

            ven = mf.getMainAttributes().getValue("Implementation-Vendor");

            bv = mf.getMainAttributes().getValue("Implementation-Version");
            if (Misc.isNotNullOrEmpty(bv)) {
                buildVersion = bv;
            }

            bd = mf.getMainAttributes().getValue("Built-Date");

            checkSum = mf.getMainAttributes().getValue("CheckValue");
        }

        try {
            buildDate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(bd).getTime();
        } catch (Exception dc) {
            buildDate = 0;
        }

        if (log != null && log.isInfoEnabled()) {
            log.info("Product: " + pro + " build version " + bv + " build date: " + new java.util.Date(buildDate));
        } else {
            System.out.println(
                    "Product: " + pro + " build version " + bv + " build date: " + new java.util.Date(buildDate));
        }

        if (checkSum == null) {
            throw new RuntimeException("Manifest file altered, missing CheckValue, application cannot continue");
        }

        String calcChecksum = checkSum(pro, ven, bv, bd);
        if (!calcChecksum.equals(checkSum)) {
            throw new RuntimeException("Manifest file altered, invalid checkvalue, application cannot continue");
        }

        return bv != null ? bv : "1.0";
    }

    private final static StringBuffer checksumSecret = new StringBuffer("yAAAZa").append("0LZ").append("5gq;-").append(
            "1#9").append("LGI").append("wAZ05").append("pqM$$Dj");

    static String checkSum(String prod, String vend, String version, String buildDate) {
        try {
            MessageDigest mdigest = MessageDigest.getInstance("SHA-256");
            String concatString = prod + vend + version + buildDate + checksumSecret.toString();
            byte[] digestResult = mdigest.digest(concatString.getBytes(StandardCharsets.UTF_8));
            return Base64.encode(digestResult);
        } catch (Exception e) {
            log.error("Digest calculation error", e);
            return null;
        }
    }

    public static void main(String[] args) {
        if (args != null && args.length > 4 && "checkvalue".equals(args[0])) {
            System.out.println(checkSum(args[1], args[2], args[3], args[4]));
            return;
        }
    }

    public String[] getSkipHeaders() {
        return skipHeaders;
    }

    /**
     * headers not to be collected for devicedata headers defaults to:
     * {"cookie","host","x-ssl-client-cert","content-length","origin","x-forwarded-for","referer"}
     *
     * @param skipHeaders
     */
    public void setSkipHeaders(String[] skipHeaders) {
        this.skipHeaders = skipHeaders;
    }

    public String getDdCollectScriptJSPPath() {
        return ddCollectScriptJSPPath;
    }

    /**
     * jsp rendering device collection scirpts and stuff defaults to
     * local war /WEB-INF/3dsmrelay.jsp
     *
     * @param ddCollectScriptJSPPath
     */
    public void setDdCollectScriptJSPPath(String ddCollectScriptJSPPath) {
        this.ddCollectScriptJSPPath = ddCollectScriptJSPPath;
    }

    public String getDdVersion() {
        return ddVersion;
    }

    /**
     * set custom devide data version if proves necessary
     *
     * @param ddVersion
     */
    public void setDdVersion(String ddVersion) {
        this.ddVersion = ddVersion;
    }
}
