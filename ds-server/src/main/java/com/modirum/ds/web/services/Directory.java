/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 12. jaan 2016
 *
 */
package com.modirum.ds.web.services;

import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.utils.db.IdGen;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.ext.fss.ThreeDSMethodRelayService;
import com.modirum.ds.hsm.ACOS5Service;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.model.CacheObject;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.Persistable;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.MessageService210;
import com.modirum.ds.services.MessageService220;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.web.core.DSConfigurableSettings;
import com.modirum.ds.web.core.DirectoryCache;
import com.modirum.ds.web.core.PresCardRangeCache;
import com.modirum.ds.web.core.RequestsTiming;
import com.modirum.ds.web.job.CleanUpJob;
import com.modirum.ds.web.job.TimingStoreJob;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import com.modirum.ds.web.context.WebContext;
import com.modirum.ds.services.server.PaymentSystemSettingsCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.management.ManagementFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.modirum.ds.web.core.DirectoryCache.C_CACHE_PERIOD;

@Component
public class Directory {

    private static final Logger log = LoggerFactory.getLogger(Directory.class);
    private static final int C_CLIENT_CACHE_PERIOD = 10 * 60 * 1000; // 10 min http clients (wo ranges reload flags set)
    private static final long FUTURE_LIMIT = 20 * 24 * 60 * 60 * 1000L;
    private static final long PAST_LIMIT = 10 * 24 * 60 * 60 * 1000L;

    private final static long cHour = 3600L * 1000L;
    private long lastCleanUpChd = 0;
    private long lastCleanUpAll = 0;

    protected DirectoryCache cache;
    protected RequestsTiming requestsTiming;

    private int schedulerOverUtilizedCount;

    private int issuerBinLength = 6;

    @Autowired
    private PaymentSystemSettingsCacheService paymentSystemSettingsCacheService;

    @Autowired
    private KeyService keyService;

    private final IssuerService issuerService;
    private ThreeDSMethodRelayService tdsmRelayService;
    private final PersistenceService persistenceService;
    private final DirectoryLicenceChecker licenceChecker;
    private final DSConfigurableSettings dsSettings;

    private final String hostId;

    private final AtomicLong requestCounter;
    private final AtomicLong processingTime;

    private long prevCount, prevCountTime, prevProcTime;

    protected volatile String cachedRangeDataSerial;
    protected volatile long cachedRangeDataTs;
    protected volatile File tempDir;

    public boolean useDiffUpdates;

    private final Map<TDSModel.MessageVersion, AbstractDirectoryService> dsServices = new HashMap<>();

    protected String dsPublicURL;

    protected WebContext webContext;

    private static volatile Directory instance;


    /**
     * Constructor.
     */
    private Directory() {
        hostId = Utils.getLocalNonLoIp();
        issuerService = ServiceLocator.getInstance().getIssuerService();
        persistenceService = ServiceLocator.getInstance().getPersistenceService();
        dsSettings = new DSConfigurableSettings(persistenceService);
        licenceChecker = new DirectoryLicenceChecker(persistenceService, ServiceLocator.getInstance().getProductInfoService());

        prevCountTime = System.currentTimeMillis();
        prevProcTime = 0;
        requestCounter = new AtomicLong(0);
        processingTime = new AtomicLong(0);

        cache = new DirectoryCache();
        requestsTiming = new RequestsTiming(persistenceService, hostId);
    }

    public void addDirectoryService(AbstractDirectoryService directoryService, TDSModel.MessageVersion messageVersion) {
        dsServices.put(messageVersion, directoryService);
        messageVersion.setSupport();
        log.info("Intialized support for " + directoryService.getMessageService().getSupportedVersion());
    }

    public ScheduledThreadPoolExecutor getScheduledExecutorService() {
        return ServiceLocator.getInstance().getScheduledExecutorService();
    }

    public Acquirer getAcquirerByBIN(Integer paymentSystemId, String acquirerBIN) throws Exception {
        Acquirer acquirer = getAcquirerByBINImpl(paymentSystemId, acquirerBIN);
        if (acquirer == null && acquirerBIN.length() > 6) {
            String nonPaddedBIN = String.valueOf(Misc.parseLong(acquirerBIN));
            acquirer = getAcquirerByBINImpl(paymentSystemId, nonPaddedBIN);
        }
        return acquirer;
    }

    public HttpsJSSEClient getHttpsJSSEClient(String key) {
        long s = System.currentTimeMillis();
        CacheObject<HttpsJSSEClient> co = cache.getHttpClientCache().get(key);
        if (co != null && co.loaded > s - C_CLIENT_CACHE_PERIOD) {
            return co.obj;
        }
        return null;
    }

    public void putHttpsJSSEClient(final String key, HttpsJSSEClient client) {
        CacheObject<HttpsJSSEClient> co = new CacheObject<>();
        co.obj = client;
        co.loaded = System.currentTimeMillis();
        synchronized (cache.getHttpClientCache()) {
            cache.getHttpClientCache().put(key, co);
        }
    }

    protected Acquirer getAcquirerByBINImpl(Integer paymentSystemId, String acquirerBIN) throws Exception {
        Acquirer acq;
        String cacheKey = String.valueOf(Objects.hash(acquirerBIN, paymentSystemId));
        CacheObject<Acquirer> cacheObject = cache.getAcquirerCache().get(cacheKey);
        if (cacheObject == null || cacheObject.loaded < System.currentTimeMillis() - C_CACHE_PERIOD) {
            acq = persistenceService.getAcquirerByBINAndPaymentSystem(acquirerBIN, paymentSystemId);
            if (cacheObject == null) {
                cacheObject = new CacheObject<>();
                synchronized (cache.getAcquirerCache()) {
                    cache.getAcquirerCache().put(cacheKey, cacheObject);
                }
            }
            cacheObject.obj = acq;
            cacheObject.loaded = System.currentTimeMillis();
        } else {
            acq = cacheObject.obj;
        }
        return acq;
    }

    public Issuer getIssuerById(Integer paymentSystemId, Integer id) throws Exception {
        Issuer is;
        CacheObject<Issuer> cacheObject = cache.getIssuerCache().get(id);
        if (cacheObject == null || cacheObject.loaded < System.currentTimeMillis() - C_CACHE_PERIOD) {
            is = issuerService.getIssuerById(id);
            if (cacheObject == null) {
                cacheObject = new CacheObject<>();
                synchronized (cache.getIssuerCache()) {
                    cache.getIssuerCache().put(id, cacheObject);
                }
            }
            cacheObject.obj = is;
            cacheObject.loaded = System.currentTimeMillis();
        } else {
            is = cacheObject.obj;
        }
        if (is != null && is.getPaymentSystemId().equals(paymentSystemId)) {
            // separate (multitenant) payment system caches are not implemented yet. Filter by PS id before return.
            return is;
        }
        return null;
    }

    public Merchant getAquirerMerchant(Integer paymentSystemId, Integer acquirerId, String acquirerMerchantId) throws Exception {
        String mk = "" + acquirerId + "-" + acquirerMerchantId;
        Merchant m;
        CacheObject<Merchant> cacheObject = cache.getMerchantCache().get(mk);
        if (cacheObject == null || cacheObject.loaded < System.currentTimeMillis() - C_CACHE_PERIOD) {
            m = persistenceService.getAquirerMerchant(acquirerId, acquirerMerchantId);
            if (cacheObject == null) {
                cacheObject = new CacheObject<>();
                synchronized (cache.getMerchantCache()) {
                    cache.getMerchantCache().put(mk, cacheObject);
                }
            }
            cacheObject.obj = m;
            cacheObject.loaded = System.currentTimeMillis();
        } else {
            m = cacheObject.obj;
        }
        if (m != null && m.getPaymentSystemId().equals(paymentSystemId)) {
            // separate (multitenant) payment system caches are not implemented yet. Filter by PS id before return.
            return m;
        }
        return null;
    }

    public Merchant getRequestorMerchant(Integer paymentSystemId, String requestorId) throws Exception {
        String mk = "requestorId-" + requestorId;
        Merchant m;
        CacheObject<Merchant> cacheObject = cache.getMerchantCache().get(mk);
        if (cacheObject == null || cacheObject.loaded < System.currentTimeMillis() - C_CACHE_PERIOD) {
            m = persistenceService.getRequestorMerchant(requestorId);
            if (cacheObject == null) {
                cacheObject = new CacheObject<>();
                synchronized (cache.getMerchantCache()) {
                    cache.getMerchantCache().put(mk, cacheObject);
                }
            }
            cacheObject.obj = m;
            cacheObject.loaded = System.currentTimeMillis();
        } else {
            m = cacheObject.obj;
        }
        if (m != null && m.getPaymentSystemId().equals(paymentSystemId)) {
            // separate (multitenant) payment system caches are not implemented yet. Filter by PS id before return.
            return m;
        }
        return null;
    }

    public TDSServerProfile getTdsServerProfile(Integer paymentSystemId, Long tdsServerId) throws Exception {
        TDSServerProfile tdsServerProfile;
        CacheObject<TDSServerProfile> cacheObject = cache.getTDSServerCache().get(tdsServerId);
        if (cacheObject == null || cacheObject.loaded < System.currentTimeMillis() - C_CACHE_PERIOD) {
            tdsServerProfile = persistenceService.getPersistableById(tdsServerId, TDSServerProfile.class);
            if (cacheObject == null) {
                cacheObject = new CacheObject<>();
                synchronized (cache.getTDSServerCache()) {
                    cache.getTDSServerCache().put(tdsServerId, cacheObject);
                }
            }
            cacheObject.obj = tdsServerProfile;
            cacheObject.loaded = System.currentTimeMillis();
        } else {
            tdsServerProfile = cacheObject.obj;
        }

        if (tdsServerProfile != null && tdsServerProfile.getPaymentSystemId().equals(paymentSystemId)) {
            // separate (multitenant) payment system caches are not implemented yet. Filter by PS id before return.
            return tdsServerProfile;
        }
        return null;
    }

    public TDSServerProfile getTdsServerProfileByRequestorID(Integer paymentSystemId, String rqid) throws Exception {
        TDSServerProfile tdsServerProfile;
        String cacheKey = paymentSystemId + ":" + rqid;
        CacheObject<TDSServerProfile> cacheObject = cache.getTDSServerCacheRID().get(cacheKey);
        if (cacheObject == null || cacheObject.loaded < System.currentTimeMillis() - C_CACHE_PERIOD) {
            Map<String, Object> query = new HashMap<>(1);
            query.put("requestorID", rqid);
            query.put("paymentSystemId", paymentSystemId);
            List<TDSServerProfile> pl = persistenceService.getPersitableList(TDSServerProfile.class, "id", null, false, null, null,
                                                                             query);
            tdsServerProfile = pl.size() > 0 ? pl.get(0) : null;

            if (cacheObject == null) {
                cacheObject = new CacheObject<>();
                synchronized (cache.getTDSServerCacheRID()) {
                    cache.getTDSServerCacheRID().put(cacheKey, cacheObject);
                }
            }
            cacheObject.obj = tdsServerProfile;
            cacheObject.loaded = System.currentTimeMillis();
        } else {
            tdsServerProfile = cacheObject.obj;
        }

        if (tdsServerProfile != null && tdsServerProfile.getPaymentSystemId().equals(paymentSystemId)) {
            // separate (multitenant) payment system caches are not implemented yet. Filter by PS id before return.
            return tdsServerProfile;
        }
        return null;
    }

    void periodicMaintenance(WebContext ctx) throws Exception {
        // setting cache reset
        if (dsSettings.isSettingsChanged()) {
            log.info("Settings change detected, reset setting cache");
            ctx.resetSettingCache();
            boolean treatEmptyValueAsMissing = Boolean.parseBoolean(ctx.getStringSetting(DsSetting.TREAT_EMPTY_VALUE_AS_MISSING.getKey()));
            MessageService210.getInstance().setTreatEmptyValueAsMissing(treatEmptyValueAsMissing);
            MessageService220.getInstance().setTreatEmptyValueAsMissing(treatEmptyValueAsMissing);
            if (tdsmRelayService != null) {
                tdsmRelayService = new ThreeDSMethodRelayService(this, ctx);
            }
        }

        if (keyService != null && keyService.isKeysChanged()) {
            log.info("Reset key cache due keys change detected");
            keyService.resetCache();
        }
        // PADSS this is now mandatory to encourage gc
        // as to free potentially sensible
        // short lived string or other objects as soon as possible
        long oldFree = Runtime.getRuntime().freeMemory();
        long oldTotal = Runtime.getRuntime().totalMemory();
        double oldUsingM = (oldTotal - oldFree) / 1000000.0d;

        Runtime.getRuntime().gc();
        Thread.sleep(5);
        // print jvm memory info
        int allThreads = ManagementFactory.getThreadMXBean().getThreadCount();
        double loadAvg = ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage();
        double newUsingM = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1000000.0d;

        long countSnap = requestCounter.get();
        long periodCount = countSnap - prevCount;
        prevCount = countSnap;
        long procTimeSnap = processingTime.get();
        long prevCountTimeSnap = prevCountTime;
        long procTimeDelta = procTimeSnap - prevProcTime;
        prevCountTime = System.currentTimeMillis();
        prevProcTime = procTimeSnap;
        double rr = periodCount > 0 ? periodCount * 60000d / (prevCountTime - prevCountTimeSnap) : 0d;
        double tpr = periodCount > 0 ? (double) procTimeDelta / periodCount / 1000d : 0d; // in seconds

        String ii =
                "Memory maxheap " + ctx.format(Runtime.getRuntime().maxMemory() / 1000000.0d, 2) + "M" + " current " +
                ctx.format(Runtime.getRuntime().totalMemory() / 1000000.0d, 2) + "M" + " free " +
                ctx.format(Runtime.getRuntime().freeMemory() / 1000000.0d, 2) + "M" + " using " +
                ctx.format(newUsingM, 2) + "M" + " freed " + ctx.format(oldUsingM - newUsingM, 2) + "M" + ", threads " +
                allThreads + ", req/min " + ctx.format(rr, 2) + " avg " + ctx.format(tpr, 2) + "s/req, avg load " +
                ctx.format(loadAvg, 2) + ", total reqs " + countSnap;

        log.info(ii);

        String debugTI = ctx.getStringSetting(DsSetting.DEBUG_THREAD_INFO.getKey());
        if ("true".equals(debugTI)) {
            java.lang.management.ThreadInfo[] til = java.lang.management.ManagementFactory.getThreadMXBean().dumpAllThreads(
                    false, false);
            StringBuilder tisb = new StringBuilder();
            for (java.lang.management.ThreadInfo ti : til) {
                tisb.append(ti.toString()).append("\n");
            }
            log.info("Thread info:\n" + tisb);
        }

        Setting intanceinfo = new Setting();
        intanceinfo.setKey("DS.instanceinfo." + getHostId());
        intanceinfo.setProcessorId((short) 0);
        intanceinfo.setComment("Runtime information " + getHostId());
        intanceinfo.setValue(
                System.getProperty("java.vm.name") + " " + System.getProperty("java.runtime.version") + " " +
                (ctx.getServletContext() != null ? ctx.getServletContext().getServerInfo() + " servlet-" +
                                                   ctx.getServletContext().getMajorVersion() + "." +
                                                   ctx.getServletContext().getMinorVersion() : "") + " " + ii);
        intanceinfo.setLastModified(new java.util.Date());
        intanceinfo.setLastModifiedBy("DServer " + getHostId());
        persistenceService.saveOrUpdate(intanceinfo);

        validateACOS5ServiceIfPresent(ctx);

        // check scheduler health
        int maxThreads = getScheduledExecutorService().getMaximumPoolSize();
        int currentThreads = getScheduledExecutorService().getPoolSize();
        int runningThreads = getScheduledExecutorService().getActiveCount();
        log.info("Scheduler state: poolsize " + currentThreads + " max pool " + maxThreads + " running now " +
                 runningThreads + " queued " + getScheduledExecutorService().getQueue().size());
        if (runningThreads >= ServiceLocator.maxSchedulerThreads &&
            getScheduledExecutorService().getQueue().size() > ServiceLocator.maxSchedulerThreads) {
            schedulerOverUtilizedCount++;
            log.warn("Scheduler service over utilized (" + schedulerOverUtilizedCount + ")!!!");
            if (schedulerOverUtilizedCount > 10) {
                log.warn(
                        "Scheduler service over utilized for extended period (most likely threads on waiting locking resources)," +
                        " consider restarting application server any time soon if situation remains!!!!");

                log.warn("Creating new Scheduler service and adding old qued jobs to new sheduler");
                List<Runnable> jobs = ServiceLocator.getInstance().getScheduledExecutorService().shutdownNow();
                ServiceLocator.getInstance().unsetScheduledExecutorService();
                ScheduledExecutorService newsexec = ServiceLocator.getInstance().getScheduledExecutorService();
                for (Runnable r : jobs) {
                    newsexec.submit(r);
                }
            }
        } else {
            schedulerOverUtilizedCount = 0;
        }

        cache.cleanCaches();

        for (AbstractDirectoryService ds : dsServices.values()) {
            ds.periodic(webContext);
        }
        cleanUP(ctx);
        storeExpCerts();
    }

    public final long countRequest() {
        return requestCounter.incrementAndGet();
    }

    public final void processingTime(long time) {
        processingTime.addAndGet(time);
    }

    // auto cleanup implementation
    void cleanUP(WebContext webContext) {
        // dont check anything if already done 4 hours ago.
        if (lastCleanUpAll < System.currentTimeMillis() - 4 * cHour) {
            Calendar c = Calendar.getInstance();
            int ch = c.get(Calendar.HOUR_OF_DAY);

            int cleanUpAll = webContext.getIntSetting(DsSetting.CLEANUP_ALL.getKey());
            int cleanUpAllRun = webContext.getIntSetting(DsSetting.CLEANUP_ALL_RUN.getKey());
            if (cleanUpAll > 5 && cleanUpAllRun > -1 && cleanUpAllRun < 24 && ch == cleanUpAllRun) {
                lastCleanUpAll = System.currentTimeMillis();
                getScheduledExecutorService().execute(
                        new CleanUpJob(persistenceService, cleanUpAll, true, webContext));
                return;
            }
        }

        // dont check anything if already done 4 hours ago.
        if (lastCleanUpChd < System.currentTimeMillis() - 4 * cHour) {
            Calendar c = Calendar.getInstance();
            int ch = c.get(Calendar.HOUR_OF_DAY);

            int cleanUpChd = webContext.getIntSetting(DsSetting.CLEANUP_CHD.getKey());
            int cleanUpChdRun = webContext.getIntSetting(DsSetting.CLEANUP_CHD_RUN.getKey());
            if (cleanUpChd > 5 && cleanUpChdRun > -1 && cleanUpChdRun < 24 && ch == cleanUpChdRun) {
                lastCleanUpChd = System.currentTimeMillis();
                getScheduledExecutorService().execute(
                        new CleanUpJob(persistenceService, cleanUpChd, false, webContext));
            }
        }
    }

    public int getIssuerBinLength() {
        return issuerBinLength;
    }

    public void init(WebContext webContext) {
        this.webContext = webContext;
        try {
            dsSettings.isSettingsChanged();
            keyService.isKeysChanged();
            useDiffUpdates = dsSettings.isDSSupportRangeDiffUpdates();
            String ibl = persistenceService.getStringSetting(DsSetting.ISSUER_BIN_STORE_LENGTH.getKey());
            if (Misc.isInt(ibl) && Misc.isInRange(ibl, 6, 10)) {
                issuerBinLength = Misc.parseInt(ibl);
            } else if (Misc.isInt(ibl)) {
                log.warn("Setting issuer.BINstoreLength value '{}', out of accepted range (6..10), ignored", ibl);
            }

            if (dsSettings.isSslCertsManagedExternally()) {
                int count = 0;
                do {
                    count++;
                    String expCertsContent = persistenceService.getStringSetting("DS.expCerts" + count);
                    if (Misc.isNotNullOrEmpty(expCertsContent)) {
                        List<String> elements = Misc.splitToList(expCertsContent, ";");
                        for (String pair : elements) {
                            String[] sdnDate = Misc.splitTwo(pair, "|");
                            if (sdnDate != null && sdnDate.length == 2) {
                                cachedExpCertificates.put(sdnDate[0], DateUtil.parseDate(sdnDate[1], dateFormat));
                            }
                        }
                    } else {
                        break;
                    }
                } while (true);
            }
        } catch (Exception e) {
            log.warn("Exception during init/restore expired cert data from db setting ", e);
        }
        getScheduledExecutorService().scheduleAtFixedRate(new TimingStoreJob(requestsTiming), 20, 15,
                                                               TimeUnit.SECONDS);
    }

    public void initRangeLoading() {
        getScheduledExecutorService().scheduleWithFixedDelay(new CardRangesLoadJob(), 3, 300, TimeUnit.SECONDS);
    }

    public String getHostId() {
        return hostId;
    }

    public final void certifcateExpirationChecks(X509Certificate remoteCert, String remoteCertificateSDN) {
        if (remoteCert.getNotAfter().getTime() < System.currentTimeMillis() + FUTURE_LIMIT &&
            remoteCert.getNotAfter().getTime() > System.currentTimeMillis() - PAST_LIMIT) {
            if (!cachedExpCertificates.containsKey(remoteCertificateSDN)) {
                synchronized (cachedExpCertificates) {
                    cachedExpCertificates.put(remoteCertificateSDN, remoteCert.getNotAfter());
                }
            }
        }
    }

    private Date cachedCertsLastStored = new Date(0);
    private final Map<String, Date> cachedExpCertificates = new HashMap<>();
    private static final String dateFormat = "yyyy-MM-dd HH:mm";

    public void storeExpCerts() {
        long period = 60 * 60 * 1000;
        if (cachedExpCertificates.size() > 0 && cachedExpCertificates.size() < 50) {
            period = period / 12;
        }

        if (cachedCertsLastStored == null || System.currentTimeMillis() - cachedCertsLastStored.getTime() > period) {

        } else {
            return;
        }

        try {
            Date throwOut = new java.util.Date(System.currentTimeMillis() - PAST_LIMIT);

            for (Object entry : cachedExpCertificates.entrySet().toArray()) {
                Map.Entry<String, Date> e = (Map.Entry<String, Date>) entry;
                if (e.getValue().before(throwOut)) {
                    synchronized (cachedExpCertificates) {
                        cachedExpCertificates.remove(e.getKey());
                    }
                }
            }

            log.info("Updating expiring certificate warning data " + cachedExpCertificates.size() + " items");

            List<Persistable> settings = new ArrayList<>();

            int setCount = 1;
            Setting expCerts = persistenceService.getSettingById("DS.expCerts." + getHostId() + "." + setCount);
            if (expCerts == null) {
                expCerts = new Setting();
                expCerts.setKey("DS.expCerts." + getHostId() + "." + setCount);
            }

            StringBuilder settingContentNew = new StringBuilder(cachedExpCertificates.size() * (32 + 14));
            for (Map.Entry<String, Date> entry : cachedExpCertificates.entrySet()) {
                String sdn = entry.getKey();
                Date expDate = entry.getValue();
                settingContentNew.append(sdn);
                settingContentNew.append('|');
                settingContentNew.append(DateUtil.formatDate(expDate, dateFormat));
                settingContentNew.append(';');

                if (settingContentNew.length() > 30000) {
                    expCerts.setValue(settingContentNew.toString());
                    expCerts.setLastModified(new java.util.Date());
                    expCerts.setLastModifiedBy("DServer " + getHostId());
                    settings.add(expCerts);

                    setCount++;
                    expCerts = persistenceService.getSettingById("DS.expCerts." + getHostId() + "." + setCount);
                    if (expCerts == null) {
                        expCerts = new Setting();
                        expCerts.setKey("DS.expCerts." + getHostId() + "." + setCount);
                    }
                    settingContentNew.setLength(0);
                }
            }

            settings.add(expCerts);
            expCerts.setValue(settingContentNew.toString());
            expCerts.setLastModified(new java.util.Date());
            expCerts.setLastModifiedBy("DServer " + getHostId());

            persistenceService.saveOrUpdate(settings);
            cachedCertsLastStored = new Date();
        } catch (Exception e) {
            log.warn("Unable to save or update setting expCerts", e);
        }

    }

    public final String getDSURL(WebContext wctx) {
        // will be updated to use ds_paymentsystem_conf.dsId in future, keeping for old Production compatibility
        String dsURL = wctx.getStringSetting(PsSetting.DS_URL + "." + wctx.getServerIntanceId());
        if (Misc.isNullOrEmpty(dsURL)) {
            dsURL = paymentSystemSettingsCacheService.getPaymentSystemSetting(wctx.getPaymentSystemId(),
                                                                               PsSetting.DS_URL);
        }

        if (Misc.isNullOrEmpty(dsURL) && wctx.getRequest() != null) {
            dsURL = wctx.getRequest().getRequestURL().toString();
        }
        return dsURL;
    }

    public final String getDSPublicURL(WebContext webContext) {
        if (dsPublicURL == null) {
            String dsURL1 = webContext.getStringSetting(DsSetting.DS_PUBLIC_URL + "." + webContext.getServerIntanceId());
            if (Misc.isNullOrEmpty(dsURL1)) {
                long dbId = IdGen.getDbId();
                if (dbId > 0) {
                    dsURL1 = webContext.getStringSetting(DsSetting.DS_PUBLIC_URL + "." + dbId);
                }
            }

            if (Misc.isNullOrEmpty(dsURL1)) {
                dsURL1 = webContext.getStringSetting(DsSetting.DS_PUBLIC_URL.getKey());
            }

            if (Misc.isNullOrEmpty(dsURL1)) {
                dsURL1 = getDSURL(webContext);
            }

            if (Misc.isNotNullOrEmpty(dsURL1)) {
                dsPublicURL = dsURL1;
            } else if (webContext.getRequest() != null) {
                dsPublicURL = webContext.getRequest().getRequestURL().toString();
            }
        }
        return dsPublicURL;
    }

    int oldReload = 0;

    /**
     * Check licensing.
     */
    public void checkLicensing() {
        licenceChecker.checkLicensing();
    }

    /**
     * Check extension licensing.
     */
    public boolean checkExtensionLicensing(String ext) {
        return licenceChecker.checkExtensionLicensing(ext);
    }

    public RequestsTiming getRequestsTiming() {
        return requestsTiming;
    }

    public Map<Integer, PresCardRangeCache> getRangesFromHistory(String serial) throws Exception {
        if (tempDir == null) {
            File rf = File.createTempFile("DSCR-", ".ser");
            tempDir = rf.getParentFile();
        }
        File histFile = new File(tempDir, "DSCR-" + serial + ".ser");
        Map<Integer, PresCardRangeCache> res = null;
        if (histFile.exists() && histFile.isFile() && histFile.canRead() && histFile.length() > 0) {
            try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(histFile))) {
                res = (Map<Integer, PresCardRangeCache>) is.readObject();
            }
        }
        return res;
    }

    /**
     * Reloads card range data from DB once per hour.
     * Reload can be forced by setting {@link DsSetting#DS_RELOAD_RANGES} to an integer higher that 0.
     */
    public synchronized void loadCardRanges() {
        try {
            Setting s = persistenceService.getSettingById(DsSetting.DS_RELOAD_RANGES.getKey());
            int needTo = s != null && Misc.isInt(s.getValue()) ? Misc.parseInt(s.getValue()) : 0;

            if (cachedRangeDataTs < System.currentTimeMillis() - 60 * 60000 || needTo > oldReload) {
                cache.clearCachesFast();

                long st = System.currentTimeMillis();
                log.info((cachedRangeDataTs == 0 ? "Loading" : "Re-loading") + " card range data into memory..");
                fillCardBins(webContext);

                // store to temp
                String serial = null;
                boolean storeok = false;
                if (useDiffUpdates) {
                    ObjectOutputStream fos = null;
                    try {
                        serial = DateUtil.formatDate(new Date(), "yyyyMMddHHmmss");
                        File rf = File.createTempFile("DSCR-", ".ser");
                        tempDir = rf.getParentFile();
                        fos = new ObjectOutputStream(new FileOutputStream(rf));
                        fos.writeObject(cache.getPresCardRangeCacheMap());
                        fos.close();
                        fos = null;
                        File rnf = new File(rf.getParent(), "DSCR-" + serial + ".ser");
                        rf.renameTo(rnf);
                        if (rnf.exists()) {
                            storeok = true;
                        }
                    } catch (Exception ee) {
                        log.warn("Failed to store card ranges snapshot to temp file, serial num will not be updated",
                                 ee);
                    } finally {
                        if (fos != null) {
                            try {
                                fos.close();
                            } catch (Exception dc) {
                            }
                        }
                    }
                }

                cachedRangeDataTs = System.currentTimeMillis();
                if (useDiffUpdates && storeok) {
                    cachedRangeDataSerial = serial;
                }

                if (needTo > 0) {
                    needTo--;
                    oldReload = needTo;
                    s.setValue("" + needTo);
                    s.setLastModified(new java.util.Date());
                    s.setLastModifiedBy("DServer " + getHostId());
                    persistenceService.saveOrUpdate(s);
                }
                long e = System.currentTimeMillis();
                log.info("Loading card ranges complete " + cache.getPresCardRangeCacheMap().size() + " in " + (e - st) + " ms");
            }
        } catch (Exception e) {
            log.error("Exception during load card range data ", e);
        }
    }

    class CardRangesLoadJob implements Runnable {
        CardRangesLoadJob() {
        }

        public void run() {
            loadCardRanges();
        }

    }

    public IssuerService getIssuerService() {
        return issuerService;
    }

    protected void fillCardBins(WebContext wctx) throws Exception {
        String[] dsURL = new String[]{getDSPublicURL(webContext)};
        Map<Integer, PresCardRangeCache> presCardRangeCacheMap = new HashMap<>();
        for (AbstractDirectoryService dse : dsServices.values()) {
            dse.initPRes();
        }
        String forwardURI = tdsmRelayService == null ? null : tdsmRelayService.getForwardURI();
        List<CardRange> cardRanges = persistenceService.getCardBins(dsURL, forwardURI);
        for (CardRange cardRange : cardRanges) {
            for (AbstractDirectoryService dse : dsServices.values()) {
                dse.createAndAddRangeData(cardRange, wctx);
            }

            if (useDiffUpdates) {
                PresCardRangeCache presCardRangeCache = presCardRangeCacheMap.get(cardRange.getPaymentSystemId());
                if (presCardRangeCache == null) {
                    presCardRangeCache = new PresCardRangeCache();
                    presCardRangeCacheMap.put(cardRange.getPaymentSystemId(), presCardRangeCache);
                }
                presCardRangeCache.put(cardRange.getBinSt() + "-" + cardRange.getEnd(), cardRange);
            }
        }

        cache.setPresCardRangeCacheMap(presCardRangeCacheMap);

        for (AbstractDirectoryService dse : dsServices.values()) {
            dse.createRawCache();
        }

        log.info("PReq fill Ranges complete " + cardRanges.size());
    }

    public AbstractDirectoryService getDirectoryService(TDSModel.MessageVersion messageVersion) {
        return dsServices.get(messageVersion);
    }

    public ThreeDSMethodRelayService getTdsmRelayService() {
        return tdsmRelayService;
    }

    public void setTdsmRelayService(ThreeDSMethodRelayService tdsmRelayService) {
        this.tdsmRelayService = tdsmRelayService;
    }

    private void validateACOS5ServiceIfPresent(WebContext ctx) {
        Map<Long, HSMDevice> hsmDevices = ServiceLocator.getInstance().getHSMDevices();
        Optional<HSMDevice> acos5optional = hsmDevices.entrySet().stream()
                .filter(e -> e.getValue() instanceof ACOS5Service).map(Map.Entry::getValue).findFirst();

        if (!acos5optional.isPresent()) {
            return;
        }

        HSMDevice acos5Service = acos5optional.get();
        try {
            String serial = acos5Service.getHardwareSerial();
            String serialPrev = (String) ctx.getCtxHolder().getApplicationAttribute("ACOS5.serial");
            if (serialPrev == null) {
                ctx.getCtxHolder().setApplicationAttribute("ACOS5.serial", serial);
            } else {
                if (serial != null && !serial.equals(serialPrev)) {
                    log.warn("Security WARNING: ACOS5 card serial number change detected " + serialPrev + " -> " +
                            serial +
                            ", card changed, if this was unauthorized change, start master key reneval procedure" +
                            " and migrate all encryption keys and data");
                } else if (serial != null && serial.equals(serialPrev)) {
                    log.info("ACO5 card present serial consistent " + serial + " all ok");
                }

                ctx.getCtxHolder().setApplicationAttribute("ACOS5.serial", serial);
            }

        } catch (Throwable t) {
            log.error("Checking presence fo ACOS5 card in cardreader failed, check hardware presence", t);
            log.warn(
                    "Security WARNING: if this was unauthorized removal or card stolen, start master key reneval procedure" +
                            " and migrate all encryption keys and data");
        }
    }
}
