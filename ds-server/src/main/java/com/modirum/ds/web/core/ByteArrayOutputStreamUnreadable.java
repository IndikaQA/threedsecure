package com.modirum.ds.web.core;

import com.modirum.ds.services.MessageService;

public class ByteArrayOutputStreamUnreadable extends MessageService.PublicBOS {

    public ByteArrayOutputStreamUnreadable(int size) {
        super(size);
    }

    public int unread() {
        return count > 0 ? (buf[--count] & 0xff) : -1;
    }

    public void recount() {
        if (count < buf.length) {
            count++;
        }
    }
}