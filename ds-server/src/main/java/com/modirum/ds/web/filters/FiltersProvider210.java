package com.modirum.ds.web.filters;

import com.modirum.ds.model.TDSModel;

import java.util.ArrayList;
import java.util.List;

public class FiltersProvider210 {

    /**
     * For forwarded messsages
     * MSGType + fieldname + Device + category
     * MSGType + fieldname + Device + category + status
     * MSGType + fieldname  (for all)
     */
    public static List<String> getFilters() {
        List<String> filters = new ArrayList<>();
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSCompInd." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSCompInd." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInd." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInd." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInd." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInd." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInfo." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInfo." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInfo." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorAuthenticationInfo." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorPriorAuthenticationInfo");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorChallengeInd." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorChallengeInd." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorChallengeInd." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorChallengeInd." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorName");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorID");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorURL." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorURL." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorURL." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorURL." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorURL." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorURL." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerRefNumber." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerRefNumber." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerRefNumber." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerRefNumber." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerRefNumber." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerRefNumber." + "03.02");

        filters.add(TDSModel.XmessageType.P_REQ.value + ".threeDSServerRefNumber." + "01.01");
        filters.add(TDSModel.XmessageType.P_REQ.value + ".threeDSServerRefNumber." + "01.02");
        filters.add(TDSModel.XmessageType.P_REQ.value + ".threeDSServerRefNumber." + "02.01");
        filters.add(TDSModel.XmessageType.P_REQ.value + ".threeDSServerRefNumber." + "02.02");
        filters.add(TDSModel.XmessageType.P_REQ.value + ".threeDSServerRefNumber." + "03.01");
        filters.add(TDSModel.XmessageType.P_REQ.value + ".threeDSServerRefNumber." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerOperatorID." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerOperatorID." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerOperatorID." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerOperatorID." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerOperatorID." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerOperatorID." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerURL." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerURL." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerURL." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerURL." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeRIInd." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctType." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctType." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctType." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctType." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctType." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctType." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerBIN." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerBIN." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerBIN." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerBIN." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerBIN." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerBIN." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerMerchantID." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerMerchantID." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerMerchantID." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerMerchantID." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerMerchantID." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerMerchantID." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acsEphemPubKey." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acsEphemPubKey." + "01.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".acsOperatorID." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsOperatorID." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsOperatorID." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsOperatorID." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsOperatorID." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsOperatorID." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsReferenceNumber");

        filters.add(TDSModel.XmessageType.A_RES.value + ".acsRenderingType." + "01.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsRenderingType." + "01.02.C");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".acsRenderingType." + "01.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".acsRenderingType." + "01.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".acsSignedContent." + "01.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsSignedContent." + "01.02.C");

        filters.add(TDSModel.XmessageType.A_RES.value + ".acsChallengeMandated." + "01.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsChallengeMandated." + "01.02.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsChallengeMandated." + "02.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsChallengeMandated." + "02.02.C");

        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "01.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "01.02.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "02.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "02.02.C");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "01.01.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "01.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "02.01.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "02.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "01.01.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "01.02.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "02.01.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "02.02.N");

        filters.add(TDSModel.XmessageType.A_RES.value + ".acsTransID");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".acsTransID");
        filters.add(TDSModel.XmessageType.R_RES.value + ".acsTransID");
        filters.add(TDSModel.XmessageType.ERRO.value + ".acsTransID");

        filters.add(TDSModel.XmessageType.A_RES.value + ".acsURL." + "01.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsURL." + "01.02.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsURL." + "02.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsURL." + "02.02.C");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".addrMatch." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".addrMatch." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".addrMatch." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".addrMatch." + "02.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "01.01.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "01.01.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "01.02.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "01.02.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "02.01.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "02.01.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "02.02.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "02.02.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "03.01.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "03.01.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "03.02.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationValue." + "03.02.A");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "01.01.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "01.01.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "01.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "01.02.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "02.01.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "02.01.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "02.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "02.02.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "03.01.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "03.01.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "03.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationValue." + "03.02.A");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".broadInfo.01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".broadInfo.01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".broadInfo.02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".broadInfo.02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".broadInfo.03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".broadInfo.03.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".broadInfo.01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".broadInfo.01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".broadInfo.02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".broadInfo.02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".broadInfo.03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".broadInfo.03.02");


        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserAcceptHeader." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserAcceptHeader." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserIP." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserIP." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserJavaEnabled." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserJavaEnabled." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserLanguage." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserLanguage." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserColorDepth." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserColorDepth." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserScreenHeight." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserScreenHeight." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserScreenWidth." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserScreenWidth." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserTZ." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserTZ." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserUserAgent." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserUserAgent." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".cardExpiryDate.01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".cardExpiryDate.01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".cardExpiryDate.02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".cardExpiryDate.02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".cardExpiryDate.03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".cardExpiryDate.03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctInfo");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctNumber");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acctID");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".billAddrCity");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".billAddrCountry");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".billAddrLine1");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".billAddrLine2");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".billAddrLine3");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".billAddrPostCode");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".billAddrState");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".homePhone");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".mobilePhone");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".workPhone");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".cardholderName");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".shipAddrCity");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".shipAddrCountry");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".shipAddrLine1");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".shipAddrLine2");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".shipAddrLine3");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".shipAddrPostCode");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".shipAddrState");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".challengeCancel");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceChannel");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceRenderOptions." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceRenderOptions." + "01.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".dsReferenceNumber");

        filters.add(TDSModel.XmessageType.A_RES.value + ".cardholderInfo." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".cardholderInfo." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".cardholderInfo." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".cardholderInfo." + "02.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".dsTransID");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".dsTransID");
        filters.add(TDSModel.XmessageType.R_RES.value + ".dsTransID");
        filters.add(TDSModel.XmessageType.ERRO.value + ".dsTransID");

        filters.add(TDSModel.XmessageType.A_RES.value + ".eci");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".eci");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".email");

        filters.add(TDSModel.XmessageType.ERRO.value + ".errorCode");
        filters.add(TDSModel.XmessageType.ERRO.value + ".errorComponent");
        filters.add(TDSModel.XmessageType.ERRO.value + ".errorDescription");
        filters.add(TDSModel.XmessageType.ERRO.value + ".errorDetail");
        filters.add(TDSModel.XmessageType.ERRO.value + ".errorMessageType");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".payTokenInd");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseInstalData." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseInstalData." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseInstalData." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseInstalData." + "02.02");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".interactionCounter." + "01.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".interactionCounter." + "01.02");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".interactionCounter." + "02.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".interactionCounter." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".mcc");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".merchantCountryCode");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".merchantName");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".merchantRiskIndicator");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".messageCategory");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".messageCategory");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".messageExtension");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".messageExtension");
        filters.add(TDSModel.XmessageType.A_RES.value + ".messageExtension");
        filters.add(TDSModel.XmessageType.R_RES.value + ".messageExtension");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".messageType");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".messageType");
        filters.add(TDSModel.XmessageType.A_RES.value + ".messageType");
        filters.add(TDSModel.XmessageType.R_RES.value + ".messageType");
        filters.add(TDSModel.XmessageType.ERRO.value + ".messageType");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".messageVersion");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".messageVersion");
        filters.add(TDSModel.XmessageType.A_RES.value + ".messageVersion");
        filters.add(TDSModel.XmessageType.R_RES.value + ".messageVersion");
        filters.add(TDSModel.XmessageType.ERRO.value + ".messageVersion");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSServerTransID");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".threeDSServerTransID");
        filters.add(TDSModel.XmessageType.A_RES.value + ".threeDSServerTransID");
        filters.add(TDSModel.XmessageType.R_RES.value + ".threeDSServerTransID");
        filters.add(TDSModel.XmessageType.ERRO.value + ".threeDSServerTransID");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".notificationURL." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".notificationURL." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseAmount." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseAmount." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseAmount." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseAmount." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseCurrency." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseCurrency." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseCurrency." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseCurrency." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseExponent." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseExponent." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseExponent." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseExponent." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseDate." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseDate." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseDate." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseDate." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringExpiry." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringExpiry." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringExpiry." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringExpiry." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringFrequency." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringFrequency." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringFrequency." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".recurringFrequency." + "02.02");

        filters.add(TDSModel.XmessageType.R_RES.value + ".resultsStatus." + "01.01");
        filters.add(TDSModel.XmessageType.R_RES.value + ".resultsStatus." + "01.02");
        filters.add(TDSModel.XmessageType.R_RES.value + ".resultsStatus." + "02.01");
        filters.add(TDSModel.XmessageType.R_RES.value + ".resultsStatus." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkAppID." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkAppID." + "01.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkEphemPubKey." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkEphemPubKey." + "01.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".sdkEphemPubKey." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".sdkEphemPubKey." + "01.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkMaxTimeout." + "01.01");
        filters.add(
                TDSModel.XmessageType.A_REQ.value + ".sdkMaxTimeout." + "01.02"); // invalid test case TC.DS.00405.002
        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkReferenceNumber." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkReferenceNumber." + "01.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkTransID." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkTransID." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".sdkTransID." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".sdkTransID." + "01.02");
        filters.add(TDSModel.XmessageType.ERRO.value + ".sdkTransID");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatus");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatus");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.01.N");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.01.U");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.01.R");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.01.N");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.01.U");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.01.R");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.01.N");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.01.U");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.01.R");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.N");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.U");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.R");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.C");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.N");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.U");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.R");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.C");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.N");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.U");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.R");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.Y");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.A");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.C");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.01.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.01.U");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.01.R");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.01.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.01.U");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.01.R");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.01.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.01.U");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.01.R");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.U");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.R");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.C");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.U");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.R");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.C");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.N");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.U");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.R");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.Y");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.A");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.C");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".transType." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".transType." + "02.01");
        return filters;
    }
}
