package com.modirum.ds.jdbc;

import com.modirum.ds.utils.db.IdGen;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Helper class for common SQL/JDBC related operations.
 */
public final class SqlUtil {

    private SqlUtil() {
        // no instance
    }

    /**
     * Gets the next ID value for <code>sequenceName</code> in the database. Sequences are retrieved from
     * <code>ds_ids</code> table. This method should be used with pooled connection providers (jdbcTemplate, dataSource).
     * Otherwise, if connections are obtained by DriverManager.getConnection() then connection should be reused, since
     * in such case these are not pooled and resource demanding.
     *
     * @param sequenceName the name of the desired id sequencer
     * @param jdbcTemplate jdbcTemplate data source to retrieve id from
     * @return next valid id.
     */
    public static long getIdGenNextValue(String sequenceName, JdbcTemplate jdbcTemplate) {
        IdGen idGen = IdGen.getInstance(sequenceName, "ds_ids", true);
        try (Connection con = jdbcTemplate.getDataSource().getConnection()) {
            return idGen.next(con);
        } catch (SQLException e) {
            // for consistency with spring JDBC template using unchecked exception.
            // there is no nice way to handle connection retrieval issues.
            throw new RuntimeException(e);
        }
    }

    /**
     * Retrieves Long value from result set. If resultSet value was null, return null object.
     *
     * @param rs db result set
     * @param columnName column name
     * @return Long value or null, if absent.
     * @throws SQLException if the column name is not valid. If a database access error occurs or this method is called
     * on a closed result set
     */
    public static Long getLong(ResultSet rs, String columnName) throws SQLException {
        Long value = rs.getLong(columnName);
        if (rs.wasNull()) {
            return null;
        }
        return value;
    }

    /**
     * Retrieves Integer value from result set. If resultSet value was null, return null object.
     *
     * @param rs db result set
     * @param columnName column name
     * @return Integer value or null, if absent.
     * @throws SQLException if the column name is not valid. If a database access error occurs or this method is called
     * on a closed result set
     */
    public static Integer getInteger(ResultSet rs, String columnName) throws SQLException {
        Integer value = rs.getInt(columnName);
        if (rs.wasNull()) {
            return null;
        }
        return value;
    }

    /**
     * Allows defaulting to NULL according to sql type.
     *
     * @param ps
     * @param index - position index of the parameter value.
     * @param value - value to set (Long, Integer or other)
     * @param dataType - {@link java.sql.Types}.type
     */
    public static void setNullableParam(PreparedStatement ps, int index, Object value,
                                        int dataType) throws SQLException {
        if (value == null) {
            ps.setNull(index, dataType);
        } else {
            ps.setObject(index, value, dataType);
        }
    }
}
