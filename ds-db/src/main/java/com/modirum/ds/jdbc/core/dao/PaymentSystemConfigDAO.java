package com.modirum.ds.jdbc.core.dao;

import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;
import com.modirum.ds.jdbc.BaseDAO;
import com.modirum.ds.jdbc.core.rowmapper.PaymentSystemConfigRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Repository
public class PaymentSystemConfigDAO extends BaseDAO {
    private final PaymentSystemConfigRowMapper paymentSystemConfigRowMapper = new PaymentSystemConfigRowMapper();
    private String[] columnNames = {"dsId", "paymentSystemId", "skey", "value", "comment", "createdDate",
            "createdUser", "lastModifiedDate", "lastModifiedBy"};

    public PaymentSystemConfig getPaymentSystemConfig(Integer dsId, Integer  paymentSystemId, String key) {
        String sql = "SELECT "+ String.join(",", columnNames) +" FROM ds_paymentsystem_conf " +
                     "WHERE dsId=:dsId AND paymentSystemId=:paymentSystemId AND skey=:skey";
        SqlParameterSource params = new MapSqlParameterSource("dsId", dsId)
                .addValue("paymentSystemId",paymentSystemId)
                .addValue("skey", key);

        List<PaymentSystemConfig> result  =  getNamedParameterJdbcTemplate().query(sql, params, paymentSystemConfigRowMapper);
        if (CollectionUtils.isEmpty(result)) {
            return null;
        }
        return result.get(0);
    }

    public List<PaymentSystemConfig> getPaymentSystemConfigs(Integer dsId, Integer paymentSystemId) {
        String sql = "SELECT "+  String.join(",", columnNames) +" FROM ds_paymentsystem_conf " +
                     "WHERE dsId=:dsId AND paymentSystemId=:paymentSystemId";
        SqlParameterSource params = new MapSqlParameterSource("dsId",dsId)
                .addValue("paymentSystemId", paymentSystemId);
        return getNamedParameterJdbcTemplate().query(sql, params, paymentSystemConfigRowMapper);
    }

    public void update(PaymentSystemConfig paymentSystemConfig){
        String sql = "UPDATE ds_paymentsystem_conf SET value=:value, comment=:comment, " +
                     "lastModifiedDate=:lastModifiedDate, lastModifiedBy=:lastModifiedBy " +
                     "WHERE dsId=:dsId AND paymentSystemId=:paymentSystemId AND skey=:skey";
        SqlParameterSource params = new MapSqlParameterSource("value", paymentSystemConfig.getValue())
                .addValue("comment", paymentSystemConfig.getComment())
                .addValue("dsId", paymentSystemConfig.getDsId())
                .addValue("paymentSystemId", paymentSystemConfig.getPaymentSystemId())
                .addValue("skey", paymentSystemConfig.getKey())
                .addValue("lastModifiedDate", paymentSystemConfig.getLastModifiedDate())
                .addValue("lastModifiedBy", paymentSystemConfig.getLastModifiedBy());
        getNamedParameterJdbcTemplate().update(sql, params);
    }

    public void insert(PaymentSystemConfig paymentSystemConfig){
        String sql = "INSERT INTO ds_paymentsystem_conf " +
                     "(dsId, paymentSystemId, skey, value, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy)" +
                     "VALUES (:dsId, :paymentSystemId, :skey, :value, :comment, :createdDate, :createdUser, :lastModifiedDate, :lastModifiedBy)";

        SqlParameterSource params = new MapSqlParameterSource("dsId", paymentSystemConfig.getDsId())
                .addValue("paymentSystemId", paymentSystemConfig.getPaymentSystemId())
                .addValue("skey", paymentSystemConfig.getKey())
                .addValue("value", paymentSystemConfig.getValue())
                .addValue("comment", paymentSystemConfig.getComment())
                .addValue("createdDate", paymentSystemConfig.getCreatedDate())
                .addValue("createdUser", paymentSystemConfig.getCreatedUser())
                .addValue("lastModifiedDate", paymentSystemConfig.getLastModifiedDate())
                .addValue("lastModifiedBy", paymentSystemConfig.getLastModifiedBy());
        getNamedParameterJdbcTemplate().update(sql, params);
    }
}
