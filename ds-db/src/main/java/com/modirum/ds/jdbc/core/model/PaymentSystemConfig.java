package com.modirum.ds.jdbc.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentSystemConfig  implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private Integer paymentSystemId;
    private Integer dsId;
    private String key;
    private String value;
    private String comment;
    private LocalDateTime createdDate;
    private String createdUser;
    private LocalDateTime lastModifiedDate;
    private String lastModifiedBy;

}