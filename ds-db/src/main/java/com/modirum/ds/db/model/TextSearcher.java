/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 23.03.2011
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TextSearcher extends Text implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private Integer start;
    private Integer limit;
    private Integer total;
    private String order;
    private String orderDirection;
    private String keyword;
}
