package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class User extends PersistableClass implements PaymentSystemResource, Serializable {

    private static final long serialVersionUID = 1L;

    public final static User SYSTEM_USER = new User(0L, "System");

    public final static List<String> VALID__SEARCH_ORDERS = Collections.unmodifiableList(
            Arrays.asList("id", "email", "firstName", "lastName", "status", "loginname"));

    protected Long id;
    protected String loginname;
    protected transient String password;
    protected transient String oldpasswords;
    protected Date lastPassChangeDate;
    protected int loginAttempts;
    protected Date lockedDate;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected String phone;
    protected String status;
    protected String locale;
    protected Set<String> roles = new HashSet<>();

    protected transient String newPassword;
    protected transient String newPasswordRepeat;
    protected boolean forcedPasswordChange;
    protected String factor2Authmethod;
    protected String factor2AuthDeviceId;
    protected String factor2Data1;
    protected String factor2Data2;
    protected String factor2Data3;
    protected String factor2Data4;
    private Integer paymentSystemId;

    protected boolean factor2Passed;

    private String paymentSystemName; // view model representation.

    public User() {
    }

    public User(Long id, String login) {
        this.id = id;
        this.loginname = login;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // TODO rename to getLoginName
    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldpasswords() {
        return oldpasswords;
    }

    public void setOldpasswords(String oldpasswords) {
        this.oldpasswords = oldpasswords;
    }

    public Date getLastPassChangeDate() {
        return lastPassChangeDate;
    }

    public void setLastPassChangeDate(Date lastPassChangeDate) {
        this.lastPassChangeDate = lastPassChangeDate;
    }

    public int getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(int loginAttampts) {
        this.loginAttempts = loginAttampts;
    }

    public Date getLockedDate() {
        return lockedDate;
    }

    public void setLockedDate(Date lockedDate) {
        this.lockedDate = lockedDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public boolean hasRole(String role) {
        return roles != null && role != null && roles.contains(role);
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRepeat() {
        return newPasswordRepeat;
    }

    public void setNewPasswordRepeat(String newPasswordRepeat) {
        this.newPasswordRepeat = newPasswordRepeat;
    }

    public boolean isForcedPasswordChange() {
        return forcedPasswordChange;
    }

    public void setForcedPasswordChange(boolean forcedPasswordChange) {
        this.forcedPasswordChange = forcedPasswordChange;
    }

    public String getFactor2Authmethod() {
        return factor2Authmethod;
    }

    public void setFactor2Authmethod(String factor2Authmethod) {
        this.factor2Authmethod = factor2Authmethod;
    }

    public String getFactor2AuthDeviceId() {
        return factor2AuthDeviceId;
    }

    public void setFactor2AuthDeviceId(String factor2AuthDeviceId) {
        this.factor2AuthDeviceId = factor2AuthDeviceId;
    }

    public boolean isFactor2Passed() {
        return factor2Passed;
    }

    public void setFactor2Passed(boolean factor2Passed) {
        this.factor2Passed = factor2Passed;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getFactor2Data1() {
        return factor2Data1;
    }

    public void setFactor2Data1(String factor2Data1) {
        this.factor2Data1 = factor2Data1;
    }

    public String getFactor2Data2() {
        return factor2Data2;
    }

    public void setFactor2Data2(String factor2Data2) {
        this.factor2Data2 = factor2Data2;
    }

    public String getFactor2Data3() {
        return factor2Data3;
    }

    public void setFactor2Data3(String factor2Data3) {
        this.factor2Data3 = factor2Data3;
    }

    public String getFactor2Data4() {
        return factor2Data4;
    }

    public void setFactor2Data4(String factor2Data4) {
        this.factor2Data4 = factor2Data4;
    }

    @Override
    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }

    public String getPaymentSystemName() {
        return paymentSystemName;
    }

    public void setPaymentSystemName(String paymentSystemName) {
        this.paymentSystemName = paymentSystemName;
    }
}
