package com.modirum.ds.db.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class License extends PersistableClass implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String extension;

    private String licensee;

    private String expiration;

    private Date issuedDate;

    private String licenseKey;

    private String maxMerchants;

    private String name;

    private String version;

}
