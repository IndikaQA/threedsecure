package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class Acquirer extends SchemeSubject implements PaymentSystemResource, Comparable<Acquirer> {

    //#1438 Should not be in used anymore in 3DS Messaging flow, leaving it here for ImportService compatibility.
    //Removing or updating ImportService is deferred to a separate task.
    @Deprecated
    protected Long tdsServerProfileId;
    private Integer paymentSystemId;

    @Override
    public int compareTo(Acquirer o) {
        if (o == null) {
            return -1;
        }
        return o.hashCode() - this.hashCode();
    }
}
