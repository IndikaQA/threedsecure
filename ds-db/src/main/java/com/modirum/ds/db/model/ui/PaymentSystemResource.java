package com.modirum.ds.db.model.ui;

/**
 * This interface must only by used by the DS-Manager.
 * Entities that implement this interface belong to a Payment System domain (e.g. Merchant, Acquirer, 3DS Server, Issuer,
 * Transaction).
 * The usage intent of the implementing classes - is to facilitate UI user access restriction. Where a DS-Manager user
 * can only access (view/edit) entities of its own Payment System only.
 */
public interface PaymentSystemResource {

    /**
     * Returns the ID of the Payment System which owns this resource (entity).
     * For certain types for resources it may be null (e.g. User, where null payment System Id means "super admin" user).
     * @return
     */
    Integer getPaymentSystemId();
}
