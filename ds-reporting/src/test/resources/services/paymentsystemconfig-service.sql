INSERT INTO ds_paymentsystem_conf (paymentSystemId, dsId, `skey`, `value`, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy)
VALUES ('1', '1', 'allowAllOtherPSUsers', 'true', 'commentOnThisSide', '2020-12-01 17:12:29.747000', 'DS', '2020-12-01 17:12:29.747000', 'DS');
INSERT INTO ds_paymentsystem_conf (paymentSystemId, dsId, `skey`, `value`, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy)
VALUES ('2', '1', 'allowDeleteEntries', 'true', 'comment for delete ', '2020-12-01 17:12:29.747000', 'DS', '2020-12-01 17:12:29.747000', 'DS');
INSERT INTO ds_paymentsystem_conf (paymentSystemId, dsId, `skey`, `value`, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy)
VALUES ('3', '1', 'allowEditEntries', 'true', 'comment for edit ', '2020-12-01 17:12:29.747000', 'DS', '2020-12-01 17:12:29.747000', 'DS');