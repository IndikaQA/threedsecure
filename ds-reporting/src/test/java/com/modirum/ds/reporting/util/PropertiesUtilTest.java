package com.modirum.ds.reporting.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.Properties;

public class PropertiesUtilTest {
    @Test
    public void propertiesLoaded() throws Exception {
        Properties properties = PropertiesUtil.loadFromFile("src/test/resources/test.properties");
        Assertions.assertEquals("value", properties.getProperty("property.key"));
    }

    @Test
    public void fileNotFoundException() {
        Assertions.assertThrows(FileNotFoundException.class, () -> PropertiesUtil.loadFromFile("unknown.properties"));
    }
}
