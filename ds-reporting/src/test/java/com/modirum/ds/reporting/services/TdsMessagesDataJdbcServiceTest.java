package com.modirum.ds.reporting.services;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.RunWithDB;
import com.modirum.ds.reporting.model.TDSMessageData;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

@RunWithDB
public class TdsMessagesDataJdbcServiceTest {
    private static DataSource dataSource = TestEmbeddedDbUtil.dataSource();
    private static TdsMessagesDataJdbcService tdsMessagesDataJdbcService = ServiceLocator.initInstance(dataSource).getTdsMessagesDataJdbcService();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeScriptsBefore = {"services/tds-messages-data.sql"}, executeStatementsAfter = {"TRUNCATE TABLE ds_tdsmessages;", "TRUNCATE TABLE ds_tdsrecords;", "TRUNCATE TABLE ds_tdsrecord_attributes;"})
    public void getByTdsRecordId() throws Exception {
        List<TDSMessageData> list = tdsMessagesDataJdbcService.getByTdsRecordId(470020L);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(4, list.size());
        Assertions.assertEquals(Long.valueOf(895060), list.get(0).getId());
        Assertions.assertEquals(Long.valueOf(895070), list.get(1).getId());
        Assertions.assertEquals(Long.valueOf(895080), list.get(2).getId());
        TDSMessageData tdsMessageData = list.get(3);
        Assertions.assertEquals(Long.valueOf(895090), tdsMessageData.getId());
        Assertions.assertEquals(Long.valueOf(470020), tdsMessageData.getTdsrId());
        Assertions.assertEquals("{\"acsReferenceNumber\":\"ACS-EMULATOR-REF-NO\",\"acsTransID\":\"14960eb4-b1f6-42c4-8b06-bd0877cfd064\",\"authenticationValue\":\"***\",\"dsReferenceNumber\":\"EMVCo1234567\",\"dsTransID\":\"20b01757-0eab-5931-8000-000000072c04\",\"eci\":\"05\",\"messageType\":\"ARes\",\"messageVersion\":\"2.2.0\",\"threeDSServerTransID\":\"b0f61cfe-d04f-446a-b648-8431b395ca0b\",\"transStatus\":\"Y\"}", tdsMessageData.getContents());
        Assertions.assertEquals("3DS:127.0.0.1", tdsMessageData.getDestIP());
        Assertions.assertEquals("DS:192.168.43.201", tdsMessageData.getSourceIP());
        Assertions.assertEquals("ARes", tdsMessageData.getMessageType());
        Assertions.assertEquals("2.2.0", tdsMessageData.getMessageVersion());
    }
}
