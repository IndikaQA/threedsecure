package com.modirum.ds.reporting.testutil;

import com.modirum.ds.reporting.util.JdbcUtils;
import com.modirum.ds.utils.Misc;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;

import javax.sql.DataSource;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * This provides helper methods when using the embedded H2 database in unit testing.
 */
public class TestEmbeddedDbUtil {

    /**
     * Read the mysql script by providing the relative path from the java module.
     * The list of pairs of replacement Strings(target and replacement CharSequences)
     * to be applied on the mysql script. Also converts the mysql script to H2 sql
     * script to be used in embedded h2 in unit test.
     *
     * @param mysqlRelativePath      The path to mysql script file relative to reporting module.
     *                               This maybe from other folders outside reporting module.
     * @param replacementStringPairs The list of pairs of replacement Strings(target and replacement CharSequences)
     *                               to be applied on the mysql script
     * @return
     * @throws Exception
     */
    public static String readProjectSqlScript(String mysqlRelativePath, List<Pair<String, String>> replacementStringPairs) throws Exception {
        String reportingDirectory = System.getProperty("user.dir");
        String sqlString = FileUtils.readFileToString(new File(
                reportingDirectory + mysqlRelativePath), StandardCharsets.UTF_8);
        if (Misc.isNotNullOrEmpty(replacementStringPairs)) {
            for (Pair<String, String> replacePair : replacementStringPairs) {
                sqlString = sqlString.replace(replacePair.getKey(), replacePair.getValue());
            }
        }
        return sqlString;
    }

    /**
     * Helper method to execute a project mysql script against H2 DataSource.
     *
     * @param dataSource
     * @param mysqlRelativePath
     * @throws Exception
     */
    public static void runProjectScript(DataSource dataSource, String mysqlRelativePath) throws Exception {
        String sql = readProjectSqlScript(mysqlRelativePath,
                                          Arrays.asList(
                                                  Pair.of("(attr, value(36))", "(attr, value)"),
                                                  Pair.of("ENGINE = InnoDB", ""),
                                                  Pair.of("COMMENT ='Acquirer entity definitions table name ds_acquirers'", ""),
                                                  Pair.of("CHARACTER SET utf8mb4", ""),
                                                  Pair.of("CHARSET utf8mb4", ""),
                                                  Pair.of("COLLATE utf8mb4_unicode_ci", ""),
                                                  Pair.of("CHARACTER SET ascii", "")));
        JdbcUtils.execute(dataSource, sql, ArrayUtils.EMPTY_OBJECT_ARRAY);
    }

    /**
     * Stops the embedded h2 database.
     *
     * @throws SQLException
     */
    public static void shutdownDB(DataSource dataSource) throws SQLException {
        if (dataSource != null) {
            JdbcUtils.execute(dataSource, "SHUTDOWN", ArrayUtils.EMPTY_OBJECT_ARRAY);
        }
    }

    /**
     * Creates datasource for this Embedded DB.
     *
     * @return
     */
    public static DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl("jdbc:h2:mem:test;SCHEMA=PUBLIC;MODE=MySQL");
        dataSource.setUsername("sa");
        dataSource.setPassword("password");
        return dataSource;
    }
}
