package com.modirum.ds.reporting.reports;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.RunWithDB;
import com.modirum.ds.reporting.services.ServiceLocator;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import com.modirum.ds.reporting.testutil.TestReportPropertiesUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import javax.sql.DataSource;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

@RunWithDB
public class TransactionReportTest {
    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        ServiceLocator.initInstance(dataSource);
        ServiceLocator.getInstance()
                      .getCsvFilteringService()
                      .initializeFromProperties(TestReportPropertiesUtil.reportProperties());

        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/mngr-texts.sql");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeScriptsBefore = {"transaction-reports/transaction-reports.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_tdsmessages;",
                                      "TRUNCATE TABLE ds_tdsrecords;",
                                      "TRUNCATE TABLE ds_tdsrecord_attributes;",
                                      "TRUNCATE TABLE ds_paymentsystems;",
                                      "TRUNCATE TABLE ds_acquirers;",
                                      "TRUNCATE TABLE ds_issuers;"})
    public void transactionReportWithOtherTimeZone() throws Exception {
        ReportParameters reportParameters = new ReportParameters();
        reportParameters.setReportingDataSource(dataSource);
        reportParameters.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
        reportParameters.setLocale(Locale.ENGLISH);
        Properties criteriaProperties = new Properties();
        criteriaProperties.setProperty(TransactionReport.START_DATE_KEY, "2020-07-23 15:00:00");
        criteriaProperties.setProperty(TransactionReport.END_DATE_KEY, "2020-07-23 16:00:00");
        criteriaProperties.setProperty(TransactionReport.PAYMENT_SYSTEM_ID_KEY, "1");
        reportParameters.setCriteriaProperties(criteriaProperties);
        StringWriter writer = new StringWriter();

        TransactionReport transactionReport = new TransactionReport();
        transactionReport.writeReport(writer, reportParameters);
        String expectedCsv = String.join(",", TransactionReport.HEADERS) + System.lineSeparator() +
                "Default PS,2020-07-23,15:44:25,470020,2.2.0,02,01,28,400000,0000,2404,216.58.197.110,John Smith,Oklahoma City,840,54 E Pear St,,,73102,OK,Oklahoma City,840,54 E Pear St,,,73102,OK,'+1123456789,02,10600,840,2,106.00 USD (840),2017-01-27 15:54:24,20170127,1234,05,Test Acquirer Name,444444,Modirum DS-3DS-Emulator,000001,246,Finland,5499,\"{\"\"shipIndicator\"\":\"\"07\"\"}\",,,DS-3DS-EMULATOR-REF-NUMBER,b0f61cfe-d04f-446a-b648-8431b395ca0b,https://dsmngr.local.com/ds-3ds-emulator/3dsServerRreqStub.jsp,03,DS-3DS-EMULATOR-OPER-ID,01,http://localhost,7979791,Modirum DS-3DS-Emulator,Test DS-3DS-EMULATOR issuer,400000,20b01757-0eab-5931-8000-000000072c04,EMVCo1234567,https://dsmngr.local.com/ds/DServer,,,,ACS-EMULATOR-REF-NO,14960eb4-b1f6-42c4-8b06-bd0877cfd064,http://localhost/ds-3ds-emulator/acsAreqStub.jsp,ACS-EMULATOR-OPER-ID,01,,,,,,true,01,,,,2020-07-23 15:44:25,2020-07-23 15:44:25,2020-07-23 15:44:29,2020-07-23 15:44:38,,,,,Y,Authentication Successful,,,Y,01,Completed,Yes,,,,,," +
                System.lineSeparator();
        Assertions.assertEquals(expectedCsv, writer.toString());
    }

    @Test
    @DataSet(executeScriptsBefore = {"transaction-reports/transaction-reports.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_tdsmessages;",
                                      "TRUNCATE TABLE ds_tdsrecords;",
                                      "TRUNCATE TABLE ds_tdsrecord_attributes;",
                                      "TRUNCATE TABLE ds_paymentsystems;",
                                      "TRUNCATE TABLE ds_acquirers;",
                                      "TRUNCATE TABLE ds_issuers;"})
    public void transactionReportDefaultTimezoneUTC() throws Exception {
        ReportParameters reportParameters = new ReportParameters();
        reportParameters.setReportingDataSource(dataSource);
        reportParameters.setTimeZone(TimeZone.getTimeZone("UTC"));
        reportParameters.setLocale(Locale.ENGLISH);
        Properties criteriaProperties = new Properties();
        criteriaProperties.setProperty(TransactionReport.START_DATE_KEY, "2020-07-23 07:00:00");
        criteriaProperties.setProperty(TransactionReport.END_DATE_KEY, "2020-07-23 08:00:00");
        criteriaProperties.setProperty(TransactionReport.PAYMENT_SYSTEM_ID_KEY, "1");
        reportParameters.setCriteriaProperties(criteriaProperties);
        StringWriter writer = new StringWriter();

        TransactionReport transactionReport = new TransactionReport();
        transactionReport.writeReport(writer, reportParameters);
        String expectedCsv = String.join(",", TransactionReport.HEADERS) + System.lineSeparator() +
                "Default PS,2020-07-23,07:44:25,470020,2.2.0,02,01,28,400000,0000,2404,216.58.197.110,John Smith,Oklahoma City,840,54 E Pear St,,,73102,OK,Oklahoma City,840,54 E Pear St,,,73102,OK,'+1123456789,02,10600,840,2,106.00 USD (840),2017-01-27 07:54:24,20170127,1234,05,Test Acquirer Name,444444,Modirum DS-3DS-Emulator,000001,246,Finland,5499,\"{\"\"shipIndicator\"\":\"\"07\"\"}\",,,DS-3DS-EMULATOR-REF-NUMBER,b0f61cfe-d04f-446a-b648-8431b395ca0b,https://dsmngr.local.com/ds-3ds-emulator/3dsServerRreqStub.jsp,03,DS-3DS-EMULATOR-OPER-ID,01,http://localhost,7979791,Modirum DS-3DS-Emulator,Test DS-3DS-EMULATOR issuer,400000,20b01757-0eab-5931-8000-000000072c04,EMVCo1234567,https://dsmngr.local.com/ds/DServer,,,,ACS-EMULATOR-REF-NO,14960eb4-b1f6-42c4-8b06-bd0877cfd064,http://localhost/ds-3ds-emulator/acsAreqStub.jsp,ACS-EMULATOR-OPER-ID,01,,,,,,true,01,,,,2020-07-23 07:44:25,2020-07-23 07:44:25,2020-07-23 07:44:29,2020-07-23 07:44:38,,,,,Y,Authentication Successful,,,Y,01,Completed,Yes,,,,,," +
                System.lineSeparator();
        Assertions.assertEquals(expectedCsv, writer.toString());
    }

    @Test
    @DataSet(executeScriptsBefore = {"transaction-reports/transaction-reports-with-error.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_tdsmessages;",
                                      "TRUNCATE TABLE ds_tdsrecords;",
                                      "TRUNCATE TABLE ds_tdsrecord_attributes;",
                                      "TRUNCATE TABLE ds_paymentsystems;"})
    public void transactionReportWithErroMessage() throws Exception {
        ReportParameters reportParameters = new ReportParameters();
        reportParameters.setReportingDataSource(dataSource);
        reportParameters.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
        reportParameters.setLocale(Locale.ENGLISH);
        StringWriter writer = new StringWriter();
        Properties criteriaProperties = new Properties();
        criteriaProperties.setProperty(TransactionReport.START_DATE_KEY, "2020-07-10 22:00:00");
        criteriaProperties.setProperty(TransactionReport.END_DATE_KEY, "2020-07-10 23:00:00");
        criteriaProperties.setProperty(TransactionReport.PAYMENT_SYSTEM_ID_KEY, "1");
        reportParameters.setCriteriaProperties(criteriaProperties);

        TransactionReport transactionReport = new TransactionReport();
        transactionReport.writeReport(writer, reportParameters);
        String expectedCsv = String.join(",", TransactionReport.HEADERS) + System.lineSeparator() +
                "Default PS,2020-07-10,22:33:53,464000,2.1.0,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2020-07-10 22:33:53,,,,,,,,,,,,,,ACS-EMULATOR-REF-NUMBER,2d0e3d66-1eaa-4c45-94df-526a6c3c36b0,https://dsmngr.local.com/ds-3ds-emulator/3dsServerRreqStub.jsp,,,,,,,,,875041cd-7aea-58c6-8000-000000071480,,,,,,,,,,,,,,,,,,,2020-07-10 22:33:53,,,,,,,,,,,,,,,,Error 3DS,No,,D,AReq,303,\"Access denied, invalid endpoint\",3DS Server 'DS-3DS-EMULATOR-REF-NUMBER' not approved" +
                System.lineSeparator();
        Assertions.assertEquals(expectedCsv, writer.toString());
    }

    @DisplayName("invalidFormatOfReportParameters [startDate, endDate, paymentSystemId, error]")
    @ParameterizedTest
    @CsvSource({
            "2020-07-23 15:00:00,  2020-07-23 16:00:00,  s,  Invalid number format for parameter [paymentSystemId]",
            "ssss-ss-ss 15:00:00,  2020-07-23 16:00:00,  1,  Invalid date format for parameter [startDate]. Please follow this format[yyyy-MM-dd HH:mm:ss]",
            "2020-07-23 15:00:00,  ssss-ss-ss 16:00:00,  1,  Invalid date format for parameter [endDate]. Please follow this format[yyyy-MM-dd HH:mm:ss]"
    })
    public void invalidFormatOfReportParameters(String startDate, String endDate, String paymentSystemId, String error) {
        ReportParameters reportParameters = new ReportParameters();
        reportParameters.setReportingDataSource(dataSource);
        reportParameters.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
        reportParameters.setLocale(Locale.ENGLISH);
        Properties criteriaProperties = new Properties();
        criteriaProperties.setProperty(TransactionReport.START_DATE_KEY, startDate);
        criteriaProperties.setProperty(TransactionReport.END_DATE_KEY, endDate);
        criteriaProperties.setProperty(TransactionReport.PAYMENT_SYSTEM_ID_KEY, paymentSystemId);
        reportParameters.setCriteriaProperties(criteriaProperties);
        StringWriter writer = new StringWriter();

        TransactionReport transactionReport = new TransactionReport();
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> transactionReport.writeReport(writer, reportParameters));
        Assertions.assertEquals(error, illegalArgumentException.getMessage());
    }

}
