package com.modirum.ds.reporting.services;

import com.modirum.ds.reporting.model.KeyData;
import com.modirum.ds.reporting.model.rowmapper.KeyDataResultSetMapper;
import com.modirum.ds.reporting.util.CryptoUtil;
import com.modirum.ds.reporting.util.JdbcUtils;
import com.modirum.ds.utils.Misc;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.Date;

public class KeyService extends com.modirum.ds.hsm.KeyServiceBase {

    // initialize safe default
    private static SecureRandom sr = new SecureRandom(CryptoUtil.SHA512(
            ("QQuu6343636ds9r4h" + new Date() + "s5NLHWM" + Runtime.getRuntime().totalMemory() + "Zd9Bsl" +
             System.currentTimeMillis() + " " + Math.random()).getBytes(StandardCharsets.UTF_8)));

    private final DataSource dataSource;

    public KeyService(DataSource dataSource) {
        this.dataSource = dataSource;
        getBouncyCastleProvider();

        // now initialize SHA1PRNG
        try {
            sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(CryptoUtil.SHA512(
                    ("sAAxdasPPQUds9r4h" + new Date() + "s5NNSGHWM" + Runtime.getRuntime().totalMemory() + "Zsld9Bsl" +
                     System.currentTimeMillis() + " " + Math.random()).getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static java.security.Provider getBouncyCastleProvider() {
        java.security.Provider bcpe = java.security.Security.getProvider(
                org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME);
        if (bcpe == null) {
            bcpe = new org.bouncycastle.jce.provider.BouncyCastleProvider();
            java.security.Security.addProvider(bcpe);
        }

        return bcpe;
    }

    public KeyData getKey(Long id) throws Exception {
        KeyData keyData = getKeyDataById(id);
        if (keyData == null || Misc.isNullOrEmpty(keyData.getKeyData())) {
            throw new RuntimeException("By Id " + id + " key not found");
        }
        return keyData;
    }

    /**
     * Fetch KeyData by id.
     *
     * @param keyDataId
     * @return
     */
    public KeyData getKeyDataById(Long keyDataId) throws SQLException {
        String query = "SELECT id,keyAlias,status,hsmdevice_id,wrapkey_id,keyData,keyCheckValue,keyDate,cryptoPeriodDays," +
                       " signedby1,signedby2,signature1,signature2" +
                       " FROM ds_keydata where id = ?";
        return JdbcUtils.findFirst(dataSource, query, new Object[]{keyDataId}, new KeyDataResultSetMapper());
    }
}
