package com.modirum.ds.reporting.util;

import com.modirum.ds.utils.Misc;

/**
 * Helper methods for String-related operations in DS.
 */
public final class DsStringUtils {
    private DsStringUtils() {
    }
    /**
     * This extracts substring starting from the end backwards with the size provided by @param lastCharactersSize.
     * @param lastCharactersSize
     * @return
     */
    public static String extractLast(int lastCharactersSize, String originalValue) {
        if (Misc.isNotNullOrEmpty(originalValue)) {
            int length = originalValue.length();
            if (length > lastCharactersSize) {
              return originalValue.substring(length - lastCharactersSize, length);
            } else {
                return originalValue;
            }
        }

        return originalValue;
    }

    /**
     * This extracts substring from the start with the size provided by @param firstCharactersSize.
     * @param firstCharactersSize
     * @return
     */
    public static String extractFirst(int firstCharactersSize, String originalValue) {
        if (Misc.isNotNullOrEmpty(originalValue)) {
            int length = originalValue.length();
            if (length > firstCharactersSize) {
                return originalValue.substring(0, firstCharactersSize);
            } else {
                return originalValue;
            }
        }

        return originalValue;
    }
}
