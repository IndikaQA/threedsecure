package com.modirum.ds.reporting.services;

import com.modirum.ds.reporting.model.AuditLog;
import com.modirum.ds.reporting.model.Id;
import com.modirum.ds.reporting.model.Persistable;
import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.util.JdbcUtils;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.Serializable;

/**
 * Jdbc Service for AuditLog entity
 */
public class AuditLogJdbcService {
    public static final String AUDIT_ID_SEQUENCE_NAME = "auditId";
    private static final Logger log = LoggerFactory.getLogger(AuditLogJdbcService.class);
    private DataSource dataSource;
    int detailMaxLen = 512;
    AuditLogJdbcService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource auditDataSource) {
        dataSource = auditDataSource;
    }
    /**
     * Generic API for audit logging any actions.
     * Allows higher level operations such as reporting to be logged as well.
     *
     * @param user
     * @param optionalEntity
     * @param action
     * @param detail
     */
    public void logAction(User user, Persistable optionalEntity, String action, String detail) {
        AuditLog auditLog = new AuditLog();
        auditLog.setById(user == null || user.getId() == null ? 0L : user.getId());
        auditLog.setWhen(new java.util.Date());
        auditLog.setBy(user != null ? user.getLoginName() : null);
        auditLog.setAction(action);
        if (optionalEntity != null) {
            auditLog.setObjectId(getId(optionalEntity));
            auditLog.setObjectClass(optionalEntity.getClass().getSimpleName());
        }
        if (auditLog.getObjectId() == null) {
            auditLog.setObjectId(Long.valueOf(0));
        }
        auditLog.setDetails(Misc.trunc(detail, detailMaxLen));

        try {
            IdService idService = ServiceLocator.getInstance().getIdService();
            Id id = idService.getNextId(AUDIT_ID_SEQUENCE_NAME);
            String sql = "INSERT INTO ds_auditlog (id,whendate,byuser,byuserId,action,objectId,objectClass,details) " +
                         "VALUES(?,?,?,?,?,?,?,?)";
            JdbcUtils.execute(dataSource, sql,
                              new Object[]{
                                      id.getSeed(),
                                      auditLog.getWhen(),
                                      auditLog.getBy(),
                                      auditLog.getById(),
                                      auditLog.getAction(),
                                      auditLog.getObjectId(),
                                      auditLog.getObjectClass(),
                                      auditLog.getDetails()
                              });
            log.debug("Successfully saved audit logs, time={}, action={}, user={}, objectClass={}, objectId={}, details={}",
                     auditLog.getWhen(), auditLog.getAction(), auditLog.getBy(), auditLog.getObjectClass(), auditLog.getObjectId(), auditLog.getDetails());
        } catch (Exception e) {
            log.error("AuditLog error ", e);
        }
    }

    private Long getId(Persistable o) {
        Serializable v1 = o != null ? o.getId() : null;
        if (v1 != null && v1 instanceof Number) {
            return Long.valueOf(((Number) v1).longValue());
        } else {
            return Long.valueOf(0);
        }
    }
}
