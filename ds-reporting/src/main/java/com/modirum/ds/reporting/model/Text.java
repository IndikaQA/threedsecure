package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Text implements java.io.Serializable, Persistable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String key;
    private String locale;
    private String message;
}
