package com.modirum.ds.reporting.services;

import com.modirum.ds.reporting.model.HsmDevice;
import com.modirum.ds.reporting.model.HsmDeviceConf;
import com.modirum.ds.reporting.model.rowmapper.HsmDeviceConfResultSetMapper;
import com.modirum.ds.reporting.model.rowmapper.HsmDeviceResultSetMapper;
import com.modirum.ds.reporting.util.JdbcUtils;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

/**
 * Jdbc Service for HsmDevice and HsmDeviceConf entities
 */
public class HsmDeviceReportService {
    private final DataSource dataSource;

    HsmDeviceReportService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Retrieves all active HsmDevice
     * @return
     * @throws SQLException
     */
    public List<HsmDevice> listActiveHsmDevices() throws SQLException {
        String query = "SELECT id, name, className, status FROM ds_hsmdevice WHERE status = 'A'";
        return JdbcUtils.query(dataSource, query, null, new HsmDeviceResultSetMapper());
    }

    /**
     * Get HsmDeviceConf by hsmDeviceId and dsId.
     * @param hsmDeviceId
     * @param dsId
     * @return
     * @throws SQLException
     */
    public HsmDeviceConf getHsmDeviceConfig(Long hsmDeviceId, int dsId) throws SQLException {
        String query = "SELECT hsmdevice_id, dsId, config FROM ds_hsmdevice_conf WHERE hsmdevice_id = ? and dsId = ?";
        return JdbcUtils.findFirst(dataSource, query, new Object[]{hsmDeviceId, dsId}, new HsmDeviceConfResultSetMapper());
    }
}
