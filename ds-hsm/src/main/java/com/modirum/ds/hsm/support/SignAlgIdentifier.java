package com.modirum.ds.hsm.support;

/**
 * Thales HSM specific signature mode identifiers.
 */
public enum SignAlgIdentifier {

    SHA1_RSA("SHA1withRSA", "01", "01"),
    SHA256_RSA("SHA256withRSA", "06", "01"),
    SHA512_RSA("SHA512withRSA", "08", "01"),
    SHA256_RSA_MGF1("SHA256withRSAandMGF1", "06", "04");

    private String signatureAlgorithm;
    private String hashIdentifier;
    private String padModeIdentifier;

    SignAlgIdentifier(String signatureAlgorithm, String hashIdentifier, String padModeIdentifier) {
        this.signatureAlgorithm = signatureAlgorithm;
        this.hashIdentifier = hashIdentifier;
        this.padModeIdentifier = padModeIdentifier;
    }

    public String getHashIdentifier() {
        return hashIdentifier;
    }

    public String getPadModeIdentifier() {
        return padModeIdentifier;
    }

    public static SignAlgIdentifier getBySignatureAlgorithm(String signatureAlgorithm) {
        for(SignAlgIdentifier identifier : SignAlgIdentifier.values()) {
            if (identifier.signatureAlgorithm.equals(signatureAlgorithm)) {
                return identifier;
            }
        }
        return null;
    }
}
