package com.modirum.ds.hsm;

import com.modirum.ds.hsm.support.HSMDataKey;
import com.modirum.ds.hsm.support.HSMUtil;
import com.modirum.ds.hsm.support.HsmException;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.HexUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


/**
 * Software encryption service. Implements DS encryption functionality using standard JCA/JCE + BouncyCastle library.
 * Does not depend on external physical devices. Keys produced using this engine are clear-text.
 */
public class SoftwareJCEService implements HSMDevice {

    private static final Logger LOG = LoggerFactory.getLogger(SoftwareJCEService.class);

    public static final String BC = "BC";
    public static final String SUN_JCE = "SunJCE";

    public static final String RSA = "RSA";
    public static final String AES = "AES";
    public static final String AES_ECB_NOPADDING = "AES/ECB/NoPadding";
    public static final String AES_ECB_PKCS5_PADDING = "AES/ECB/PKCS5Padding";
    public static final String RSA_NONE_PKCS1_PADDING = "RSA/None/PKCS1Padding";
    public static final String RSA_NONE_OAEP_SHA1_MGF1= "RSA/None/OAEPWithSHA1AndMGF1Padding";
    public static final String RSA_NONE_OAEP_SHA256_MGF1 = "RSA/None/OAEPWithSHA256AndMGF1Padding";

    public static final byte[] ZERO_BYTE_16_BLOCK = new byte[16];

    public SoftwareJCEService() {
        // explicitly add bouncy castle JCA implementation
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Override
    public void initializeHSM(String config) {
        // not customizable configuration yet.
    }

    /**
     * Generates software AES-256 bit random key.
     * @return AES key 256 bit length.
     * @throws Exception in case of any key generation issues.
     */
    @Override
    public Key generateHSMDataEncryptionKey() throws Exception {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance(AES);
            kgen.init(256);
            SecretKey aesKey = kgen.generateKey();
            byte[] keyBytes = aesKey.getEncoded();

            Cipher cipher = Cipher.getInstance(AES_ECB_NOPADDING, SUN_JCE);
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] kcv = cipher.doFinal(ZERO_BYTE_16_BLOCK);

            return new HSMDataKey(keyBytes, HSMUtil.head(3, kcv));
        } catch (Exception e) {
            LOG.error("Error generating software engine AES key.");
            throw e;
        }
    }

    /**
     * Encrypt data using AES cipher in ECB block chaining mode with PKCS5 padding.
     *
     * @param aesKeyBytes AES key bytes.
     * @param dataToEncrypt data to encrypt
     * @return Encrypted data
     * @throws Exception in case of any encryption  errors.
     */
    @Override
    public byte[] encryptData(byte[] aesKeyBytes, byte[] dataToEncrypt) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance(AES_ECB_PKCS5_PADDING, SUN_JCE);
            SecretKeySpec secretKey = new SecretKeySpec(aesKeyBytes, AES);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(dataToEncrypt);
        } catch (Exception e) {
            LOG.error("Error encrypting data using software AES key.");
            throw e;
        }
    }

    /**
     * Decrypt data using AES cipher in ECB block chaining mode with PKCS5 padding.
     *
     * @param aesKeyBytes AES key bytes.
     * @param dataToDecrypt encrypted data
     * @return Decrypted data.
     * @throws Exception in case of any decryption errors.
     */
    @Override
    public byte[] decryptData(byte[] aesKeyBytes, byte[] dataToDecrypt) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance(AES_ECB_PKCS5_PADDING, SUN_JCE);
            SecretKeySpec secretKey = new SecretKeySpec(aesKeyBytes, AES);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return cipher.doFinal(dataToDecrypt);
        } catch (Exception e) {
            LOG.error("Error decrypting data using software AES key.");
            throw e;
        }
    }

    /**
     * Always returns 0-stub. Master key is not used by software engine.
     * @return "000000" value.
     */
    @Override
    public String getMasterKeyCheckValue() {
        // Software engine does not use master keys. Returning a 0-stub to support the interface.
        return "000000";
    }

    @Override
    public String getHardwareSerial() throws Exception {
        // No hardware for software engine. Returning null is ok. It's only used for ACOS5 service.
        return null;
    }

    @Override
    public byte[] encryptDataInHSMWithMaster(byte[] data, boolean encrypt) throws Exception, UnsupportedOperationException {
        // no master key means no encryption with it.
        throw new UnsupportedOperationException("Software JCE does not support encrypting with master.");
    }

    @Override
    public KeyPair generateRSAdecryptAndSign(int keyLength)
        throws HsmException {
        return generateGeneralPurposeRSA(keyLength);
    }

    private KeyPair generateGeneralPurposeRSA(int keysize) throws HsmException {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance(RSA, BC);
            keyGen.initialize(keysize);
            return keyGen.genKeyPair();
        } catch (Exception e) {
            throw new HsmException("Error generating software RSA keypair.", e);
        }
    }

    @Override
    public byte[] rsaPrivateDecrypt(byte[] rsaPrivateKey, byte[] encryptedData, RSADecryptPadMode padMode)
        throws Exception {
        try {
            String rsaCipherTransformation;
            switch (padMode) {
                case PKCS1:
                    rsaCipherTransformation = RSA_NONE_PKCS1_PADDING;
                    break;
                case OAEP_SHA1_MGF1:
                    rsaCipherTransformation = RSA_NONE_OAEP_SHA1_MGF1;
                    break;
                case OAEP_SHA256_MGF1:
                    rsaCipherTransformation = RSA_NONE_OAEP_SHA256_MGF1;
                    break;
                default:
                    throw new HsmException("Unknown padding mode for RSA decrypt: " + padMode.name());
            }

            PrivateKey privateKey = toRSAprivateKey((rsaPrivateKey));

            Cipher cipher = Cipher.getInstance(rsaCipherTransformation, BC);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return cipher.doFinal(encryptedData);
        } catch (Exception e) {
            LOG.error("Unable to perform software RSA decryption.");
            throw e;
        }
    }

    @Override
    public byte[] ecPrivateDecrypt(byte ecPrivateKey[], byte encryptedData[], String cipherTransformation)
            throws Exception {

        KeyFactory keyFactory = KeyFactory.getInstance("ECDSA");
        PrivateKey privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(ecPrivateKey));

        Cipher cipher = Cipher.getInstance(cipherTransformation);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(encryptedData);
    }

    @Override
    public ContentSigner getContentSigner(final byte[] rsaPrivateKeyBytes, final String signatureAlgorithm) throws Exception {
        PrivateKey signerPrivateKey = toRSAprivateKey(rsaPrivateKeyBytes);
        return new JcaContentSignerBuilder(signatureAlgorithm).setProvider(BouncyCastleProvider.PROVIDER_NAME)
                .build(signerPrivateKey);
    }

    @Override
    public PrivateKey toRSAprivateKey(byte[] privateKeyBytes) throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance(RSA, BC);
        return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
    }

    public static void main(String args[]) {
        try {
            SoftwareJCEService softwareJCEService = new SoftwareJCEService();
            softwareJCEService.initializeHSM(null);

            Key aesKey = softwareJCEService.generateHSMDataEncryptionKey();
            System.out.println("AES key hex: " + HexUtils.toString(aesKey.getValue()));
            System.out.println("AES key KCV: " + HexUtils.toString(aesKey.getCheckValue()));

            String clearTextData = "clear text 19 bytes";
            byte[] encryptedData = softwareJCEService.encryptData(aesKey.getValue(), clearTextData.getBytes());
            System.out.println("Original data length: " + clearTextData.length() + "; crypted data length: " + encryptedData.length);

            byte[] decryptedData = softwareJCEService.decryptData(aesKey.getValue(), encryptedData);
            System.out.println("Decrypted data equal to original data: " + clearTextData.equals(new String(decryptedData)));

            KeyPair rsaKeyResult = softwareJCEService.generateRSAdecryptAndSign(2048);
            System.out.println("Private key: " + Base64.encode(rsaKeyResult.getPrivate().getEncoded()));
            System.out.println("Public key: " + Base64.encode(rsaKeyResult.getPublic().getEncoded()));

            KeyFactory keyFactory = KeyFactory.getInstance(RSA, BC);
            PublicKey rsaKeyResultPublic = keyFactory.generatePublic(new X509EncodedKeySpec(rsaKeyResult.getPublic().getEncoded()));
            Cipher rsaEncryptCipher = Cipher.getInstance(RSA_NONE_OAEP_SHA256_MGF1); // BC is used.
            rsaEncryptCipher.init(Cipher.ENCRYPT_MODE, rsaKeyResultPublic);
            byte[] rsaEncryptedData = rsaEncryptCipher.doFinal(aesKey.getValue());

            byte[] rsaDecryptedData = softwareJCEService.rsaPrivateDecrypt(rsaKeyResult.getPrivate().getEncoded(), rsaEncryptedData, RSADecryptPadMode.OAEP_SHA256_MGF1);
            System.out.println("Decrypted RSA data: " + HexUtils.toString(rsaDecryptedData));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
