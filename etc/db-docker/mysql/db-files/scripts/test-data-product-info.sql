INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy)
VALUES ('product.extensions', 'fss, tdsmrelay', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licensee', 'Azur license', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licenseexp', '2023-01-31', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licenseissued', '2021-01-29', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licensekey', 'Q4gCu6ODod+jCjpRDlT96PFdkKIVE8zbhw24llocbTQ3906SdvtwT0ELgxKXjKOpE2xb9fuSClPG\r\nAb4kXrUPAqo27pmOMNeHuNB/tIx/5uWvgG3Fxr0INA8Yedyb5JO3QJ4BvkuV91uhKi3eopecsSNX\r\ntR5M0bkRwZF1cV5ZYYlmp5DV//Aaq0qSPEMK0FKPWRqdIcyMyXoAAfP+BFmrMBI7AblbkanLO0bw\r\nVDqbaOpOJKeoC1Ov64r205qid2g3RIy4I7+kmOq4r3SpUSinWvhBXmzSc/I6nqu7utH7V0uEZywr\r\nUbhLyLNlC3Kic3CIDk2afuS0G+KNlw+MoXbO5A==', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.maxmerchants', 'Unlimited', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.name', 'Modirum DS', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.version', '1.0', NULL, '2020-07-24 00:31:55.000000', 'admin');