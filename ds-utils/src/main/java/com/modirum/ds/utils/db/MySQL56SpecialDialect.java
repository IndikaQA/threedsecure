/*
 * Copyright (C) 2012 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 21.05.2012
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils.db;

import java.sql.Types;

public class MySQL56SpecialDialect extends org.hibernate.dialect.MySQL57Dialect {

    String tableTypeString = null;

    public MySQL56SpecialDialect() {
        registerColumnType(Types.TIMESTAMP, 0, "datetime");
        registerColumnType(Types.TIMESTAMP, 6, "datetime($p)");
    }


}
