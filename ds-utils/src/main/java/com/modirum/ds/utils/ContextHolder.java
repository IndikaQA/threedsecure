/*
 * Copyright (C) 2012 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 21.06.2012
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.util.Enumeration;

public interface ContextHolder {


    public String getApplicationParameter(String key);

    public java.util.Properties getParams();

    public void setParams(java.util.Properties params);

    public Object getSessionAttribute(String key);

    public Object removeSessionAttribute(String key);

    public void setSessionAttribute(String key, Object value);

    public Object getRequestAttribute(String key);

    public Object removeRequestAttribute(String key);

    public void setRequestAttribute(String key, Object value);

    @SuppressWarnings("rawtypes")
    public Enumeration getApplicationAttributeNames();

    public Object getApplicationAttribute(String key);

    public Object removeApplicationAttribute(String key);

    public void setApplicationAttribute(String key, Object value);
}
