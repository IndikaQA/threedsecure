/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Properties;

public class Utils {
    static boolean test = false;

    public static void main(String[] arg) {
        test = true;
        configureLog4j("/media/andri/home/andri/modirum/GIT/MPI/mdpaympi/webapp/WEB-INF/classes/log4j2.xml");
        configureLog4j(System.getProperties());
        System.out.println("Origin of String: " + Utils.classOrigin(new String("test")));
        System.out.println("Origin of org.slf4j.LoggerFactory: " + Utils.classOrigin(org.slf4j.LoggerFactory.getILoggerFactory()));
    }

    public static void configureLog4j(String cfgFile) {

        try {
            Object pcf = Class.forName("org.apache.log4j.PropertyConfigurator").newInstance();
            Method[] allMethods = pcf.getClass().getDeclaredMethods();
            for (Method m : allMethods) {
                if (Modifier.isStatic(m.getModifiers()) && Modifier.isPublic(m.getModifiers()) && "configure".equals(m.getName())
                        && m.getParameterTypes() != null && m.getParameterTypes().length == 1
                        && m.getParameterTypes()[0].getCanonicalName().equals(String.class.getCanonicalName())) {
                    m.invoke(pcf, cfgFile);
                }
            }
            if (test) {
                System.out.println("log4j configuration ok");
            }

        } catch (Throwable t) {
            System.out.println("log4j configuration failed " + t + ", trying with log4j2");

            try {

                Class lcc2 = Class.forName("org.apache.logging.log4j.core.config.ConfigurationSource");
                Constructor cc = lcc2.getConstructor(java.io.InputStream.class);
                Object config = cc.newInstance(new java.io.FileInputStream(cfgFile));

                Class lcc2c = Class.forName("org.apache.logging.log4j.core.config.Configurator");
                Method m = lcc2c.getMethod("initialize", ClassLoader.class, lcc2);
                m.invoke(null, Object.class.getClassLoader(), config);
                if (test) {
                    System.out.println("log4j2 configuration ok");
                }
            } catch (Throwable t2) {
                System.out.println("log4j2 configuration failed " + t2);
                //t2.printStackTrace();
            }

        }

    }

    public static void configureLog4j(Properties props) {

        try {
            Object pcf = Class.forName("org.apache.log4j.PropertyConfigurator").newInstance();

            Method[] allMethods = pcf.getClass().getDeclaredMethods();
            for (Method m : allMethods) {
                if (Modifier.isStatic(m.getModifiers()) && Modifier.isPublic(m.getModifiers()) && "configure".equals(m.getName())
                        && m.getParameterTypes() != null && m.getParameterTypes().length == 1
                        && m.getParameterTypes()[0].getCanonicalName().equals(Properties.class.getCanonicalName())) {
                    m.invoke(pcf, props);
                }
            }
            if (test) {
                System.out.println("log4j props configuration ok");
            }

        } catch (Throwable t) {

            System.out.println("log4j property configuration failed " + t + ", trying with log4j2");

            try {
                java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
                props.store(baos, null);
                Class sourceClass = Class.forName("org.apache.logging.log4j.core.config.ConfigurationSource");
                Object source = sourceClass.getConstructor(java.io.InputStream.class).newInstance(new java.io.ByteArrayInputStream(baos.toByteArray()));
                Object f = Class.forName("org.apache.logging.log4j.core.config.properties.PropertiesConfigurationFactory").newInstance();

                Method getCtx = Class.forName("org.apache.logging.log4j.LogManager").getMethod("getContext", boolean.class);
                Object context = getCtx.invoke(null, false);
                Object configuration = f.getClass().getMethod("getConfiguration", context.getClass(), source.getClass()).invoke(f, context, source);
                //org.apache.logging.log4j.core.config.properties.PropertiesConfiguration configuration = f.getConfiguration(context, source);
                //configuration.start();
                configuration.getClass().getMethod("start", null).invoke(configuration, null);
                //context.updateLoggers(configuration);
                Class ccc = Class.forName("org.apache.logging.log4j.core.config.Configuration");
                context.getClass().getMethod("updateLoggers", ccc).invoke(context, configuration);

                if (test) {
                    System.out.println("log4j2 props configuration ok");
                }

            } catch (Throwable t2) {
                System.out.println("log4j2 property configuration failed " + t2);
            }

        }
    }

    public static String encode(String s) {
        try {
            return encode(s, "ISO-8859-1");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String encode(String str, String encoding) throws Exception {
        return URLEncoder.encode(str, encoding);
    }

    public static byte[] readFile(String fn) throws Exception {
        return readFile(new java.io.File(fn));
    }

    public static byte[] readFile(java.io.File f) throws Exception {
        java.io.FileInputStream fi = null;

        int bz = 4096;
        if (f.isFile() && f.length() > 10000000) {
            bz = 256 * 1024;
        }
        if (f.isFile() && f.length() > 1000000) {
            bz = 64 * 1024;
        } else if (f.isFile() && f.length() > 100000) {
            bz = 8 * 1024;
        }
        byte[] buf = new byte[bz];

        java.io.ByteArrayOutputStream bo = new java.io.ByteArrayOutputStream((int) f.length());

        try {
            fi = new java.io.FileInputStream(f);
            do {
                int len = fi.read(buf);
                if (len > 0) {
                    bo.write(buf, 0, len);
                } else {
                    break;
                }


            } while (true);

        } catch (Exception e) {
            throw e;
        } finally {
            if (fi != null) {
                try {
                    fi.close();
                } catch (Exception dc) {
                }
            }
        }

        return bo.toByteArray();
    }

    public static void saveFile(String fn, byte[] data) throws Exception {
        java.io.FileOutputStream fo = null;
        try {
            fo = new java.io.FileOutputStream(fn);
            fo.write(data);
            fo.flush();
        } catch (Exception e) {
            throw e;
        } finally {
            if (fo != null) {
                try {
                    fo.close();
                } catch (Exception dc) {
                }
            }
        }

    }

    public static String getLocalNonLoIp() {
        Enumeration<NetworkInterface> net = null;
        try {
            net = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        String ipAddr = null;
        while (net.hasMoreElements()) {
            NetworkInterface element = net.nextElement();
            try {
                if (element.isLoopback()) {
                    continue;
                }
            } catch (SocketException e) {
                throw new RuntimeException(e);
            }

            Enumeration<InetAddress> addresses = element.getInetAddresses();

            while (addresses.hasMoreElements()) {
                InetAddress ip = addresses.nextElement();
                if (!ip.isLoopbackAddress()) {

                    ipAddr = ip.getHostAddress();
                    if (ip instanceof Inet4Address) {
                        return ipAddr;
                    }

                }
            }
        }
        return ipAddr;
    }

    public static String classOrigin(Object o) {
        if (o == null) {
            return null;
        }

        Class<?> c = o.getClass();
        ClassLoader loader = c.getClassLoader();
        if (loader == null) {
            loader = ClassLoader.getSystemClassLoader();
            while (loader != null && loader.getParent() != null) {
                loader = loader.getParent();
            }
        }
        if (loader != null) {
            String name = c.getCanonicalName();
            java.net.URL resource = loader.getResource(name.replace(".", "/") + ".class");
            if (resource != null) {
                return resource.toString();
            }
        }
        return "Unknown";
    }
}
