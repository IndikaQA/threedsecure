/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author andri
 */
public class Context implements Serializable {

    private static final long serialVersionUID = 1L;

    protected transient static Logger log = LoggerFactory.getLogger(Context.class);

    protected ContextHolder ctxHolder;
    protected transient SettingService settingService;
    static String hostId;

    public static class DefaultContextHolder implements ContextHolder, Serializable {
        protected java.util.Properties params = new java.util.Properties();
        protected java.util.Hashtable<String, Object> appScope = new java.util.Hashtable<String, Object>();
        protected java.util.Hashtable<String, Object> sessionScope = new java.util.Hashtable<String, Object>();
        protected java.util.Hashtable<String, Object> requestScope = new java.util.Hashtable<String, Object>();

        public Object getApplicationAttribute(String key) {
            return appScope.get(key);
        }

        @SuppressWarnings("rawtypes")
        public Enumeration getApplicationAttributeNames() {
            return appScope.keys();
        }

        public String getApplicationParameter(String key) {
            return params.getProperty(key);
        }

        public Object getRequestAttribute(String key) {
            return requestScope.get(key);
        }

        public Object getSessionAttribute(String key) {
            return sessionScope.get(key);
        }

        public Object removeApplicationAttribute(String key) {
            return appScope.remove(key);
        }

        public Object removeRequestAttribute(String key) {
            return requestScope.remove(key);
        }

        public Object removeSessionAttribute(String key) {
            return sessionScope.remove(key);
        }

        public void setApplicationAttribute(String key, Object value) {
            appScope.put(key, value);

        }

        public void setRequestAttribute(String key, Object value) {
            requestScope.put(key, value);
        }

        public void setSessionAttribute(String key, Object value) {
            sessionScope.put(key, value);
        }

        public java.util.Properties getParams() {
            return params;
        }

        public void setParams(java.util.Properties params) {
            this.params = params;
        }

    }

    /**
     * Class consructor
     */
    public Context() {
        this.ctxHolder = new DefaultContextHolder();
    }

    public Context(ContextHolder ctxHolder) {
        this.ctxHolder = ctxHolder;
    }


    /**
     * @param l
     * @return locale
     */

    public java.util.Locale getBestLocaleForLang(String l) {

        // if (log.isDebugEnabled()) log.debug("getBestLocaleForLang: "+l);
        if (l == null || l.length() > 5) {
            return null;
        }
        java.util.Locale retl = null;

        try {
            java.util.Locale[] llst = java.util.Locale.getAvailableLocales();
            //List slocaleList = null; //this.getLocaleList();
            for (int i = 0; llst != null && i < llst.length; i++) {
                if (llst[i] != null && l.equals(llst[i].toString())) {
                    // set exact localelocale to maching in supported locales
                    if (log.isDebugEnabled()) {
                        log.debug("found matching locale: " + llst[i]);
                    }
                    retl = llst[i];
                    break;
                } else if (llst[i] != null && l.equals(llst[i].getLanguage() + "_" + llst[i].getCountry())) {
                    // if only lang matches set it but continue so it might happen better match
                    if (log.isDebugEnabled()) {
                        log.debug("found lang+country matching locale: " + llst[i]);
                    }
                    retl = llst[i];
                    break;
                } else if (llst[i] != null && l.equals(llst[i].getLanguage())) {
                    // if only lang matches set it but continue so it might happen better match
                    if (log.isDebugEnabled()) {
                        log.debug("found lang matching locale: " + llst[i]);
                    }
                    retl = llst[i];
                }

            } // for

            // default (not safe)
            // setLocale(new java.util.Locale(l, java.util.Locale.getDefault().getCountry()) );

        } catch (Exception ee) {
            if (log.isDebugEnabled()) {
                log.debug("Locale ex: " + ee, ee);
            }
        }
        if (retl == null) {
            log.warn("getBestLocaleForLang did not find locale for " + l);
        }

        return retl;
    }

    public void setLang(String l) {

        if (log.isDebugEnabled()) {
            log.debug("setLang: " + l);
        }
        if (l == null || l.length() > 5) {
            return;
        }

        java.util.Locale ml = getBestLocaleForLang(l);
        if (ml != null) {
            this.setLocale(ml);
        }

    }

    static final String locakeKey = "ctx.locale";

    public void setLocale(java.util.Locale l) {

        ctxHolder.setSessionAttribute(locakeKey, l);
        ctxHolder.setRequestAttribute(locakeKey, l);
        if (log.isDebugEnabled()) {
            log.debug("setLocale session: " + l);
        }
        // remove some cached items
        // clear formats after locale change
        ctxHolder.removeSessionAttribute("priceformat");
        // FULL, LONG, MEDIUM, and SHORT
        ctxHolder.removeSessionAttribute("dateformat." + java.text.DateFormat.FULL);
        ctxHolder.removeSessionAttribute("dateformat." + java.text.DateFormat.LONG);
        ctxHolder.removeSessionAttribute("dateformat." + java.text.DateFormat.MEDIUM);
        ctxHolder.removeSessionAttribute("dateformat." + java.text.DateFormat.SHORT);
    }

    /**
     * @return current locale
     */
    public java.util.Locale getLocale() {

        java.util.Locale l = (java.util.Locale) ctxHolder.getRequestAttribute(locakeKey);
        if (l == null) {
            l = (java.util.Locale) ctxHolder.getSessionAttribute(locakeKey);
        }

        // might be we have it inrequest after session invalidation ????
        if (l == null) {
            Object tmp = ctxHolder.getRequestAttribute("locale.last");
            if (tmp != null && tmp instanceof java.util.Locale) {
                l = (java.util.Locale) tmp;
                if (log.isDebugEnabled()) {
                    log.debug("Setting locale to locale.last " + tmp);
                }
                setLocale(l);

                tmp = null;
            }
        }

        java.util.Locale rl = (l != null ? l : getDefaultLocale());
        // log.debug("getLocale ret:"+rl);
        return rl;
    }

    /**
     * @return locale
     */
    public java.util.Locale getDefaultLocale() {

        java.util.Locale defaultLocale = (java.util.Locale) ctxHolder.getSessionAttribute("dafaultlocale");

        if (defaultLocale != null) {
            return defaultLocale;
        }

        java.util.Locale[] llst = java.util.Locale.getAvailableLocales();
        String defLoc = null;
        try {
            defLoc = getStringSetting("defaultlocale");

        } catch (Exception e) {
            log.error("getStringSetting err", e);
        }
        if (defLoc != null && defLoc.length() == 5) {
            String lng = defLoc.substring(0, 2);
            String cntry = defLoc.substring(3);

            for (int i = 0; llst != null && i < llst.length; i++) {
                if (llst[i] != null && lng.equals(llst[i].getLanguage())) {
                    if (cntry.equals(llst[i].getCountry())) {
                        defaultLocale = llst[i];
                        break;
                    } else {
                        defaultLocale = llst[i];
                    }
                }
            } // for
        } // if

        if (defaultLocale == null) {
            defaultLocale = java.util.Locale.US;
        }

        ctxHolder.setSessionAttribute("dafaultlocale", defaultLocale);

        return defaultLocale;
    }

    public List<java.util.Locale> getSupportedLocaleList() throws Exception {
        List<java.util.Locale> loList = (List<java.util.Locale>) this.getApplicationAttribute("loList");
        if (loList != null) {
            return loList;
        }

        loList = new java.util.ArrayList<java.util.Locale>(10);
        String loListStr = this.getStringSetting("supportedLocales");
        if (Misc.isNotNullOrEmpty(loListStr)) {
            String[] loListAr = Misc.split(loListStr, ',');
            for (String lx : loListAr) {
                java.util.Locale lxx = this.getBestLocaleForLang(lx);
                if (lxx != null) {
                    loList.add(lxx);
                }
            }
        } else {
            loList.add(this.getDefaultLocale());
        }

        this.setApplicationAttribute("loList", loList);
        return loList;
    }

    /**
     * get the setting object
     *
     * @param key
     * @return sb
     * @throws Exception
     */
    public Setting getSetting(String key) {

        return getSetting(key, null);
    }

    public Setting getSetting(String key, Short processorId) {
        if (key == null) {
            return null; // "";
        }

        //String settingValue=servletContext.getInitParameter(key);
        Setting setting = null;
        java.util.Map<String, Setting> settings = null;
        if (ctxHolder != null) {
            settings = (java.util.Map<String, Setting>) ctxHolder.getApplicationAttribute("settings");
            if (settings == null) {
                settings = new java.util.TreeMap<String, Setting>();
                ctxHolder.setApplicationAttribute("settings", settings);
            }

            setting = settings.get(key + "." + processorId);

            if (setting == null) {
                setting = settings.get(key);
            }

            if (setting != null) {
                return setting;
            }
        }

        if (setting == null && settingService != null) {
            try {
                setting = settingService.getSettingByKey(key, processorId);
            } catch (Exception e) {
                log.error("getSetting error", e);
                throw new RuntimeException(e);
            }
        }
        if (setting == null) {
            java.util.Properties properties = (java.util.Properties) ctxHolder.getApplicationAttribute("properties");
            if (properties != null) {
                String v = properties.getProperty(key + "[" + processorId + "]");
                if (v == null) {
                    v = properties.getProperty(key);
                }
                if (v != null) {
                    setting = new Setting();
                    setting.setKey(key);
                    setting.setProcessorId(processorId);
                    setting.setValue(v);
                }
            }

            if (setting == null) {
                log.warn("getSetting:" + key + " not found.");
                setting = new Setting(); // cache also not found setting
                setting.setKey(key);
                setting.setProcessorId(processorId);
            }
        }

        if (settings != null && setting != null) {
            synchronized (settings) {
                settings.put(key + (setting.getProcessorId() != null ? "." + processorId : ""), setting);
            }
        }

        return setting;
    }

    /**
     * Method getStringSetting
     *
     * @param key
     * @return setting
     */
    public String getStringSetting(String key) {
        Setting s = getSetting(key);

        return s != null ? s.getValue() : null;
    }

    /**
     * Method getLocaleStringSetting
     *
     * @param key
     * @return setting
     */
    public String getLocaleStringSetting(String key) {
        Setting sb = getSetting(key + "." + getLocale());

        String lkey = key + "." + getLocale().getLanguage();
        if (sb == null) {
            sb = getSetting(lkey);
        }

        if (sb == null) {
            log.warn("getLocalizedSetting: " + lkey + " not found.");
            lkey = key + "." + getDefaultLocale().getLanguage();
            sb = getSetting(lkey);
        }

        if (sb == null) {
            log.warn("getLocalizedSetting: " + lkey + " not found.");
        }

        return sb != null ? sb.getValue() : null;
    }

    /**
     * Method getIntSetting
     *
     * @param key
     * @return setting
     */
    public int getIntSetting(String key) throws RuntimeException {
        Setting sb = getSetting(key);
        return (sb != null && Misc.isInt(sb.getValue()) ? Misc.parseInt(sb.getValue()) : 0);
    }

    /**
     * @param style
     * @param dateorandtime
     * @return formatter
     */
    public java.text.DateFormat getDateFormat(int style, int dateorandtime) {
        java.util.Locale loc = getLocale();
        if (style != java.text.DateFormat.FULL && style != java.text.DateFormat.LONG
                && style != java.text.DateFormat.MEDIUM && style != java.text.DateFormat.SHORT) {
            style = java.text.DateFormat.MEDIUM;
        }

        String key = "dateformat." + loc + "." + style + "." + dateorandtime;

        java.text.DateFormat df = (java.text.DateFormat) ctxHolder.getSessionAttribute(key);

        if (df == null) {
            String dfSetting = null;
            try {
                dfSetting = this.getLocaleStringSetting("dateformat");
            } catch (Exception e) {
                log.error("getLocaleStringSetting err", e);
            }


            if (Misc.isNotNullOrEmpty(dfSetting) && style == java.text.DateFormat.SHORT) {
                df = new java.text.SimpleDateFormat(dfSetting);
            } else if (dateorandtime == 1) {
                df = java.text.DateFormat.getDateInstance(style, loc);
            } else if (dateorandtime == 2) {
                df = java.text.DateFormat.getTimeInstance(style, loc);
            } else {
                df = java.text.DateFormat.getDateTimeInstance(style, style, loc);
            }
            if (style == java.text.DateFormat.SHORT) {
                try {
                    java.text.SimpleDateFormat sdf = (java.text.SimpleDateFormat) df;
                    // java.text.DateFormatSymbols sdfs=sdf.getDateFormatSymbols();
                    String pattern = sdf.toPattern();
                    String patternOld = pattern;
                    if (pattern != null && pattern.indexOf("yyyy") < 0 && pattern.indexOf("yy") >= 0) {
                        int start = pattern.indexOf("yy");
                        pattern = pattern.substring(0, start + 2) + "yy" + pattern.substring(start + 2);

                    }

                    if (pattern != null && !pattern.equals(patternOld)) {
                        sdf.applyPattern(pattern);
                    }

                } catch (Exception dc) {
                }

            }

            ctxHolder.setSessionAttribute(key, df);
        }

        return df;
    }

    /**
     * @param frdigits
     * @return nf
     */
    public java.text.NumberFormat getNumberFormat(int frdigits) {
        java.text.NumberFormat pf = null;

        String key = "numberformat." + getLocale() + "." + frdigits;
        pf = (java.text.NumberFormat) ctxHolder.getSessionAttribute(key);

        if (pf == null) {
            pf = java.text.NumberFormat.getInstance(getLocale());
            pf.setGroupingUsed(false);
            if (pf instanceof java.text.DecimalFormat) {
                ((java.text.DecimalFormat) pf).setMaximumFractionDigits(frdigits);
                ((java.text.DecimalFormat) pf).setMinimumFractionDigits(frdigits);

            } // if pf

            ctxHolder.setSessionAttribute(key, pf);
        }

        return pf;
    }

    /**
     * @param o
     * @param fdigits
     * @return formatted
     */
    public String format(Number o, int fdigits) {
        if (o == null) {
            return "";
        }
        return getNumberFormat(fdigits).format(o);
    }

    /**
     * @param o
     * @return string
     */
    public String formatDateOnly(java.util.Date o) {
        if (o == null) {
            return null;
        }
        return getDateFormat(java.text.DateFormat.SHORT, 1).format(o);
    }

    /**
     * @param key
     * @param attr
     */
    public void setSessionAttribute(String key, Object attr) {
        if (ctxHolder != null) {
            ctxHolder.setSessionAttribute(key, attr);
        }
    }

    /**
     * @param key
     * @return object
     */
    public Object getSessionAttribute(String key) {
        if (ctxHolder != null) {
            return ctxHolder.getSessionAttribute(key);
        }

        return null;
    }

    public void setApplicationAttribute(String key, Object attr) {
        if (ctxHolder != null) {
            ctxHolder.setApplicationAttribute(key, attr);
        }
    }

    /**
     * @param key
     * @return object
     */
    public Object getApplicationAttribute(String key) {
        if (ctxHolder != null) {
            return ctxHolder.getApplicationAttribute(key);
        }

        return null;
    }


    public void setRequestAttribute(String key, Object attr) {
        if (ctxHolder != null) {
            ctxHolder.setRequestAttribute(key, attr);
        }
    }

    /**
     * @param key
     * @return object
     */
    public Object getRequestAttribute(String key) {
        if (ctxHolder != null) {
            return ctxHolder.getRequestAttribute(key);
        }

        return null;
    }

    public void resetSettingCache() {
        ctxHolder.removeApplicationAttribute("settings");
    }

    /**
     * @return unique value id:time
     */
    public static synchronized String getUnique() {

        try {
            Thread.sleep(2);
        } catch (Exception dc) {
            return "" + System.currentTimeMillis();
        }
        return "" + System.currentTimeMillis();

    }

    public void setServerIntanceId(String s) {
        hostId = s;
    }

    public String getServerIntanceId() {
        return hostId;
    }


    public String getInitParameter(String paramName) {
        return ctxHolder.getApplicationParameter(paramName);
    }

    public ContextHolder getCtxHolder() {
        return ctxHolder;
    }

    public void setCtxHolder(ContextHolder ctxHolder) {
        this.ctxHolder = ctxHolder;
    }

    public SettingService getSettingService() {
        return settingService;
    }

    public void setSettingService(SettingService settingService) {
        this.settingService = settingService;
    }
}
