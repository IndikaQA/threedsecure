/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Collections;
import java.util.IllformedLocaleException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class Misc {

    private static String IP_REGEX = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    private static Pattern ipPattern = Pattern.compile(IP_REGEX);

    private static String IPV6_REGEX = "^[0-9a-fA-F]{4}:[0-9a-fA-F]{4}:[0-9a-fA-F]{4}:[0-9a-fA-F]{4}:[0-9a-fA-F]{4}:[0-9a-fA-F]{4}:[0-9a-fA-F]{4}:[0-9a-fA-F]{4}$";
    private static Pattern ipv6Pattern = Pattern.compile(IPV6_REGEX);

    final static char[] spaceChars = new char[1024];
    final static char[] zeroChars = new char[1024];

    static {
        for (int i = 0; i < 1024; i++) {
            spaceChars[i] = ' ';
            zeroChars[i] = '0';
        }
    }

    /**
     * return true if value is null or "" string
     */
    public final static boolean isNullOrEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof CharSequence && ((CharSequence) o).length() == 0) {
            return true;
        }
        if (o instanceof byte[] && ((byte[]) o).length == 0) {
            return true;
        }
        if (o instanceof char[] && ((char[]) o).length == 0) {
            return true;
        }
        if (o instanceof String[]) {
            return isNullOrEmpty((String[]) o);
        }
        if (o instanceof Collection) {
            return isNullOrEmpty((Collection) o);
        }

        return false;
    }

    public final static boolean isNotNullOrEmpty(Object o) {
        return !isNullOrEmpty(o);
    }

    public final static boolean isNullOrEmpty(@SuppressWarnings("rawtypes") Collection o) {
        if (o == null || o.isEmpty()) {
            return true;
        }
        return false;
    }

    public final static boolean isNotNullOrEmpty(@SuppressWarnings("rawtypes") Collection o) {
        return !isNullOrEmpty(o);
    }

    public final static String merge(CharSequence s1, CharSequence s2) {
        if (s1 == null && s2 == null) {
            return null;
        }

        if (!isNullOrEmpty(s1) && !isNullOrEmpty(s2)) {
            return new StringBuilder(s1.length() + s2.length() + 1).append(s1)
                    .append(' ').append(s2).toString();
        }

        return isNullOrEmpty(s1) ? s2.toString() : s1.toString();
    }

    public final static byte[] lastBytes(byte[] s1, int start) {
        byte[] last = new byte[s1.length - start];
        System.arraycopy(s1, start, last, 0, last.length);
        return last;
    }

    // this is the only way pass a str array so we dont make additional copy
    // of sensitive string
    public final static boolean isNullOrEmpty(String[] o) {
        if (o == null || o.length < 1) {
            return true;
        }

        if (o[0] == null || o[0].length() < 1) {
            return true;
        }

        return false;
    }

    public final static boolean isNotNullOrEmpty(String[] o) {
        return !isNullOrEmpty(o);
    }

    public final static boolean isNullOrEmptyTrimmed(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof String && ((String) o).trim().length() == 0) {
            return true;
        }

        return false;
    }

    public final static boolean isNotNullOrEmptyTrimmed(Object o) {
        return !isNullOrEmptyTrimmed(o);
    }

    public final static String cut(String s, int len) {
        if (s != null && s.length() > len) {
            return s.substring(0, len);
        }

        return s;
    }

    public final static String cutLeft(String s, int len) {
        if (s != null && s.length() > len) {
            return s.substring(s.length() - len);
        }

        return s;
    }

    /* return null if s is "" or s is null or s */
    public final static String defaultToNull(String s) {
        if (s == null) {
            return null;
        }
        if (s.length() == 0) {
            return null;
        }

        return s;
    }

    /* return "" if s is null */
    public final static String defaultString(Object o) {
        if (o == null) {
            return "";
        }

        return o.toString();
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> nullSafe(List<T> list) {
        return list != null ? list : (List<T>) Collections.EMPTY_LIST;
    }

    /* return ture if intger */
    public final static boolean isInt(String s) {
        if (s == null) {
            return false;
        }

        try {
            Integer.parseInt(s);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public final static boolean isLong(String s) {
        try {
            Long.parseLong(s);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /* return int or 0 if not parsable */
    public final static int parseInt(String s) {
        if (s == null || s.length() < 1) {
            return 0;
        }

        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }

    }

    public final static long parseLong(String s) {
        if (s == null || s.length() < 1) {
            return 0;
        }
        try {
            return Long.parseLong(s);
        } catch (Exception e) {
            return 0;
        }

    }

    public final static Long parseLongBoxed(String s) {
        if (s == null || s.length() < 1) {
            return null;
        }
        try {
            return Long.parseLong(s);
        } catch (Exception e) {
            return null;
        }

    }


    /* return double or 0 if not parsable */
    public final static double parseDouble(String s) {
        if (s == null || s.length() < 1) {
            return 0.0d;
        }

        try {
            return Double.parseDouble(s.replace(',', '.'));
        } catch (Exception e) {
            return 0.0d;
        }

    }

    /**
     * Method trunc
     *
     * @param in
     * @param toLen
     * @return trunc
     */
    public static final String trunc(String in, int toLen) {
        if (in != null && in.length() > toLen) {
            String trunc = in.substring(0, toLen);
            return trunc;
        }
        return in;
    }

    public final static String replace(String content1, String olds, String news) {
        // dont process if content old and new are equal
        if (olds == null || (olds.equals(news) || olds.length() == 0)) {
            return content1 != null ? content1.toString() : null;
        }
        StringBuilder content = new StringBuilder(content1.length() + content1.length() / 4).append(content1);
        replace(content, olds, news, Integer.MAX_VALUE);
        return content.toString();
    }

    public final static void replace(StringBuilder content, CharSequence olds, CharSequence news, int count) {
        // dont process if content old and new are equal
        if (olds == null || (olds.equals(news) || olds.length() == 0)) {
            return;
        }
        // , oldi+1 ensure the replacing adwanses and wont stuck if the replaced
        // content somehow results as content to be replaced..
        String oldsStr = olds.toString();
        String newsStr = news.toString();
        int oldi = -1;
        int done = 0;
        boolean samelen = olds.length() == news.length();
        while ((oldi = content.indexOf(oldsStr, oldi + 1)) > -1) {
            if (samelen) {
                for (int i = 0; i < olds.length(); i++) {
                    content.setCharAt(oldi + i, news.charAt(i));
                }
            } else {
                content.replace(oldi, oldi + olds.length(), newsStr);
            }
            oldi += news.length();
            done++;
            if (done >= count) {
                break;
            }
        }
    }

    public final static void replace(StringBuffer content, CharSequence olds, CharSequence news) {
        replace(content, olds, news, Integer.MAX_VALUE);
    }

    public final static void replace(StringBuffer content, CharSequence olds, CharSequence news, int count) {
        // dont process if content old and new are equal
        if (olds == null || (olds.equals(news) || olds.length() == 0)) {
            return;
        }
        // , oldi+1 ensure the replacing adwanses and wont stuck if the replaced
        // content somehow results as content to be replaced..
        String oldsStr = olds.toString();
        String newsStr = news.toString();
        int oldi = -1;
        int done = 0;
        boolean samelen = olds.length() == news.length();
        while ((oldi = content.indexOf(oldsStr, oldi + 1)) > -1) {
            if (samelen) {
                for (int i = 0; i < olds.length(); i++) {
                    content.setCharAt(oldi + i, news.charAt(i));
                }
            } else {
                content.replace(oldi, oldi + olds.length(), newsStr);
            }
            oldi += news.length();
            done++;
            if (done >= count) {
                break;
            }
        }
    }

    public static String[] split(CharSequence in, char delimiter) {
        return split(in, String.valueOf(delimiter));
    }

    /**
     * @param in        the string to split
     * @param delimiter the delimiter string
     * @return the array of strings parsed using delimiter
     */
    public static String[] split(CharSequence in, String delimiter) {
        if (in == null || delimiter == null) {
            return null;
        }

        java.util.List<String> elements = splitToList(in, delimiter);

        String[] elms = new String[elements.size()];
        elements.toArray(elms);

        return elms;
    }

    public static final String[] splitTwo(String in, String delimiter) {
        if (in == null || delimiter == null) {
            return null;
        }

        int del = in.indexOf(delimiter);
        if (del > -1) {
            String[] two = new String[2];
            two[0] = in.substring(0, del);
            if (in.length() > del + 1) {
                two[1] = in.substring(in.indexOf(delimiter) + 1);
            } else {
                two[1] = "";
            }

            return two;
        }

        return new String[]{in};
    }

    public static java.util.List<String> splitToList(CharSequence in, String delimiter) {
        if (in == null || delimiter == null) {
            return null;
        }

        java.util.List<String> elements = new java.util.LinkedList<String>();

        int lpos = 0;
        int cpos = 0;
        String elem = null;
        do {
            cpos = indexOf(in, delimiter, lpos);

            if (cpos >= lpos) {
                elem = in.subSequence(lpos, cpos).toString();
                // System.out.println("elem='"+elem+"'");

                lpos = cpos + delimiter.length();

                elements.add(elem);
            } else {
                break;
            }

        } while (true); // while

        // add the reminder after last delimiter
        elements.add(in.subSequence(lpos, in.length()).toString());

        return elements;
    }

    public final static String mask(String pan, int fromEnds) {
        if (pan == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(pan);
        for (int i = fromEnds; i < pan.length() - fromEnds; i++) {
            //if (i >= fromEnds && i < pan.length() - fromEnds)
            //{
            sb.setCharAt(i, '#');
            //}
        }
        return sb.toString();
    }

    public final static String maskEnd(String pan, int start) {
        if (pan == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(pan);
        for (int i = start; i < pan.length(); i++) {
            //if (i >= start)
            //{
            sb.setCharAt(i, '#');
            //}
        }
        return sb.toString();
    }

    public final static StringBuilder toStringBuilder(byte[] buf) {
        if (buf == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(buf.length);
        for (int i = 0; i < buf.length; i++) {
            sb.append((char) buf[i]);
        }
        return sb;
    }

    public final static byte[] toBytes(CharSequence sb) {
        if (sb == null) {
            return null;
        }

        byte[] buf = new byte[sb.length()];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = (byte) sb.charAt(i);
        }
        return buf;
    }

    public final static String mask(String pan, int start, int end) {
        if (pan == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(pan);
        for (int i = start; i < pan.length() - end; i++) {
            //if (i >= start && i < pan.length() - end)
            //{
            sb.setCharAt(i, '#');
            //}
        }
        return sb.toString();
    }

    public final static String maskNumsOnly(String pan, int start, int end) {
        if (pan == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(pan);
        for (int i = start; i < pan.length() - end; i++) {
            //if (i >= start && i < pan.length() - end)
            //{
            char c = sb.charAt(i);
            if (c >= '0' && c <= '9') {
                sb.setCharAt(i, '#');
            }
            //}
        }
        return sb.toString();
    }

    public final static String maskIntelligent(String value, int fromEnds) {
        if (value == null) {
            return null;
        }

        if (value.length() > 2 * fromEnds + 4) {
            String start = value.substring(0, fromEnds);
            String end = value.substring(value.length() - fromEnds);

            String middle = "##.." + (value.length() - 2 * fromEnds - 4) + "..##";

            return new StringBuilder(start.length() + middle.length() + end.length())
                    .append(start).append(middle).append(end).toString();
        }
        return mask(value, fromEnds);
    }

    public static String encodeHTMLForForm(CharSequence s) {
        if (s == null) {
            return "";
        }
        StringBuilder to = new StringBuilder(s.length() + s.length() / 5);

        for (int i = 0; i < s.length(); i++) {
            char xc = s.charAt(i);
            if (xc == '&') {
                to.append("&amp;");
            } else if (xc == '<') {
                to.append("&lt;");
            } else if (xc == '>') {
                to.append("&gt;");
            } else if (xc == '"') {
                to.append("&quot;");
            } else {
                to.append(xc);
            }
        }

        return to.toString();
    }


    public static int indexOf(byte[] array, byte[] search) {
        return indexOf(array, search, 0);
    }

    public static int indexOf(byte[] array, byte[] search, int offs) {
        if (array == null || search == null || array.length == 0 || search.length == 0) {
            return -1;
        }

        int ix = -1;
        for (int i = offs; i < array.length; i++) {
            if (array[i] == search[0]) {
                if (search.length > 1) {
                    int matches = 1;
                    for (int j = 1; j < search.length; j++) {
                        if (i + j < array.length && array[i + j] == search[j]) {
                            matches++;
                        } else {
                            break;
                        }
                    }
                    if (matches == search.length) {
                        ix = i;
                        break;
                    }
                } else {
                    ix = i;
                    break;
                }
            }
        }

        return ix;
    }

    public static int indexOf(CharSequence array, CharSequence search, int fromIndex) {
        if (array == null || search == null || array.length() == 0 || search.length() == 0 || fromIndex > array.length()) {
            return -1;
        }

        int ix = -1;
        for (int i = fromIndex; i < array.length(); i++) {
            if (array.charAt(i) == search.charAt(0)) {
                if (search.length() > 1) {
                    int matches = 1;
                    for (int j = 1; j < search.length(); j++) {
                        if (i + j < array.length() && array.charAt(i + j) == search.charAt(j)) {
                            matches++;
                        } else {
                            break;
                        }
                    }
                    if (matches == search.length()) {
                        ix = i;
                        break;
                    }
                } else {
                    ix = i;
                    break;
                }
            }
        }

        return ix;
    }


    public static final byte[] longToBytes(long l) {
        byte[] buf = new byte[8];
        buf[0] = (byte) (l >>> 56);
        buf[1] = (byte) (l >>> 48);
        buf[2] = (byte) (l >>> 40);
        buf[3] = (byte) (l >>> 32);
        buf[4] = (byte) (l >>> 24);
        buf[5] = (byte) (l >>> 16);
        buf[6] = (byte) (l >>> 8);
        buf[7] = (byte) (l >>> 0);

        return buf;
    }

    public static final long bytesToLong(byte[] buf) {
        return (((long) buf[0] << 56) + ((long) (buf[1] & 255) << 48) + ((long) (buf[2] & 255) << 40) + ((long) (buf[3] & 255) << 32)
                + ((long) (buf[4] & 255) << 24) + ((buf[5] & 255) << 16) + ((buf[6] & 255) << 8) + ((buf[7] & 255) << 0));
    }

    /**
     * Validates IP address
     *
     * @param ip - IP address in String
     * @return - True if valid
     */
    public static boolean isIpV4Address(String ip) {
        return (!(ip == null || ip.length() < 8)) && ipPattern.matcher(ip).matches();
    }

    public static int[] parseIpV4AddressRange(String ipRange) {
        if (ipRange == null || ipRange.length() < 14 || !ipRange.contains("-")) {
            return null;
        }
        String[] range = Misc.splitTwo(ipRange, "-");

        if (range != null && range.length == 2 && Misc.isIpV4Address(range[0]) && Misc.isIpV4Address(range[1])) {
            int[] irange = new int[2];
            irange[0] = parseIpV4Value(range[0]);
            irange[1] = parseIpV4Value(range[1]);
            return irange;
        }

        return null;
    }

    public static int parseIpV4Value(String ipV4) {
        if (ipV4 == null || ipV4.length() < 7 || !ipV4.contains(".")) {
            return 0;
        }

        String[] addr = Misc.split(ipV4, '.');
        if (addr.length == 4) {

            int address = Misc.parseInt(addr[3]) & 0xFF;
            address |= ((Misc.parseInt(addr[2]) << 8) & 0xFF00);
            address |= ((Misc.parseInt(addr[1]) << 16) & 0xFF0000);
            address |= ((Misc.parseInt(addr[0]) << 24) & 0xFF000000);
            return address;
        }

        return 0;
    }

    public static boolean isIpV6Address(String ip) {
        return ((ip != null) && ipv6Pattern.matcher(ip).matches());
    }

    /**
     * Tells if a given string contains a number inside the range defined by two given numbers..
     *
     * @param number
     * @param min
     * @param max
     * @return
     */
    public static final boolean isInRange(final String number, final double min, final double max) {
        if (!isNumber(number)) {
            return false;
        }
        double value = Misc.parseDouble(number);
        return value >= min && value <= max;
    }


    public static void main(String[] args) {
        String template = "Some template $val1$, some more text $val1$ some other text $val2$, go longer $val1$, even more longer $val1$ $val1$ $val1$ $val1$";

        int cyc = 200000;
        String r1 = null;

        long s1 = System.currentTimeMillis();
        for (int i = 0; i < cyc; i++) {
            r1 = Misc.replace(template, "$val1$", "*VAL1*");
        }
        long e1 = System.currentTimeMillis();

        System.out.println("String " + (e1 - s1) + "ms " + r1);
        // conclusion using optimized length stringbuilder is approx 4 times faster
    }


    public final static String padCutStringLeft(String s, char p, int len) {
        if (s == null) {
            throw new IllegalArgumentException("invalid string null");
        }
        if (len < 0) {
            throw new IllegalArgumentException("invalid length " + len);
        }

        if (s.length() < len) {
            if (('0' == p || ' ' == p) && len < 1025) {
                StringBuilder sb = new StringBuilder(len);
                sb.append('0' == p ? zeroChars : spaceChars, 0, len - s.length()).append(s);
                s = sb.toString();
            } else {

                char[] pad = new char[len - s.length()];
                for (int i = 0; i < pad.length; i++) {
                    pad[i] = p;
                }
                StringBuilder sb = new StringBuilder(len);
                sb.append(pad).append(s);
                s = sb.toString();
            }
        }

        if (s.length() > len) {
            s = s.substring(s.length() - len);
        }

        return s;
    }

    public final static String padCutStringRight(String s, char p, int len) {
        if (s == null) {
            throw new IllegalArgumentException("invalid string null");
        }
        if (len < 0) {
            throw new IllegalArgumentException("invalid length " + len);
        }

        if (s.length() < len) {
            StringBuilder sb = new StringBuilder(len);
            if (('0' == p || ' ' == p) && len < 1025) {
                sb.append(s).append('0' == p ? zeroChars : spaceChars, 0, len - s.length());
                s = sb.toString();
            } else {
                char[] pad = new char[len - s.length()];
                for (int i = 0; i < pad.length; i++) {
                    pad[i] = p;
                }
                sb.append(s).append(pad);
                s = sb.toString();
            }
        }

        if (s.length() > len) {
            s = s.substring(0, len);
        }

        return s;
    }

    public final static boolean isBase64(byte[] array) {
        return isBase64(array, Base64.Mode.cNormal);
    }

    public final static boolean isBase64(byte[] array, int mode) {
        if (array == null || array.length < 1) {
            return false;
        }

        for (byte b : array) {
            char c = (char) b;
            if (c >= '0' && c <= '9' ||
                    c >= 'A' && c <= 'Z' ||
                    c >= 'a' && c <= 'z'
                    || (mode == Base64.Mode.cNormal && (c == '+' || c == '/' || c == '='))
                    || (mode == Base64.Mode.cURL && (c == '-' || c == '_'))
                    || Character.isWhitespace(c)) {

            } else {
                return false;
            }
        }


        return true;
    }

    public final static Integer parseIntBoxed(String s) {
        if (Misc.isNullOrEmpty(s)) {
            return null;
        }

        try {
            return Integer.valueOf(s);
        } catch (Exception e) {
            return null;
        }
    }

    public static final boolean isNumber(CharSequence in) {
        if (in == null || in.length() < 1) {
            return false;
        }

        for (int i = 0; i < in.length(); i++) {
            char c = in.charAt(i);
            if ((c < '0' || c > '9') && c != '.') {
                return false;
            }
        }
        return true;
    }

    public final static byte[] toUtf8Bytes(CharSequence sb) {
        return toBytes(StandardCharsets.UTF_8, sb, 0, sb.length());
    }

    public final static byte[] toBytes(Charset cs, CharSequence sb) {
        return toBytes(cs, sb, 0, sb.length());
    }

    public final static byte[] toBytes(Charset cs, CharSequence sb, int offs, int len) {
        ByteBuffer bb = cs.encode(CharBuffer.wrap(sb, offs, len));
        if (bb.hasArray()) {
            byte[] b = new byte[bb.remaining()];
            System.arraycopy(bb.array(), bb.arrayOffset(), b, 0, b.length);
            return b;
        }

        byte[] b = new byte[bb.remaining()];
        bb.get(b);
        return b;
    }

    public static final String calculateSHA256AndEncodeBASE64(CharSequence data) {
        try {
            MessageDigest mdigest = MessageDigest.getInstance("SHA-256");
            byte[] digestResult = mdigest.digest(toUtf8Bytes(data));
            return Base64.encode(digestResult);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public final static String arrayToCSV(Object[] array) {
        if (array == null) {
            return null;
        }
        if (array.length == 0) {
            return "";
        }

        StringBuilder ad = new StringBuilder(array.length * 8);
        for (Object o : array) {
            if (ad.length() > 0) {
                ad.append(',');
            }
            ad.append(String.valueOf(o));
        }

        return ad.toString();
    }

    public final static String listToCSV(List<? extends Object> array) {
        if (array == null) {
            return null;
        }
        if (array.size() == 0) {
            return "";
        }

        StringBuilder ad = new StringBuilder(array.size() * 8);
        for (Object o : array) {
            if (ad.length() > 0) {
                ad.append(',');
            }
            ad.append(String.valueOf(o));
        }

        return ad.toString();
    }

    public final static boolean in(Object search, Object[] array) {
        if (array == null || array.length < 1) {
            return false;
        }
        for (int i = 0; i < array.length; i++) {
            if (search == array[i] || search != null && search.equals(array[i])) {
                return true;
            }
        }

        return false;
    }

    public final static boolean isBCP47LanguageTag(String value) {
        try {
            if (isNotNullOrEmpty(value)) {
                new Locale.Builder().setLanguageTag(value);
                return true;
            }
        } catch (IllformedLocaleException ex) {
            // not a valid BCP47 language tag
        }
        return false;
    }

}
