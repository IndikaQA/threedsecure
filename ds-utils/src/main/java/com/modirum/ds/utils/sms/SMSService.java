/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 23.05.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils.sms;

import com.modirum.ds.utils.SettingService;

public interface SMSService {

    /**
     * @param dest         destination full number
     * @param source       source full number
     * @param content      - message content
     * @param validityMins - optional minutes of max validity for delivery
     * @param se           - source of settings
     * @return
     * @throws Exception
     */
    public boolean sendSMS(String dest, String source, String content, Integer validityMins, SettingService se) throws Exception;


}
