/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 04.09.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils.http;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SSLSocketHelper {
    private transient static Logger log = LoggerFactory.getLogger(SSLSocketHelper.class);

    public interface SSLProto {
        public String TLS = "TLS";
        public String TLSv1 = "TLSv1";
        public String TLSv11 = "TLSv1.1";
        public String TLSv12 = "TLSv1.2";
        public String DEFAULT = TLSv12;
    }

    final static String clientKeyAlias = "clientKeyAlias";

    public static SSLSocketFactory initFactory(String type, String keyManFactory, PrivateKey clientPriKey,
                                               Certificate[] clientCertChain, KeyStore trustCerts, boolean trustAny, String[] defChipers, String[] supChipers) throws Exception {
        if (keyManFactory == null) {
            keyManFactory = KeyManagerFactory.getDefaultAlgorithm(); //"SunX509";
        }

        KeyManager[] keyManagers = null;
        if (clientPriKey != null && clientCertChain != null) {
            KeyStore clientkeystore = KeyStore.getInstance("JKS");
            clientkeystore.load(null);
            clientkeystore.setKeyEntry(clientKeyAlias, clientPriKey, "password".toCharArray(), clientCertChain);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(keyManFactory);
            kmf.init(clientkeystore, "password".toCharArray());
            keyManagers = new KeyManager[]{new EnchX509KeyManager(kmf.getKeyManagers()[0])};
        }

        TrustManager[] trustManagers = null;
        if (trustAny) {
            trustManagers = new TrustManager[]{new TrustingX509TrustManager()};
        } else if (trustCerts != null) {
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(keyManFactory);
            tmf.init(trustCerts);
            trustManagers = tmf.getTrustManagers();
        }

        SSLContext context = null;

        try {
            context = SSLContext.getInstance(type);
        } catch (Exception e) {
            log.warn("SSL Context for " + type + " failed", e);

            try {
                context = SSLContext.getInstance(SSLProto.DEFAULT);
            } catch (Exception e2) {
                log.warn("SSL Context for " + SSLProto.DEFAULT + " failed", e);

                try {
                    context = SSLContext.getInstance(SSLProto.TLSv1);
                } catch (Exception e3) {
                    log.warn("SSL Context for " + SSLProto.TLSv1 + " failed", e3);
                    throw e3;
                }

            }

        }

        context.init(keyManagers, trustManagers, null);
        SSLSocketFactory factory = context.getSocketFactory();
        if (defChipers != null || supChipers != null) {
            SSLSocketFactoryWrp fwp = new SSLSocketFactoryWrp(factory, defChipers, supChipers);
            return fwp;
        }

        return factory;
    }


    public static class SSLSocketFactoryWrp extends SSLSocketFactory {
        SSLSocketFactory factory = null;
        String[] defaultChipers = null;
        String[] supChipers = null;

        public SSLSocketFactoryWrp(SSLSocketFactory factory, String[] defaultChipers, String[] supChipers) {
            this.factory = factory;
            this.defaultChipers = defaultChipers;
            this.supChipers = supChipers;
        }

        @Override
        public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
            return factory.createSocket(s, host, port, autoClose);
        }

        @Override
        public String[] getDefaultCipherSuites() {
            return defaultChipers != null ? defaultChipers : factory.getDefaultCipherSuites();
        }

        @Override
        public String[] getSupportedCipherSuites() {
            return supChipers != null ? supChipers : factory.getSupportedCipherSuites();
        }

        @Override
        public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
            return factory.createSocket(host, port);
        }

        @Override
        public Socket createSocket(InetAddress host, int port) throws IOException {
            return factory.createSocket(host, port);
        }

        @Override
        public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
            // TODO Auto-generated method stub
            return factory.createSocket(host, port, localHost, localPort);
        }

        @Override
        public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
            return factory.createSocket(address, port, localAddress, localPort);
        }

    }


    public static class TrustingX509TrustManager implements X509TrustManager {

        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            if (log.isDebugEnabled()) {
                log.debug("TrustingX509TrustManager.getAcceptedIssuers()");
            }
            return null;
        }

        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
            // TODO Auto-generated method stub

        }

        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
            // TODO Auto-generated method stub

        }
    }

    public static class EnchX509KeyManager implements X509KeyManager {

        protected X509KeyManager km;

        public EnchX509KeyManager(KeyManager km) {
            this.km = (X509KeyManager) km;
        }

        public String chooseClientAlias(String keyType, Principal[] issuers) {
            /*
             * if (log.isDebugEnabled()) log.debug("chooseClientAlias [" + keyType + "]");
             *
             * for (int i = 0; i < issuers.length; i++) { if (log.isDebugEnabled()) log.debug("issuer = " + issuers[i]); }
             *
             * if (log.isDebugEnabled()) log.debug("returned client alias = " + clientKeyAlias);
             */
            return clientKeyAlias;
        }

        public X509Certificate[] getCertificateChain(String alias) {

            if (log.isDebugEnabled()) {
                log.debug("getCertificateChain [" + clientKeyAlias + "]");
            }

            X509Certificate[] ret = km.getCertificateChain(clientKeyAlias);

            if (log.isDebugEnabled()) {
                log.debug("certificates found= " + Arrays.toString(ret));
            }

            for (int i = 0; ret != null && i < ret.length; i++) {
                if (log.isDebugEnabled()) {
                    log.debug("certificate[" + i + "] = " + ret[i]);
                }
            }

            return ret;
        }

        public String[] getClientAliases(String keyType, Principal[] issuers) {
            if (log.isDebugEnabled()) {
                log.debug("getClientAliases [" + keyType + "]");
            }

            for (int i = 0; i < issuers.length; i++) {
                if (log.isDebugEnabled()) {
                    log.debug("issuer = " + issuers[i]);
                }
            }
            String[] ret = km.getClientAliases(keyType, issuers);
            for (int i = 0; i < ret.length; i++) {
                if (log.isDebugEnabled()) {
                    log.debug("returning alias[" + i + "] = " + ret[i]);
                }
            }
            return ret;
        }

        public PrivateKey getPrivateKey(String alias) {
            log.debug("getPrivateKey [" + clientKeyAlias + "]");
            PrivateKey ret = km.getPrivateKey(alias);
            log.debug("private key = " + ret);
            return ret;
        }

        public String[] getServerAliases(String keyType, Principal[] issuers) {
            return null;
        }

        public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
            return this.chooseClientAlias(keyType[0], issuers);

        }

        public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {

            return this.chooseClientAlias(keyType, issuers);
        }
    }

    public static class OkHostnameVerifier implements HostnameVerifier {
        public boolean verify(String urlHostName, String certHostname) {
            return true;
        }

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}
