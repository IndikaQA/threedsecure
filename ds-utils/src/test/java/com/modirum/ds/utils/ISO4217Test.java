package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ISO4217Test {

    @Test
    public void checkISO4217() {
        for (ISO4217 entry : ISO4217.values()) {
            Assertions.assertEquals(entry, ISO4217.findFromNumeric(entry.getNumeric()));
            Assertions.assertTrue(entry.getCurrencyExponent() >= 0);
            Assertions.assertEquals(3, entry.getA3code().length());
            Assertions.assertFalse(Misc.isNullOrEmpty(entry.getName()));
            Assertions.assertFalse(Misc.isNullOrEmpty(entry.getSymbol()));
        }

        ISO4217 ugandaShilling = ISO4217.findFromNumeric((short) 800);
        Assertions.assertNotNull(ugandaShilling);
        Assertions.assertEquals(0, ugandaShilling.getCurrencyExponent());
        Assertions.assertEquals(2, ugandaShilling.getCurrencyExponent(DateUtil.parseDate("2017-01-14", "yyyy-MM-dd")));
        Assertions.assertEquals(2, ugandaShilling.getCurrencyExponent(DateUtil.parseDate("2017-10-14", "yyyy-MM-dd")));
        Assertions.assertEquals(0, ugandaShilling.getCurrencyExponent(DateUtil.parseDate("2017-10-15", "yyyy-MM-dd")));
        Assertions.assertEquals(0, ugandaShilling.getCurrencyExponent(DateUtil.parseDate("2021-10-15", "yyyy-MM-dd")));

        Assertions.assertNull(ISO4217.findFromNumeric((short)0));
    }

}
