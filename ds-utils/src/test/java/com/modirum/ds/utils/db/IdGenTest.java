package com.modirum.ds.utils.db;

import com.modirum.ds.utils.db.IdGen;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class IdGenTest {

    @Mock
    Connection mockConnection;

    @Mock
    PreparedStatement mockStatement;

    @Mock
    ResultSet mockResultSet;

    @Mock
    SharedSessionContractImplementor mockSession;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.initMocks(this);
        try {
            Mockito.when(mockSession.connection()).thenReturn(mockConnection);
            Mockito.when(mockConnection.prepareStatement(Mockito.anyString())).thenReturn(mockStatement);
            Mockito.when(mockStatement.executeQuery()).thenReturn(mockResultSet);
            Mockito.when(mockStatement.executeUpdate()).thenReturn(1);
        } catch (Throwable t) {
            Assertions.fail("Failed to test id generation", t);
        }
    }

    @Test
    public void generateIdUseDbid() {
        try {
            HibernateIdGenerator personIdGenerator = new HibernateIdGenerator();
            Properties params = new Properties();
            params.put("name", "id");
            params.put("idType", "Integer");
            params.put("idTable", "person");
            personIdGenerator.configure(null, params,null);

            // no entry yet. seed = IdGen.idStart = 10
            Mockito.when(mockResultSet.next()).thenReturn(false);

            Assertions.assertEquals(10000, personIdGenerator.generate(mockSession, null));

            for (int i = 1; i <= 99; i++) {
                Assertions.assertEquals(Integer.parseInt(1000 + i + "0"), personIdGenerator.generate(mockSession, null));
            }

            // seed was previously set to 10
            Mockito.when(mockResultSet.next()).thenReturn(true);
            Mockito.when(mockResultSet.getLong(1)).thenReturn(10L);
            for (int i = 0; i <= 99; i++) {
                Assertions.assertEquals(Integer.parseInt(1100 + i + "0"), personIdGenerator.generate(mockSession, null));
            }

            // seed was previously set to 100
            Mockito.when(mockResultSet.getLong(1)).thenReturn(100L);
            for (int i = 0; i <= 99; i++) {
                Assertions.assertEquals(Integer.parseInt(10100 + i + "0"), personIdGenerator.work.execute(mockConnection));
            }

        } catch (Throwable t) {
            Assertions.fail("Failed to test id generation", t);
        }
    }

    @Test
    public void generateIdNotUseDbid() {
        try {
            // no entry yet
            Mockito.when(mockResultSet.next()).thenReturn(false);
            IdGen personId = IdGen.getInstance("id", "car", false);
            Assertions.assertEquals(1000, personId.next(mockConnection));

            for (int i = 1; i <= 99; i++) {
                Assertions.assertEquals(1000 + i, personId.next(mockConnection));
            }

            Mockito.when(mockResultSet.next()).thenReturn(true);
            Mockito.when(mockResultSet.getLong(1)).thenReturn(10L);
            for (int i = 0; i <= 99; i++) {
                Assertions.assertEquals(1100 + i, personId.next(mockConnection));
            }

            Mockito.when(mockResultSet.getLong(1)).thenReturn(100L);
            for (int i = 0; i <= 99; i++) {
                Assertions.assertEquals(10100 + i, personId.next(mockConnection));
            }
        } catch (Throwable t) {
            Assertions.fail("Failed to test id generation", t);
        }
    }

}
