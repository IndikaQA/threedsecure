/* some default settings to get quickly going */

INSERT INTO ds.ds_settings (skey,svalue,comments,lastModified,lastModifiedBy) 
VALUES
  ('acceptedMCCList','1234,1235,5555,4444,5656,5544,5655,4511,7922','','2017-09-28 17:42:25.000','admin (1000)'),
  ('acceptedTDSServerList','PosterForm,123456,3DS,3DS_LOA_SER_PPFU_020100_00008','csv list', '2018-03-15 17:50:22.000','parkkinen (1800)'),
  ('cardType.01.visa','1','','2016-03-16 12:49:46.000','admin (1000)'),
  ('cardType.02.mastercard','2','','2016-03-16 12:49:57.000','admin (1000)'),
  ('cardType.03.amex','3','','2017-05-11 11:35:11.000','andri (1300)'),
  ('cardType.04.jcb','4','', '2017-05-11 11:34:54.000','andri (1300)'),
  ('cardType.05.discovery','5','', '2017-05-11 11:35:56.000','andri (1300)'),
  ('cardType.06.diners','6','', '2017-05-11 11:34:40.000','andri (1300)'),
  ('cardType.07.mistercash','7','','2017-05-11 11:36:18.000','andri (1300)'),
  ('cardType.08.mir','8','','2017-05-11 11:36:35.000','andri (1300)');

