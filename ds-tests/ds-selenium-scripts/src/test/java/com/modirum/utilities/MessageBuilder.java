package com.modirum.utilities;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;

@SuppressWarnings("unchecked")
public class MessageBuilder {
    private Map<String, Object> messageMap = new HashMap<>();

    private List<String> required;
    private List<String> optional;
    private List<String> conditional;

    private Map<String, Object> defaultValues;

    public MessageBuilder(){
        super();
    }

    public MessageBuilder(MessageBuilder that){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            this.defaultValues = objectMapper.readValue(JsonData.getAsString(that.defaultValues), new TypeReference<Map<String, Object>>() {});
            this.messageMap = objectMapper.readValue(JsonData.getAsString(that.messageMap), new TypeReference<Map<String, Object>>() {});
            String temp = objectMapper.writeValueAsString(that.getRequiredFields());
            this.required = objectMapper.readValue(temp, new TypeReference<List<String>>() {});
            temp = objectMapper.writeValueAsString(that.getOptionalFields());
            this.optional = objectMapper.readValue(temp, new TypeReference<List<String>>() {});
            temp = objectMapper.writeValueAsString(that.getConditionalFields());
            this.conditional = objectMapper.readValue(temp, new TypeReference<List<String>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void build(String messageType, String messageVersion, String deviceChannel, String messageCategory){
        messageMap.clear();

        Map<String, Object> fieldInclusions =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("threeds" + File.separator + messageVersion
                        + File.separator + "messages",
                        messageType.toLowerCase() + ".json"));
        initFieldInclusions(fieldInclusions, deviceChannel, messageCategory);
        initDefaultValues(messageVersion);

        for(String reqField : required){
            addOrUpdateFieldWithDefaultValue(reqField);
        }
        addOrUpdateField("messageType", messageType);
        addOrUpdateField("messageVersion", messageVersion);
    }

    private void initDefaultValues(String messageVersion){
        if(defaultValues != null) return;

        defaultValues = JsonData.readFromFile(FileUtil.getTestResourceFilePath("threeds" + File.separator + messageVersion,
                "defaultValues.json"));
    }

    private void initFieldInclusions(Map<String, Object> fieldInclusions, String deviceChannel, String messageCategory){
        if(fieldInclusions == null) return;

        Map<String, Object> filteredInclusions =
                (Map<String, Object>)fieldInclusions.get(deviceChannel.toUpperCase() + "-" + messageCategory.toUpperCase());

        List<String> requiredModirum = (List<String>) filteredInclusions.get("required-modirum");
        required = (List<String>) filteredInclusions.get("required");
        if(requiredModirum != null)
            required.addAll(requiredModirum);

        optional = (List<String>) filteredInclusions.get("optional");
        conditional = (List<String>) filteredInclusions.get("conditional");
    }

    public Map<String, Object> build(String jsonString){
        messageMap.clear();
        messageMap = JsonData.readFromString(jsonString);
        return messageMap;
    }

    @Override
    public String toString(){
        return JsonData.getAsString(messageMap);
    }

    public List<String> getRequiredFields(){
        return required;
    }

    public List<String> getOptionalFields(){
        return optional;
    }

    public List<String> getConditionalFields(){
        return conditional;
    }

    public void addOrUpdateField(String fieldName, Object fieldValue){
        JsonData.addOrUpdateField(messageMap, fieldName, fieldValue);
    }

    public void addOrUpdateFieldWithDefaultValue(String fieldName){
        Object defVal = defaultValues.get(fieldName);
        if(defVal instanceof String) {
            addOrUpdateField(fieldName, generateAutoValue((String)defVal));
        } else {
            // current limitation: list values and json objects are added without checking for auto value keywords.
            addOrUpdateField(fieldName, defVal);
        }
    }

    public Object getField(String fieldName){
        return JsonData.getField(messageMap, fieldName);
    }

    public String generateAutoValue(String keyValue){
        if(keyValue.toUpperCase().equals("UUID")){
            return UUID.randomUUID().toString();
        }
        // key is not found
        return keyValue;
    }

    public void removeField(String fieldName){
        JsonData.removeField(messageMap, fieldName);
    }
}

