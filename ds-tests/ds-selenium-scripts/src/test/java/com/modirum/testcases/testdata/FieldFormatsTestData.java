package com.modirum.testcases.testdata;

import com.modirum.models.DataType;

public class FieldFormatsTestData {
    class FieldFormats {
        String fieldName;
        DataType dataType;
        Object[] validValues;
        Object[] invalidValues;

        public FieldFormats(String name, DataType type, Object[] validVals, Object[] invalidVals) {
            this.fieldName = name;
            this.dataType = type;
            this.validValues = validVals;
            this.invalidValues = invalidVals;
        }

        public String getFieldName() {
            return fieldName;
        }

        public DataType getDataType() {
            return dataType;
        }

        public Object[] getValidValues() {
            return validValues;
        }

        public Object[] getInvalidValues() {
            return invalidValues;
        }
    }


    static Object[][] modirum_formats = new Object[][]{
            { "threeDSRequestorAuthenticationInd", DataType.STRING, new String[]{}, new String[] {}},
            { "threeDSReqAuthMethodInd", DataType.STRING, new String[] { "01", "02", "03" }, new String [] { "04", "79", "80", "99", "1", "100"} },
            { "acctType", DataType.STRING, new String[] { "01", "02", "03" }, new String [] { "04", "79", "80", "99", "1", "100"} }
    };


    static Object[][] eftpos_formats = new Object[][]{
            { "threeDSReqAuthMethodInd", DataType.STRING, null, new String [] { "01", "02", "03", "04", "79", "80", "99", "1", "100"} },
            { "acctType", DataType.STRING, null, new String [] { "01", "02", "03", "04", "79", "80", "99", "1", "100"} }
    };

    public static Object[] getFormats(String targetDS, String field){
        Object[][] temp ;
        if(targetDS.equalsIgnoreCase("eftpos")){
            temp = eftpos_formats;
        } else {
            temp = modirum_formats;
        }

        for( Object[] f : temp){
            if(f[0].toString().equals(field)) return f;
        }
        return null;
    }
}
