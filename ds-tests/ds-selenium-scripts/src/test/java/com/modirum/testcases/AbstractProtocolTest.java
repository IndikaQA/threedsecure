package com.modirum.testcases;

import com.modirum.reports.ITestReporter;
import com.modirum.reports.SuiteListener;
import com.modirum.reports.TestListener;
import com.modirum.models.DeviceChannel;
import com.modirum.models.MessageCategory;
import com.modirum.utilities.MessageBuilder;
import com.modirum.utilities.FileUtil;
import com.modirum.utilities.SelDriver;
import org.testng.Assert;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

@Listeners({SuiteListener.class, TestListener.class})
public abstract class AbstractProtocolTest implements ITest {
    protected SelDriver selDriver = null; // cannot be static, each Test must have its own selDriver
    protected Properties configProps = new Properties();
    protected String testEnv = null;

    protected String messageVersion;
    protected DeviceChannel deviceChannel;
    protected MessageCategory messageCategory;

    /*
     * Abstract Methods
     */
    protected abstract void initDefaultMessages();
    protected abstract void setTestContext(ITestContext context);

    /*
     * For Reports
     */
    protected ITestReporter reporter;

    /*
     * For Test Name Changes
     */
    protected ThreadLocal<String> testName = new ThreadLocal<>();

    @BeforeMethod
    public void BeforeMethod(Method method, Object[] testData, ITestContext ctx) {
        String group = "";
        if(!deviceChannel.equals(DeviceChannel.NA))
            group = "(" + group + deviceChannel.alias();
        if(!messageCategory.equals(MessageCategory.NA))
            group = group + "-" + messageCategory.alias() + ") ";

        if (testData.length > 0) {
            testName.set(group + method.getName() + " : " + testData[0] + " ");
            ctx.setAttribute("testName", testName.get());
        } else {
            testName.set(group + method.getName());
            ctx.setAttribute("testName", testName.get());
        }
    }

    @Override
    public String getTestName() {
        return testName.get();
    }


    /*
     * Setup Methods
     */
    @Parameters({"messageVersion"})
    @BeforeClass(alwaysRun = true)
    public void setUp(ITestContext context, String messageVersion){
        this.messageVersion = messageVersion;

        FileInputStream fis = null;
        FileInputStream fisDefaultVals = null;
        selDriver = new SelDriver();

        try {
            // Read Config.properties
            fis = new FileInputStream(FileUtil.getTestResourceFilePath("properties", "Config.properties"));
            configProps.load(fis);
            this.testEnv = configProps.getProperty("url.env");
        }catch(Exception e) {
        }finally {
            try {
                // make sure filestreams are closed
                if (fis != null) {
                    fis.close();
                }

                if(fisDefaultVals != null){
                    fisDefaultVals.close();
                }
            }catch(IOException f){
                // do nothing
            }
        }
    }

    @AfterClass
    public void shutDown(ITestContext context){
        selDriver.closeBrowser();
    }

    /*
     * Common Methods
     */
    protected MessageBuilder generateMessage(String messageType){
        MessageBuilder msg = new MessageBuilder();
        msg.build(messageType, messageVersion, deviceChannel.alias(), messageCategory.alias());
        if(messageType.equals("AReq")) {
            msg.addOrUpdateField("deviceChannel", deviceChannel.value());
            msg.addOrUpdateField("messageCategory", messageCategory.value());
        }
        return msg;
    }

    protected MessageBuilder getMessageForNewTransaction(MessageBuilder defaultMessage){
        MessageBuilder txMsg = new MessageBuilder(defaultMessage);
        txMsg.addOrUpdateFieldWithDefaultValue("threeDSServerTransID");
        return txMsg;
    }

    /*
     * Common Assertions
     */
    protected void assertThatResponseIsJson(String responseString){
        Assert.assertTrue(responseString.startsWith("{"), "Response MUST be JSON but is : " + responseString);
    }

    protected MessageBuilder assertThatResponseIsARes(String responseString, String transStatus, String transStatusReason){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        Assert.assertEquals(response.getField("messageType"), "ARes", "ARes Message Type");

        SoftAssert softly = new SoftAssert();
        softly.assertEquals(response.getField("messageVersion"), messageVersion, "ARes Message Version");
        softly.assertEquals(response.getField("transStatus"), transStatus, "ARes Transaction Status");
        if(!transStatusReason.equals("")){
            softly.assertEquals(response.getField("transStatusReason"), transStatusReason, "ARes Transaction Status Reason");
        }
        softly.assertAll();
        return response;
    }

    protected MessageBuilder assertThatResponseIsRRes(String responseString, String resultsStatus){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        SoftAssert softly = new SoftAssert();
        Assert.assertEquals(response.getField("messageType"), "RRes", "DS RRes messageType");
        softly.assertEquals(response.getField("messageVersion"), messageVersion, "RRes messageVersion");
        softly.assertEquals(response.getField("resultsStatus"), resultsStatus, "RRes resultsStatus");
        softly.assertAll();
        return response;
    }

    protected void assertThatResponseIsPRes(String responseString, MessageBuilder preq){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        Assert.assertEquals(response.getField("messageType"), "PRes", "Response should be PRes");

        SoftAssert softly = new SoftAssert();
        softly.assertEquals(response.getField("messageVersion"), messageVersion, "Response Message Version");
        softly.assertTrue(response.getField("cardRangeData") != null, "Card Range Data field should exist");
        softly.assertTrue(response.getField("dsStartProtocolVersion") != null, "DS Start Protocol Version field should exist");
        softly.assertTrue(response.getField("dsEndProtocolVersion") != null, "DS End Protocol Version field should exist");
        softly.assertTrue(response.getField("dsTransID") != null, "DS Trans ID field should exist");
        softly.assertEquals(response.getField("threeDSServerTransID"),preq.getField("threeDSServerTransID"), "threeDSServerTransID should match PReq");

        softly.assertAll();
    }

    protected void assertThatResponseIsErro101Or201(String responseString){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        Assert.assertEquals(response.getField("messageType"), "Erro", "Message Type of Response");

        String responseErrorCode = (String)response.getField("errorCode");
        SoftAssert softly = new SoftAssert();
        softly.assertEquals(response.getField("messageVersion"), messageVersion, "Message Version of Response");
        softly.assertTrue(responseErrorCode.equals("101") || responseErrorCode.equals("201"), "Error Code of Response should be either 101 or 201");
        softly.assertTrue(response.getField("errorDetail") != null, "Error Detail exists");
        softly.assertEquals(response.getField("errorComponent"), "D", "Error Component of Response");
        softly.assertAll();
    }

    protected void assertThatResponseIsErro101(String responseString){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        Assert.assertEquals(response.getField("messageType"), "Erro", "Message Type of Response");

        SoftAssert softly = new SoftAssert();
        softly.assertEquals(response.getField("messageVersion"), messageVersion, "Message Version of Response");
        softly.assertEquals(response.getField("errorCode"), "101", "Error Code of Response");
        softly.assertTrue(response.getField("errorDetail") != null, "Error Detail exists");
        softly.assertEquals(response.getField("errorComponent"), "D", "Error Component of Response");
        softly.assertAll();
    }

    protected void assertThatResponseIsErro201(String responseString, String removedField){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        Assert.assertEquals(response.getField("messageType"), "Erro", "Message Type of Response");

        SoftAssert softly = new SoftAssert();
        softly.assertEquals(response.getField("errorCode"), "201", "Error Code of Response");
        String errDetail = response.getField("errorDetail").toString();
        softly.assertTrue(errDetail.contains(removedField), "Actual Error Detail : " + errDetail);
        if(removedField.equals("messageVersion")){
            String version = (String)response.getField("messageVersion");
            softly.assertTrue(version != null, "Message Version of Response: " + version);
        } else {
            softly.assertEquals(response.getField("messageVersion"), messageVersion, "Message Version of Response");
        }
        softly.assertEquals(response.getField("errorComponent"), "D", "Error Component of Response");
        softly.assertAll();
    }

    protected void assertThatResponseIsErro203(String responseString, String field){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        Assert.assertEquals(response.getField("messageType"), "Erro", "Message Type of Response");

        SoftAssert softly = new SoftAssert();
        softly.assertEquals(response.getField("messageVersion"), messageVersion, "Message Version of Response");
        softly.assertEquals(response.getField("errorCode"), "203", "Error Code of Response");
        String errDetail = response.getField("errorDetail").toString();
        softly.assertTrue(errDetail.contains(field), "Actual Error Detail : " + errDetail);
        softly.assertEquals(response.getField("errorComponent"), "D", "Error Component of Response");
        softly.assertAll();
    }

    protected void assertThatResponseIsErro303(String responseString){
        assertThatResponseIsJson(responseString);

        MessageBuilder response = new MessageBuilder();
        response.build(responseString);

        Assert.assertEquals(response.getField("messageType"), "Erro", "Message Type of Response MUST BE Erro");

        SoftAssert softly = new SoftAssert();
        softly.assertEquals(response.getField("errorCode"), "303", "Error Code of Response");
        softly.assertEquals(response.getField("errorComponent"), "D", "Error Component of Response");
        softly.assertAll();
    }
}
