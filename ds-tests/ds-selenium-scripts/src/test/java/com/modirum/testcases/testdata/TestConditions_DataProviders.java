package com.modirum.testcases.testdata;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestConditions_DataProviders {
    private List<Object[]> getConditionalTestData(String fieldCondition, Object fieldValueCondition, String[] listFields,
                                                      List<String> rFields, List<String> oFields, List<String> cFields){
        List<Object[]> data = new ArrayList<>();

        if((fieldCondition == "DS_SPECIFIC" || fieldCondition == "REGIONAL_MANDATE") ||
                (rFields.contains(fieldCondition) || oFields.contains(fieldCondition) || cFields.contains(fieldCondition))
        ){
            for(String testField : listFields){
                if(cFields.contains(testField)){
                    data.add(new Object[] { fieldCondition, fieldValueCondition, testField, listFields });
                }
            }
        }
        return data;
    }

    /*********************************************************************************
     * AREQ
     *********************************************************************************/
    @DataProvider(name = "DP_AREQ_CONDITIONALLY_REQUIRED_FIELDS")
    private Iterator<Object[]> AReq_ConditionallyRequiredFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredAreqFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalAreqFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalAreqFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> browserJavascriptEnabled = true
        fieldCondition = "browserJavascriptEnabled";
        fieldValueCondition = true;
        listFields = FieldConditionsTestData.getAReqConditionalFields_browserJavascriptEnabled_True();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                 rFields,  oFields, cFields));
        //---> threeDSRequestorAuthenticationInd = 02
        fieldCondition = "threeDSRequestorAuthenticationInd";
        fieldValueCondition = "02";
        listFields = FieldConditionsTestData.getAReqConditionalFields_threeDSRequestorAuthenticationInd_02();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> threeDSRequestorAuthenticationInd = 03
        fieldCondition = "threeDSRequestorAuthenticationInd";
        fieldValueCondition = "03";
        listFields = FieldConditionsTestData.getAReqConditionalFields_threeDSRequestorAuthenticationInd_03();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> whiteListStatus = Y
        fieldCondition = "whiteListStatus";
        fieldValueCondition = "Y";
        listFields = FieldConditionsTestData.getAReqConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> whiteListStatus = N
        fieldCondition = "whiteListStatus";
        fieldValueCondition = "N";
        listFields = FieldConditionsTestData.getAReqConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> payTokenInd = true       (TODO) NOTE: payTokenInd itself is conditional
        fieldCondition = "payTokenInd";
        fieldValueCondition = true;
        listFields = FieldConditionsTestData.getAReqConditionalFields_payTokenInd_true();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        return data.iterator();
    }

    @DataProvider(name = "DP_AREQ_CONDITIONALLY_OPTIONAL_FIELDS")
    private Iterator<Object[]> AReq_ConditionallyOptionalFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredAreqFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalAreqFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalAreqFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> browserJavascriptEnabled = false
        fieldCondition = "browserJavascriptEnabled";
        fieldValueCondition = false;
        listFields = FieldConditionsTestData.getAReqConditionalFields_browserJavascriptEnabled_True();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> threeDSRequestorAuthenticationInd != 02 or 03
        fieldCondition = "threeDSRequestorAuthenticationInd";
        fieldValueCondition = "01";
        listFields = FieldConditionsTestData.getAReqConditionalFields_threeDSRequestorAuthenticationInd_03();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> whiteListStatus = not present
        fieldCondition = "whiteListStatus";
        fieldValueCondition = null;
        listFields = FieldConditionsTestData.getAReqConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> payTokenInd = false
        fieldCondition = "payTokenInd";
        fieldValueCondition = false;
        listFields = FieldConditionsTestData.getAReqConditionalFields_payTokenInd_true();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> payTokenInd = not present (payTokenInd itself is conditionally optional)
        fieldCondition = "payTokenInd";
        fieldValueCondition = null;
        listFields = FieldConditionsTestData.getAReqConditionalFields_payTokenInd_true();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        /*********************************************************************************
         * The following fields do not have clear conditions in EMVco specs.
         * For now, they will be treated as conditionally optional fields.
         *********************************************************************************/
        /* TODO: disabling these for now. There are DS-specific rules from some payment systems which will cause these to fail if run for regression tests.
        fieldValueCondition = null;

        //---> DS Specific Rules (treat as conditionally optional for now)
        fieldCondition = "DS_SPECIFIC";
        listFields = FieldConditionsTestData.getAReqConditionalFields_DsSpecificRules();
        data.addAll(getConditionalTestData(fieldCondition, fieldValueCondition, listFields,
                rFields, oFields, cFields));
        //---> Required Regional Mandate Rules (treat as conditionally optional for now)
        fieldCondition = "REGIONAL_MANDATE";
        listFields = FieldConditionsTestData.getAReqConditionalFields_RequiredRegionalMandate();
        data.addAll(getConditionalTestData(fieldCondition, fieldValueCondition, listFields,
                rFields, oFields, cFields));
         */

        return data.iterator();
    }

    /*********************************************************************************
     * ARES
     *********************************************************************************/
    @DataProvider(name = "DP_ARES_CONDITIONALLY_REQUIRED_FIELDS")
    private Iterator<Object[]> ARes_ConditionallyRequiredFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredAresFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalAresFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalAresFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> transStatus = C
        fieldCondition = "transStatus";
        fieldValueCondition = "C";
        listFields = FieldConditionsTestData.getAResConditionalFields_transStatus_C();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = Y
        fieldCondition = "transStatus";
        fieldValueCondition = "Y";
        listFields = FieldConditionsTestData.getAResConditionalFields_transStatus_YA();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = A
        fieldCondition = "transStatus";
        fieldValueCondition = "A";
        listFields = FieldConditionsTestData.getAResConditionalFields_transStatus_YA();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = N
        fieldCondition = "transStatus";
        fieldValueCondition = "N";
        listFields = FieldConditionsTestData.getAResConditionalFields_transStatus_N();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = U
        fieldCondition = "transStatus";
        fieldValueCondition = "U";
        listFields = FieldConditionsTestData.getAResConditionalFields_transStatus_UR();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = R
        fieldCondition = "transStatus";
        fieldValueCondition = "R";
        listFields = FieldConditionsTestData.getAResConditionalFields_transStatus_UR();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        //---> whiteListStatus = Y
        fieldCondition = "whiteListStatus";
        fieldValueCondition = "Y";
        listFields = FieldConditionsTestData.getAResConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> whiteListStatus = N
        fieldCondition = "whiteListStatus";
        fieldValueCondition = "N";
        listFields = FieldConditionsTestData.getAResConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        return data.iterator();
    }

    @DataProvider(name = "DP_ARES_CONDITIONALLY_OPTIONAL_FIELDS")
    private Iterator<Object[]> ARes_ConditionallyOptionalFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredAresFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalAresFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalAresFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> whiteListStatus = not present
        fieldCondition = "whiteListStatus";
        fieldValueCondition = null;
        listFields = FieldConditionsTestData.getAReqConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        /*********************************************************************************
         * The following fields do not have clear conditions in EMVco specs.
         * For now, they will be treated as conditionally optional fields.
         *********************************************************************************/
        /* TODO: disabling these for now. There are DS-specific rules from some payment systems which will cause these to fail if run for regression tests.
        fieldValueCondition = null;

        //---> DS Specific Rules (treat as conditionally optional for now)
        fieldCondition = "DS_SPECIFIC";
        listFields = FieldConditionsTestData.getAResConditionalFields_DsSpecificRules();
        data.addAll(getConditionalTestData(fieldCondition, fieldValueCondition, listFields,
                rFields, oFields, cFields));

         */

        return data.iterator();
    }

    @DataProvider(name = "DP_ARES_DECOUPLED_CONDITIONALLY_REQUIRED_FIELDS")
    private Iterator<Object[]> ARes_Decoupled_ConditionallyRequiredFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredAresFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalAresFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalAresFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> transStatus = C
        fieldCondition = "transStatus";
        fieldValueCondition = "D";
        listFields = FieldConditionsTestData.getAResConditionalFields_transStatus_D();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> acsDecConInd = Y
        fieldCondition = "acsDecConInd";
        fieldValueCondition = "Y";
        listFields = FieldConditionsTestData.getAResConditionalFields_acsDecConInd_Y();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        return data.iterator();
    }

    @DataProvider(name = "DP_ARES_DECOUPLED_CONDITIONALLY_OPTIONAL_FIELDS")
    private Iterator<Object[]> ARes_Decoupled_ConditionallyOptionalFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredAresFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalAresFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalAresFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> acsDecConInd = N
        fieldCondition = "acsDecConInd";
        fieldValueCondition = "N";
        listFields = FieldConditionsTestData.getAResConditionalFields_acsDecConInd_Y();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        return data.iterator();
    }

    /*********************************************************************************
     * RREQ
     *********************************************************************************/
    @DataProvider(name = "DP_RREQ_CONDITIONALLY_REQUIRED_FIELDS")
    private Iterator<Object[]> RReq_ConditionallyRequiredFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredRreqFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalRreqFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalRreqFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> transStatus = Y
        fieldCondition = "transStatus";
        fieldValueCondition = "Y";
        listFields = FieldConditionsTestData.getRReqConditionalFields_transStatus_Y();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = N
        fieldCondition = "transStatus";
        fieldValueCondition = "N";
        listFields = FieldConditionsTestData.getRReqConditionalFields_transStatus_N();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = U
        fieldCondition = "transStatus";
        fieldValueCondition = "U";
        listFields = FieldConditionsTestData.getRReqConditionalFields_transStatus_UAR();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = A
        fieldCondition = "transStatus";
        fieldValueCondition = "A";
        listFields = FieldConditionsTestData.getRReqConditionalFields_transStatus_UAR();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> transStatus = R
        fieldCondition = "transStatus";
        fieldValueCondition = "R";
        listFields = FieldConditionsTestData.getRReqConditionalFields_transStatus_UAR();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> whiteListStatus = Y
        fieldCondition = "whiteListStatus";
        fieldValueCondition = "Y";
        listFields = FieldConditionsTestData.getRReqConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));
        //---> whiteListStatus = N
        fieldCondition = "whiteListStatus";
        fieldValueCondition = "N";
        listFields = FieldConditionsTestData.getRReqConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        return data.iterator();
    }

    @DataProvider(name = "DP_RREQ_CONDITIONALLY_OPTIONAL_FIELDS")
    private Iterator<Object[]> RReq_ConditionallyOptionalFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredRreqFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalRreqFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalRreqFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        //---> transStatus :: TODO :: how to test conditionally optional fields for different transStatus values ?

        //---> whiteListStatus = not present
        fieldCondition = "whiteListStatus";
        fieldValueCondition = null;
        listFields = FieldConditionsTestData.getRReqConditionalFields_whiteListStatus_PRESENT();
        data.addAll(getConditionalTestData(fieldCondition,  fieldValueCondition,  listFields,
                rFields,  oFields, cFields));

        /*********************************************************************************
         * The following fields do not have clear conditions in EMVco specs.
         * For now, they will be treated as conditionally optional fields.
         *********************************************************************************/
        /* TODO: disabling these for now. There are DS-specific rules from some payment systems which will cause these to fail if run for regression tests.
        fieldValueCondition = null;

        //---> DS Specific Rules (treat as conditionally optional for now)
        fieldCondition = "DS_SPECIFIC";
        listFields = FieldConditionsTestData.getRReqConditionalFields_DsSpecificRules();
        data.addAll(getConditionalTestData(fieldCondition, fieldValueCondition, listFields,
                rFields, oFields, cFields));

         */

        return data.iterator();
    }

    /*********************************************************************************
     * RRES
     *********************************************************************************/
    @DataProvider(name = "DP_RRES_CONDITIONALLY_OPTIONAL_FIELDS")
    private Iterator<Object[]> RRes_ConditionallyOptionalFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredRresFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalRresFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalRresFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        /*********************************************************************************
         * The following fields do not have clear conditions in EMVco specs.
         * For now, they will be treated as conditionally optional fields.
         *********************************************************************************/
        /* TODO: disabling these for now. There are DS-specific rules from some payment systems which will cause these to fail if run for regression tests.
        fieldValueCondition = null;

        //---> DS Specific Rules (treat as conditionally optional for now)
        fieldCondition = "DS_SPECIFIC";
        listFields = FieldConditionsTestData.getRResConditionalFields_DsSpecificRules();
        data.addAll(getConditionalTestData(fieldCondition, fieldValueCondition, listFields,
                rFields, oFields, cFields));
         */

        return data.iterator();
    }

    /*********************************************************************************
     * PREQ
     *********************************************************************************/
    @DataProvider(name = "DP_PREQ_CONDITIONALLY_OPTIONAL_FIELDS")
    private Iterator<Object[]> PReq_ConditionallyOptionalFields(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> rFields = (List<String>)context.getAttribute("requiredPreqFields");
        List<String> oFields = (List<String>)context.getAttribute("optionalPreqFields");
        List<String> cFields = (List<String>)context.getAttribute("conditionalPreqFields");

        String fieldCondition = "";
        Object fieldValueCondition;
        String[] listFields;

        List<Object[]> data = new ArrayList<>();

        /*********************************************************************************
         * The following fields do not have clear conditions in EMVco specs.
         * For now, they will be treated as conditionally optional fields.
         *********************************************************************************/
        fieldValueCondition = null;

        //---> DS Specific Rules (treat as conditionally optional for now)
        fieldCondition = "DS_SPECIFIC";
        listFields = FieldConditionsTestData.getPReqConditionalFields_DsSpecificRules();
        data.addAll(getConditionalTestData(fieldCondition, fieldValueCondition, listFields,
                rFields, oFields, cFields));

        return data.iterator();
    }
}
