package com.modirum.pages.dsemulator.areqrreq;

import com.modirum.models.DeviceChannel;
import com.modirum.models.TransactionFlow;
import com.modirum.models.MessageCategory;
import com.modirum.utilities.SelDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AreqRreqEmulatorPage implements AreqRreqEmulator{
    private static final Logger LOGGER = LogManager.getLogger(AreqRreqEmulatorPage.class.getName());
    protected SelDriver selDriver;
    private static final String pageUrl = "/ds-3ds-emulator/areqRreq.html";

    @FindBy(id = "dsEntrypointUrl")             private WebElement txtDsEntrypointUrl;
    @FindBy(id = "areq")                        private WebElement txtAreq;
    @FindBy(id = "acsAres")                     private WebElement txtAcsAres;
    @FindBy(id = "ares")                        private WebElement txtDsAres;
    @FindBy(id = "genAreq210PaBrwBtn")          private WebElement btnGenerateAreqPaBrw210;
    @FindBy(id = "genAreq220PaBrwBtn")          private WebElement btnGenerateAreqPaBrw220;
    @FindBy(id = "genAcsAresChallengeBtn")      private WebElement btnGenerateAcsAresChallenge;
    @FindBy(id = "genAcsAresFrictionlessBtn")   private WebElement btnGenerateAcsAresFrictionless;
    @FindBy(id = "cacheAresBtn")                private WebElement btnCacheAcsAres;
    @FindBy(id = "sendAreqBtn")                 private WebElement btnSendAreq;

    @FindBy(id = "rresAcsTransId")              private WebElement txtAcsTransIdForRres;
    @FindBy(id = "rresJson")                    private WebElement txtRresJson;
    @FindBy(id = "genRresBtn")                  private WebElement btnGenerateRres;
    @FindBy(id = "cacheRresBtn")                private WebElement btnCacheRres;

    @FindBy(id = "rreqJson")                    private WebElement txtRreqJson;
    @FindBy(id = "rres")                        private WebElement txtRres;
    @FindBy(id = "genPareqBtn")                 private WebElement btnGenerateRreqJson;
    @FindBy(id = "sendCreqBtn")                 private WebElement btnSendRreq;

    public AreqRreqEmulatorPage(SelDriver selDriver, String baseURL){
        this.selDriver = selDriver;
        selDriver.openUrl(baseURL + pageUrl);
        PageFactory.initElements(selDriver.getDriver(), this);

    }

    public AreqRreqEmulatorPage(SelDriver selDriver){
        this.selDriver = selDriver;
        PageFactory.initElements(selDriver.getDriver(), this);
    }

    /***************************************************
     * Getters
     ***************************************************/
    public String getDsEntrypointUrl(){ return selDriver.getValue(txtDsEntrypointUrl); }
    public String get3dssAreq(){ return selDriver.getValue(txtAreq); }
    public String getAcsAres(){ return selDriver.getValue(txtAcsAres); }
    public String getDsAres(){ return selDriver.getValue(txtDsAres); }
    public String getAcsTransIdForRres(){ return selDriver.getValue(txtAcsTransIdForRres); }
    public String get3dssRresJson(){ return selDriver.getValue(txtRresJson); }
    public String getAcsRreqJson(){ return selDriver.getValue(txtRreqJson); }
    public String getDsRres(){ return selDriver.getValue(txtRres); }

    /***************************************************
     * Setters / Clickers
     ***************************************************/
    public AreqRreqEmulatorPage setDsEntrypointUrl(String dsEntrypointUrl){ selDriver.setInputField(txtDsEntrypointUrl, dsEntrypointUrl); return this; }
    public AreqRreqEmulatorPage set3dssAreq(String areq){ selDriver.setInputField(txtAreq, areq); return this; }
    public void send3dssAreq(){ selDriver.click(btnSendAreq); }
    public AreqRreqEmulatorPage setAcsAres(String acsAres){ selDriver.setInputField(txtAcsAres, acsAres); return this; }
    public AreqRreqEmulatorPage setDsAres(String dsAres){ selDriver.setInputField(txtDsAres, dsAres); return this; }
    public void generateAreqPaBrw210(){ selDriver.click(btnGenerateAreqPaBrw210); }
    public void generateAreqPaBrw220(){ selDriver.click(btnGenerateAreqPaBrw220); }
    public void generateAcsAresFrictionless(){ selDriver.click(btnGenerateAcsAresFrictionless); }
    public void generateAcsAresChallenge(){ selDriver.click(btnGenerateAcsAresChallenge); }
    public void cacheAcsAres(){ selDriver.click(btnCacheAcsAres); }
    public AreqRreqEmulatorPage setAcsTransIdForRres(String acsTransId){ selDriver.setInputField(txtAcsTransIdForRres, acsTransId); return this; }
    public AreqRreqEmulatorPage setRresJson(String rresJson){ selDriver.setInputField(txtRresJson, rresJson); return this; }
    public void generate3dssRres(){ selDriver.click(btnGenerateRres); }
    public void cache3dssRres(){ selDriver.click(btnCacheRres); }
    public void generateAcsRreq(){ selDriver.click(btnGenerateRreqJson); }
    public AreqRreqEmulatorPage setAcsRreqJson(String rreqJson){ selDriver.setInputField(txtRreqJson, rreqJson); return this; }
    public void sendAcsRreq(){ selDriver.click(btnSendRreq); }


    /***************************************************
     * Common Methods
     ***************************************************/
    public boolean waitForDsResponseJson(WebElement el){
        try {
            int timeout = 15;
            WebDriverWait wait = new WebDriverWait(selDriver.getDriver(), timeout);
            wait.until(ExpectedConditions.textToBePresentInElementValue(el, "{"));
        }catch(TimeoutException e){
            LOGGER.warn("Expected condition failed: waiting for condition to not be valid: text ('{') to be present in element");
            LOGGER.warn("ACTUAL VALUE: " + selDriver.getValue(el));
            return false;
        }

        return true;
    }

    public String waitForDsAres(){
        waitForDsResponseJson(txtDsAres);
        return getDsAres();
    }

    public String waitForDsRres(){
        waitForDsResponseJson(txtRres);
        return getDsRres();
    }

    public void generate3dssAreq(String version, MessageCategory category, DeviceChannel channel){
        // TODO : Log warnings/errors for unsupported paths
        switch(version){
            case "2.1.0":
                switch(category){
                    case PA:
                        switch(channel){
                            case BRW:
                                generateAreqPaBrw210();
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            case "2.2.0":
                switch(category){
                    case PA:
                        switch(channel){
                            case BRW:
                                generateAreqPaBrw220();
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public void generateAcsAres(TransactionFlow transactionFlow){
        switch(transactionFlow){
            case FRICTIONLESS:
                generateAcsAresFrictionless();
                return;
            case CHALLENGE:
                generateAcsAresChallenge();
                return;
            default:
                return;
        }
    }
}
