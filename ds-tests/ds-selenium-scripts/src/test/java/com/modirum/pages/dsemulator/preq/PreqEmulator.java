package com.modirum.pages.dsemulator.preq;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public interface PreqEmulator {
    /***************************************************
     * Getters
     ***************************************************/
    public String getDsEntrypointUrl();
    public String getPreq();
    public String getDsPres();

    /***************************************************
     * Setters / Clickers
     ***************************************************/
    public PreqEmulator setDsEntrypointUrl(String dsEntrypointUrl);
    public PreqEmulator setPreq(String areq);
    public void generatePreqPaBrw210();
    public void sendPreq();

    /***************************************************
     * Common Methods
     ***************************************************/
    public String waitForDsPres();
}
