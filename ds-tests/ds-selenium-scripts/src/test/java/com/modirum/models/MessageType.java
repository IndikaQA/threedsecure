package com.modirum.models;

public enum MessageType {
    AREQ("AReq"),
    ARES("ARes"),
    CREQ("CReq"),
    CRES("CRes"),
    RREQ("RReq"),
    RRES("RRes"),
    PREQ("PReq"),
    PRES("PRes"),
    ERRO("Erro");

    private final String value;

    MessageType(final String value){
        this.value = value;
    }

    @Override
    public final String toString() {
        return value;
    }
}
