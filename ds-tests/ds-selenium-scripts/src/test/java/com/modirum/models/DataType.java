package com.modirum.models;

public enum DataType {
    BOOLEAN,
    STRING,
    ARRAY,
    OBJECT,
    UUID
}
