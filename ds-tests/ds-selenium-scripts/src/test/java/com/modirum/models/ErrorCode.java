package com.modirum.models;

public enum ErrorCode {
    INVALID_MESSAGE("101"),
    INVALID_VERSION("102"),
    EXCEEDED_MESSAGE_LIMIT("103"),
    MISSING_REQUIRED_ELEMENT("201"),
    INVALID_CRITICAL_MESSAGE_EXTENSION("202"),
    INVALID_FORMAT("203"),
    DUPLICATE_DATA_ELEMENT("204"),
    TRANSID_NOT_RECOGNISED("301"),
    DATA_DECRYPTION_FAILURE("302"),
    ACCESS_DENIED_INVALID_ENDPOINT("303"),
    INVALID_ISO_CODE("304"),
    INVALID_TRANS_DATA("305"),
    INVALID_MCC("306"),
    INVALID_SERIAL_NUMBER("307"),
    TRANSACTION_TIMEOUT("402"),
    TRANSIENT_SYSTEM_FAILURE("403"),
    PERMANENT_SYSTEM_FAILURE("404"),
    SYSTEM_CONNECTION_FAILURE("405");

    private final String value;

    ErrorCode(final String value){
        this.value = value;
    }

    @Override
    public final String toString() {
        return value;
    }
}
